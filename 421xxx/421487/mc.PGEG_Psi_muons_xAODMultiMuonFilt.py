# based on 900456
evgenConfig.description = "ParticleGun psi(2S)->mu3p5mu3p5 uniform pt"
evgenConfig.keywords = ["charmonium","2muon"]
evgenConfig.contact = [ 'Tamar.Zakareishvili@cern.ch' ]
evgenConfig.nEventsPerJob = 10000

include("ParticleGun/ParticleGun_Common.py")

import ROOT,math

import ParticleGun as PG

class PtRapidityMPhiSampler(PG.PtEtaMPhiSampler):

    def __init__(self, pt, rap, mass=0.0, phi=[0, 2*math.pi]):
        self.pt = pt
        self.rap = rap
        self.mass = mass
        self.phi = phi

    @property
    def rap(self):
        "Rapidity sampler"
        return self._rap
    @rap.setter
    def rap(self, rap):
        self._rap = PG.mksampler(rap)

    def shoot(self):
        """
        rap = arctanh(pz/E) =>
        pz = E * tanh(rap),
        E^2 = pT^2 + pz^2 + m^2 =>
        E = sqrt( (pT^2 + m^2) / (1 - tanh(rap)^2) )
        """
        rap = self.rap()
        pt = self.pt()
        phi = self.phi()
        m = self.mass()
        e = math.sqrt( (pt**2 + m**2) / (1 - (math.tanh(rap))**2) )
        px = pt * math.cos(phi)
        py = pt * math.sin(phi)
        pz = e * math.tanh(rap)
        v4 = ROOT.TLorentzVector(px, py, pz, e)
        return v4
      
class PosCylindricalSampler(PG.PosSampler):
  
    def __init__(self, r, z, phi=[0, 2*math.pi], t=0):
        self.r = r
        self.phi = phi
        self.z = z
        self.t = t
        
    @property
    def r(self):
        "r sampler"
        return self._r
    @r.setter
    def r(self, r):
        self._r = PG.mksampler(r)
        
    @property
    def phi(self):
        "phi sampler"
        return self._phi
    @phi.setter
    def phi(self, phi):
        self._phi = PG.mksampler(phi)
        
    def shoot(self):
        r = self.r()
        phi = self.phi()
        x = r * math.cos(phi)
        y = r * math.sin(phi)
        z = self.z()
        t = self.t()
        #print "POS =", x, y, z, t
        return ROOT.TLorentzVector(x, y, z, t)


genSeq.ParticleGun.sampler.mom = PtRapidityMPhiSampler(pt=[7.5e3,150e3], rap=[-2.7,2.7], mass=3686.09)
genSeq.ParticleGun.sampler.pid = 100443
genSeq.ParticleGun.sampler.pos = PosCylindricalSampler(r=[0.,0],z=0.)

include("EvtGen_i/EvtGen_Fragment.py")
genSeq.EvtInclusiveDecay.decayFile = "Psi2SMuMu.dec"
evgenConfig.auxfiles += ['inclusiveP8.pdt','Psi2SMuMu.dec']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8.pdt"
genSeq.EvtInclusiveDecay.printHepMCBeforeEvtGen = True

### Set lepton filters
include('GeneratorFilters/xAODMultiMuonFilter_Common.py')
filtSeq.xAODMultiMuonFilter.Ptcut = 3500.
filtSeq.xAODMultiMuonFilter.Etacut = 2.7
filtSeq.xAODMultiMuonFilter.NMuons = 2 





