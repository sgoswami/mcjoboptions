evgenConfig.description = "Sherpa Z/gamma* -> tau tau + 0,1,2j@NLO + 3,4,5j@LO with di-leptonic tau decays and b-jet filter taking input from existing unfiltered input file."
evgenConfig.keywords = ["SM", "Z", "2tau", "2lepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch", "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 2

if runArgs.trfSubstepName == 'generate' :
   print("ERROR: These JO require an input file.  Please use the --afterburn option")
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   include("GeneratorFilters/xAODLeptonFilter_Common.py")
   filtSeq.xAODLeptonFilter.Ptcut = 13.*GeV  

   if not hasattr(genSeq, 'xAODCnv'):
      from xAODTruthCnv.xAODTruthCnvConf import xAODMaker__xAODTruthCnvAlg
      prefiltSeq += xAODMaker__xAODTruthCnvAlg('xAODCnv',WriteTruthMetaData=False)
      prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'

   include("GeneratorFilters/FindJets.py")
   CreateJets(prefiltSeq, 0.4)
   CreateJets(prefiltSeq, 0.6)
   AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)

   from AthenaCommon.SystemOfUnits import GeV
   filtSeq.QCDTruthJetFilter.MinPt = 53.*GeV

   include ("GeneratorFilters/xAODTauFilter_Common.py")

   filtSeq.xAODTauFilter.Ntaus = 2 
   filtSeq.xAODTauFilter.EtaMaxe = 2.7 
   filtSeq.xAODTauFilter.EtaMaxmu = 2.7 
   filtSeq.xAODTauFilter.EtaMaxhad = 2.7 # no hadronic tau decays
   filtSeq.xAODTauFilter.Ptcute = 31.*GeV
   filtSeq.xAODTauFilter.Ptcutmu = 33.*GeV

   filtSeq.Expression = "(xAODLeptonFilter) and ( not QCDTruthJetFilter) and (xAODTauFilter) "

   postSeq.CountHepMC.CorrectRunNumber = True

