include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ReducedShowerWeights.py")

evgenConfig.description = "Pythia8 dijet events with prompt (fragmentation) photons in 17 < pT_ylead < 35."
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.nEventsPerJob = 20
evgenConfig.contact = ["frank.siegert@cern.ch"]

## Configure Pythia
genSeq.Pythia8.Commands += ["HardQCD:gg2gg = on",
                            "HardQCD:gg2qqbar = on",
                            "HardQCD:qg2qg = on",
                            "HardQCD:qq2qq = on",
                            "HardQCD:qqbar2gg = on",
                            "HardQCD:qqbar2qqbarNew = on",
                            "PhaseSpace:pTHatMin = 15.0"]

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 17000. ]
filtSeq.DirectPhotonFilter.Ptmax = [ 35000. ]
filtSeq.DirectPhotonFilter.OrderPhotons = True
