###############################################################
#
# Job options file for Hijing (with diffraction) generation of
# p + p collisions at 5020 GeV CMS with n_charged cut
#
# Andrzej Olszewski
#
# Created: December 2020
#==============================================================

# use common fragment
include("Hijing_i/Hijing_Common.py")

evgenConfig.description = "Hijing p+p collisions at 5020 GeV, nch>40"
evgenConfig.keywords = ["ND"]
evgenConfig.contact = ["Andrzej.Olszewski@ifj.edu.pl"]

evgenConfig.nEventsPerJob = 10000

#----------------------
# Hijing Parameters
#----------------------
Hijing = genSeq.Hijing
Hijing.Initialize = ["efrm 5020.", "frame CMS", "proj P", "targ P",
                     "iap 1", "izp 1", "iat 1", "izt 1",
# simulation of minimum-bias events
                     "bmin 0", "bmax 10",
# turns OFF jet quenching:
                     "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ...
                     "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                     "ihpr2 21 1",
# turning OFF string radiation
                     "ihpr2 1 0",
# Nov 10,11, set minimum pt for hard scatterings to default, 2 GeV
                     "hipr1 8 2",
# Nov 10,11, turn off diffractive scatterings
                     "ihpr2 13 0"
                     ]

from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter 
if "ChargedTracksFilter" not in filtSeq:
   filtSeq += ChargedTracksFilter()
filtSeq.ChargedTracksFilter.Ptcut = 400
filtSeq.ChargedTracksFilter.NTracks = 40
