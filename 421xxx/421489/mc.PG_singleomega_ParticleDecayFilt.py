evgenConfig.description = "Single Omega with flat log energy distributions"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact = ["m.hodgkinson@sheffield.ac.uk"] 
evgenConfig.nEventsPerJob = 10000

#omega(782)
parentParticleType = 223

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ['ParticleGun']
genSeq.ParticleGun.sampler.pid = parentParticleType
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(5000, 2000000.), eta=[-3.0, 3.0], mass=782)

include("EvtGen_i/EvtGen_Fragment.py")
evgenConfig.auxfiles+=['inclusive.pdt']
genSeq.EvtInclusiveDecay.allowAllKnownDecays=True

from GeneratorFilters.GeneratorFiltersConf import ParticleDecayFilter
ParticleDecayFilter.ParentPdgId=parentParticleType
ParticleDecayFilter.ChildrenPdgIds=[211, -211, 111]
filtSeq += ParticleDecayFilter()

