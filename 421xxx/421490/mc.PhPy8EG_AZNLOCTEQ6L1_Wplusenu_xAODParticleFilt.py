#--------------------------------------------------------------
# Powheg W setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_W_Common.py')

#--------------------------------------------------------------
# Set the decay mode, checking which syntax to use
#--------------------------------------------------------------
if hasattr(PowhegConfig, "idvecbos"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.idvecbos   = 24 # W+
    PowhegConfig.vdecaymode = 1  # enu
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "w+ > e+ ve"

# Configure Powheg setup
# PowhegConfig.withdamp   = 1
PowhegConfig.ptsqmin    = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 1.5 # increase number of generated events by 50%
PowhegConfig.running_width = 1

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Wplus->enu production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'W', 'drellYan', 'electron', 'neutrino' ]
evgenConfig.nEventsPerJob = 1000

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include("GeneratorFilters/xAODParticleFilter_Common.py")
xAODParticleFilter = filtSeq.xAODParticleFilter
xAODParticleFilter.Ptcut = 13000.
#xAODParticleFilter.Etacut = 10.0
xAODParticleFilter.PDG = 11
xAODParticleFilter.MinParts = 1
#xAODParticleFilter.StatusReq = 11



