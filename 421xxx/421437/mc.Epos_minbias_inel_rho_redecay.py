# based on 900311
# example JOs for particles redecay with EvtGen
evgenConfig.description = "Low-pT EPOS inelastic minimum bias events for pileup, using low-pT jet and photon filters."
evgenConfig.keywords = ["QCD", "minBias" , "SM"]
evgenConfig.contact  = [ "jeff.dandoy@cern.ch", "jan.kretzschmar@cern.ch", "mdshapiro@lbl.gov" ]
evgenConfig.nEventsPerJob = 50000
evgenConfig.tune = "EPOS LHC"

evgenConfig.saveJets = True
evgenConfig.savePileupTruthParticles = True

include("Epos_i/Epos_Base_Fragment.py")

include ("GeneratorFilters/AddPileupTruthParticles.py")

include("EvtGen_i/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"

# The whiteList is the list of particles EvtGen will decay or  redecay
# Note we are using = and not += since we want to override the default whiteList
# In this example we are just redecaying the rho0, rho+ and rho-
genSeq.EvtInclusiveDecay.whiteList=[113,213,-213]

# to redecay everything: 
# genSeq.EvtInclusiveDecay.allowAllKnownDecays=True

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
CreateJets(prefiltSeq, 0.6)

AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
filtSeq.QCDTruthJetFilter.MaxPt = 35.*GeV

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 8000. ]
filtSeq.DirectPhotonFilter.Etacut = 4.5

filtSeq.Expression = 'not DirectPhotonFilter and QCDTruthJetFilter'


