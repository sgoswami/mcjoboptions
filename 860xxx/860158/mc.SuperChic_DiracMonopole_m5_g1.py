from Superchic_i.SuperChicUtils import SuperChicConfig, SuperChicRun
import Superchic_i.EventFiller as EF

# Parameters
mass = 5
charge = 1

evgenConfig.description = 'SuperChic Dirac yy->monopole pair production in UPC collisions at 5020 GeV'
evgenConfig.keywords = ['2photon', 'magneticmonopole']
evgenConfig.contact = ['krzysztof.marcin.ciesla@cern.ch', 'mateusz.dyndal@cern.ch']
evgenConfig.generators = ['Superchic']
evgenConfig.specialConfig = 'MASS=%s;GCHARGE=%s;preInclude=SimulationJobOptions/preInclude.Monopole.py' % (mass, charge)

scConfig = SuperChicConfig(runArgs)

scConfig.proc = 71
scConfig.beam = 'ion'
scConfig.diff = 'el'
scConfig.mpol = mass
scConfig.ymin = -6.0
scConfig.ymax = 6.0
scConfig.mmin = 1
scConfig.mmax = 1000
scConfig.gencuts = False

SuperChicRun(scConfig, genSeq)

# Replace monopole pdgId
include('CnvLHE_SC_Monopole.py')

# Transform modified LHE to EVNT
include('Superchic_i/LheEventFiller_Common.py')
