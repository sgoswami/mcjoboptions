# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

import os
import re
import xml.dom.minidom

from GeneratorModules.EvgenAlg import EvgenAlg
from AthenaPython.PyAthena import StatusCode


class Converter(EvgenAlg):
    '''
    Class for converting output LHE file from SuperChic. Used for monopole production processes.
    Changes monopole pgdId from +-91, used by SuperChic, to +-4110000, recognised by Geant.
    '''

    def __init__(self, name='SC_MonopoleCnv'):
        super(Converter, self).__init__(name=name)

    lheFile = 'evrecs/evrecout.dat'
    done = False

    def initialize(self):
        if(os.path.isfile(self.lheFile)):
            print(self.lheFile)

            return self.convert()
        else:
            return StatusCode.Failure

    def convert(self):
        '''
        Converts `evrecs/evrecout.dat`:
            - Changes pdgid of monopoles to +-4110000
        '''

        if not self.done:
            DOMTree = xml.dom.minidom.parse(self.lheFile)
            collection = DOMTree.documentElement

            #                       # of particles in event
            event_header = r'^(\s*)(\S*)(.*)$'
            event_particle = \
                r'^(\s*)([0-9-]+)(\s+[0-9-]+\s+[0-9-]+\s+[0-9-]+\s+[0-9-]+\s+[0-9-]+\s+)(\S+)(\s+)(\S+)(\s+)(\S+)(\s+)(\S+)(.*)$'
            #             pdgId      status    mothers             daughters             px        py        pz        e

            events = collection.getElementsByTagName('event')
            for i, event in enumerate(events):
                new_particles = []

                particles = re.findall(event_particle, event.firstChild.data, re.MULTILINE)
                for particle in particles:
                    particle = list(particle)
                    if '91' in particle[1]:  # replace monopole pdgId...
                        particle[1] = particle[1].replace('91', '4110000')
                        new_particles.append(''.join(particle))
                    else:  # ... or save everything else unchanged
                        new_particles.append(''.join(particle))

                header = re.search(event_header, event.firstChild.data, re.MULTILINE)
                header = list(header.groups())
                header[1] = str(len(new_particles))
                header = ''.join(header)

                event.firstChild.data = '\n'.join([header] + new_particles) + '\n '

            with open(self.lheFile, 'w') as output:
                output.write(DOMTree.toxml().replace('<?xml version="1.0" ?>', ' '))

            self.done = True

        return StatusCode.Success


lc = Converter()
genSeq += lc
