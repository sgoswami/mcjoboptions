#--------------------------------------------------------------
# ggF POWHEG+PDF4LHC15 HH production 
#--------------------------------------------------------------

include('PowhegControl/PowhegControl_ggF_HH_Common.py')


PowhegConfig.mtdep = 3 # full theory
PowhegConfig.mass_H = 125 # Higgs boson mass - need to stick to that value as it is hardcoded in the ME
PowhegConfig.mass_t = 173 # top-quark mass - need to stick to that value as it is hardcoded in the ME
PowhegConfig.cHHH = 0
PowhegConfig.hdamp = 250

#PDF4LHC15_nlo_30_pdfas 
PowhegConfig.PDF = list(range(90400,90433)) + [260000] + [266000] + [265000] + [13100] + [25200] 
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]

PowhegConfig.generate()

evgenConfig.description = "POWHEG, DiHiggs"
evgenConfig.keywords    = [ "gluonFusionHiggs"]
evgenConfig.contact     = [ 'Timothee Theveneaux-Pelzer <tpelzer@cern.ch>', 'Yanlin Liu <yanlin.liu@cern.ch>']
evgenConfig.inputconfcheck = "ggFHH_PDF4LHC15"
evgenConfig.generators  = [ 'Powheg' ]
evgenConfig.nEventsPerJob = 400
