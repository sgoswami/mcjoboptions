#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with MadSpin decay, Powheg hdamp equal 1.5*top mass, A14 tune, at least one single lepton filter, Anti-B->Jpsi->mumu filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton', 'Jpsi']
evgenConfig.contact     = [ 'aknue@cern.ch', 'asada@hepl.phys.nagoya-u.ac.jp', 'derue@lpnhe.in2p3.fr' ]
evgenConfig.nEventsPerJob = 10000
#evgenConfig.inputfilecheck="TXT" # D.P.: maybe not needed? 
#evgenConfig.nEventsPerJob = 2000  # H.A. optional? to be adjusted.
#evgenConfig.inputFilesPerJob = 4 # H.A. for madspin simulation, input LHE files aren't needed. 

include('PowhegControl/PowhegControl_tt_Common.py')


# Initial settings
#PowhegConfig.topdecaymode = "00000"

if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax                                                                                                                                                    
    PowhegConfig.topdecaymode = 00000 # inclusive top decays                                                                                                                                              
else:
    # Use PowhegControl-00-03-XY (and later) syntax                                                                                                                                                       
    PowhegConfig.decay_mode = "t t~ > undecayed"


PowhegConfig.hdamp        = 258.75
#PowhegConfig.mu_F         = [1.0]
#PowhegConfig.mu_R         = [1.0]
#PowhegConfig.PDF          = [260000]

PowhegConfig.MadSpin_taus_are_leptons = True
PowhegConfig.MadSpin_decays= ["decay t > w+ b, w+ > all all", "decay t~ > w- b~, w- > all all"]
PowhegConfig.MadSpin_process = "generate p p > t t~ [QCD]"
PowhegConfig.MadSpin_mode = "full"
PowhegConfig.MadSpin_model = "loop_sm-no_b_mass"
PowhegConfig.MadSpin_nFlavours = 5

#"MadSpin_model")[0].value = "loop_sm-no_b_mass"

# Parameters are hardcoded to avoid madspin picking up it's own defaults.
PowhegConfig.alphaem_inv  = 1.323489e+02
PowhegConfig.G_F          = 1.166370e-05
PowhegConfig.alphaqcd     = 1.184000e-01
PowhegConfig.mass_W       = 80.399
PowhegConfig.mass_Z       = 9.118760e+01
PowhegConfig.mass_H       = 125.0

PowhegConfig.BR_t_to_Wb = 1.000000
PowhegConfig.BR_t_to_Ws = 0.00000
PowhegConfig.BR_t_to_Wd = 0.00000

# new line added to match MC@NLO config
PowhegConfig.bwcutoff = 50

#PowhegConfig.nEvents     *= 2.2 # safety factor
PowhegConfig.nEvents     *= 10 # safety factor
#PowhegConfig.nEvents     *= 100 # safety factor
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'POWHEG:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]


#--------------------------------------------------------------
# Special decay of anti-B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += ['AntiB2Jpsimumu.DEC']
genSeq.EvtInclusiveDecay.userDecayFile = 'AntiB2Jpsimumu.DEC'

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# apply a J/psi to muons filter
include('GeneratorFilters/TTbarWithJpsimumuFilter.py')
filtSeq.TTbarWithJpsimumuFilter.JpsipTMinCut = 5000.

