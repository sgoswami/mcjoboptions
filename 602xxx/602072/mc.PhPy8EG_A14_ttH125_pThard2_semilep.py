#--------------------------------------------------------------
# JO to be used with this input TXT container:
# mc15_13TeV.346306.Powheg_NNPDF30ME_ttH125_semilep_LHE.evgen.TXT.e7020
#--------------------------------------------------------------

evgenConfig.process        = "ttH semilep H->all"
evgenConfig.description    = 'POWHEG+Pythia8.230 ttH (semilep) production with A14 NNPDF2.3 tune'
evgenConfig.keywords       = [ 'SM', 'top', 'Higgs' ]
evgenConfig.contact        = [ 'nedaa-alexandra.asbah@cern.ch' ]
evgenConfig.nEventsPerJob  = 10000
evgenConfig.inputFilesPerJob = 5
evgenConfig.generators     = [ 'Powheg', 'Pythia8', 'EvtGen' ]

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Inclusive Higgs decay in Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:oneChannel = 1 0.5770   100 5 -5',
                             '25:addChannel = 1 0.0291   100 4 -4',
                             '25:addChannel = 1 0.000246 100 3 -3',
                             '25:addChannel = 1 0.00000  100 6 -6',
                             '25:addChannel = 1 0.000219 100 13 -13',
                             '25:addChannel = 1 0.0632   100 15 -15',
                             '25:addChannel = 1 0.0857   100 21 21',
                             '25:addChannel = 1 0.00228  100 22 22',
                             '25:addChannel = 1 0.00154  100 22 23',
                             '25:addChannel = 1 0.0264   100 23 23',
                             '25:addChannel = 1 0.2150   100 24 -24'
]

#--------------------------------------------------------------
# TTbarWToLeptonFilter
# l+jets ttbar decay - input lhe files are inclusive
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

