evgenConfig.generators   += ["Powheg","Pythia8","EvtGen"]
evgenConfig.description   = 'Powheg bb4l with A14 tune, NNPDF30 PDF, all flavour dilep combinations, hdamp 1.5 mtop, top width and PY8 splitting kernel weights'
evgenConfig.keywords     += [ 'SM', 'top', 'WWbb', 'lepton']
evgenConfig.contact       = [ 'Andrea Knue <andrea.knue@cern.ch>', 'Katharina Voss <katharina.voss@cern.ch>' ]
evgenConfig.nEventsPerJob = 500

# -------------------- # Load ATLAS defaults for the Powheg bblvlv process # ----------------------------
#included in AthGeneration >= 23.6.20
include("PowhegControl/PowhegControl_bblvlv_modified_Common.py")
PowhegConfig.width_t         = 1.32
PowhegConfig.twidth_phsp     = 1.32
PowhegConfig.tmass_phsp      = 172.5
PowhegConfig.mass_t          = 172.5
PowhegConfig.hdamp           = 258.75
PowhegConfig.ptsqmin         = 1.2 # setting this in accordance with the pTminVeto

# enabling for_reweighting=1 will speed up event generation at expense of some spread in weights
PowhegConfig.for_reweighting = 0

### nominal PDFs as in main hvq sample:
# NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118
PowhegConfig.PDF             = [260000, 25200, 13165, 90900, 265000, 266000, 303400] 
PowhegConfig.PDF.extend(range(260001, 260101))         # Include the NNPDF3.0 error set
PowhegConfig.PDF.extend(range(90901 , 90931 ))         # Include the PDF4LHC error set
# include some newer PDF sets 
# CT18NLO, MSHT20nlo_as118, NNPDF40_nlo_as_01180, PDF4LHC21
PowhegConfig.PDF.extend([14400, 27100, 331100, 93300]) 
# include additional NNPDF3.0 sets with varied alphaS value NNPDF30_nlo_as_0115, NNPDF30_nlo_as_0121
PowhegConfig.PDF.extend([ 264000, 267000])

## enable all different-flavour lepton combinations
## will only work in new release >=21.6.74
PowhegConfig.decay_mode = "b l+ vl b~ l- vl~"

### Optimised fold parameters: (2,2,2) gives 3.9% negative weights
PowhegConfig.foldcsi      = 2
PowhegConfig.foldphi      = 2
PowhegConfig.foldy        = 2

### turn up integration parameters
PowhegConfig.ncall1       = 120000*2
PowhegConfig.ncall2       = 180000*2
PowhegConfig.nubound      = 100000*2
PowhegConfig.itmx1        = 2
PowhegConfig.itmx2        = 8
PowhegConfig.xupbound     = 3.

PowhegConfig.nEvents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*1.1


# width reweighting
PowhegConfig.define_event_weight_group( group_name='topwidth', parameters_to_vary=['width_t'] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt0p5', parameter_values=[ 0.5 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt0p8', parameter_values=[ 0.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p0', parameter_values=[ 1.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p2', parameter_values=[ 1.2 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p4', parameter_values=[ 1.4 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p6', parameter_values=[ 1.6 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p8', parameter_values=[ 1.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p0', parameter_values=[ 2.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p2', parameter_values=[ 2.2 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p4', parameter_values=[ 2.4 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p6', parameter_values=[ 2.6 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p8', parameter_values=[ 2.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt3p0', parameter_values=[ 3.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt3p5', parameter_values=[ 3.5 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt4p0', parameter_values=[ 4.0 ] )


# --------------------- # Generate events # --------------------------------------------------------------
PowhegConfig.generate()


################################################################
#                      adding UserHook                         #
################################################################         
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg.py")

genSeq.Pythia8.Commands += [ 'UncertaintyBands:overSampleFSR=10' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:FSRpTmin2Fac=8' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:overSampleISR=10' ]

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    print ('UserHook present')
    genSeq.Pythia8.UserHooks += ['PowhegBB4Ltms']

genSeq.Pythia8.Commands += ["POWHEG:veto=1"]
genSeq.Pythia8.Commands += ["POWHEG:vetoCount = 3"]
genSeq.Pythia8.Commands += ["POWHEG:pThard = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTemt = 0"]
genSeq.Pythia8.Commands += ["POWHEG:emitted = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTdef = 1"]
genSeq.Pythia8.Commands += ["POWHEG:nFinal = -1"]
genSeq.Pythia8.Commands += ["POWHEG:MPIveto = 1"]
genSeq.Pythia8.Commands += ["POWHEG:QEDveto = 1"]

genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:veto = 1"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:onlyDistance1 = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:vetoQED = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:vetoAtPL = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:dryRunFSR = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:vetoDipoleFrame = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTpythiaVeto = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:ScaleResonance:veto = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:PartonLevel:veto = 0"]
#genSeq.Pythia8.Commands += ["POWHEG:bb4l:PartonLevel:excludeFSRConflicting = false"]                                                                                                                              
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTminVeto = 1.2"] # change from default 0.8 to 1.2 to have same as in HERWIG setting
genSeq.Pythia8.Commands += ["POWHEG:bb4l:DEBUG = 0"]


if "ShowerWeightNames" in genSeq.Pythia8.__slots__.keys():
      print ("Initalizing Shower Weights from Pythia8_ShowerWeights.py")
# New Shower Weights which need to overwrite the existing ones so duplicate for now
# Var3c - A14 tune variation
# Red - reduced sqrt(2)
# Def - default 2
# Con - conservative 4
# Splitting with default 2
# \- g2gg, g2qq, q2qg, x2xg; where x is b,t or heavier if nFlavQ = 4
# cNS - non-singular terms, relevant if no matrix element correction (MEC)
genSeq.Pythia8.Commands += ['UncertaintyBands:doVariations = on',
"UncertaintyBands:List = {\
Var3cUp isr:muRfac=0.549241,\
Var3cDown isr:muRfac=1.960832,\
isr:PDF:plus=1,\
isr:PDF:minus=2,\
isrRedHi isr:muRfac=0.707,\
fsrRedHi fsr:muRfac=0.707,\
isrRedLo isr:muRfac=1.414,\
fsrRedLo fsr:muRfac=1.414,\
isrDefHi isr:muRfac=0.5,\
fsrDefHi fsr:muRfac=0.5,\
isrDefLo isr:muRfac=2.0,\
fsrDefLo fsr:muRfac=2.0,\
isrConHi isr:muRfac=0.25,\
fsrConHi fsr:muRfac=0.25,\
isrConLo isr:muRfac=4.0,\
fsrConLo fsr:muRfac=4.0,\
isr_cNS_dn isr:cNS=-2.0,\
isr_cNS_up isr:cNS=2.0,\
fsr_cNS_dn fsr:cNS=-2.0,\
fsr_cNS_up fsr:cNS=2.0,\
fsr_G2GG_muR_dn fsr:G2GG:muRfac=0.5,\
fsr_G2GG_muR_up fsr:G2GG:muRfac=2.0,\
fsr_G2QQ_muR_dn fsr:G2QQ:muRfac=0.5,\
fsr_G2QQ_muR_up fsr:G2QQ:muRfac=2.0,\
fsr_Q2QG_muR_dn fsr:Q2QG:muRfac=0.5,\
fsr_Q2QG_muR_up fsr:Q2QG:muRfac=2.0,\
fsr_X2XG_muR_dn fsr:X2XG:muRfac=0.5,\
fsr_X2XG_muR_up fsr:X2XG:muRfac=2.0,\
fsr_G2GG_cNS_dn fsr:G2GG:cNS=-2.0,\
fsr_G2GG_cNS_up fsr:G2GG:cNS=2.0,\
fsr_G2QQ_cNS_dn fsr:G2QQ:cNS=-2.0,\
fsr_G2QQ_cNS_up fsr:G2QQ:cNS=2.0,\
fsr_Q2QG_cNS_dn fsr:Q2QG:cNS=-2.0,\
fsr_Q2QG_cNS_up fsr:Q2QG:cNS=2.0,\
fsr_X2XG_cNS_dn fsr:X2XG:cNS=-2.0,\
fsr_X2XG_cNS_up fsr:X2XG:cNS=2.0,\
isr_G2GG_muR_dn isr:G2GG:muRfac=0.5,\
isr_G2GG_muR_up isr:G2GG:muRfac=2.0,\
isr_G2QQ_muR_dn isr:G2QQ:muRfac=0.5,\
isr_G2QQ_muR_up isr:G2QQ:muRfac=2.0,\
isr_Q2QG_muR_dn isr:Q2QG:muRfac=0.5,\
isr_Q2QG_muR_up isr:Q2QG:muRfac=2.0,\
isr_X2XG_muR_dn isr:X2XG:muRfac=0.5,\
isr_X2XG_muR_up isr:X2XG:muRfac=2.0,\
isr_G2GG_cNS_dn isr:G2GG:cNS=-2.0,\
isr_G2GG_cNS_up isr:G2GG:cNS=2.0,\
isr_G2QQ_cNS_dn isr:G2QQ:cNS=-2.0,\
isr_G2QQ_cNS_up isr:G2QQ:cNS=2.0,\
isr_Q2QG_cNS_dn isr:Q2QG:cNS=-2.0,\
isr_Q2QG_cNS_up isr:Q2QG:cNS=2.0,\
isr_X2XG_cNS_dn isr:X2XG:cNS=-2.0,\
isr_X2XG_cNS_up isr:X2XG:cNS=2.0\
}"]

genSeq.Pythia8.ShowerWeightNames = ["Var3cUp",
                                    "Var3cDown",
                                    "isr:PDF:plus",
                                    "isr:PDF:minus",
                                    "isrRedHi", 
                                    "fsrRedHi", 
                                    "isrRedLo", 
                                    "fsrRedLo", 
                                    "isrDefHi", 
                                    "fsrDefHi", 
                                    "isrDefLo", 
                                    "fsrDefLo", 
                                    "isrConHi", 
                                    "fsrConHi", 
                                    "isrConLo", 
                                    "fsrConLo", 
                                    "isr_cNS_dn",                                    
                                    "isr_cNS_up",
                                    "fsr_cNS_dn",
                                    "fsr_cNS_up",
                                    "fsr_G2GG_muR_dn", 
                                    "fsr_G2GG_muR_up", 
                                    "fsr_G2QQ_muR_dn", 
                                    "fsr_G2QQ_muR_up", 
                                    "fsr_Q2QG_muR_dn", 
                                    "fsr_Q2QG_muR_up", 
                                    "fsr_X2XG_muR_dn", 
                                    "fsr_X2XG_muR_up", 
                                    "fsr_G2GG_cNS_dn", 
                                    "fsr_G2GG_cNS_up", 
                                    "fsr_G2QQ_cNS_dn", 
                                    "fsr_G2QQ_cNS_up", 
                                    "fsr_Q2QG_cNS_dn", 
                                    "fsr_Q2QG_cNS_up", 
                                    "fsr_X2XG_cNS_dn", 
                                    "fsr_X2XG_cNS_up", 
                                    "isr_G2GG_muR_dn", 
                                    "isr_G2GG_muR_up", 
                                    "isr_G2QQ_muR_dn", 
                                    "isr_G2QQ_muR_up", 
                                    "isr_Q2QG_muR_dn", 
                                    "isr_Q2QG_muR_up", 
                                    "isr_X2XG_muR_dn", 
                                    "isr_X2XG_muR_up", 
                                    "isr_G2GG_cNS_dn", 
                                    "isr_G2GG_cNS_up", 
                                    "isr_G2QQ_cNS_dn", 
                                    "isr_G2QQ_cNS_up", 
                                    "isr_Q2QG_cNS_dn", 
                                    "isr_Q2QG_cNS_up", 
                                    "isr_X2XG_cNS_dn", 
                                    "isr_X2XG_cNS_up"]

# New settings
genSeq.Pythia8.Commands += ['UncertaintyBands:nFlavQ = 4', # define X=bottom/top in X2XG variations
                            'UncertaintyBands:MPIshowers = on',
                            'UncertaintyBands:overSampleFSR = 10.0',
                            'UncertaintyBands:overSampleISR = 10.0',
                            'UncertaintyBands:FSRpTmin2Fac = 20',
                            'UncertaintyBands:ISRpTmin2Fac = 1']
