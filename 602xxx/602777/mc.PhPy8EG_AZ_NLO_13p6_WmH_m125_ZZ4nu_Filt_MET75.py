#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
 
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
#--------------------------------------------------------------
# H->ZZ->4v decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 12 14 16']

 
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Wm+jet->inv+all production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"  ]
evgenConfig.contact     = [ 'mohamed.zaazoua@cern.ch' ]
evgenConfig.process = "qq->WmH, H->inv, W->all"
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 28


#--------------------------------------------------------------
# Missing Et filter 
#--------------------------------------------------------------
include('GeneratorFilters/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV
filtSeq.Expression = "MissingEtFilter" 

