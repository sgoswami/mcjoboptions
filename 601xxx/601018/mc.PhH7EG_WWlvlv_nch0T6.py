# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 WW_lvlv production with AZNLO CTEQ6L1 tune'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WW', '2lepton', 'neutrino' ]
evgenConfig.contact     = [ 'oldrich.kepka@cern.ch' ]
evgenConfig.generators  = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.nEventsPerJob   = 1000

#--------------------------------------------------------------
# Powheg WW setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361600

include('PowhegControl/PowhegControl_WW_Common.py')
PowhegConfig.decay_mode = 'w+ w- > l+ vl l\'- vl\'~'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.PDF = [10800,13000, 303600, 25100] #CT10nnlo, CT14nnlo, NNPDF3.1nnlo, MMHT2014nlo68cl
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 500
PowhegConfig.generate()


# --------------------------------------------------------------
# Shower settings              
# --------------------------------------------------------------    
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10") # not possible to use CT18
## Please note from: https://herwig.hepforge.org/tutorials/faq/pdf.html#set-pdf-of-the-lhereader
# For POWHEG matching, there is basically no cross talk between the hard subprocess PDFs and the shower PDFs,
# so choosing a LO shower PDF different from whatever (NLO) PDF has been used with Powheg should not be a problem.

Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()


#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py')
## Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2

include('GeneratorFilters/ChargedTrackWeightFilter.py')
filtSeq.ChargedTracksWeightFilter.NchMin = 0
filtSeq.ChargedTracksWeightFilter.NchMax = 6
filtSeq.ChargedTracksWeightFilter.Ptcut = 500
filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 5, 15, 25, 35 ]
filtSeq.ChargedTracksWeightFilter.SplineY =[ 0.006, 0.006, 0.028, 0.078, 0.1, 0.1 ]
