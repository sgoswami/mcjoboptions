#--------------------------------------------------------------
# POWHEG+Pythia8 gg->H->tautau->ll production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15',
                             '15:onMode = off', # decay of taus
                             '15:onIfAny = 11 13' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8 ggF H->tautau"
evgenConfig.process     = "gg->H->tautau->ll"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2tau", "mH125" ]
evgenConfig.contact     = [ 'huanguo.li@cern.ch' ]
evgenConfig.inputFilesPerJob = 13
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Set up tau filters
#--------------------------------------------------------------
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep13lep7filter = TauFilter("lep13lep7filter")
  filtSeq += lep13lep7filter

filtSeq.lep13lep7filter.UseNewOptions = True
filtSeq.lep13lep7filter.Ntaus = 2
filtSeq.lep13lep7filter.Nleptaus = 2
filtSeq.lep13lep7filter.Nhadtaus = 0
filtSeq.lep13lep7filter.EtaMaxlep = 2.6
filtSeq.lep13lep7filter.EtaMaxhad = 2.6
filtSeq.lep13lep7filter.Ptcutlep = 7000.0 #MeV
filtSeq.lep13lep7filter.Ptcutlep_lead = 13000.0 #MeV
filtSeq.lep13lep7filter.Ptcuthad = 20000.0 #MeV
filtSeq.lep13lep7filter.Ptcuthad_lead = 20000.0 #MeV


