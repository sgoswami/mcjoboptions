# EVGEN configuration
# Note:  This JO are designed to run Powheg and make an LHE file and to not run a showering
# generator afterwards

#--------------------------------------------------------------
# Powheg ttH setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ttH_Common.py')
PowhegConfig.decay_mode   = "t t~ > all" 
PowhegConfig.hdamp        = 352.5

PowhegConfig.runningscales = 1 ## dynamic scale
PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0, 0.5, 2.0]
PowhegConfig.generate()


##--------------------------------------------------------------
## Pythia8 showering with the A14 NNPDF 2.3 tune
##--------------------------------------------------------------
#include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
#include('Pythia8_i/Pythia8_Powheg_Main31.py')
#genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
#genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
#genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
#genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
#genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
#genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
#genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
#genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
#
##--------------------------------------------------------------
## Pythia8 corrections to BR for Higgs inclusive decay
##--------------------------------------------------------------
#include("Pythia8_SMHiggs125_inc.py")
#
#include("GeneratorFilters/TTbarWToLeptonFilter.py")
#filtSeq.TTbarWToLeptonFilter.NumLeptons = 1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
#filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0
#
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = 'Powheg ttH semilep production PDF4LHC21'
evgenConfig.keywords       = [ 'SM', 'top', 'Higgs' ]
evgenConfig.contact        = [ 'sabidi@cern.ch' ]
evgenConfig.generators     = [ 'Powheg' ]
evgenConfig.nEventsPerJob = 25000
