#--------------------------------------------------------------
# POWHEG+Pythia8 qq -> WmH -> Winc + Htautau production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'HiggsH1:parity = 2',
                             '25:onMode = off',
                             '25:onIfMatch = 15 15' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process          = "qq->WmH, W->all, H->tautau"
evgenConfig.description      = "POWHEG+PYTHIA8, H+Wm+jet, W->all, H->tautau"
evgenConfig.keywords         = [ "SM", "Higgs", "SMHiggs", "WHiggs", "2tau", "mH125" ]
evgenConfig.contact          = [ 'huanguo.li@cern.ch' ]
evgenConfig.inputFilesPerJob = 11 
evgenConfig.nEventsPerJob    = 10000
