#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process = "qq->WpH, H->ZZ->4l, W->all"
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Wp+jet->4l (no tau) + all production with A14 NNPDF2.3 tune"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"  ]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch' ]
evgenConfig.generators       = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.inputFilesPerJob = 11
evgenConfig.nEventsPerJob    = 10000

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
  genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']
else:
  genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# H->ZZ->4l decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 13']
