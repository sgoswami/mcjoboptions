#-------------------------------------------------------------- 
# EVGEN configuration
#-------------------------------------------------------------- 
evgenConfig.description = "POWHEG+H7 NC DY production"
evgenConfig.keywords = ["SM", "Z"]
evgenConfig.contact = ["gianna.monig@cern.ch, joshua.angus.mcfayden"] 
evgenConfig.generators  = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune     = "H7.1-Default"                                                                                               



# --------------------------------------------------------------
# Shower settings              
# --------------------------------------------------------------    
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10") # not possible to use CT18
## Please note from: https://herwig.hepforge.org/tutorials/faq/pdf.html#set-pdf-of-the-lhereader
# For POWHEG matching, there is basically no cross talk between the hard subprocess PDFs and the shower PDFs,
# so choosing a LO shower PDF different from whatever (NLO) PDF has been used with Powheg should not be a problem.

Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

#String hadronisation      
include("Herwig7_i/Herwig7_TheP8I.py")

# run Herwig7
Herwig7Config.run()
