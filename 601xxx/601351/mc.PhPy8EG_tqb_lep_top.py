#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen single-top-quark t-channel (2->3) production (top),MadSpin, A14 tune, ME NNPDF3.04f NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch']
evgenConfig.nEventsPerJob = 1000

include('PowhegControl/PowhegControl_t_tch_4FS_Common.py')

PowhegConfig.decay_mode   = "t > undecayed"


# List of PDFs adopted from the default settings but changed to 4FS PDFs 
PowhegConfig.PDF = list(range(260400, 260501)) # NNPDF30_nlo_as_0118_nf_4 central with eigensets
PowhegConfig.PDF += [266400, 265400]      # NNPDF30_nlo_as_0119_nf_4 and NNPDF30_nlo_as_0117_nf_4
PowhegConfig.PDF += [261400]              # NNPDF30_nnlo_as_0118_nf_4
PowhegConfig.PDF += [27810, 27610]        # MSHT20nnlo_nf4, MSHT20nlo_nf4
PowhegConfig.PDF += [14000, 14400]        # CT18NNLO, CT18NLO
PowhegConfig.PDF += [320900, 320500]      # NNPDF31_nnlo_as_0118_nf_4, NNPDF31_nlo_as_0118_nf_4
PowhegConfig.PDF += [334300, 334700]      # NNPDF40_nnlo_as_01180_nf_4, NNPDF40_nlo_as_nf_4
PowhegConfig.PDF += [14200, 14300, 14100] # CT18ANNLO, CT18XNNLO and CT18ZNNLO
PowhegConfig.PDF += list(range(93500, 93541))  # PDF4LHC21_40_nf4 with eigensets

PowhegConfig.nEvents *= 1.1 # Safety factor

PowhegConfig.MadSpin_decays= ["decay t > w+ b, w+ > l+ vl"]
PowhegConfig.MadSpin_process = "generate p p > t b~ j $$ w+ w- [QCD]" 

# use diagonal CKM matrix
PowhegConfig.CKM_Vud = 1.0
PowhegConfig.CKM_Vus = 0.000001
PowhegConfig.CKM_Vub = 0.000001
PowhegConfig.CKM_Vcd = 0.000001
PowhegConfig.CKM_Vcs = 1.0
PowhegConfig.CKM_Vcb = 0.000001
PowhegConfig.CKM_Vtd = 0.000001
PowhegConfig.CKM_Vts = 0.000001
PowhegConfig.CKM_Vtb = 1.0

# use BWcutoff of 50 to be consistent with ttbar
PowhegConfig.bwcutoff = 50

# Initial settings
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]


