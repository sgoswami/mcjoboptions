#--------------------------------------------------------------
# Use LHE files as input
#--------------------------------------------------------------

# In 20.7.9.9.6, LHE merging means this is no longer needed
#evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

#--------------------------------------------------------------
# Defining the function to extract parameters
#--------------------------------------------------------------
dict_pdgIds = {}
dict_pdgIds["b"]   = 5
dict_pdgIds["mu"]  = 13
dict_pdgIds["tau"] = 15
dict_pdgIds["g"]   = 21
dict_pdgIds["y"]   = 22

def getParameters():
    import re

    #--- Read parts of the job option
    jonamelist = jofile.rstrip(".py").split("_")
    tune = jonamelist[2]
    process = jonamelist[3]
    ma = float(jonamelist[5].split("a")[-1].replace("p", "."))
    decayChan = str(jonamelist[6])
    partFilter = None
    if len(jonamelist)>7:
        partFilter = str(jonamelist[7])

    #--- list of decays, e.g. [mu, tau] for 2mu2tau
    decayProducts = []
    for part in dict_pdgIds.keys():
        decay = re.findall("[1-4]%s" % part, decayChan)
        if len(decay)>0:
            decayProducts.append(decay[0][1:]) # remove the number in front of the letter
    process = re.sub(r'\d+', '', process)

    return tune, process, ma, decayChan, decayProducts, partFilter

#    MC15.999999.PowhegPy8EG_ggH_H125_a60a60_2mu2tau_[filtXXX].py
tune, process, ma, decayChan, decayProducts, partFilter = getParameters()
print ("Parameters: ")
print (tune, process, ma, decayChan, decayProducts, partFilter)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
tunelongname = ""
if tune == "A14":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
    tunelongname = "A14NNPDF23LO"
elif tune == "A14v1d":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Down_EvtGen_Common.py')
    tunelongname = "A14v1dNNPDF23LO"
elif tune == "A14v1u":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Up_EvtGen_Common.py')
    tunelongname = "A14v1uNNPDF23LO"
elif tune == "A14v2d":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Down_EvtGen_Common.py')
    tunelongname = "A14v2dNNPDF23LO"
elif tune == "A14v2u":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Up_EvtGen_Common.py')
    tunelongname = "A14v2uNNPDF23LO"
elif tune == "A14v3ad":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aDown_EvtGen_Common.py')
    tunelongname = "A14v3adNNPDF23LO"
elif tune == "A14v3au":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aUp_EvtGen_Common.py')
    tunelongname = "A14v3auNNPDF23LO"
elif tune == "A14v3bd":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bDown_EvtGen_Common.py')
    tunelongname = "A14v3bdNNPDF23LO"
elif tune == "A14v3bu":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bUp_EvtGen_Common.py')
    tunelongname = "A14v3buNNPDF23LO"
elif tune == "A14v3cd":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py')
    tunelongname = "A14v3cdNNPDF23LO"
elif tune == "A14v3cu":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py')
    tunelongname = "A14v3cuNNPDF23LO"
else:
    print ('ERROR: tune ' + tune + ' not found')
genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
# Confirming Pythia8 default value
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg_Main31.py")
if process=="ggH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']
elif process=="VBF":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WpH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WmH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ggZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']


#--------------------------------------------------------------
# Higgs->aa at Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',
                            '35:m0 = 125',
                            '35:mWidth = 0.00407',
                            '35:doForceWidth = on',
                            '35:onMode = off',
                            '35:onIfMatch = 36 36', # h->aa
                            '36:onMode = off',
                            ]


#--------------------------------------------------------------
# a->XX at Pythia8
#--------------------------------------------------------------

if len(decayProducts)==1: # a->4X
    genSeq.Pythia8.Commands += [
                                '36:onIfAny = %d' % dict_pdgIds[decayProducts[0]], # decay a->XX
                                '36:m0 = %.1f' % ma, #scalar mass
                                '36:mMin = 0',
                                '36:tau0 = 0',
                                ]
elif len(decayProducts)==2: # a->2X2Y

    sign = [-1,-1]
    for count, part in enumerate(decayProducts):
        pdgId = dict_pdgIds[part]
        if pdgId==21 or pdgId==22:
            sign[count] = 1
    print( ' Allowing0: %d %d' % (dict_pdgIds[decayProducts[0]], sign[0]*dict_pdgIds[decayProducts[0]]) )
    print( ' Allowing1: %d %d' % (dict_pdgIds[decayProducts[1]], sign[1]*dict_pdgIds[decayProducts[1]]) )
    genSeq.Pythia8.Commands += [
                                '36:oneChannel = 1 0.5 100 %d %d' % (dict_pdgIds[decayProducts[0]], sign[0]*dict_pdgIds[decayProducts[0]]), #a->XX
                                '36:addChannel = 1 0.5 100 %d %d' % (dict_pdgIds[decayProducts[1]], sign[1]*dict_pdgIds[decayProducts[1]]), #a->YY
                                '36:m0 %.1f' % ma, #scalar mass
                                '36:mMin %.1f' % (ma-0.5), #scalar mass
                                '36:mMax %.1f' % (ma+0.5), #scalar mass
                                '36:mWidth 0.01', # narrow width
                                '36:tau0 0', #scalarlife time
                                ]


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125"]

if process=="ggH" and (decayChan=="2b2mu" or decayChan=="2mu2b"):
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->aa->bbmumu mh=125 GeV"
    evgenConfig.process     = "ggH H->aa->bbmumu"
    evgenConfig.contact     = [ 'christopher.hayes@cern.ch','ljiljana.morvaj@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="VBF" and (decayChan=="2b2mu" or decayChan=="2mu2b"):
    evgenConfig.description = "POWHEG+Pythia8 VBF H production, H->aa->bbmumu mh=125 GeV"
    evgenConfig.process     = "VBF H->aa->bbmumu"
    evgenConfig.contact     = [ 'christopher.hayes@cern.ch','ljiljana.morvaj@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="ggH" and decayChan=="4tau":
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->aa->4tau mh=125 GeV"
    evgenConfig.process     = "ggH H->aa->4tau"
    evgenConfig.contact     = [ 'rachel.smith@cern.ch', 'huacheng.cai@cern.ch', 'roger.caminal.armadans@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="ggH" and (decayChan=="2mu2tau" or decayChan=="2tau2mu"):
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->aa->mumutautau mh=125 GeV"
    evgenConfig.process     = "ggH H->aa->mumutautau"
    evgenConfig.contact     = [ 'josefina.alconada@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="ggH" and (decayChan=="2b2tau" or decayChan=="2tau2b"):
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->aa->bbtautau mh=125 GeV"
    evgenConfig.process     = "ggH H->aa->bbtautau"
    evgenConfig.contact     = [ 'christopher.hayes@cern.ch','kevin.nelson@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="VBF" and (decayChan=="2b2tau" or decayChan=="2tau2b"):
    evgenConfig.description = "POWHEG+Pythia8 VBF H production, H->aa->bbtautau mh=125 GeV"
    evgenConfig.process     = "VBF H->aa->bbtautau"
    evgenConfig.contact     = [ 'christopher.hayes@cern.ch','kevin.nelson@cern.ch' ]
    evgenConfig.contact     = tunelongname

if process=="ggH" and (decayChan=="2y2g" or decayChan=="2g2y"):
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->aa->gamgamgg mh=125 GeV"
    evgenConfig.process     = "ggH H->aa->gamgamgg"
    evgenConfig.contact     = [ 'rubbo@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="VBF" and (decayChan=="2y2g" or decayChan=="2g2y"):
    evgenConfig.description = "POWHEG+Pythia8 VBF H production, H->aa->gamgamgg mh=125 GeV"
    evgenConfig.process     = "VBF H->aa->gamgamgg"
    evgenConfig.contact     = [ 'rubbo@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="ggH" and (decayChan=="4mu"):
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->aa->4mu mh=125 GeV"
    evgenConfig.process     = "ggH H->aa->4mu"
    evgenConfig.contact     = [ 'roger.caminal.armadans@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="WpH" and decayChan=="4b":
    evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l+vbbbarbbbar production"
    evgenConfig.process     = "WpH, H->aa->4b, W->lv"
    evgenConfig.contact     = [ 'roger.caminal.armadans@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="WmH" and decayChan=="4b":
    evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W-jet->l-vbbbarbbbar production"
    evgenConfig.process     = "WmH, H->aa->4b, W->lv"
    evgenConfig.contact     = [ 'roger.caminal.armadans@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="ZH" and decayChan=="4b":
    evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-bbbarbbbar production"
    evgenConfig.process     = "ZH, H->aa->4b, Z->ll"
    evgenConfig.contact     = [ 'roger.caminal.armadans@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="ggZH" and decayChan=="4b":
    evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->H+Z+jet->l+l-bbbarbbbar production"
    evgenConfig.process     ="ggZH, H->2a->4b, Z->ll"
    evgenConfig.contact     = [ 'roger.caminal.armadans@cern.ch' ]
    evgenConfig.tune        = tunelongname

if process=="ggH" and (decayChan=="2y2tau" or decayChan=="2tau2y"):
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->aa->2tau2y mh=125 GeV"
    evgenConfig.process     = "ggH H->aa->tautauyy"
    evgenConfig.contact     = [ 'luis.pascual@cern.ch','ishabat@cern.ch' ]
    evgenConfig.tune        = tunelongname
#--------------------------------------------------------------
# FILTERS (if needed)
#--------------------------------------------------------------
if partFilter=="filterXXYY" or partFilter=="lh" or partFilter=="ll" or \
   partFilter=="llsf" or partFilter=="llof" or partFilter=="hh":
    if not hasattr(filtSeq, "XtoVVDecayFilter"):
        from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
        filtSeq += XtoVVDecayFilter()
        if partFilter=="lh" or partFilter=="ll" or \
           partFilter=="llsf" or partFilter=="llof" or partFilter=="hh":
          filtSeq+= XtoVVDecayFilter("tauscalarFilter")
#    ## Add this filter to the algs required to be successful for streaming
#    if "XtoVVDecayFilter" not in StreamEVGEN.RequireAlgs:
#        StreamEVGEN.RequireAlgs += ["XtoVVDecayFilter"]
    filtSeq.XtoVVDecayFilter.PDGGrandParent = 35
    filtSeq.XtoVVDecayFilter.PDGParent = 36
    filtSeq.XtoVVDecayFilter.StatusParent = 22
    print ([dict_pdgIds[decayProducts[0]]], [dict_pdgIds[decayProducts[1]]])
    print ([dict_pdgIds[part] for part in decayProducts])
    filtSeq.XtoVVDecayFilter.PDGChild1 = [dict_pdgIds[decayProducts[0]]] #[dict_pdgIds[part] for part in decayProducts]
    filtSeq.XtoVVDecayFilter.PDGChild2 = [dict_pdgIds[decayProducts[1]]] #[dict_pdgIds[part] for part in decayProducts]
    if(partFilter=="lh" or partFilter=="ll"):
      filtSeq.tauscalarFilter.PDGGrandParent = 36
      filtSeq.tauscalarFilter.PDGParent = 15
      filtSeq.tauscalarFilter.StatusParent = 2
      filtSeq.tauscalarFilter.PDGChild1 = [11,13]
      if partFilter=="ll":
        filtSeq.tauscalarFilter.PDGChild2 = [11,13]
      elif partFilter=="lh":
        filtSeq.tauscalarFilter.PDGChild2 = [111,130,211,221,223,310,311,321,323]
    if partFilter=="hh":
      filtSeq.tauscalarFilter.PDGGrandParent = 36
      filtSeq.tauscalarFilter.PDGParent = 15
      filtSeq.tauscalarFilter.StatusParent = 2
      filtSeq.tauscalarFilter.PDGChild1 = [111,130,211,221,223,310,311,321,323]
      filtSeq.tauscalarFilter.PDGChild2 = [111,130,211,221,223,310,311,321,323]
    if(partFilter=="llsf" or partFilter=="llof"):
      filtSeq.tauscalarFilter.PDGGrandParent = 36
      filtSeq.tauscalarFilter.PDGParent = 15
      filtSeq.tauscalarFilter.StatusParent = 2
      filtSeq.tauscalarFilter.PDGChild1 = [11,13]
      filtSeq.tauscalarFilter.PDGChild2 = [11,13]
      if not hasattr(filtSeq, "LeptonPairFilter"):
        from GeneratorFilters.GeneratorFiltersConf import LeptonPairFilter
      filtSeq += LeptonPairFilter("leptonPairFilter")
      if partFilter=="llsf":
        filtSeq.leptonPairFilter.NSFOS_Max = -1
        filtSeq.leptonPairFilter.NSFOS_Min = -1
        filtSeq.leptonPairFilter.NOFOS_Max =  0
        filtSeq.leptonPairFilter.NOFOS_Max =  0
    # trigger ROI definition
    if partFilter=="lh" or partFilter=="ll" or partFilter=="llof" or partFilter=="llsf":
      from GeneratorFilters.GeneratorFiltersConf import TauFilter
      # require at least one leptonic tau with 12 GeV of pT and |eta| < 3
      filtSeq += TauFilter("leptonTriggerRegion")
      filtSeq.leptonTriggerRegion.Ntaus    = 1
      filtSeq.leptonTriggerRegion.EtaMaxe  = 3
      filtSeq.leptonTriggerRegion.EtaMaxmu = 3
      filtSeq.leptonTriggerRegion.Ptcute   = 12000
      filtSeq.leptonTriggerRegion.Ptcutmu  = 12000
      filtSeq.leptonTriggerRegion.Ptcuthad = 13000000

elif partFilter=="filter2taulep2tauhad":
    from GeneratorFilters.GeneratorFiltersConf import FourTauLepLepHadHadFilter
    filtSeq += FourTauLepLepHadHadFilter()

############################
###Generator filters

##Diphoton filter

if not hasattr( filtSeq, "DiPhotonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import DiPhotonFilter
    filtSeq += DiPhotonFilter()
    pass


filtSeq.DiPhotonFilter.PtCut1st = 15000
filtSeq.DiPhotonFilter.PtCut2nd = 15000

##Single tau filter

#if not hasattr( filtSeq, "TauFilter" ):
#    from GeneratorFilters.GeneratorFiltersConf import TauFilter
#    filtSeq += TauFilter()
#    pass


#filtSeq.TauFilter.Ntaus=2	#added to include both taus.
#filtSeq.TauFilter.Ptcuthad = 10000.0


##evgenConfig

evgenConfig.inputFilesPerJob = 40
evgenConfig.nEventsPerJob = 5000




