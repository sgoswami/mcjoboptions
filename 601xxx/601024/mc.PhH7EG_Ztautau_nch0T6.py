#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->tautau production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["oldrich.kepka@cern.ch"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2tau' ]
evgenConfig.generators  = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.nEventsPerJob= 200
#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361108

include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.decay_mode = "z > tau+ tau-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 12000

PowhegConfig.running_width = 1
PowhegConfig.generate()

# --------------------------------------------------------------
# Shower settings              
# --------------------------------------------------------------    
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10") # not possible to use CT18
## Please note from: https://herwig.hepforge.org/tutorials/faq/pdf.html#set-pdf-of-the-lhereader
# For POWHEG matching, there is basically no cross talk between the hard subprocess PDFs and the shower PDFs,
# so choosing a LO shower PDF different from whatever (NLO) PDF has been used with Powheg should not be a problem.

Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py')
### Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2

from GeneratorFilters.GeneratorFiltersConf import TauFilter
tauLepFilter = TauFilter("tauLepFilter")
filtSeq += tauLepFilter

filtSeq.tauLepFilter.Ntaus = 2 
filtSeq.tauLepFilter.EtaMaxe = 2.7 
filtSeq.tauLepFilter.EtaMaxmu = 2.7 
filtSeq.tauLepFilter.EtaMaxhad = 2.7 # no hadronic tau decays
filtSeq.tauLepFilter.Ptcute = 12000.0
filtSeq.tauLepFilter.Ptcutmu = 12000.0

include('GeneratorFilters/ChargedTrackWeightFilter.py')
filtSeq.ChargedTracksWeightFilter.NchMin = 0
filtSeq.ChargedTracksWeightFilter.NchMax = 6
filtSeq.ChargedTracksWeightFilter.Ptcut = 500
filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 4, 10, 15, 20, 35 ]
filtSeq.ChargedTracksWeightFilter.SplineY =[ 0.06, 0.06, 0.2, 0.48, 0.65, 0.74, 0.74 ]
