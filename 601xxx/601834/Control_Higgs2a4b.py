#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

#evgenConfig.inputfilecheck = "TXT"
#evgenConfig.inputfilecheck = "merged_lhef"

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)

    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

###Run Number Encoding and decoding
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
print(get_physics_short())
tokens = get_physics_short().replace(".py","").replace("p",".").split('_')

ma=float(tokens[-2].split('ma')[-1])
ctau=float(tokens[-1].split('ctau')[-1])

print('#############################################################')
print('ma='+str(ma))
print('This file is edited in /FilesCondorNeeds.')
print('ctau='+str(ctau))
print('#############################################################')

#################################################
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
#Assumption taken from https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/blob/master/common/Powheg/PowhegPythia8EvtGenControl_BSM_VBFH_aa_2mu2tau.py
# and https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/600xxx/600070/mc.PhPy8_VBF_H125_yyv_myv0_MET75.py
#genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']

#genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3'] #Works

#genSeq.Pythia8.Commands += [ 'Powheg:NFinal = -1']
#################################################

prodMode = tokens[1].replace("125", "").replace(".","p") # p replacement is for WpH, all others should be fine 

nFinalPerProdMode = {
    'ggH': 2,
    'VBF': 3, #was VBFH
    'WpH': 3,
    'WmH': 3,
    'ttH': 3,
    'ttHsemilep':3,
    'ttHdilep':3,
    'ttHallhad':3,
    'ZH': 3, 
}

genSeq.Pythia8.Commands += [ 'Powheg:NFinal = %d' % nFinalPerProdMode[prodMode] ]



# inputFilesPerJobDict = {
#     'ggH':        20, #2200/LHE, needed 23k/10k
#     'WpH':        100, #550/LHE, needed 22k/10k
#     'WmH':        100, #550/LHE, needed 23k/10k
#     'ttHsemilep': 10, #6000/LHE, needed 33k/10k
#     'ttHdilep':   10, #6000/LHE, needed 33k/10k
#     'ttHallhad':  10, #6000/LHE, needed 33k/10k
#     'ZH':        500, #110/LHE, needed 24k/10k
# }
# evgenConfig.nEventsPerJob=10000
# evgenConfig.inputFilesPerJob = inputFilesPerJobDict[prodMode]

#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',
                            #'HiggsBSM:ffbar2H2Z = on',

                            '35:m0 = 125',
                            '35:doForceWidth = on',
                            '35:mWidth = 0.00407',
                            '35:oneChannel = 1 1.0 100 70 70',


                            '70:new = S S 1 0 0 %.1f' % ma,#scalar mass
                            '70:tau0 = %.1f' % ctau,#nominal proper lifetime (in mm/c)
                            '70:isResonance = 0',
                            '70:oneChannel = 1 1.0 0 5 -5',
                            
]

genSeq.Pythia8.Commands = [i for i in genSeq.Pythia8.Commands if (("limitTau0" not in i) and ("tau0Max" not in i))]
genSeq.Pythia8.Commands += [
                            'ParticleDecays:tau0Max = 100000.0',
                            'ParticleDecays:limitTau0 = off'
                           ]

print(genSeq.Pythia8.Commands)

testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.contact  = ["david.rousso@cern.ch"]
#evgenConfig.keywords += [ 'SUSY','VBF','Higgs','simplifiedModel']
evgenConfig.keywords += [ 'SUSY','Higgs','simplifiedModel']
evgenConfig.description = "POWHEG+Pythia8 H->2a->4b production"
#evgenConfig.description = "POWHEG+Pythia8 VBF->H->2a->4b production"
#evgenConfig.description = "POWHEG+Pythia8 ggH->H->2a->4b production"
#evgenConfig.description = "POWHEG+Pythia8 ZH->H->2a->4b production"
#evgenConfig.description = "POWHEG+Pythia8 WpH->H->2a->4b production"
#evgenConfig.description = "POWHEG+Pythia8 WmH->H->2a->4b production"
#evgenConfig.description = "POWHEG+Pythia8 ttH->H->2a->4b production"
