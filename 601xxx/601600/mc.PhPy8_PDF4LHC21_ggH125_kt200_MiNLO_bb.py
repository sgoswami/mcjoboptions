#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "ggH bornktmin=200 H->bb"
evgenConfig.description    = "POWHEG+PYTHIA8+EVTGEN, H+jet production with MiNLO and A14 tune, Hbb mh=125 GeV"
evgenConfig.keywords       = [ "SM", "Higgs", "SMHiggs", "bbbar", "mH125" ]
evgenConfig.contact        = [ 'andrea.sciandra@cern.ch' ]
evgenConfig.nEventsPerJob  = 2000

##--------------------------------------------------------------
## Pythia8 showering
##--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.Commands  += ['Powheg:NFinal = 2']

else:
        genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

##--------------------------------------------------------------
## H->bb decay
##--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
        '25:onIfMatch = 5 -5' ]
