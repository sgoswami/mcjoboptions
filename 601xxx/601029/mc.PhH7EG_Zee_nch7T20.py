#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->ee production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["oldrich.kepka@cern.ch"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2electron' ]
evgenConfig.generators  = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361106
include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.decay_mode = "z > e+ e-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.running_width = 1
PowhegConfig.nEvents *= 15
PowhegConfig.generate()

# --------------------------------------------------------------
# Shower settings              
# --------------------------------------------------------------    
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10") # not possible to use CT18
## Please note from: https://herwig.hepforge.org/tutorials/faq/pdf.html#set-pdf-of-the-lhereader
# For POWHEG matching, there is basically no cross talk between the hard subprocess PDFs and the shower PDFs,
# so choosing a LO shower PDF different from whatever (NLO) PDF has been used with Powheg should not be a problem.

Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py')
## Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2

include('GeneratorFilters/ChargedTrackWeightFilter.py')
filtSeq.ChargedTracksWeightFilter.NchMin = 7
filtSeq.ChargedTracksWeightFilter.NchMax = 20
filtSeq.ChargedTracksWeightFilter.Ptcut = 500
filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 5, 10, 20, 30 ]
filtSeq.ChargedTracksWeightFilter.SplineY =[ 2, 2, 8, 15, 22, 22 ]
