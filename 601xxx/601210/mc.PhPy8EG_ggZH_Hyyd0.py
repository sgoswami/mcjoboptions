#---------------------------------------------------------------
# LHE files of ggZH used as inputs: mc15_13TeV.345061.PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_HgamgamZinc.evgen.TXT.e5762 
# POWHEG+Pythia8 ggZH,Z->ll  H-> gam gam_d, m_gam_d = 0 GeV
#---------------------------------------------------------------
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 69

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py') 

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
	genSeq.Pythia8.Commands  += ['Powheg:NFinal = 2']
else:
	genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
#--------------------------------------------------------------
# H->y+yd decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on', # BSM Higgs
                            '25:onMode = off', # decay of Higgs
                            '25:addChannel = 1 1. 103 22 4900022', # H->y+yd HV
                            '25:onIfMatch = 22 4900022',
                            '4900022:m0 = 0.0', # yd mass
                            '4900022:onMode = off', # yd decay off
                            '4900022:tau0 = off', # yd no decay to tau
                            ]
#--------------------------------------------------------------
# Lepon filter for Z->ll
#--------------------------------------------------------------
if not hasattr( filtSeq, "MultiLeptonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    filtSeq += MultiLeptonFilter()
    pass
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2
 
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ggZH production: H->y+yd, Hidden Valley, 0 GeV"
evgenConfig.keywords    = [ 'BSM' , 'Higgs' , 'BSMHiggs' , 'ZHiggs' , 'photon' , 'darkPhoton' , 'hiddenValley']
evgenConfig.contact     = [ 'hassnae.el.jarrari@cern.ch' ]
evgenConfig.process = "gg->H, H->y+yd"
