#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO, top->qX decays'
evgenConfig.keywords    = [ 'top', 'ttbar', 'FCNC' ]
evgenConfig.contact     = [ 'oliver.thielmann@cern.ch']

## get the top JO name
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short() 

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.decay_mode      = "t t~ > undecayed"
PowhegConfig.MadSpin_enabled = True

PowhegConfig.hdamp        = 258.75                                        # 1.5 * mtop
PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          = [260000, 25200, 13165, 90900]                 # NNPDF30, MMHT, CT14, PDF4LHC - PDF variations with nominal scale variation
PowhegConfig.PDF.extend(range(260001, 260101))                            # Include the NNPDF error set
PowhegConfig.PDF.extend(range(90901 , 90931 ))                            # Include the PDF4LHC error set

PowhegConfig.MadSpin_nFlavours = 5
PowhegConfig.MadSpin_model = '/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/TopFCNC-onlyh'
PowhegConfig.MadSpin_process = "generate p p > t t~ [QCD]"
PowhegConfig.nEvents=int(10.0*runArgs.maxEvents)

if "Q2cbarH" in shortname:
    PowhegConfig.MadSpin_decays= ["decay t~ > c~ h", "decay t > w+ b, w+ > All All"]
    os.system('get_files -jo param_card_ctH.dat')
    PowhegConfig.MadSpin_paramcard = "./param_card_ctH.dat"
elif "Q2cH" in shortname:
    PowhegConfig.MadSpin_decays= ["decay t > c h", "decay t~ > w- b~, w- > All All"]
    os.system('get_files -jo param_card_ctH.dat')
    PowhegConfig.MadSpin_paramcard = "./param_card_ctH.dat"
elif "Q2ubarH" in shortname:
    PowhegConfig.MadSpin_decays= ["decay t~ > u~ h", "decay t > w+ b, w+ > All All"]
    os.system('get_files -jo param_card_utH.dat')
    PowhegConfig.MadSpin_paramcard = "./param_card_utH.dat"
elif "Q2uH" in shortname:
    PowhegConfig.MadSpin_decays= ["decay t > u h", "decay t~ > w- b~, w- > All All"]
    os.system('get_files -jo param_card_utH.dat')
    PowhegConfig.MadSpin_paramcard = "./param_card_utH.dat"
else:
    raise RuntimeError("shortname not recognized :'( ")

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#### For Rivet or Paver or whatever. 
#from AthenaCommon.AlgSequence import AlgSequence
#rivetSeq = AlgSequence()
#from Rivet_i.Rivet_iConf import Rivet_i
#rivet = Rivet_i()
#rivet.Analyses += ["MC_TTBAR:TTMODE=TWOLEP"]
#rivet.HistoFile = 'decayFCNC_newLocal.yoda'
#rivet.SkipWeights=True
#rivetSeq += rivet

include('GeneratorFilters/MultiLeptonFilter.py')
filtSeq.MultiLeptonFilter.Ptcut = 5000.
filtSeq.MultiLeptonFilter.Etacut = 4.5
filtSeq.MultiLeptonFilter.NLeptons = 2

genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]