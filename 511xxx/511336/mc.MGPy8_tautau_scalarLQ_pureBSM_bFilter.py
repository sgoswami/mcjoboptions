import re
import os
import math
import subprocess

from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtils import remap_lhe_pdgids

nevents = runArgs.maxEvents*10. if runArgs.maxEvents>0 else 10.*evgenConfig.nEventsPerJob

process_def = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model LO_LQ_S1Tilde
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
generate p p > ta+ ta- QED<=3 NP<=0 @0
add process p p > ta+ ta- j QED<=3 NP<=0 @1
add process p p > ta+ ta- j j QED<=3 NP<=0 @2
output -f\n"""


process_dir = new_process(process_def)


lhaid=260000
pdflabel='lhapdf'

settings = { 'lhe_version' : '3.0',
             'cut_decays'  : 'F',
             'lhaid'       : lhaid,
             'pdlabel'     : "'"+pdflabel+"'",
             'ickkw'       : 0,
             'ptj'         : 20,
             'ptb'         : 20,
             'xqcut'       : 0.,
             'drjj'        : 0.0,
             'mmll'        : 120., 
             'maxjetflavor': 5,
             'ktdurham'    : 30,
             'dparameter'  : 0.4,
             'use_syst'    : 'False',
             'nevents':int(nevents)}

modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=settings)

if os.path.exists("param_card.dat"):
    os.remove("param_card.dat")

params = {}
params['LQPARAM'] = {'MS1t': '{:e}'.format(1e12)}
params['MASS'] = {'s1tm43': '{:e}'.format(1e12)}
params['YUKS1tRR'] = {'yRR1x1': '{:e}'.format(0.0), 
                      'yRR1x2': '{:e}'.format(0.0), 
                      'yRR1x3': '{:e}'.format(0.0), 
                      'yRR2x1': '{:e}'.format(0.0), 
                      'yRR2x2': '{:e}'.format(0.0), 
                      'yRR2x3': '{:e}'.format(0.0), 
                      'yRR3x1': '{:e}'.format(0.0), 
                      'yRR3x2': '{:e}'.format(0.0), 
                      'yRR3x3': '{:e}'.format(0.0)}

# If possible, build the param card from the one that comes with the model
modify_param_card(process_dir=process_dir,params=params)

print_cards()

# Reweighting setup
reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("""change process p p > ta- ta+ QED<=3 NP<=2 NP^2==4
change process p p > ta- ta+ j QED<=3 NP<=2 NP^2==4 --add
change process p p > ta- ta+ j j QED<=3 NP<=2 NP^2==4 --add
change allow_missing_finalstate True
launch --rwgt_name=lambda1p0_M1p4TeV --allow_missing_finalstate=True
    change allow_missing_finalstate True
    set LQPARAM 1 1.4e+03
    set YUKS1tRR 3 3 1.0
""")
reweight_card_f.close()


generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(runArgs=runArgs, process_dir=process_dir, lhe_version=3, saveProcDir=False)

# remap the PDG ID in the LHE file after its generation
remap_lhe_pdgids(runArgs.inputGeneratorFile+".events",pdgid_map={9000005:42})


evgenConfig.description = 'Toolbox scalar LQ single production of S1~, generation: 3, SM -> BSM reweighting, pure BSM contribution'
evgenConfig.keywords += ['BSM', 'exotic', 'leptoquark', 'scalar']

evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> tau tau + jets'
evgenConfig.contact = ["Patrick Rieck <patrick.rieck@cern.ch>"]


process="pp>ta+ta-"
nJetMax=2

PYTHIA8_nJetMax=nJetMax
PYTHIA8_Process=process
PYTHIA8_Dparameter=settings['dparameter']
PYTHIA8_TMS=settings['ktdurham']
PYTHIA8_nQuarksMerge=settings['maxjetflavor']
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")

# event filter
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq,0.4)
from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
filtSeq += MultiBjetFilter()
filtSeq.MultiBjetFilter.NBJetsMin = 1
filtSeq.MultiBjetFilter.InclusiveEfficiency = 0.15
filtSeq.Expression = "MultiBjetFilter"
