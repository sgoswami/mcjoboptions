include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

#get the file name and split it on _ to extract relavent information
jobConfigParts = JOName.split("_")

masses['1000006'] = float(jobConfigParts[4])
masses['1000022'] = float(jobConfigParts[5].split('.')[0])
if masses['1000022']<0.5: masses['1000022']=0.5

process = '''
generate p p > t1 t1~ $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @1
add process p p > t1 t1~ j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
add process p p > t1 t1~ j j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
'''

#set 100% branching fraction for stop->charm+neutralino
decays['1000006'] = """DECAY   1000006     1.34259598E-01   # stop1 decays
     0.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.00000000E+00    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
"""

njets = 2

if 'MET' in JOName.split('_')[-1]:
    include ( 'GeneratorFilters/MissingEtFilter.py' )

    metFilter = JOName.split('_')[-1]
    metFilter = int(metFilter.split("MET")[1].split(".")[0])

    print "Using MET Filter: " + str(metFilter)
    filtSeq.MissingEtFilter.METCut = metFilter*GeV
    evt_multiplier = metFilter / 10

else:
    print "No MET Filter applied"


evgenLog.info('Registered generation of stop pair production, stop to c+LSP; grid point '+str(runArgs.jobConfig[0])+' decoded into mass point mstop=' + str(masses['1000006']) + ', mlsp='+str(masses['1000022']))

evgenConfig.contact  = [ "edmund.xiang.lin.ting@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','charm']
evgenConfig.description = 'stop direct pair production, st->c+LSP in simplified model'

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
    else:
        genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'
    #genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,-1000006}"]
