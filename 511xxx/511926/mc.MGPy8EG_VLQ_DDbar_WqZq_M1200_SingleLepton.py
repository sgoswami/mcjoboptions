import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
vlqMass = "1.200e+03"
nevents=runArgs.maxEvents*7

process = """
import model VLQ_v4_4FNS_UFO
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define lq = u d u~ d~
define small = u c d s u~ c~ d~ s~ a ve vm vt e- mu- ve~ vm~ vt~ e+ mu+ b b~ ta- ta+
generate p p > bp bp~
output -f"""

process_dir = new_process(process)

topDecays = "1.0000e+00 2 5 24"
wDecays = "3.163684e-01 2 2 -1\n3.163684e-01 2 4 -3\n1.111203e-01 2 -11 12\n1.111203e-01 2 -13 14\n1.110377e-01 2 -15 16\n1.699249e-02 2 -1 4\n1.699249e-02 -3 2"
zDecays = "1.520543e-01 2 3 -3\n1.520543e-01 2 1 -1\n1.504122e-01 2 5 -5\n1.178122e-01 2 4 -4\n1.178122e-01 2 2 -2\n6.877657e-02 2 16 -16\n6.877657e-02 2 14 -14\n6.877657e-02 2 12 -12\n3.453449e-02 2 11 -11\n3.453449e-02 2 13 -13\n3.453449e-02 2 15 -15"
vlqDecays = "3.333334e-01 2 -24 2\n3.333333e-01 2 23 1\n3.333333e-01 2 25 1"

#Edit the parameter card
paramFile = process_dir+"/Cards/param_card.dat"
params = {
    "MASS" : {"6000006": vlqMass,
              "6000007": vlqMass},
    "DECAY": {"6": "1.31971862037e+00\n"+topDecays,
              "23": "2.411888e+00\n"+zDecays,
              "24": "2.00278e+00\n"+wDecays,
              "6000007": "5.0000e+00\n"+vlqDecays}
}
modify_param_card(param_card_input=paramFile, output_location = paramFile, params=params)


#Fetch default LO run_card.dat and set parameters
settings = {'lhe_version':'2.0', 
            'cut_decays' :'F', 
            'nevents'    :int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay bp > w- lq, w- > small small
decay bp~ > z lq, z > small small
decay w+ > small small
decay w- > small small
decay z > small small
# running the actual code
launch"""%runArgs.randomSeed)
mscard.close()

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

###############################
### Shower JOs will go here ###
###############################
evgenConfig.description = "Madgraph+Pythia8 production JO with NNPDF30NLOnf4 for pair production of vector-like down quarks with a mass of 1200 GeV decaying to WqZq with a single-lepton filter"
evgenConfig.keywords = ['BSM', 'exotic']
evgenConfig.contact = ['evan.vandewall@cern.ch']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

include("GeneratorFilters/OneLeptonFilter.py")
filtSeq.WZtoLeptonFilter.Ptcut_electron = 15000
filtSeq.WZtoLeptonFilter.Ptcut_muon = 15000
