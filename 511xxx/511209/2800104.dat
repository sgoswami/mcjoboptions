# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  14:56
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.41097189E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.60664270E+03  # scale for input parameters
    1    3.98566901E+01  # M_1
    2    7.78886351E+02  # M_2
    3    2.74867555E+03  # M_3
   11    6.10544876E+03  # A_t
   12    6.92342703E+02  # A_b
   13   -9.56432552E+02  # A_tau
   23   -9.93318434E+01  # mu
   25    2.31386770E+01  # tan(beta)
   26    4.43677375E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.01151624E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.28080754E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.60518759E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.60664270E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.60664270E+03  # (SUSY scale)
  1  1     8.39219851E-06   # Y_u(Q)^DRbar
  2  2     4.26323684E-03   # Y_c(Q)^DRbar
  3  3     1.01384499E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.60664270E+03  # (SUSY scale)
  1  1     3.90206518E-04   # Y_d(Q)^DRbar
  2  2     7.41392384E-03   # Y_s(Q)^DRbar
  3  3     3.86962593E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.60664270E+03  # (SUSY scale)
  1  1     6.80938908E-05   # Y_e(Q)^DRbar
  2  2     1.40796570E-02   # Y_mu(Q)^DRbar
  3  3     2.36796668E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.60664270E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     6.10544903E+03   # A_t(Q)^DRbar
Block Ad Q=  2.60664270E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     6.92342720E+02   # A_b(Q)^DRbar
Block Ae Q=  2.60664270E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -9.56432557E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.60664270E+03  # soft SUSY breaking masses at Q
   1    3.98566901E+01  # M_1
   2    7.78886351E+02  # M_2
   3    2.74867555E+03  # M_3
  21    1.95073121E+07  # M^2_(H,d)
  22    1.41633663E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.01151624E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.28080754E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.60518759E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.29245868E+02  # h0
        35     4.43595187E+03  # H0
        36     4.43677375E+03  # A0
        37     4.43690499E+03  # H+
   1000001     1.01022926E+04  # ~d_L
   2000001     1.00777092E+04  # ~d_R
   1000002     1.01019327E+04  # ~u_L
   2000002     1.00808306E+04  # ~u_R
   1000003     1.01022966E+04  # ~s_L
   2000003     1.00777151E+04  # ~s_R
   1000004     1.01019367E+04  # ~c_L
   2000004     1.00808325E+04  # ~c_R
   1000005     3.01463652E+03  # ~b_1
   2000005     4.65651198E+03  # ~b_2
   1000006     2.23253101E+03  # ~t_1
   2000006     3.04344537E+03  # ~t_2
   1000011     1.00210326E+04  # ~e_L-
   2000011     1.00088832E+04  # ~e_R-
   1000012     1.00202726E+04  # ~nu_eL
   1000013     1.00210476E+04  # ~mu_L-
   2000013     1.00089126E+04  # ~mu_R-
   1000014     1.00202876E+04  # ~nu_muL
   1000015     1.00172411E+04  # ~tau_1-
   2000015     1.00253202E+04  # ~tau_2-
   1000016     1.00245549E+04  # ~nu_tauL
   1000021     3.16266733E+03  # ~g
   1000022     3.57002386E+01  # ~chi_10
   1000023     1.20266067E+02  # ~chi_20
   1000025     1.22666942E+02  # ~chi_30
   1000035     8.51743715E+02  # ~chi_40
   1000024     1.12105698E+02  # ~chi_1+
   1000037     8.51600961E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.15023886E-02   # alpha
Block Hmix Q=  2.60664270E+03  # Higgs mixing parameters
   1   -9.93318434E+01  # mu
   2    2.31386770E+01  # tan[beta](Q)
   3    2.43358028E+02  # v(Q)
   4    1.96849613E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -2.00555923E-01   # Re[R_st(1,1)]
   1  2     9.79682255E-01   # Re[R_st(1,2)]
   2  1    -9.79682255E-01   # Re[R_st(2,1)]
   2  2    -2.00555923E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99999920E-01   # Re[R_sb(1,1)]
   1  2     3.99141933E-04   # Re[R_sb(1,2)]
   2  1    -3.99141933E-04   # Re[R_sb(2,1)]
   2  2    -9.99999920E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.34728246E-02   # Re[R_sta(1,1)]
   1  2     9.99909237E-01   # Re[R_sta(1,2)]
   2  1    -9.99909237E-01   # Re[R_sta(2,1)]
   2  2    -1.34728246E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.07538806E-01   # Re[N(1,1)]
   1  2    -9.18796119E-03   # Re[N(1,2)]
   1  3    -4.04314435E-01   # Re[N(1,3)]
   1  4    -1.13219847E-01   # Re[N(1,4)]
   2  1    -3.66617202E-01   # Re[N(2,1)]
   2  2    -7.23625131E-02   # Re[N(2,2)]
   2  3    -6.30865264E-01   # Re[N(2,3)]
   2  4    -6.79973906E-01   # Re[N(2,4)]
   3  1    -2.04782671E-01   # Re[N(3,1)]
   3  2     6.25651110E-02   # Re[N(3,2)]
   3  3    -6.62174657E-01   # Re[N(3,3)]
   3  4     7.18104719E-01   # Re[N(3,4)]
   4  1     5.40366998E-03   # Re[N(4,1)]
   4  2    -9.95371717E-01   # Re[N(4,2)]
   4  3     7.97369486E-03   # Re[N(4,3)]
   4  4     9.56157183E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.13547398E-02   # Re[U(1,1)]
   1  2    -9.99935533E-01   # Re[U(1,2)]
   2  1     9.99935533E-01   # Re[U(2,1)]
   2  2    -1.13547398E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.35334238E-01   # Re[V(1,1)]
   1  2     9.90800002E-01   # Re[V(1,2)]
   2  1     9.90800002E-01   # Re[V(2,1)]
   2  2     1.35334238E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02873234E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.23665651E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.34379146E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.19264035E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.43015407E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     6.97709921E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.21809593E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     6.76281264E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.99421513E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.07870747E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03663954E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     8.22503075E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.34480387E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.22042329E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     7.83421250E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.43055137E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     6.97968778E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.22847493E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     7.97190313E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.99338834E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.07702863E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.26877268E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.97005071E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.53561233E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     9.54761214E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.53779335E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.06333161E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.54243933E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.58336954E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.97299762E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.28538451E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.77731193E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.63826689E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43020002E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.52581060E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     4.70151904E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     9.11658192E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02881873E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.12933502E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.96748570E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43059729E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.52373200E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     4.70022049E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     9.11406395E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02798231E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.15663546E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.96583810E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.54262607E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.98031420E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     4.36073789E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     8.45578297E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.80931449E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     8.29387816E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.53510107E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.52121126E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.10584147E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.15932308E-03    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     3.61724462E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.91372816E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.81682494E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.65936559E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.57730783E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.56680714E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.12143056E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.29954248E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.52143646E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.10835494E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.16598849E-03    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     3.69098935E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.91339414E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.81697581E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.66163796E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.63897779E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.56670205E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.12141021E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.29938700E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.72067821E+02   # ~b_1
#    BR                NDA      ID1      ID2
     1.07887244E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.07057532E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.35561254E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     6.48181979E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.48410276E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.36968198E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     3.94752726E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.36939573E+02   # ~b_2
#    BR                NDA      ID1      ID2
     3.21720930E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     4.28166669E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     4.51604761E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.00885715E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     7.78898294E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     6.69293494E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.77026430E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.51963886E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.41013106E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.66366619E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.81669275E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.32148312E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.07744998E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.54521298E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     2.08320079E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.10101138E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.29925864E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.69300928E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.77024092E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.52210739E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.41292275E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.66356100E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.81684331E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.32151806E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.07958924E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.54511265E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     2.09724910E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.10099064E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.29910311E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     9.35250160E+01   # ~t_1
#    BR                NDA      ID1      ID2
     4.28906431E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.37198331E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.38945238E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.24965028E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.43798341E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.46457776E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.90763738E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.01925511E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.45531719E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.73645170E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     5.46829537E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     7.20500800E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.03041549E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.87557750E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.53298227E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.12007038E-03   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.33480522E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.33233158E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11159829E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11159322E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10967168E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     7.24546686E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.21139205E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.49180774E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.53723885E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.48658527E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.36599893E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.57231443E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.35442948E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     3.66299510E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.96137681E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     4.43469067E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.18917719E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.18686354E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.52471148E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52469882E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.49898133E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.44036967E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.44031771E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.42505826E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.03901744E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     2.90254949E-03   # chi^0_3
#    BR                NDA      ID1      ID2
     8.96596434E-04    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.18746132E-01    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     1.18574532E-01    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     1.52255117E-01    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     1.52254178E-01    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     1.50371842E-01    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     3.43588706E-02    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     3.43585250E-02    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     3.42565196E-02    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     2.03611483E-01    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000035     7.79848934E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.35904671E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.35904671E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.55329983E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     9.21819441E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.39750790E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     6.01590083E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.29556099E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.56154888E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.02735996E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.07327789E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     3.13396418E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.13396418E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.67143737E+01   # ~g
#    BR                NDA      ID1      ID2
     1.04798494E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.04798494E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     4.72832100E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.72832100E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.46387284E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.46387284E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     2.71485512E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
     2.16783810E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     4.09443299E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     5.80657405E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     6.92715436E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.20989433E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.65117930E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.65117930E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.82394919E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.82394919E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     6.20164292E-03   # Gamma(h0)
     1.68511407E-03   2        22        22   # BR(h0 -> photon photon)
     1.34109184E-03   2        22        23   # BR(h0 -> photon Z)
     2.88442056E-02   2        23        23   # BR(h0 -> Z Z)
     2.20320548E-01   2       -24        24   # BR(h0 -> W W)
     4.98829922E-02   2        21        21   # BR(h0 -> gluon gluon)
     2.77957008E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.23641501E-04   2       -13        13   # BR(h0 -> Muon muon)
     3.56532068E-02   2       -15        15   # BR(h0 -> Tau tau)
     7.86547597E-08   2        -2         2   # BR(h0 -> Up up)
     1.52671963E-02   2        -4         4   # BR(h0 -> Charm charm)
     3.19809267E-07   2        -1         1   # BR(h0 -> Down down)
     1.15667589E-04   2        -3         3   # BR(h0 -> Strange strange)
     3.07399288E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     3.39366647E-01   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     8.36356079E+01   # Gamma(HH)
     8.43458835E-09   2        22        22   # BR(HH -> photon photon)
     2.79143325E-08   2        22        23   # BR(HH -> photon Z)
     3.46367028E-07   2        23        23   # BR(HH -> Z Z)
     6.50761208E-08   2       -24        24   # BR(HH -> W W)
     8.78544020E-07   2        21        21   # BR(HH -> gluon gluon)
     4.04514212E-09   2       -11        11   # BR(HH -> Electron electron)
     1.80130340E-04   2       -13        13   # BR(HH -> Muon muon)
     5.20270127E-02   2       -15        15   # BR(HH -> Tau tau)
     1.93673979E-13   2        -2         2   # BR(HH -> Up up)
     3.75743571E-08   2        -4         4   # BR(HH -> Charm charm)
     2.81645061E-03   2        -6         6   # BR(HH -> Top top)
     2.84660899E-07   2        -1         1   # BR(HH -> Down down)
     1.02964391E-04   2        -3         3   # BR(HH -> Strange strange)
     2.72347422E-01   2        -5         5   # BR(HH -> Bottom bottom)
     9.25498551E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.98835468E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.98835468E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.19980023E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     2.05555991E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.34956382E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.07900870E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.90986842E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.56419079E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     9.14063209E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     8.29573113E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     5.60818713E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     8.02062663E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     5.99253862E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.94432342E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     8.21784095E+01   # Gamma(A0)
     3.88027854E-11   2        22        22   # BR(A0 -> photon photon)
     1.12425671E-08   2        22        23   # BR(A0 -> photon Z)
     6.06983889E-06   2        21        21   # BR(A0 -> gluon gluon)
     3.89679973E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.73527732E-04   2       -13        13   # BR(A0 -> Muon muon)
     5.01200208E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.80559949E-13   2        -2         2   # BR(A0 -> Up up)
     3.50294186E-08   2        -4         4   # BR(A0 -> Charm charm)
     2.64636450E-03   2        -6         6   # BR(A0 -> Top top)
     2.74220823E-07   2        -1         1   # BR(A0 -> Down down)
     9.91878855E-05   2        -3         3   # BR(A0 -> Strange strange)
     2.62359319E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     9.30790074E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.02515316E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.02515316E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.63765828E-05   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.98890752E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.10765760E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.42644409E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.87521154E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.03278787E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     9.13085131E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     7.14336446E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     6.87293799E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     9.57787129E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     8.29735892E-06   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     8.37584622E-07   2        23        25   # BR(A0 -> Z h0)
     9.15997113E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     8.91583857E+01   # Gamma(Hp)
     4.78368849E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.04517624E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     5.78492358E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.92121876E-07   2        -1         2   # BR(Hp -> Down up)
     4.95241261E-06   2        -3         2   # BR(Hp -> Strange up)
     3.25910974E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.56320500E-08   2        -1         4   # BR(Hp -> Down charm)
     1.05321052E-04   2        -3         4   # BR(Hp -> Strange charm)
     4.56392120E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.12006220E-07   2        -1         6   # BR(Hp -> Down top)
     4.77956123E-06   2        -3         6   # BR(Hp -> Strange top)
     3.10725769E-01   2        -5         6   # BR(Hp -> Bottom top)
     4.49894546E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     7.27804373E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.65124332E-02   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.44935211E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.70603364E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.63658022E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.87498315E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.71453491E-10   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     7.72299744E-07   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.23388099E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.35474985E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.35398373E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00014309E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.72467483E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.86776810E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.23388099E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.35474985E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.35398373E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99997149E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.85075032E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99997149E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.85075032E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25736264E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    5.68417372E-04        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    9.35593472E-02        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.85075032E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99997149E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.05419540E-04   # BR(b -> s gamma)
    2    1.59178154E-06   # BR(b -> s mu+ mu-)
    3    3.52351230E-05   # BR(b -> s nu nu)
    4    3.23767966E-15   # BR(Bd -> e+ e-)
    5    1.38309595E-10   # BR(Bd -> mu+ mu-)
    6    2.89300261E-08   # BR(Bd -> tau+ tau-)
    7    1.09723380E-13   # BR(Bs -> e+ e-)
    8    4.68736545E-09   # BR(Bs -> mu+ mu-)
    9    9.93407322E-07   # BR(Bs -> tau+ tau-)
   10    9.66404494E-05   # BR(B_u -> tau nu)
   11    9.98258843E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42458195E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93817408E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15925855E-03   # epsilon_K
   17    2.28167793E-15   # Delta(M_K)
   18    2.47952440E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28913359E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.55179975E-16   # Delta(g-2)_electron/2
   21   -1.09097835E-11   # Delta(g-2)_muon/2
   22   -3.08932484E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.48623940E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.70657049E-01   # C7
     0305 4322   00   2     8.97969043E-06   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -8.74547304E-02   # C8
     0305 6321   00   2    -4.60287638E-05   # C8'
 03051111 4133   00   0     1.61146053E+00   # C9 e+e-
 03051111 4133   00   2     1.61188822E+00   # C9 e+e-
 03051111 4233   00   2     1.43759545E-04   # C9' e+e-
 03051111 4137   00   0    -4.43415263E+00   # C10 e+e-
 03051111 4137   00   2    -4.43080223E+00   # C10 e+e-
 03051111 4237   00   2    -1.08735715E-03   # C10' e+e-
 03051313 4133   00   0     1.61146053E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61188814E+00   # C9 mu+mu-
 03051313 4233   00   2     1.43759520E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43415263E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43080231E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.08735719E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50462482E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.35934502E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50462482E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.35934504E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50462482E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.35935081E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85827932E-07   # C7
     0305 4422   00   2    -9.59977396E-06   # C7
     0305 4322   00   2     1.36677047E-06   # C7'
     0305 6421   00   0     3.30487140E-07   # C8
     0305 6421   00   2     4.37752855E-06   # C8
     0305 6321   00   2     7.81120790E-07   # C8'
 03051111 4133   00   2    -3.22289917E-07   # C9 e+e-
 03051111 4233   00   2     2.70305172E-06   # C9' e+e-
 03051111 4137   00   2     7.65275782E-06   # C10 e+e-
 03051111 4237   00   2    -2.04460832E-05   # C10' e+e-
 03051313 4133   00   2    -3.22291080E-07   # C9 mu+mu-
 03051313 4233   00   2     2.70305145E-06   # C9' mu+mu-
 03051313 4137   00   2     7.65275934E-06   # C10 mu+mu-
 03051313 4237   00   2    -2.04460840E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.63042748E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     4.43638691E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.63042742E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     4.43638691E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.63040929E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     4.43638690E-06   # C11' nu_3 nu_3
