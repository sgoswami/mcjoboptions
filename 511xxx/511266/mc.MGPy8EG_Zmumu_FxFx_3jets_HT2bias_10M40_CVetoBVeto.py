evgenConfig.description = 'aMcAtNlo Zmumu+0,1,2,3j NLO FxFx HT2-biased CVetoBVeto, low mass mll=10-40GeV'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch", "jan.kretzschmar@cern.ch"]
evgenConfig.keywords += ['SM', 'Z', 'muon', 'jets', 'NLO']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 27.4%
# CVetoBVeto+dilep eff 44.8%
# one LHE file contains 32000 events
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 3

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO                                                 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)
PYTHIA8_nJetMax=3
PYTHIA8_qCut=20.
print "PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax
print "PYTHIA8_qCut = %i"%PYTHIA8_qCut

include("Pythia8_i/Pythia8_FxFx_A14mod.py")

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
include("GeneratorFilters/BHadronFilter.py")
HeavyFlavorBHadronFilter.BottomEtaMax = 2.9
HeavyFlavorBHadronFilter.BottomPtMin = 5*GeV
HeavyFlavorBHadronFilter.RequireTruthJet = True
HeavyFlavorBHadronFilter.JetPtMin = 10*GeV
HeavyFlavorBHadronFilter.JetEtaMax = 2.9
HeavyFlavorBHadronFilter.TruthContainerName = "AntiKt4TruthJets"
HeavyFlavorBHadronFilter.DeltaRFromTruth = 0.4
filtSeq += HeavyFlavorBHadronFilter 

include("GeneratorFilters/CHadronPt4Eta3_Filter.py")
filtSeq += HeavyFlavorCHadronPt4Eta3_Filter

include("GeneratorFilters/MultiLeptonFilter.py")
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.0
MultiLeptonFilter.NLeptons = 2

filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (not HeavyFlavorCHadronPt4Eta3_Filter) and (MultiLeptonFilter)"
