evgenConfig.description = "Single electron and positron with flat pT = [20 , 100] GeV, flat |eta| = [2.3 , 5.0] and flat phi"
evgenConfig.keywords = ["singleParticle", "electron"]
evgenConfig.contact = ["lderamo@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG 
genSeq += PG.ParticleGun() 
evgenConfig.generators += ["ParticleGun"]
       
genSeq.ParticleGun.sampler.pid = {11,-11}
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[20000.0 , 100000.0] , eta=[-5.0 , -2.3 , 2.3 , 5.0])