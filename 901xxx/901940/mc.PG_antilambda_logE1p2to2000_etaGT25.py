evgenConfig.description = "Single anti-lambda with log energy distribution"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact = ["christopher.young@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ['ParticleGun']
genSeq.ParticleGun.sampler.pid = (-3122)
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(1200, 2000000.), eta=[(-4.9,-2.5), (2.5,4.9)])


