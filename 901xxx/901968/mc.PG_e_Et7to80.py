evgenConfig.description = "Single electrons with flat ET in [7,80] GeV"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact = ["martindl@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.generators += ['ParticleGun']

import ParticleGun as PG
pg = PG.ParticleGun()
pg.sampler.pid = (11,-11)
pg.sampler.mom = PG.PtEtaMPhiSampler(pt=[7000, 80000],eta=[-2.5, 2.5])

genSeq += pg
