# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  22:15
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.86532907E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.75106490E+03  # scale for input parameters
    1   -4.25265916E+01  # M_1
    2   -6.09561685E+02  # M_2
    3    4.19405296E+03  # M_3
   11   -4.34790187E+03  # A_t
   12   -1.20589152E+03  # A_b
   13    1.19853058E+03  # A_tau
   23    2.80406375E+02  # mu
   25    3.76678261E+01  # tan(beta)
   26    1.50794683E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.44732506E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.13350811E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.14205372E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.75106490E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.75106490E+03  # (SUSY scale)
  1  1     8.38732622E-06   # Y_u(Q)^DRbar
  2  2     4.26076172E-03   # Y_c(Q)^DRbar
  3  3     1.01325638E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.75106490E+03  # (SUSY scale)
  1  1     6.34854700E-04   # Y_d(Q)^DRbar
  2  2     1.20622393E-02   # Y_s(Q)^DRbar
  3  3     6.29576928E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.75106490E+03  # (SUSY scale)
  1  1     1.10786788E-04   # Y_e(Q)^DRbar
  2  2     2.29071940E-02   # Y_mu(Q)^DRbar
  3  3     3.85261318E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.75106490E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -4.34790178E+03   # A_t(Q)^DRbar
Block Ad Q=  2.75106490E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.20589155E+03   # A_b(Q)^DRbar
Block Ae Q=  2.75106490E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.19853058E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.75106490E+03  # soft SUSY breaking masses at Q
   1   -4.25265916E+01  # M_1
   2   -6.09561685E+02  # M_2
   3    4.19405296E+03  # M_3
  21    1.97732839E+06  # M^2_(H,d)
  22    3.74677142E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.44732506E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.13350811E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.14205372E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.26113822E+02  # h0
        35     1.50752176E+03  # H0
        36     1.50794683E+03  # A0
        37     1.51135472E+03  # H+
   1000001     1.00779875E+04  # ~d_L
   2000001     1.00540234E+04  # ~d_R
   1000002     1.00777476E+04  # ~u_L
   2000002     1.00571860E+04  # ~u_R
   1000003     1.00779957E+04  # ~s_L
   2000003     1.00540375E+04  # ~s_R
   1000004     1.00777558E+04  # ~c_L
   2000004     1.00571879E+04  # ~c_R
   1000005     3.22524117E+03  # ~b_1
   2000005     3.50268242E+03  # ~b_2
   1000006     2.15473809E+03  # ~t_1
   2000006     3.51242600E+03  # ~t_2
   1000011     1.00212496E+04  # ~e_L-
   2000011     1.00087479E+04  # ~e_R-
   1000012     1.00204848E+04  # ~nu_eL
   1000013     1.00212864E+04  # ~mu_L-
   2000013     1.00088190E+04  # ~mu_R-
   1000014     1.00205214E+04  # ~nu_muL
   1000015     1.00288782E+04  # ~tau_1-
   2000015     1.00319449E+04  # ~tau_2-
   1000016     1.00309355E+04  # ~nu_tauL
   1000021     4.59219438E+03  # ~g
   1000022     4.22679484E+01  # ~chi_10
   1000023     2.81798595E+02  # ~chi_20
   1000025     2.92193770E+02  # ~chi_30
   1000035     6.73876983E+02  # ~chi_40
   1000024     2.81349937E+02  # ~chi_1+
   1000037     6.73929536E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.60911901E-02   # alpha
Block Hmix Q=  2.75106490E+03  # Higgs mixing parameters
   1    2.80406375E+02  # mu
   2    3.76678261E+01  # tan[beta](Q)
   3    2.43330563E+02  # v(Q)
   4    2.27390364E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     8.14255295E-02   # Re[R_st(1,1)]
   1  2     9.96679428E-01   # Re[R_st(1,2)]
   2  1    -9.96679428E-01   # Re[R_st(2,1)]
   2  2     8.14255295E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.48998484E-02   # Re[R_sb(1,1)]
   1  2     9.99888991E-01   # Re[R_sb(1,2)]
   2  1    -9.99888991E-01   # Re[R_sb(2,1)]
   2  2     1.48998484E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     2.84777430E-01   # Re[R_sta(1,1)]
   1  2     9.58593665E-01   # Re[R_sta(1,2)]
   2  1    -9.58593665E-01   # Re[R_sta(2,1)]
   2  2     2.84777430E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.87077349E-01   # Re[N(1,1)]
   1  2     1.80203433E-03   # Re[N(1,2)]
   1  3    -1.59021819E-01   # Re[N(1,3)]
   1  4    -1.96753763E-02   # Re[N(1,4)]
   2  1     1.26627678E-01   # Re[N(2,1)]
   2  2     1.42060116E-01   # Re[N(2,2)]
   2  3    -6.99115985E-01   # Re[N(2,3)]
   2  4    -6.89217813E-01   # Re[N(2,4)]
   3  1     9.76596699E-02   # Re[N(3,1)]
   3  2    -6.16727383E-02   # Re[N(3,2)]
   3  3    -6.94728972E-01   # Re[N(3,3)]
   3  4     7.09937122E-01   # Re[N(3,4)]
   4  1     1.03114820E-02   # Re[N(4,1)]
   4  2    -9.87933272E-01   # Re[N(4,2)]
   4  3    -5.74504621E-02   # Re[N(4,3)]
   4  4    -1.43460686E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -8.15037858E-02   # Re[U(1,1)]
   1  2     9.96673032E-01   # Re[U(1,2)]
   2  1    -9.96673032E-01   # Re[U(2,1)]
   2  2    -8.15037858E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     2.03428568E-01   # Re[V(1,1)]
   1  2     9.79089791E-01   # Re[V(1,2)]
   2  1     9.79089791E-01   # Re[V(2,1)]
   2  2    -2.03428568E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02864158E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.74362240E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.60104182E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.52192023E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.05373155E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.43735204E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.47455273E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.34741659E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.93510644E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     4.07063849E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.04171362E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04953996E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.70388891E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.69540028E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.04795603E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.11705498E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     2.05221923E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.43840184E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.47023790E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.38192891E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.78070285E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.93299907E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     4.06768257E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.03732672E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.14728543E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.22845896E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.42728594E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.14630054E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.09997508E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.55389504E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     4.34062008E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.68254831E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.48443557E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     9.07970477E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.43502884E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.37870279E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.89893284E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.88239096E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43739931E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.59025286E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.69638660E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.97008765E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.00097405E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.53561676E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.82977424E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43844899E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.58401560E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.69515489E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.96720508E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.99879528E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.60590026E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.82558953E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.73464035E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.12568583E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.40717125E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.29323199E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.48937973E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.90388113E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.84716653E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.02626043E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.08803802E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.78863469E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     1.06406708E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.88833012E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.32752029E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.21049485E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     9.99786862E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     4.37546696E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.80642860E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.29933254E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.38033971E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.89323982E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.02685432E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.08805699E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.07016306E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.34212206E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.88719036E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.32785649E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.21154169E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.02216876E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.59675365E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.80608771E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.35388844E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.38026989E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.89283359E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     5.17421586E+01   # ~b_1
#    BR                NDA      ID1      ID2
     4.72332724E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.37698905E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.32832574E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     9.70586299E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.79217228E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.93819148E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.09242848E-04    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.87190452E+02   # ~b_2
#    BR                NDA      ID1      ID2
     6.05168186E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     7.21762231E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     7.09944509E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     7.54362556E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.60269407E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.67700033E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.47241819E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     5.19792105E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.20974519E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     6.91741574E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     4.11401453E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.56794852E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.32749456E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.12163157E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.89654618E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.37233928E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     6.75587738E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.79323282E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.33203874E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.89288708E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.19799522E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.20968603E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     6.95045222E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     4.14912273E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.56781512E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.32783041E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.12152283E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.89917613E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.40121225E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     6.75553553E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.83852650E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.33197216E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.89248082E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     8.83191691E+01   # ~t_1
#    BR                NDA      ID1      ID2
     5.18636559E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.23277143E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.35990037E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.29392982E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.49521956E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.63980274E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.98158503E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.00675734E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.72890890E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.81712993E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.20019149E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.52367144E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.31657643E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.11973788E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.18009843E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.68240842E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.80348584E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99857478E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.42438107E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     5.21087826E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.19142599E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.75185317E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.47468677E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.47837858E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.24719630E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.42011766E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.06221293E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.38955257E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     5.13880630E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     4.86088245E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.14210146E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.04634587E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.95352115E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     6.44354500E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.24377893E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.24377893E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.40083720E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     3.32612830E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.85554218E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     8.61632356E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.51486730E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.76538691E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.41218429E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.41218429E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.63974778E+02   # ~g
#    BR                NDA      ID1      ID2
     3.89681751E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     3.89681751E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.47856030E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.47856030E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     7.27549734E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     7.27549734E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.06417222E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.06417222E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     7.25040552E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     7.25040552E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.72683300E-03   # Gamma(h0)
     2.52947016E-03   2        22        22   # BR(h0 -> photon photon)
     1.71765302E-03   2        22        23   # BR(h0 -> photon Z)
     3.30459792E-02   2        23        23   # BR(h0 -> Z Z)
     2.67103825E-01   2       -24        24   # BR(h0 -> W W)
     7.74664590E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.71331386E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.09657194E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.04529331E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.31805713E-07   2        -2         2   # BR(h0 -> Up up)
     2.55815586E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.44627210E-07   2        -1         1   # BR(h0 -> Down down)
     1.96979396E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.24182230E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     7.51257349E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     4.01479545E+01   # Gamma(HH)
     1.31197024E-07   2        22        22   # BR(HH -> photon photon)
     2.99052064E-07   2        22        23   # BR(HH -> photon Z)
     1.04401560E-06   2        23        23   # BR(HH -> Z Z)
     8.28472757E-07   2       -24        24   # BR(HH -> W W)
     2.10155643E-05   2        21        21   # BR(HH -> gluon gluon)
     7.71215637E-09   2       -11        11   # BR(HH -> Electron electron)
     3.43308670E-04   2       -13        13   # BR(HH -> Muon muon)
     9.91406752E-02   2       -15        15   # BR(HH -> Tau tau)
     6.53536786E-14   2        -2         2   # BR(HH -> Up up)
     1.26753865E-08   2        -4         4   # BR(HH -> Charm charm)
     8.21315427E-04   2        -6         6   # BR(HH -> Top top)
     6.10019581E-07   2        -1         1   # BR(HH -> Down down)
     2.20657789E-04   2        -3         3   # BR(HH -> Strange strange)
     5.70850312E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.27475063E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     8.96563401E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     8.96563401E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     5.54385113E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     2.08749958E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.21911956E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.67244705E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.37444428E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.13136981E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     5.51735337E-05   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     3.84334000E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.49598307E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     5.22031411E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.83386424E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     5.14681316E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     3.90653388E+01   # Gamma(A0)
     1.72222213E-08   2        22        22   # BR(A0 -> photon photon)
     1.69211334E-07   2        22        23   # BR(A0 -> photon Z)
     3.69200246E-05   2        21        21   # BR(A0 -> gluon gluon)
     7.60625424E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.38594551E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.77799180E-02   2       -15        15   # BR(A0 -> Tau tau)
     6.18866438E-14   2        -2         2   # BR(A0 -> Up up)
     1.20008847E-08   2        -4         4   # BR(A0 -> Charm charm)
     8.24605123E-04   2        -6         6   # BR(A0 -> Top top)
     6.01620473E-07   2        -1         1   # BR(A0 -> Down down)
     2.17619690E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.63002762E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.44725932E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     9.10600315E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     9.10600315E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.40006210E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     2.12507852E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.11120014E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.88228880E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.58243685E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.21186806E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.83540297E-05   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     5.24175892E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.00115326E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.97942786E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     7.18982839E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.42371206E-06   2        23        25   # BR(A0 -> Z h0)
     5.68439002E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.34169441E+01   # Gamma(Hp)
     8.60083798E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.67712684E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.04009843E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.89947071E-07   2        -1         2   # BR(Hp -> Down up)
     9.87656055E-06   2        -3         2   # BR(Hp -> Strange up)
     6.32423974E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.79150687E-08   2        -1         4   # BR(Hp -> Down charm)
     2.12638216E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.85618919E-04   2        -5         4   # BR(Hp -> Bottom charm)
     6.35162232E-08   2        -1         6   # BR(Hp -> Down top)
     1.69095578E-06   2        -3         6   # BR(Hp -> Strange top)
     5.91233532E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.95554947E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.74131731E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     2.07672563E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     8.92151902E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.46662867E-03   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     8.30575662E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     8.51577894E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     6.87115794E-08   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.29313136E-06   2        24        25   # BR(Hp -> W h0)
     2.58885816E-11   2        24        35   # BR(Hp -> W HH)
     1.43857042E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.66354156E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.41889877E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.41886512E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00002371E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.81075417E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.04788626E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.66354156E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.41889877E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.41886512E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999797E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.02888114E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999797E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.02888114E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25812486E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.16701315E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.35481843E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.02888114E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999797E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.22501865E-04   # BR(b -> s gamma)
    2    1.58941201E-06   # BR(b -> s mu+ mu-)
    3    3.52404025E-05   # BR(b -> s nu nu)
    4    3.52468233E-15   # BR(Bd -> e+ e-)
    5    1.50566140E-10   # BR(Bd -> mu+ mu-)
    6    3.12703693E-08   # BR(Bd -> tau+ tau-)
    7    1.17930855E-13   # BR(Bs -> e+ e-)
    8    5.03786651E-09   # BR(Bs -> mu+ mu-)
    9    1.06057545E-06   # BR(Bs -> tau+ tau-)
   10    9.34088525E-05   # BR(B_u -> tau nu)
   11    9.64877685E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42074449E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93477728E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15791390E-03   # epsilon_K
   17    2.28166848E-15   # Delta(M_K)
   18    2.47947482E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28909188E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.52434853E-16   # Delta(g-2)_electron/2
   21   -1.50677926E-11   # Delta(g-2)_muon/2
   22   -4.27021223E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.84457098E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.88037700E-01   # C7
     0305 4322   00   2    -2.76070828E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.08938639E-01   # C8
     0305 6321   00   2    -4.26189033E-04   # C8'
 03051111 4133   00   0     1.61379636E+00   # C9 e+e-
 03051111 4133   00   2     1.61409975E+00   # C9 e+e-
 03051111 4233   00   2     2.08398546E-04   # C9' e+e-
 03051111 4137   00   0    -4.43648847E+00   # C10 e+e-
 03051111 4137   00   2    -4.43367662E+00   # C10 e+e-
 03051111 4237   00   2    -1.57071624E-03   # C10' e+e-
 03051313 4133   00   0     1.61379636E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61409958E+00   # C9 mu+mu-
 03051313 4233   00   2     2.08395563E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43648847E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43367679E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.57071357E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50474170E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.40634253E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50474170E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.40634883E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50474171E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.40812441E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85837447E-07   # C7
     0305 4422   00   2    -5.39562711E-06   # C7
     0305 4322   00   2     1.85706625E-06   # C7'
     0305 6421   00   0     3.30495290E-07   # C8
     0305 6421   00   2    -5.92599717E-06   # C8
     0305 6321   00   2     7.01580939E-07   # C8'
 03051111 4133   00   2    -1.41680951E-07   # C9 e+e-
 03051111 4233   00   2     5.63910658E-06   # C9' e+e-
 03051111 4137   00   2     4.81535689E-06   # C10 e+e-
 03051111 4237   00   2    -4.25177046E-05   # C10' e+e-
 03051313 4133   00   2    -1.41683268E-07   # C9 mu+mu-
 03051313 4233   00   2     5.63910519E-06   # C9' mu+mu-
 03051313 4137   00   2     4.81535980E-06   # C10 mu+mu-
 03051313 4237   00   2    -4.25177088E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.01952590E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     9.22063272E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.01952577E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     9.22063272E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.01948945E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     9.22063270E-06   # C11' nu_3 nu_3
