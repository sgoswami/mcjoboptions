# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  20:07
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    9.87713885E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.20592864E+03  # scale for input parameters
    1    4.70562926E+01  # M_1
    2    1.18083061E+03  # M_2
    3    4.87563871E+03  # M_3
   11    4.73158234E+02  # A_t
   12    1.36303432E+03  # A_b
   13   -1.92380331E+03  # A_tau
   23    2.96928472E+02  # mu
   25    9.43975757E+00  # tan(beta)
   26    4.84758045E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.31105500E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.19195408E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.59043981E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.20592864E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.20592864E+03  # (SUSY scale)
  1  1     8.43128646E-06   # Y_u(Q)^DRbar
  2  2     4.28309352E-03   # Y_c(Q)^DRbar
  3  3     1.01856713E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.20592864E+03  # (SUSY scale)
  1  1     1.59931840E-04   # Y_d(Q)^DRbar
  2  2     3.03870496E-03   # Y_s(Q)^DRbar
  3  3     1.58602270E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.20592864E+03  # (SUSY scale)
  1  1     2.79092756E-05   # Y_e(Q)^DRbar
  2  2     5.77075303E-03   # Y_mu(Q)^DRbar
  3  3     9.70545726E-02   # Y_tau(Q)^DRbar
Block Au Q=  2.20592864E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.73158238E+02   # A_t(Q)^DRbar
Block Ad Q=  2.20592864E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.36303435E+03   # A_b(Q)^DRbar
Block Ae Q=  2.20592864E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.92380330E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.20592864E+03  # soft SUSY breaking masses at Q
   1    4.70562926E+01  # M_1
   2    1.18083061E+03  # M_2
   3    4.87563871E+03  # M_3
  21    9.89252992E+04  # M^2_(H,d)
  22   -1.34096381E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.31105500E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.19195408E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.59043981E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.17193302E+02  # h0
        35     4.85192560E+02  # H0
        36     4.84758045E+02  # A0
        37     4.88646539E+02  # H+
   1000001     1.00451368E+04  # ~d_L
   2000001     1.00239779E+04  # ~d_R
   1000002     1.00449821E+04  # ~u_L
   2000002     1.00270712E+04  # ~u_R
   1000003     1.00451388E+04  # ~s_L
   2000003     1.00239792E+04  # ~s_R
   1000004     1.00449842E+04  # ~c_L
   2000004     1.00270739E+04  # ~c_R
   1000005     2.27959806E+03  # ~b_1
   2000005     3.62272198E+03  # ~b_2
   1000006     2.12985983E+03  # ~t_1
   2000006     2.28471427E+03  # ~t_2
   1000011     1.00203342E+04  # ~e_L-
   2000011     1.00087052E+04  # ~e_R-
   1000012     1.00195753E+04  # ~nu_eL
   1000013     1.00203374E+04  # ~mu_L-
   2000013     1.00087116E+04  # ~mu_R-
   1000014     1.00195785E+04  # ~nu_muL
   1000015     1.00105064E+04  # ~tau_1-
   2000015     1.00212820E+04  # ~tau_2-
   1000016     1.00205056E+04  # ~nu_tauL
   1000021     5.21296355E+03  # ~g
   1000022     4.50978724E+01  # ~chi_10
   1000023     2.96963207E+02  # ~chi_20
   1000025     2.99935180E+02  # ~chi_30
   1000035     1.26579555E+03  # ~chi_40
   1000024     2.93735192E+02  # ~chi_1+
   1000037     1.26476245E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.11050553E-01   # alpha
Block Hmix Q=  2.20592864E+03  # Higgs mixing parameters
   1    2.96928472E+02  # mu
   2    9.43975757E+00  # tan[beta](Q)
   3    2.43568637E+02  # v(Q)
   4    2.34990362E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -1.10269221E-01   # Re[R_st(1,1)]
   1  2     9.93901755E-01   # Re[R_st(1,2)]
   2  1    -9.93901755E-01   # Re[R_st(2,1)]
   2  2    -1.10269221E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99999911E-01   # Re[R_sb(1,1)]
   1  2     4.22587620E-04   # Re[R_sb(1,2)]
   2  1    -4.22587620E-04   # Re[R_sb(2,1)]
   2  2     9.99999911E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     3.96515862E-02   # Re[R_sta(1,1)]
   1  2     9.99213567E-01   # Re[R_sta(1,2)]
   2  1    -9.99213567E-01   # Re[R_sta(2,1)]
   2  2     3.96515862E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.87178337E-01   # Re[N(1,1)]
   1  2     3.61121324E-03   # Re[N(1,2)]
   1  3    -1.54672137E-01   # Re[N(1,3)]
   1  4     3.92736446E-02   # Re[N(1,4)]
   2  1     1.37448179E-01   # Re[N(2,1)]
   2  2     6.48734867E-02   # Re[N(2,2)]
   2  3    -6.98064375E-01   # Re[N(2,3)]
   2  4     6.99718199E-01   # Re[N(2,4)]
   3  1     8.11150165E-02   # Re[N(3,1)]
   3  2    -3.27981374E-02   # Re[N(3,2)]
   3  3    -6.98751907E-01   # Re[N(3,3)]
   3  4    -7.09993246E-01   # Re[N(3,4)]
   4  1     2.69856673E-03   # Re[N(4,1)]
   4  2    -9.97347819E-01   # Re[N(4,2)]
   4  3    -2.29876303E-02   # Re[N(4,3)]
   4  4     6.90044531E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -3.24767422E-02   # Re[U(1,1)]
   1  2     9.99472491E-01   # Re[U(1,2)]
   2  1     9.99472491E-01   # Re[U(2,1)]
   2  2     3.24767422E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.76109425E-02   # Re[V(1,1)]
   1  2     9.95224650E-01   # Re[V(1,2)]
   2  1     9.95224650E-01   # Re[V(2,1)]
   2  2     9.76109425E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02858202E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.74564197E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.88603558E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     6.56839242E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.40729015E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.59703145E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     5.98397925E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.00999907E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.59978710E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06351097E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02990907E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.74310852E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.89195185E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     6.63091384E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.31463870E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.40735689E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.59673933E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     6.00664205E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.00985759E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.59947622E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06322547E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.41807963E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.05515589E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     3.42678675E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     2.22567869E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     9.56565968E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.50853314E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.91785927E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.42477017E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.52311620E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.23539836E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     6.73072288E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.96988902E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     4.39619916E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.98255609E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.40733054E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.83484224E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.81303389E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02712183E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.96124035E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01142230E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.40739728E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.83442611E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.81294850E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02697931E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.00799299E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01113977E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.42621417E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.71867500E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.78919532E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.98733690E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.90125931E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.93255183E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.26718930E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.27800878E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.47335077E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.86886331E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.53207608E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.57220663E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.30207972E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.78063242E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.70690149E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.68307252E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.54654540E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.65227660E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.26722732E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.27800785E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.49426236E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.86877854E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.53213339E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.57226006E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.31824680E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.79682680E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.70682346E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.74857475E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.54653032E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.65220108E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.08027476E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.75911375E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     9.09341435E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     9.20127338E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     7.86121598E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     7.32084234E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.63266627E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.98317807E-03    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     5.89331400E+00   # ~b_2
#    BR                NDA      ID1      ID2
     3.41634340E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.54309907E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.50422582E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.41263478E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.01849897E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.76758868E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.10337617E-04    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     2.55726004E-02    2     2000006       -37   # BR(~b_2 -> ~t_2 H^-)
     1.07240120E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.26532347E-02    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     1.46480237E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     1.26666755E-02    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     4.43835999E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.91640004E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.51457406E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.31358844E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.49552828E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.53213970E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.36920554E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.36916843E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.69184783E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.52035772E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.53338467E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.65189701E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.43843560E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.91631869E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.55472144E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.35501416E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.49537217E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.53219682E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.36919171E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.40148405E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.69177321E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.52365921E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.53336921E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.65182139E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     8.76460992E+01   # ~t_1
#    BR                NDA      ID1      ID2
     5.12099299E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.36000202E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.38118446E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.97102138E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.65715094E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     5.98136040E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     6.17813736E+01   # ~t_2
#    BR                NDA      ID1      ID2
     8.45461721E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     3.55297819E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     3.69415735E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.68174883E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.97646492E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.48164514E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     9.34871056E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.15030599E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.94685735E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99776495E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.22901969E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.97084457E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.55351442E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.56290084E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     4.10508694E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     8.67280644E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     1.07386208E-01    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.54346755E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     8.83420522E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.49609140E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     8.73606400E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.90934813E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.61710406E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     2.89524059E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.61132428E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     3.51830983E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     6.48158865E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.38709402E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.98369182E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.01587133E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     2.07094745E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.48501652E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.48501652E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     8.37427242E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     8.37427242E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     2.94494249E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.19523661E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.80334574E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     2.70463125E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     1.75741671E-02    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.13787675E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     7.39756788E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     2.11085493E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     5.45894348E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     8.63408664E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.51073271E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.22669501E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     3.24700208E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.24700208E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.49960730E+02   # ~g
#    BR                NDA      ID1      ID2
     1.55588083E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.55588083E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.41215992E-01    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.41215992E-01    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.44205043E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.44205043E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     5.89379699E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     5.89379699E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.24075992E-03   # Gamma(h0)
     2.16718087E-03   2        22        22   # BR(h0 -> photon photon)
     8.20779663E-04   2        22        23   # BR(h0 -> photon Z)
     1.15693785E-02   2        23        23   # BR(h0 -> Z Z)
     1.11411754E-01   2       -24        24   # BR(h0 -> W W)
     7.14004639E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.66442327E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.51958753E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.26362762E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.34581160E-07   2        -2         2   # BR(h0 -> Up up)
     2.61165591E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.63115168E-07   2        -1         1   # BR(h0 -> Down down)
     2.39834046E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.39369109E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     6.40159018E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     9.87344549E-01   # Gamma(HH)
     1.44478939E-06   2        22        22   # BR(HH -> photon photon)
     4.90479967E-07   2        22        23   # BR(HH -> photon Z)
     1.64184271E-03   2        23        23   # BR(HH -> Z Z)
     3.50153036E-03   2       -24        24   # BR(HH -> W W)
     5.14165495E-04   2        21        21   # BR(HH -> gluon gluon)
     6.52154418E-09   2       -11        11   # BR(HH -> Electron electron)
     2.90208269E-04   2       -13        13   # BR(HH -> Muon muon)
     8.37862325E-02   2       -15        15   # BR(HH -> Tau tau)
     1.51001164E-11   2        -2         2   # BR(HH -> Up up)
     2.92768058E-06   2        -4         4   # BR(HH -> Charm charm)
     8.69229709E-02   2        -6         6   # BR(HH -> Top top)
     6.07079584E-07   2        -1         1   # BR(HH -> Down down)
     2.19579919E-04   2        -3         3   # BR(HH -> Strange strange)
     5.78364056E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.25095217E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     6.64159896E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.39071911E-01   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.67565141E-02   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.02655004E+00   # Gamma(A0)
     2.09740543E-06   2        22        22   # BR(A0 -> photon photon)
     6.57577964E-07   2        22        23   # BR(A0 -> photon Z)
     7.43816490E-04   2        21        21   # BR(A0 -> gluon gluon)
     6.22391012E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.76963562E-04   2       -13        13   # BR(A0 -> Muon muon)
     7.99667215E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.16870175E-11   2        -2         2   # BR(A0 -> Up up)
     2.26659426E-06   2        -4         4   # BR(A0 -> Charm charm)
     1.38642957E-01   2        -6         6   # BR(A0 -> Top top)
     5.79451732E-07   2        -1         1   # BR(A0 -> Down down)
     2.09586992E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.52166679E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.49467535E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.42113032E-01   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     5.81782897E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.74959321E-03   2        23        25   # BR(A0 -> Z h0)
     5.29804025E-36   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.01561673E+00   # Gamma(Hp)
     7.76225605E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.31860660E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.38666572E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.29979129E-07   2        -1         2   # BR(Hp -> Down up)
     1.03761148E-05   2        -3         2   # BR(Hp -> Strange up)
     6.57420652E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.72797123E-07   2        -1         4   # BR(Hp -> Down charm)
     2.30185512E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.20616610E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.21327463E-05   2        -1         6   # BR(Hp -> Down top)
     2.64783937E-04   2        -3         6   # BR(Hp -> Strange top)
     6.76333138E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.25076423E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.94643988E-03   2        24        25   # BR(Hp -> W h0)
     6.52905958E-10   2        24        35   # BR(Hp -> W HH)
     1.17962670E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.10668424E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    8.90023387E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    8.91090230E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.98802767E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.24194408E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.12222081E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.10668424E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    8.90023387E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    8.91090230E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99969648E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.03516015E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99969648E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.03516015E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25407565E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.95931461E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.17217527E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.03516015E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99969648E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.10174188E-04   # BR(b -> s gamma)
    2    1.59054949E-06   # BR(b -> s mu+ mu-)
    3    3.52823948E-05   # BR(b -> s nu nu)
    4    2.56345719E-15   # BR(Bd -> e+ e-)
    5    1.09507980E-10   # BR(Bd -> mu+ mu-)
    6    2.29243545E-08   # BR(Bd -> tau+ tau-)
    7    8.63385856E-14   # BR(Bs -> e+ e-)
    8    3.68838160E-09   # BR(Bs -> mu+ mu-)
    9    7.82336304E-07   # BR(Bs -> tau+ tau-)
   10    9.43864926E-05   # BR(B_u -> tau nu)
   11    9.74976333E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43716874E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94188499E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16487378E-03   # epsilon_K
   17    2.28173938E-15   # Delta(M_K)
   18    2.48187463E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29499503E-11   # BR(K^+ -> pi^+ nu nu)
   20    9.38192279E-17   # Delta(g-2)_electron/2
   21    4.01106651E-12   # Delta(g-2)_muon/2
   22    1.13470927E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -2.37265935E-06   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.76327576E-01   # C7
     0305 4322   00   2    -1.73282536E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.89393877E-01   # C8
     0305 6321   00   2    -1.84677869E-03   # C8'
 03051111 4133   00   0     1.60537333E+00   # C9 e+e-
 03051111 4133   00   2     1.60630703E+00   # C9 e+e-
 03051111 4233   00   2    -1.13172351E-05   # C9' e+e-
 03051111 4137   00   0    -4.42806544E+00   # C10 e+e-
 03051111 4137   00   2    -4.42931922E+00   # C10 e+e-
 03051111 4237   00   2     8.69301511E-05   # C10' e+e-
 03051313 4133   00   0     1.60537333E+00   # C9 mu+mu-
 03051313 4133   00   2     1.60630702E+00   # C9 mu+mu-
 03051313 4233   00   2    -1.13177668E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.42806544E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42931922E+00   # C10 mu+mu-
 03051313 4237   00   2     8.69306817E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50562361E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -1.88886646E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50562361E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -1.88885491E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50562361E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -1.88559882E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821917E-07   # C7
     0305 4422   00   2     3.05079025E-06   # C7
     0305 4322   00   2     1.54627622E-07   # C7'
     0305 6421   00   0     3.30481988E-07   # C8
     0305 6421   00   2     5.58059657E-07   # C8
     0305 6321   00   2     3.44801247E-08   # C8'
 03051111 4133   00   2     3.11172899E-07   # C9 e+e-
 03051111 4233   00   2     3.50133641E-07   # C9' e+e-
 03051111 4137   00   2    -1.92190440E-07   # C10 e+e-
 03051111 4237   00   2    -2.66681577E-06   # C10' e+e-
 03051313 4133   00   2     3.11172781E-07   # C9 mu+mu-
 03051313 4233   00   2     3.50133634E-07   # C9' mu+mu-
 03051313 4137   00   2    -1.92190323E-07   # C10 mu+mu-
 03051313 4237   00   2    -2.66681579E-06   # C10' mu+mu-
 03051212 4137   00   2     6.06454357E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     5.79431077E-07   # C11' nu_1 nu_1
 03051414 4137   00   2     6.06454410E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     5.79431077E-07   # C11' nu_2 nu_2
 03051616 4137   00   2     6.06469625E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     5.79431076E-07   # C11' nu_3 nu_3
