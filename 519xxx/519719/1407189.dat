# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:45
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    6.41597140E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.34586435E+03  # scale for input parameters
    1    4.65537866E+01  # M_1
    2    1.67742706E+03  # M_2
    3    4.15460569E+03  # M_3
   11    7.56694702E+02  # A_t
   12    1.03205580E+03  # A_b
   13    1.01217980E+03  # A_tau
   23    4.08942228E+02  # mu
   25    6.09827894E+00  # tan(beta)
   26    3.09557775E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.97046393E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.10199677E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.40886117E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.34586435E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.34586435E+03  # (SUSY scale)
  1  1     8.49635085E-06   # Y_u(Q)^DRbar
  2  2     4.31614623E-03   # Y_c(Q)^DRbar
  3  3     1.02642743E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.34586435E+03  # (SUSY scale)
  1  1     1.04116598E-04   # Y_d(Q)^DRbar
  2  2     1.97821537E-03   # Y_s(Q)^DRbar
  3  3     1.03251040E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.34586435E+03  # (SUSY scale)
  1  1     1.81691078E-05   # Y_e(Q)^DRbar
  2  2     3.75679524E-03   # Y_mu(Q)^DRbar
  3  3     6.31831157E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.34586435E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     7.56694711E+02   # A_t(Q)^DRbar
Block Ad Q=  3.34586435E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.03205580E+03   # A_b(Q)^DRbar
Block Ae Q=  3.34586435E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.01217979E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.34586435E+03  # soft SUSY breaking masses at Q
   1    4.65537866E+01  # M_1
   2    1.67742706E+03  # M_2
   3    4.15460569E+03  # M_3
  21    9.10975335E+06  # M^2_(H,d)
  22    1.84791745E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.97046393E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.10199677E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.40886117E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.17806101E+02  # h0
        35     3.09547452E+03  # H0
        36     3.09557775E+03  # A0
        37     3.09983542E+03  # H+
   1000001     1.00885263E+04  # ~d_L
   2000001     1.00656929E+04  # ~d_R
   1000002     1.00882002E+04  # ~u_L
   2000002     1.00684079E+04  # ~u_R
   1000003     1.00885271E+04  # ~s_L
   2000003     1.00656932E+04  # ~s_R
   1000004     1.00882010E+04  # ~c_L
   2000004     1.00684092E+04  # ~c_R
   1000005     4.51377353E+03  # ~b_1
   2000005     5.05589826E+03  # ~b_2
   1000006     2.21354814E+03  # ~t_1
   2000006     5.05740446E+03  # ~t_2
   1000011     1.00197647E+04  # ~e_L-
   2000011     1.00092106E+04  # ~e_R-
   1000012     1.00190195E+04  # ~nu_eL
   1000013     1.00197654E+04  # ~mu_L-
   2000013     1.00092118E+04  # ~mu_R-
   1000014     1.00190201E+04  # ~nu_muL
   1000015     1.00095745E+04  # ~tau_1-
   2000015     1.00199538E+04  # ~tau_2-
   1000016     1.00192065E+04  # ~nu_tauL
   1000021     4.62884136E+03  # ~g
   1000022     4.44874437E+01  # ~chi_10
   1000023     4.13114545E+02  # ~chi_20
   1000025     4.14808393E+02  # ~chi_30
   1000035     1.79046333E+03  # ~chi_40
   1000024     4.10335483E+02  # ~chi_1+
   1000037     1.79043084E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.54965545E-01   # alpha
Block Hmix Q=  3.34586435E+03  # Higgs mixing parameters
   1    4.08942228E+02  # mu
   2    6.09827894E+00  # tan[beta](Q)
   3    2.43166694E+02  # v(Q)
   4    9.58260161E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -3.83638164E-03   # Re[R_st(1,1)]
   1  2     9.99992641E-01   # Re[R_st(1,2)]
   2  1    -9.99992641E-01   # Re[R_st(2,1)]
   2  2    -3.83638164E-03   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     7.73986551E-04   # Re[R_sb(1,1)]
   1  2     9.99999700E-01   # Re[R_sb(1,2)]
   2  1    -9.99999700E-01   # Re[R_sb(2,1)]
   2  2     7.73986551E-04   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.27935513E-02   # Re[R_sta(1,1)]
   1  2     9.99918159E-01   # Re[R_sta(1,2)]
   2  1    -9.99918159E-01   # Re[R_sta(2,1)]
   2  2     1.27935513E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.93329305E-01   # Re[N(1,1)]
   1  2    -2.10869990E-03   # Re[N(1,2)]
   1  3     1.11495730E-01   # Re[N(1,3)]
   1  4    -2.93453267E-02   # Re[N(1,4)]
   2  1    -9.97079614E-02   # Re[N(2,1)]
   2  2    -4.69258710E-02   # Re[N(2,2)]
   2  3     7.02322702E-01   # Re[N(2,3)]
   2  4    -7.03277404E-01   # Re[N(2,4)]
   3  1     5.79082659E-02   # Re[N(3,1)]
   3  2    -2.11842183E-02   # Re[N(3,2)]
   3  3    -7.02833995E-01   # Re[N(3,3)]
   3  4    -7.08676398E-01   # Re[N(3,4)]
   4  1     1.35931405E-03   # Re[N(4,1)]
   4  2    -9.98671490E-01   # Re[N(4,2)]
   4  3    -1.83275752E-02   # Re[N(4,3)]
   4  4     4.81404958E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.58924578E-02   # Re[U(1,1)]
   1  2     9.99664734E-01   # Re[U(1,2)]
   2  1     9.99664734E-01   # Re[U(2,1)]
   2  2     2.58924578E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.80859708E-02   # Re[V(1,1)]
   1  2     9.97679458E-01   # Re[V(1,2)]
   2  1     9.97679458E-01   # Re[V(2,1)]
   2  2     6.80859708E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02883800E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.86747453E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     9.90867260E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     3.34214125E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.36591698E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.01962874E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.23016015E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01343811E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     4.31474348E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.04767669E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02939876E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.86638247E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     9.93503819E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     3.36927706E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     1.36594513E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.01946913E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.24021804E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.01337632E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     4.31465488E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.04755256E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.18892489E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.56585419E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.71216236E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.06741867E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.53628710E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.69799133E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.37373160E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.97358631E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     6.06440536E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.92264350E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.99607783E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.89648799E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.01279656E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.36594187E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.16329351E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.74007154E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02190614E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.98320662E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02306303E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.36596999E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.16310538E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.73989209E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02184412E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.00365608E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02293955E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.37390076E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     9.11038138E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     8.68960434E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.00446397E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     8.73433188E-03    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.98833637E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.99629451E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.10975513E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.11445188E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.88753388E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.22704919E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.28460163E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.67430328E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.33687221E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.97058866E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.99631038E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.10975358E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.12213782E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.88750306E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.22709517E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.28460057E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.67425464E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.01254207E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.33686272E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.97053110E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     4.39906012E+00   # ~b_1
#    BR                NDA      ID1      ID2
     5.67917425E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.11153599E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.07529122E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.13297988E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     1.68272184E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.31295643E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.21247830E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.23681909E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.00940273E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     6.17190413E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.04965369E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     6.20825851E-02    2     1000021         5   # BR(~b_2 -> ~g b)
     4.05637663E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     5.16769095E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.29294954E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.31104356E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.45409188E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.56493916E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.22690124E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.17819333E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.97333291E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.66775040E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     6.58952655E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.33155057E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.97024559E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.16776635E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.29288791E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.34658339E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.49020641E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.56480156E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.22694692E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.17818262E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.00290191E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.66770342E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     6.60200567E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.33154093E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.97018793E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     9.05276835E+01   # ~t_1
#    BR                NDA      ID1      ID2
     5.35525235E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.34250653E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.36258428E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.37833315E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.75495486E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.05075497E-04    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.67924877E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.61883002E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     3.07705186E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     3.12088418E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.02407316E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     7.73094590E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.01728039E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.70141298E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     2.03423209E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.67034598E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.97443593E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.98158349E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.84165032E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.80299318E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.04182035E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.43484662E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.44197974E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.42722335E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.42880655E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.15140704E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     8.75402485E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     6.33021056E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.51220543E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     3.13762344E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     6.86232395E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     3.31514862E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     7.11704853E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.88253751E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.92189792E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.28504229E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.28504229E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.81183918E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.90941147E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.88122706E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.01205116E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.85613324E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.62669287E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     7.99587467E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.99587467E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     8.22428301E+01   # ~g
#    BR                NDA      ID1      ID2
     4.95397253E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.95397253E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.00674032E-03    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.00674032E-03    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     8.58163540E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     8.74439405E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.32345384E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.41405986E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     9.13421113E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     9.13421113E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     4.73974863E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     4.73974863E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.08897623E-03   # Gamma(h0)
     2.31795223E-03   2        22        22   # BR(h0 -> photon photon)
     9.22631636E-04   2        22        23   # BR(h0 -> photon Z)
     1.32601546E-02   2        23        23   # BR(h0 -> Z Z)
     1.26291199E-01   2       -24        24   # BR(h0 -> W W)
     7.68198523E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.59295427E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.48780145E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.17211683E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.60486522E-07   2        -2         2   # BR(h0 -> Up up)
     3.11409549E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.54315390E-07   2        -1         1   # BR(h0 -> Down down)
     2.36649857E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.29140165E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     4.78996724E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     2.63504472E+01   # Gamma(HH)
     2.92174450E-07   2        22        22   # BR(HH -> photon photon)
     1.99922919E-07   2        22        23   # BR(HH -> photon Z)
     2.67670741E-05   2        23        23   # BR(HH -> Z Z)
     7.75740115E-06   2       -24        24   # BR(HH -> W W)
     2.32608346E-05   2        21        21   # BR(HH -> gluon gluon)
     7.02853880E-10   2       -11        11   # BR(HH -> Electron electron)
     3.12946641E-05   2       -13        13   # BR(HH -> Muon muon)
     9.03832650E-03   2       -15        15   # BR(HH -> Tau tau)
     6.84186391E-12   2        -2         2   # BR(HH -> Up up)
     1.32727232E-06   2        -4         4   # BR(HH -> Charm charm)
     9.78036144E-02   2        -6         6   # BR(HH -> Top top)
     5.13791380E-08   2        -1         1   # BR(HH -> Down down)
     1.85841315E-05   2        -3         3   # BR(HH -> Strange strange)
     4.82738643E-02   2        -5         5   # BR(HH -> Bottom bottom)
     4.03747028E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.35384977E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.35384977E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.81283315E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.53259033E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     8.38051623E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.10166965E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.20309955E-08   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.44943744E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     6.02561605E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.53944195E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.73531353E-01   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.49755546E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.61382352E+01   # Gamma(A0)
     4.81205255E-07   2        22        22   # BR(A0 -> photon photon)
     5.01651772E-07   2        22        23   # BR(A0 -> photon Z)
     4.50682054E-05   2        21        21   # BR(A0 -> gluon gluon)
     6.88519624E-10   2       -11        11   # BR(A0 -> Electron electron)
     3.06564378E-05   2       -13        13   # BR(A0 -> Muon muon)
     8.85401072E-03   2       -15        15   # BR(A0 -> Tau tau)
     6.42338004E-12   2        -2         2   # BR(A0 -> Up up)
     1.24604587E-06   2        -4         4   # BR(A0 -> Charm charm)
     9.29716729E-02   2        -6         6   # BR(A0 -> Top top)
     5.03311551E-08   2        -1         1   # BR(A0 -> Down down)
     1.82050417E-05   2        -3         3   # BR(A0 -> Strange strange)
     4.72890689E-02   2        -5         5   # BR(A0 -> Bottom bottom)
     5.55614387E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.36333553E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.36333553E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     3.34013533E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     8.64290770E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.39056284E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.21559027E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.44717333E-08   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     7.78896025E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.74210997E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     8.85501155E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     6.07550184E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     4.49205767E-05   2        23        25   # BR(A0 -> Z h0)
     8.70483488E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.68872368E+01   # Gamma(Hp)
     7.84834286E-10   2       -11        12   # BR(Hp -> Electron nu_e)
     3.35541170E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.49101273E-03   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.98703109E-08   2        -1         2   # BR(Hp -> Down up)
     8.41991415E-07   2        -3         2   # BR(Hp -> Strange up)
     5.37164162E-07   2        -5         2   # BR(Hp -> Bottom up)
     6.79497799E-08   2        -1         4   # BR(Hp -> Down charm)
     1.93722440E-05   2        -3         4   # BR(Hp -> Strange charm)
     7.52243795E-05   2        -5         4   # BR(Hp -> Bottom charm)
     7.18213049E-06   2        -1         6   # BR(Hp -> Down top)
     1.56620864E-04   2        -3         6   # BR(Hp -> Strange top)
     1.61022407E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.30103246E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.24133631E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     2.35468016E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.30913278E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.67142380E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.29570774E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.31698795E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     4.38760059E-05   2        24        25   # BR(Hp -> W h0)
     7.98597488E-11   2        24        35   # BR(Hp -> W HH)
     7.08429695E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.09765440E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.72792406E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.71890060E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00242638E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.44632900E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.68896673E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.09765440E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.72792406E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.71890060E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99942717E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.72828957E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99942717E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.72828957E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27499647E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.03294821E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.76900781E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.72828957E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99942717E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.25872365E-04   # BR(b -> s gamma)
    2    1.58962916E-06   # BR(b -> s mu+ mu-)
    3    3.52529109E-05   # BR(b -> s nu nu)
    4    2.56062159E-15   # BR(Bd -> e+ e-)
    5    1.09386847E-10   # BR(Bd -> mu+ mu-)
    6    2.28990666E-08   # BR(Bd -> tau+ tau-)
    7    8.62772638E-14   # BR(Bs -> e+ e-)
    8    3.68576196E-09   # BR(Bs -> mu+ mu-)
    9    7.81782369E-07   # BR(Bs -> tau+ tau-)
   10    9.67826038E-05   # BR(B_u -> tau nu)
   11    9.99727244E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42547265E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93855190E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15959069E-03   # epsilon_K
   17    2.28168141E-15   # Delta(M_K)
   18    2.47986330E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29020709E-11   # BR(K^+ -> pi^+ nu nu)
   20    5.37077452E-17   # Delta(g-2)_electron/2
   21    2.29617400E-12   # Delta(g-2)_muon/2
   22    6.49493190E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -2.01162497E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.94478542E-01   # C7
     0305 4322   00   2    -1.21404795E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.03207132E-01   # C8
     0305 6321   00   2    -1.61686370E-04   # C8'
 03051111 4133   00   0     1.61997909E+00   # C9 e+e-
 03051111 4133   00   2     1.62042450E+00   # C9 e+e-
 03051111 4233   00   2     6.36966189E-06   # C9' e+e-
 03051111 4137   00   0    -4.44267119E+00   # C10 e+e-
 03051111 4137   00   2    -4.44103053E+00   # C10 e+e-
 03051111 4237   00   2    -4.72695680E-05   # C10' e+e-
 03051313 4133   00   0     1.61997909E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62042450E+00   # C9 mu+mu-
 03051313 4233   00   2     6.36966165E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.44267119E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44103053E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.72695680E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50499555E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.02363060E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50499555E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.02363061E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50499555E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.02363170E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822131E-07   # C7
     0305 4422   00   2     1.19525930E-06   # C7
     0305 4322   00   2     7.38713343E-08   # C7'
     0305 6421   00   0     3.30482171E-07   # C8
     0305 6421   00   2     3.27222086E-08   # C8
     0305 6321   00   2     1.52209219E-08   # C8'
 03051111 4133   00   2     1.49399171E-07   # C9 e+e-
 03051111 4233   00   2     1.31644513E-07   # C9' e+e-
 03051111 4137   00   2    -1.57282836E-07   # C10 e+e-
 03051111 4237   00   2    -9.77943160E-07   # C10' e+e-
 03051313 4133   00   2     1.49399133E-07   # C9 mu+mu-
 03051313 4233   00   2     1.31644512E-07   # C9' mu+mu-
 03051313 4137   00   2    -1.57282798E-07   # C10 mu+mu-
 03051313 4237   00   2    -9.77943163E-07   # C10' mu+mu-
 03051212 4137   00   2     4.70733305E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.11776951E-07   # C11' nu_1 nu_1
 03051414 4137   00   2     4.70733321E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.11776951E-07   # C11' nu_2 nu_2
 03051616 4137   00   2     4.70737931E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.11776951E-07   # C11' nu_3 nu_3
