#############################################
# MadGraph control script for the creation  #
# of dark pion samples                      #
#                                           #
# Setup based on a script by Bryan Ostdiek  #
# * bostdiek@g.harvard.edu                  #
#                                           #
# Adapted for ATLAS by Jochen Jens Heinrich #
# * jochen.jens.heinrich@cern.ch            #
#############################################

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import os
import sys
from subprocess import call
from shutil import copy

mode = 0

# Check model is known
if dmmodel not in ['Gaugephobic_SU2L', 'Gaugephilic_SU2L', 'Gaugephobic_SU2R']:
        raise RuntimeError("The specified dark meson model is not defined in the control file.")

# Check beam energy is specified
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# We need to run in two stages.
# During stage 1 the total decay widths are calculated and the parameter cards are built
# In the second stage the decay to dark pions is generated

print "//////////////////////////////"
print "// Start processing stage 1 //"
print "//////////////////////////////"

if dmmodel == 'Gaugephobic_SU2R':
    rho_def="define rho = rho0"
    width_exp="compute_widths rho0 --body_decay=2 --min_br=0.001"
else:
    rho_def="define rho = rho+ rho- rho0"
    width_exp="compute_widths rho0 rho+ --body_decay=2 --min_br=0.001"

# Write process
full_process = """
import model """+dmmodel+"""
define dall = u d c s t b u~ d~ c~ s~ t~ b~ e+ e- mu+ mu- ta+ ta- vl vl~ w+ w- z h dp+ dp- dp0
"""+rho_def+"""
generate p p > rho, rho > dall dall QED<=4 NP<=4
output -f
launch
set lhc 13
set pionmass """+str(dpionmass)+"""
set fermioneta """+str(fermioneta)+"""
set xi """+str((125.0*125.0)/(dpionmass*dpionmass))+"""
update dependent
compute_widths dp+ dp0 --body_decay=3 --min_br=0.001
"""+width_exp+"""
"""

print full_process

process_dir = new_process(full_process)

# Copy parameter card into main directory
copy(process_dir+'/Cards/param_card.dat', 'param_card.dat')

# Set parameters
settings = { 'lhe_version':'3.0',
             'cut_decays':'F',
             #'pdlabel':"'nn23lo1'",
             #'use_syst':"False",
             #'parton_shower':'PYTHIA8',
             'nevents':1.5*runArgs.maxEvents
}

# Modify all cards
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

print_cards()

print "//////////////////////////////"
print "// Start processing stage 2 //"
print "//////////////////////////////"

# Write process for stage 2
full_process_stage2 = """
import model """+dmmodel+"""
define pi = dp+ dp- dp0
generate p p > pi pi NP=99 QED=99
output -f
"""


process_dir_2 = new_process(full_process_stage2)

modify_run_card(process_dir=process_dir_2,runArgs=runArgs,settings=settings)
copy('param_card.dat', process_dir_2+'/Cards/param_card.dat')

print_cards()

# Generation of events
generate(process_dir=process_dir_2,runArgs=runArgs)

arrange_output(process_dir=process_dir_2,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower 
evgenConfig.description = 'MadGraph_DarkMesons'
evgenConfig.keywords+=['exotic','BSM']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.contact  = [ 'jochen.jens.heinrich@cern.ch' ]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
