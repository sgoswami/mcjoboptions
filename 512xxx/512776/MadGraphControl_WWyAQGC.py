import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# Infer EFT parameter and decomposition from DSID
dsid_to_run_params = {
        999999: ["M0", 2],
        999998: ["M0", 1],
        999997: ["M1", 2],
        999996: ["M1", 1],
        999995: ["M2", 2],
        999994: ["M2", 1],
        999993: ["M3", 2],
        999992: ["M3", 1],
        999991: ["M4", 2],
        999990: ["M4", 1],
        999989: ["M5", 2],
        999988: ["M5", 1],
        999987: ["M7", 2],
        999986: ["M7", 1],
        999985: ["T0", 2],
        999984: ["T0", 1],
        999983: ["T1", 2],
        999982: ["T1", 1],
        999981: ["T2", 2],
        999980: ["T2", 1],
        999979: ["T5", 2],
        999978: ["T5", 1],
        999977: ["T6", 2],
        999976: ["T6", 1],
        999975: ["T7", 2],
        999974: ["T7", 1],
        999973: ["M3", 0] # ``SM" sample
}

evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["SM", "AQGC"]
evgenConfig.contact = ["Daniel Wilbern <daniel.wilbern@cern.ch>"]
evgenConfig.description = "MadGraph+Pythia8 for WWy AQGC with param {}^2=={}".format(EFT_param, EFT_decomposition)

# Process card
process = "import model SM_Ltotal_Ind5v2020v2_UFO\n"
process += "generate p p > w+ w- a "
#for eft_param in set([x[0] for x in dsid_to_run_params.values()]):
#process += "{}^2=={} ".format(eft_param, EFT_decomposition if eft_param == EFT_param else 0)
process += "{}^2=={} ".format(EFT_param, EFT_decomposition)
process += "\noutput -f"
process_dir = new_process(process)

# Run card
evgenConfig.nEventsPerJob = 10000
if EFT_decomposition == 0:
    evtMultiplier = 70
elif EFT_decomposition == 1:
    evtMultiplier = 40
elif EFT_decomposition == 2:
    evtMultiplier = 25
run_settings = {
    "nevents": evtMultiplier*runArgs.maxEvents*1.1 if runArgs.maxEvents > 0 else evtMultiplier*1.1*evgenConfig.nEventsPerJob,
    "dynamical_scale_choice": 3}
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=run_settings)

# Param card
param_settings = {"ANOINPUTS": {
    "1": "1e-20 # S0",
    "2": "1e-20 # S1",
    "3": "1e-20 # S2",
    "4": "1e-20 # M0",
    "5": "1e-20 # M1",
    "6": "1e-20 # M2",
    "7": "1e-20 # M3",
    "8": "1e-20 # M4",
    "9": "1e-20 # M5",
    "10": "1e-20 # M6",
    "11": "1e-20 # M7",
    "12": "1e-20 # T0",
    "13": "1e-20 # T1",
    "14": "1e-20 # T2",
    "15": "1e-20 # T3",
    "16": "1e-20 # T4",
    "17": "1e-20 # T5",
    "18": "1e-20 # T6",
    "19": "1e-20 # T7",
    "20": "1e-20 # T8",
    "21": "1e-20 # T9"}
    }

if EFT_decomposition != 0:
    for i, j in param_settings["ANOINPUTS"].items():
        if EFT_param in j:
            param_settings["ANOINPUTS"][i] = param_settings["ANOINPUTS"][i].replace("1e-20", "1e-12")
    modify_param_card(process_dir=process_dir, params=param_settings)

# Generate LHE file OTF
generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=False)

# Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
check_reset_proc_number(opts) # Force serial processing
include("Pythia8_i/Pythia8_MadGraph.py")

# Generator filters
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
filtSeq += PhotonFilter()
filtSeq.PhotonFilter.Ptcut = 15000.
filtSeq.PhotonFilter.Etacut = 2.5
filtSeq.PhotonFilter.NPhotons = 1

from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
filtSeq += ElectronFilter()
filtSeq.ElectronFilter.Ptcut = 15000.
filtSeq.ElectronFilter.Etacut = 2.5

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
filtSeq += MuonFilter()
filtSeq.MuonFilter.Ptcut = 15000.
filtSeq.MuonFilter.Etacut = 2.5

