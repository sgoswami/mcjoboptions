from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *
import re
import os
import subprocess
import math

from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

nevents = runArgs.maxEvents*1.5 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob

print runArgs.maxEvents
print nevents

#THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
#job_option_name = [ f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]
job_option_name = get_physics_short()
print job_option_name

quark_flavour = job_option_name.split('_U1')[-1][0]
quark_flavours = ['c','c~', 'b', 'b~','s','s~', 't']
quark_flavour_index = quark_flavours.index(quark_flavour) + 1

if quark_flavour not in quark_flavours and not  ("tautauLQ" in job_option_name or "nunuLQ" in job_option_name) :
    print("Cannot determine quark flavour from job option name: {:s}.".format(job_option_name))

# Get Mass from jo file
matches = re.search("M([0-9]+).*", job_option_name)
if matches is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    lqmass = float(matches.group(1))
print lqmass


    
# Get Kappa from jo file : For Yangs-Mills : kappa=0 ; Minimal coupling kappa=1
matches_kappa = re.search("K([0-1]+).*", job_option_name)
if matches_kappa is None:
    raise RuntimeError("Cannot find kappa string in job option name: {:s}.".format(job_option_name))
else:
    lkappa = float(matches_kappa.group(1))


#Get overall coupling strength gu from jo file
matches_gu = re.search("GU([0-9]_[0-9]+).*", job_option_name)
if matches_gu is None:
    print("Cannot find gU string in job option name: {:s} -- using default value gU =3.0".format(job_option_name))
    lgu=3.0
else:
    lgu = (math.sqrt(2))*float(matches_gu.group(1).replace("_", "."))



#Get betaL32 from jo file
matches_betaL32 = re.search("32L([0-9]_[0-9]+).*", job_option_name)
if matches_betaL32 is None:
    print("Cannot find beta 32 string in job option name: {:s}.".format(job_option_name))
    betaL32=0.0
else:
    betaL32 = float(matches_lambda33.group(1).replace("_", "."))

#Get betaL23 from jo file
matches_betaL23 = re.search("23L([0-9]_[0-9]+).*", job_option_name)
if matches_betaL23 is None:
    print("Cannot find beta 23 string in job option name: {:s}.".format(job_option_name))
    betaL23=0.0
else:
    betaL23 = float(matches_betaL23.group(1).replace("_", "."))

#Get betaL33 from jo file
matches_betaL33 = re.search("33L([0-9]_[0-9]+).*", job_option_name)
if matches_betaL33 is None:
    print("Cannot find betaL 33 string in job option name: {:s}.".format(job_option_name))
    betaL33=0.0
else:
    betaL33= float(matches_betaL33.group(1).replace("_", "."))
    
#Get betaR33 from jo file
matches_betaR33 = re.search("33R([0-9]_[0-9]+).*", job_option_name)
if matches_betaR33 is None:
    print("Cannot find betaR 33 string in job option name: {:s}.".format(job_option_name))
    betaR33=0.0
else:
    betaR33= float(matches_betaR33.group(1).replace("_", "."))


process_1=""
process_2=""

process_def="""
    set group_subprocesses Auto
    set ignore_six_quark_processes False
    set loop_color_flows False
    set gauge unitary
    set complex_mass_scheme False
    set max_npoint_for_channel 0
    import model ./vector_LQ_UFO
    define p = g u c d s b u~ c~ d~ s~ b~
    generate p p > {finalState} {npOrder}
    add process p p > {finalStateCC} {npOrder}
    output -f"""

if "LQnu" in job_option_name:
    # Setup for tau + t/c nu 
    process_1 = "ta- vt~ {:s}".format(quark_flavour)
    process_2 = "ta+ vt {:s}~".format(quark_flavour)
   
 
elif "LQtau" in job_option_name:
    # Setup for nu + b/s tau 
    process_1 = "ta+ vt {:s}".format(quark_flavour)
    process_2 = "ta- vt~ {:s}~".format(quark_flavour)


elif "LQ2tau" in job_option_name:
    # Setup for tau + b/s tau 
    process_1 = "ta- ta+ {:s}".format(quark_flavour)
    process_2 = "ta+ ta- {:s}~".format(quark_flavour)
    
    
elif "tautauLQ" in job_option_name:
    # Setup for non res and remove  cc process due to symmetry
    process_1 = "ta- ta+"
    process_def=process_def.replace("add process p p > {finalStateCC} {npOrder}", "")
    
    
elif "nunuLQ" in job_option_name:
    # Setup for non res  and remove  cc process due to symmetry
    process_1 = "nu nu~"
    process_def=process_def.replace("add process p p > {finalStateCC} {npOrder}", "")


process_def_run= process_def.replace("{finalState}", process_1).replace("{finalStateCC}",process_2)

    

process_def_SM            = process_def_run.replace("{npOrder}", "NP==0")
process_def_allNonSM      = process_def_run.replace("{npOrder}", "NP^2>0")
process_def_interfer      = process_def_run.replace("{npOrder}", "NP^2==2")
process_def_BSM           = process_def_run.replace("{npOrder}", "NP==2")
process_def_all           = process_def_run.replace("{npOrder}", "")
# Get beam energy
beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



process_dir = new_process(process_def_all)

 
extras = {
    'lhe_version'   : '3.0',
    'sde_strategy' : 2,
    'maxjetflavor' : 5,
    #'ptj1min' : 100,
    #'mmll' : 120,
    'cut_decays' : 'F',
    'nevents' : int(nevents)
}


#----------------------------------------------------------------------------------------
# Setting  model parameters
#----------------------------------------------------------------------------------------
#---Define model parameter: - for LQ production no couplings to Z_prime and heavy gluon
#                           - beta23 and beta33 taken from job_option_name (encoded cf table)
#                            

vLQ_par={
    'gU' :  '{:e}' .format(lgu) ,
    'betaL33': '{:e}'  .format(betaL33) ,
    'betaRd33' : '{:e}'.format(betaR33),
    'betaL23' : '{:e}' .format(betaL23),
    'betaL32' : '{:e}' .format(betaL32),
    'kappaU' : '{:e}' .format(lkappa),
    
    }
zP_par={
    'gZp' :  '{:e}' .format(0.0) ,
    'zetaq33': '{:e}' .format(0.0) ,
    'zetal33' : '{:e}'.format(0.0),
    'zetaRu33' : '{:e}' .format(0.0),
    'zetaRd33' : '{:e}' .format(0.0),
    'zetaRe33' : '{:e}' .format(0.0),
    'zetaqll' : '{:e}' .format(0.0),
    'zetal22' : '{:e}' .format(0.0),
    'zetal23' : '{:e}' .format(0.0),
    'zetaRull' : '{:e}' .format(0.0),
    'zetaRdll' : '{:e}' .format(0.0),
    'zetaRe22' : '{:e}' .format(0.0),
    
    }
gP_par={
    'gGp' :  '{:e}' .format(0.0) ,
    'kappaq33': '{:e}'  .format(0.0) ,
    'kappaRu33' : '{:e}'.format(0.0),
    'kappaRd33' : '{:e}' .format(0.0),
    'kappaqll': '{:e}'  .format(0.0) ,
    'kappaRull' : '{:e}'.format(0.0),
    'kappaRdll' : '{:e}' .format(0.0),
    'kappaG1' : '{:e}'.format(0.0),
    'kappaG2' : '{:e}' .format(0.0),
    
    }

vLQ_Mass={
    '9000006' : '{:e}' .format(1e9),
    '9000006' : '{:e}' .format(1e9),
    '9000007' : '{:e}' .format(lqmass),
    }
vLQ_Width={
    '9000007' : 'Auto',
    }
 
params = {}

params['NPLQCOUP']= vLQ_par
params['NPGPCOUP']= gP_par
params['NPZPCOUP']= zP_par
params['MASS'] = vLQ_Mass
params['DECAY'] = vLQ_Width

modify_param_card(process_dir=process_dir, params=params)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

pdgfile = open("pdgid_extras.txt" , "w+")
pdgfile.write("""9000007
    -9000007
""")
pdgfile.close()


#reweight_card=process_dir+'/Cards/reweight_card.dat'
#reweight_card_f = open(reweight_card,'w')
#
#b33=0.2
#while b33 < 1.2:
#    b23=0.2
#    while b23 < 1.2:
#        reweight_card_f.write("""
#            launch --rwgt_name=23L{:s}_33L{:s}
#            set NPLQCOUP  4 {:e}
#            set NPLQCOUP  2 {:e}""".format(str(b23).replace(".", "_"),str(b33).replace(".", "_" ),b23,b33))
#        b23=b23+0.2
#    b33=b33+0.2
#reweight_card_f.close()


print_cards()

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs, lhe_version=3, saveProcDir=True)
#----------------------------------------------------------------------------------------
# EVGEN configuration
#----------------------------------------------------------------------------------------

evgenConfig.description = 'Vector LO single production of U1 to, : {0:s} , mLQ={1:d}'.format( quark_flavour, int(lqmass))
evgenConfig.keywords += ['BSM', 'exotic', 'leptoquark']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp>vlqtau'
evgenConfig.contact = ["Patrick Bauer <patrick.bauer@cern.ch>"]
                                                                                                                                               
check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
