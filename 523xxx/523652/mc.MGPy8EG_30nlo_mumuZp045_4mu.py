from MadGraphControl.MadGraphUtils import *

#
## muon-tau model with same coupling constants for left-hand leptons and neutrinos,
## and right-hand leptons.
## No cut is applied to lepton at parton level
## 3 muons are required to have their pt more than 2GeV and eta within 3.0 after event generation.
##  
## 2 inputs parameters, mass of Zp (zpm), 1 coupling gzpmul
##
## Example 1: 15.0GeV Zp with coupling to muon and tau pp --> munu+Zp --> 3munu
##
## zpm=15.0
## gzpmul=3.000000e-01
##
gzpmul=0.023
zpm=45.0
print ("Z' mass zpm ",zpm, " GeV; Coupling constant gzpmul ",gzpmul )
gzpel=0.000000e+00
gzper=0.000000e+00
gzpmur=gzpmul
gzptal=gzpmul
gzptar=gzpmul
maxjetflavor=5
## safe factor applied to nevents, to account for the filter efficiency
# if zpm <= 4.0:
#   safefactor=3.90
# elif zpm <= 6.0:
#   safefactor=2.81
# elif zpm <= 8.0:
#   safefactor=2.43
# elif zpm <= 10.0:
#   safefactor=2.23
# elif zpm <= 12.0:
#   safefactor=2.09
# elif zpm <= 14.0:
#   safefactor=2.01
# elif zpm <= 16.0:
#   safefactor=1.95
# elif zpm <= 18.0:
#   safefactor=1.91
# elif zpm <= 21.0:
#   safefactor=1.89
# elif zpm <= 25.0:
#   safefactor=1.87
# elif zpm <= 29.0:
#   safefactor=1.86
# elif zpm <= 33.0:
#   safefactor=1.85
# elif zpm <= 37.0:
#   safefactor=1.84
# elif zpm <= 40.5:
#   safefactor=1.84
# elif zpm <= 43.5:
#   safefactor=1.83
# elif zpm <= 46.5:
#   safefactor=1.83
# elif zpm <= 49.5:
#   safefactor=1.82
# elif zpm <= 52.5:
#   safefactor=1.81
# elif zpm <= 55.5:
#   safefactor=1.79
# elif zpm <= 58.5:
#   safefactor=1.77
# elif zpm <= 61.5:
#   safefactor=1.73
# elif zpm <= 64.5:
#   safefactor=1.68
# elif zpm <= 67.5:
#   safefactor=1.62
# elif zpm <= 70.5:
#   safefactor=1.57
# elif zpm <= 73.5:
#   safefactor=1.53
# else:
#   safefactor=1.50
safefactor=1.1

# create the process string to be copied to proc_card_mg5.dat
process="""
import model Leptophilic_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
set group_subprocesses Auto
set ignore_six_quark_processes False
set gauge unitary
set complex_mass_scheme False
generate p p > mu+ mu- Zp, Zp > mu+ mu-
output -f
"""
print ('process string: ',process)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# MG5 run Card
#---------------------------------------------------------------------------
#
# ptj default value 20GeV, using ktdurham just in case its lower than 20GeV 
#
nevents=int(runArgs.maxEvents*safefactor) 
extras = {
    'pdlabel'      : "'lhapdf'",
    'lhaid'        : '260000',
    'lhe_version'  : '3.0',
    'maxjetflavor' : maxjetflavor,
    'asrwgtflavor' : maxjetflavor,
    'nevents'      : nevents,
    'ptj'          : 0.0,
    'ptb'          : 0.0,
    'pta'          : 0.0,
    'ptl'          : 0.0,
    'etaj'         : -1.0,
    'etab'         : -1.0,
    'etaa'         : -1.0,
    'etal'         : -1.0,
    'drjj'         : 0.0,
    'drbb'         : 0.0,
    'drll'         : 0.0,
    'draa'         : 0.0,
    'drbj'         : 0.0,
    'draj'         : 0.0,
    'drjl'         : 0.0,
    'drab'         : 0.0,
    'drbl'         : 0.0,
    'dral'         : 0.0,
    'cut_decays'   : 'F',
    'use_syst'     : 'T',
    'sys_scalefact' :'1 0.5 2',
    'sys_alpsfact' : 'None',
    'sys_matchscale' : 'None',
    # 'sys_pdf'      : 'CT14nlo && MMHT2014nlo68clas118' #'NNPDF23_lo_as_0130_qed'
    'sys_pdf'       : 'NNPDF30_nlo_as_0118'
}

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 param Card
#---------------------------------------------------------------------------
## params is a dictionary of dictionaries (each dictionary is aseparate block)
## parameters for newcoup
ncoups={}
ncoups['GZpeL']=gzpel
ncoups['GZpeR']=gzper
ncoups['GZpmuL']=gzpmul
ncoups['GZpmuR']=gzpmur
ncoups['GZptaL']=gzptal
ncoups['GZptaR']=gzptar
## mass parameters
masses={}
masses['999888']=zpm
## decays
decays={}
decays['999888'] = """DECAY  999888   Auto  
#  BR             NDA  ID1    ID2   ...
   3.333332e-01   2    13  -13 # without consider phase-space difference due mass diff.
   1.667668e-01   2    14  -14 # without consider phase-space difference due mass diff.
   3.333332e-01   2    15  -15 # without consider phase-space difference due mass diff.
   1.667888e-01   2    16  -16 # without consider phase-space difference due mass diff.
"""

params={}
params['mass'] = masses
params['ZPINPUTS']=ncoups
params['decay'] = decays

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=params)

print_cards()

runName='run_01'

# and the generation
generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#
#### Pythia8 Showering with A14_NNPDF23LO 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_MadGraph.py")                                                                                                                                                             
evgenConfig.nEventsPerJob = 10000
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "Leptophilic Zprime, https://arxiv.org/pdf/1411.7394.pdf"
evgenConfig.keywords = ["BSM","Zprime"]
evgenConfig.process = "pp --> mumu+Zp --> 4mu around Z peak"
evgenConfig.contact = ["Tiesheng Dai <tiesheng.dai@cern.ch>"]
# evgenConfig.inputfilecheck = runName
# evgenConfig.inputfilecheck = ""

genSeq.Pythia8.Commands += [
                            '23:onMode = off'
                           ]

#
### 3 Muon filter
#

