from MadGraphControl.MadGraphUtils import *
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')

#Strip out pdf variations
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING = {
    'central_pdf': 303000,
    'pdf_variations':None,
    'alternative_pdfs':None,
    'scale_variations':[0.5,1.,2.],
}


#mc.MGPy8EG_A14N23LO_SqSq_Hplus_XXX_YYY.py
#Read filename of jobOptions to obtain: productionmode, ewkino mass.
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
tokens = get_physics_short().split('_')
try:
    _  = float(tokens[-1])
    event_filter = ''
except ValueError:
    event_filter = tokens[-1]
    tokens = tokens[:-1]

productionmode = tokens[2] #SqSq / C1C1
decay          = tokens[3] #Hplus

if productionmode == 'SqSq':
    squark_mass    = float(tokens[4])
    Hplus_mass     = float(tokens[5])
    if len(tokens)>6:
        chargino_mass  = float(tokens[6])
    else:
        chargino_mass  = (squark_mass+Hplus_mass)/2
elif productionmode == 'C1C1':
    squark_mass    = 4.5e9
    chargino_mass  = float(tokens[4])
    Hplus_mass     = float(tokens[5])
else:
    print(f"Production mode not recognized: {productionmode}")
    raise RunTimeError

neutralino_mass= 0

squark_decay = '''DECAY   1000001  1.0
#       BR      Nbody    ID1    ID2
        1.0     2        2      -1000024 #Sq -> q C1
'''
chargino_decay = '''DECAY   1000024  1.0
#       BR      Nbody    ID1    ID2
        1.0     2        37     1000022 #C1 -> H+ N1
'''
Hplus_decay = '''DECAY   37  1.0
#       BR      Nbody    ID1    ID2
        0.5     2        4      -5   #H+ -> b~ c
        0.5     2        22     -15  #H+ -> y tau+
'''

masses['1000001'] = squark_mass #Sq
masses['1000024'] = chargino_mass #C1
masses['37']      = Hplus_mass #H+
masses['1000022'] = neutralino_mass #N1

decays['1000001'] = squark_decay
decays['1000024'] = chargino_decay
decays['37']      = Hplus_decay
#stable neutralino


# production
if productionmode == 'SqSq':
    process = '''
    define exclude = go ul ur dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
    generate    p p > dl dl~     / exclude @1
    add process p p > dl dl~ j   / exclude @2
    '''
elif productionmode == 'C1C1':
    process = '''
    define exclude = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
    generate    p p > x1- x1+     / exclude @1
    add process p p > x1- x1+ j   / exclude @2
    '''

if '1tau' in event_filter:
    evt_multiplier = 4.
    from GeneratorFilters.GeneratorFiltersConf import TauFilter
    filtSeq += TauFilter()
    filtSeq.TauFilter.UseNewOptions = True
    filtSeq.TauFilter.UseMaxNTaus = True
    filtSeq.TauFilter.Nhadtaus = 1
    filtSeq.TauFilter.MaxNhadtaus = 1
    filtSeq.TauFilter.MaxNleptaus = 0
    filtSeq.TauFilter.Ptcuthad_lead = 20*GeV


evgenConfig.contact = ["jmontejo@cern.ch"]
evgenConfig.keywords +=['SUSY', 'squark']
evgenConfig.description = f'Squark production, decay via charginos and charged higgs to (H+->bc or H+->tauy), exactly 1tau filter, m_Sq={squark_mass}, m_C1={chargino_mass}, m_Hplus={Hplus_mass}, m_N1={neutralino_mass}'

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
