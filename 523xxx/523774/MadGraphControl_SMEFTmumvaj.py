from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl.MadGraphUtils import add_reweighting,get_expected_reweight_names
import re,shutil,os,gzip

mgmodels='/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/'
#mgmodels='/afs/cern.ch/work/a/amendesj/private/IncWgammaXS/source/EFTjobOptions/'

# function to create reweight card for different scenarios
# needs process, required coupling order, mode (see below), and suffix for weight names
def reweight_smeft(p,order,coeffs,mode,suffix=''):
    reweight_text='change helicity False\n'
    ps=p.replace('\n',';').split(';')
    rwgt_ps=[]
    for p in ps:
        if 'generate' in p:
            rwgt_ps.append(p.replace('generate','change process').replace('EFTORDER',order))
        if 'add process' in p:
            rwgt_ps.append(p.replace('add process','change process').replace('EFTORDER',order)+' --add')
    reweight_text+='\n'.join(rwgt_ps)+'\n'
    #sm: set all to zero
    if mode==0:
        reweight_text+="launch --rwgt_name={0} --rwgt_info={0}\n".format(suffix)
        for cnot in coeffs:
            if not cnot==c:
                reweight_text+="set {0} 0\n".format(cnot)
    # one at a time non-zero
    if mode==1:
        for c in coeffs:
            reweight_text+="launch --rwgt_name={0}{1} --rwgt_info={0}{1}\nset {0} 1\n".format(c,suffix)
            for cnot in coeffs:
                if not cnot==c:
                    reweight_text+="set {0} 0\n".format(cnot)
    # all pairs non zero
    if mode==2:
        from itertools import combinations
        for c1,c2 in combinations(coeffs,2):
            reweight_text+="launch --rwgt_name={0}_{1}{2} --rwgt_info={0}_{1}{2}\nset {0} 1\nset {1} 1\n".format(c1,c2,suffix)
            for cnot in coeffs:
                if not cnot in [c1,c2]:
                    reweight_text+="set {0} 0\n".format(cnot)
    return reweight_text

# behaviour of job is steered by job option name
physics_short=get_physics_short()

# determine process from jo name (dict of possible processes given in main job option)
process=None
for p in processes:
    if p in physics_short.split('_'):
        process=definitions+processes[p]
        break
assert not process is None

# determine EFT parameter setting from jo name (e.g. cW_1_cHq3_-0p5 -> cW=1, cHq3=-0.5)
# assumption is that all EFT paramters start with 'c'!
params_re=r"_(?P<param>c[A-Za-z0-9]+)_(?P<value>[-+]?\d+p?\d*)"
eft_params_list=re.findall(params_re,physics_short)
eft_params={}
for e in eft_params_list:
    eft_params[e[0]]=e[1].replace('p','.')

# check for predefined settings with name smeftpars_xy
# these will be activated if xy in job option name
for v in physics_short.split('_'):
    if 'smeftpars_'+v in locals() and isinstance(locals()['smeftpars_'+v],dict):
        assert len(eft_params_list)==0
        eft_params=locals()['smeftpars_'+v]

# determine EFT order jo name
prop_corr=False
no_syst=False
assert 'EFTORDER' in process
actual_process=None
for e in physics_short.split('_'):
    if e.lower()=='sm':
        eft_order='NP==0'
        actual_process=process.replace('EFTORDER',eft_order)
        break
    elif e.lower()=='lin':
        eft_order='NP<=1 NP^2==1'
        if len(eft_params.keys())==1:
            eft_order=eft_order+" NP{}^2==1".format(eft_params.keys()[0])
        actual_process=process.replace('EFTORDER',eft_order)
        no_syst=True
        break
    elif e.lower()=='quad':
        eft_order='NP==1'
        if len(eft_params.keys())==1:
            eft_order=eft_order+" NP{}==1".format(eft_params.keys()[0])
        actual_process=process.replace('EFTORDER',eft_order)
        no_syst=True
        break
    elif e.lower()=='cross' or e.lower()=='x':
        assert len(eft_params.keys())==2
        eft_order='NP<=1 NP^2==2'
        eft_order=eft_order+" NP{}^2==1 NP{}^2==1".format(*(eft_params.keys()))
        actual_process=process.replace('EFTORDER',eft_order)
        no_syst=True
        break
    elif e.lower()=='full':
        eft_order='NP<=1'
        actual_process=process.replace('EFTORDER',eft_order)
        break
    elif e.lower()=='rwgtlin':
        eft_order='NP^2<=1'
        actual_process=process.replace('EFTORDER',eft_order)
        break
    elif e.lower()=='rwgtquad' or e.lower()=='rwgtcross' or e.lower()=='rwgtx':
        eft_order='NP<=1'
        actual_process=process.replace('EFTORDER',eft_order)
        break
    elif e.lower()=='linprop':
        eft_order='NP==0 NProp<=2 NPprop^2==2'
        actual_process=process.replace('EFTORDER',eft_order)
        prop_corr=True
        no_syst=True
        break
    elif e.lower()=='quadprop':
        eft_order='NP==0 NPprop==2'
        actual_process=process.replace('EFTORDER',eft_order)
        prop_corr=True
        no_syst=True
        break
assert actual_process is not None

# propagator corrections are implemented using unphysical dummy particle that we need to whitelist
if prop_corr:
    with open("pdgid_extras.txt","w") as f:
        for i in range(9000005,9000009):
            f.write(str(i)+'\n')
    testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'

# determine model from jo name
# four possible models: two input parameter schemes and with/without propagator corrections
model=None
for m in physics_short.split('_'):
    if m=="SMEFTWp" or m=="SMEFTW" and prop_corr:
        model="SMEFTsim_topU3l_MwScheme_PropCorr_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_massless.dat'
        break
    elif m=="SMEFTW":
        model="SMEFTsim_topU3l_MwScheme_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_massless.dat'
        break
    elif m=="SMEFTap" or m=="SMEFTa" and prop_corr:
        model="SMEFTsim_topU3l_alphaScheme_PropCorr_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_massless.dat'
        break
    elif m=="SMEFTa":
        model="SMEFTsim_topU3l_alphaScheme_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_massless.dat'
        break
    elif m=="SMEFTWtop":
        model="SMEFTsim_top_MwScheme_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_massless.dat'
        break
    elif m=="SMEFTWtopV":
        model="SMEFTsim_top_MwScheme_UFO"
        eft_parameter_block='SMEFTcpv'
        restriction='topCPV_massless'
        sm_card='param_card_topCPV_massless.dat'
        break
    elif m=="SMEFTWU35":
        model="SMEFTsim_U35_MwScheme_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_massless.dat'
        break
    elif m=="SMEFTWU35V":
        model="SMEFTsim_U35_MwScheme_UFO"
        eft_parameter_block='SMEFTcpv'
        restriction='CPV_massless'
        sm_card='param_card_CPV_massless.dat'
        break
assert not model is None

actual_process='import model {}-{}\n{}\noutput -f'.format(model,restriction,actual_process)
#actual_process='import model {}{}-{}\n{}\noutput -f'.format(mgmodels,model,restriction,actual_process)
#actual_process='import model {}\n{}\noutput -f'.format(model,actual_process)

# systematics are deactivated for interference or NP-only samples
# consistently using 4fs as model assumes massive b quarks
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260400, 
    'pdf_variations': None if no_syst else [260400,92000],
    'alternative_pdfs':None if no_syst else [266400,265400,13191,25510],
    'scale_variations':None if no_syst else [0.5,1.,2.],
}
from MadGraphControl.MadGraphUtils import *

process_dir = new_process(actual_process)

# setup reweighting
reweight_text=None
for r in physics_short.split('_'):
    if r=='rwgtsm' or r=='rwgtall' or r=='rwall' or r=='rwsm' :
        reweight_text=reweight_smeft(process,'NP==0',[],0,'sm')
        break
    elif r=='rwgtlin' or r=='rwlin' :
        reweight_text=reweight_smeft(process,'NP<=1 NP^2==1',relevant_coeffs,1,'_lin')
        break
    elif r=='rwgtquad' or r=='rwquad' or r=='rwqd':
        reweight_text=reweight_smeft(process,'NP==1',relevant_coeffs,1,'_quad')
        break
    elif r=='rwgtcross' or r=='rwgtx' or r=='rwx':
        reweight_text=reweight_smeft(process,'NP<=1 NP^2==2 '+' '.join(['NP{}^2<=1'.format(c) for c in relevant_coeffs]),relevant_coeffs,2,'_cross')
        break

if not reweight_text is None:
    with open(process_dir+'/Cards/reweight_card.dat','w') as f:
        f.write(reweight_text)
        f.close()

# fetch default param card (all EFT parameters zero)
shutil.copy('/'.join([mgmodels,model,sm_card]),process_dir+'/Cards/param_card.dat')
# set eft parameters
modify_param_card(process_dir=process_dir,params={eft_parameter_block:eft_params})
# additional user set parameters
modify_param_card(process_dir=process_dir,params=params)

# modify run card (set in main job option)
nevents = runArgs.maxEvents*1.5 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob
if not 'nevents' in settings:
    settings['nevents']=int(nevents)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# generate events
generate(runArgs=runArgs,process_dir=process_dir)

# perform addtional reweightings 
if 'rwgtall' in physics_short.split('_') or 'rwall' in physics_short.split('_'):
    reweight_texts=[]
    reweight_texts.append(reweight_smeft(process,'NP<=1 NP^2==1',relevant_coeffs,1,'_lin'))
    reweight_texts.append(reweight_smeft(process,'NP==1',relevant_coeffs,1,'_quad'))
    reweight_texts.append(reweight_smeft(process,'NP<=1 NP^2==2 '+' '.join(['NP{}^2<=1'.format(c) for c in relevant_coeffs]),relevant_coeffs,2,'_cross'))
    for reweight_text in reweight_texts:
        if os.path.exists(process_dir+'/Cards/reweight_card.dat'):
            os.remove(process_dir+'/Cards/reweight_card.dat')
        with open(process_dir+'/Cards/reweight_card.dat','w') as f:
            f.write(reweight_text)
            f.close()
        # required for reweighting to work around issues with cvmfs mg being in python path
        pythonpath_backup=os.environ['PYTHONPATH']
        os.environ['PYTHONPATH']=':'.join([p for p in pythonpath_backup.split(':') if 'madgraph5amc' not in p])
        add_reweighting('run_01',None,process_dir=process_dir)
        os.environ['PYTHONPATH']=pythonpath_backup
    
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
   
# shower
PYTHIA8_nJetMax=1
#PYTHIA8_Process='pp>{a,22}LEPTONS,NEUTRINOS'
PYTHIA8_Process='pp>{a,22}mu-vm~'
PYTHIA8_Dparameter=settings['dparameter']
PYTHIA8_TMS=settings['ktdurham']
PYTHIA8_nQuarksMerge=settings['maxjetflavor']
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")

# turn off mpi for faster generation and smaller files in 'noMPI' is part of name
if 'noMPI' in physics_short:
    genSeq.Pythia8.Commands += [' PartonLevel:MPI = off']
