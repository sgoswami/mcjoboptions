import subprocess
retcode = subprocess.Popen(['get_files', '-jo', 'SMWeinbergCommon_v2_fix.py'])
if retcode.wait() != 0:
    raise IOError('could not locate SMWeinbergCommon_v2_fix.py')

import SMWeinbergCommon_v2_fix

evgenConfig.contact.append("Karolos Potamianos <karolos.potamianos@cern.ch>")

SMWeinbergCommon_v2_fix.process = SMWeinbergCommon_v2_fix.available_processes['emuchannel']
SMWeinbergCommon_v2_fix.parameters_paramcard['nuphysics']['Lambda'] = 500e3
SMWeinbergCommon_v2_fix.parameters_paramcard['nuphysics']['Cem'] = 1.0

SMWeinbergCommon_v2_fix.run_evgen(runArgs, evgenConfig, opts)
