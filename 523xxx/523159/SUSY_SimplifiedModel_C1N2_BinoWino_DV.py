#These three lines are needed for r21.
from MadGraphControl.MadGraphUtils import * 
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')  #this name is different from the r19 PreInclude.py
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short #get_physics_short is the r21 was to return the name of the file

JOName = get_physics_short()
jobConfigParts = JOName.split('_')
run_settings.update({'time_of_flight':1E-25, 'event_norm':'average'}) 
fixEventWeightsForBridgeMode = True
include( "evt_multiplier_setting_" + str(jobConfigParts[2]) + "_" + str(jobConfigParts[3]) + "_" + str(jobConfigParts[4]) + "_" + str(jobConfigParts[5]) + "_" + str(jobConfigParts[6]) + ".py" )

decayMode_str = ''
if 'WZ' in jobConfigParts[2]:
   decayMode_str = '# higgs mode off \ndecay n2 > n1 f f /h01 \ndecay x1 > f f n1\n'
elif 'Wh' in jobConfigParts[2]:
   if 'h2b' in jobConfigParts[2]:
     decayMode_str = '# Z mode off \ndecay n2 > n1 b b~ /z \ndecay x1 > f f n1 \n'
   elif 'h2tau' in jobConfigParts[2]:
     decayMode_str = '# Z mode off \ndecay n2 > n1 ta ta- /z \ndecay x1 > f f n1\n'
   elif 'h2c' in jobConfigParts[2]:
     decayMode_str = '# Z mode off \ndecay n2 > n1 c c~ /z \ndecay x1 > f f n1\n'
   elif 'h2g' in jobConfigParts[2]:
     decayMode_str = '# Z mode off \ndecay n2 > n1 g g /z \ndecay x1 > f f n1\n'
   else:
     decayMode_str = '# Z mode off \ndecay n2 > n1 f f /z \ndecay x1 > f f n1\n'
else:
   print('unkown decay mode')
  
run_settings.update({'bwcutoff':500000.0}) 

masses['1000024'] = float(jobConfigParts[3])
masses['1000023'] = float(jobConfigParts[3])
masses['1000022'] = float(jobConfigParts[4])

if masses['1000022']<0.5: masses['1000022']=0.5
process = """
import model MSSM_SLHA2
generate p p > x1+ n2 / susystrong @1
add process p p > x1- n2 / susystrong @1
add process p p > x1+ n2 j / susystrong @2
add process p p > x1- n2 j / susystrong @2
add process p p > x1+ n2 j j / susystrong @3
add process p p > x1- n2 j j / susystrong @3
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~ g
define x1 = x1+ x1-
output -f -nojpeg
"""
njets = 2
evgenLog.info('Registered generation of ~chi1+/- ~chi20 production, decay via Wh; mass point ' + str(masses['1000024']) + ' ' + str(masses['1000022']))

evgenConfig.contact  = [ "risa.ushioda@cern.ch" ]
evgenConfig.keywords += ['SUSY', 'gaugino', 'chargino', 'neutralino']
evgenConfig.description = '~chi1+/- ~chi20 production, decay via W and h with displaced vertex'
keepOutput=True

if 'p03ns' in jobConfigParts[5]:
  evgenLog.info('lifetime of 1000023 is set as 0.03ns, decay via Higgs')
  N2Width = 2.19403976E-14

elif 'p3ns' in jobConfigParts[5]:
  evgenLog.info('lifetime of 1000023 is set as 0.3ns, decay via Higgs')
  N2Width = 2.19403976E-15

elif '3ns' in jobConfigParts[5]:
  evgenLog.info('lifetime of 1000023 is set as 3ns, decay via Higgs')
  N2Width = 2.19403976E-16

decays ={'1000023':'''DECAY 1000023 ''' + str(N2Width)  + '''
# BR  NDA ID1   ID2 
   1.00000000E+00    2     1000022        25   #BR(~chi_20 -> ~chi_10 h)
   0.00000000E+00    3     1000022        11     -11   # BR(~chi_20 -> ~chi_10 e+ e-)
#
   ''', 
  '1000024':'''DECAY 1000024 1.70414503E-02   # chargino1+ decays
# BR  NDA ID1   ID2  
   1.00000000E+00   2     1000022          24  # BR(~chi_1+ -> ~chi_10  W+)
   0.00000000E+00   3     1000022         -11   12  # BR(~chi_1+ -> ~chi_10  e+ nu_e)
#
   '''} 

from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter

filtSeq += MissingEtFilter("MissingEtFilter")
Filter =  jobConfigParts[6]
metFilter = int(Filter.split("MET")[1].split(".")[0])
evgenLog.info('MET is greater than ' + str(metFilter))
 
filtSeq.MissingEtFilter.METCut = metFilter*GeV

#--------------------------------------------------------------
# Madspin configuration
#

madspindecays=True
if madspindecays==True:
  madspin_card='madspin_card_C1N2_Wh.dat'

  mscard = open(madspin_card,'w')  

  mscard.write("""#************************************************************
#*                        MadSpin                           *                
#*                                                          *                
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *                
#*                                                          *                
#*    Part of the MadGraph5_aMC@NLO Framework:              *                
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *                
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *                
#*                                                          *                
#************************************************************                
#Some options (uncomment to apply)                                           
#                                                                            
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
 set BW_cut 500000             # cut on how far the particle can be off-shell         
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event   
#
 set seed %i
 set spinmode none

# specify the decay for the final state particles

%s
# running the actual code
launch
""" % (runArgs.randomSeed, decayMode_str))
  mscard.close()

#--------------------------------------------------------------

genSeq.Pythia8.Commands += ["23:mMin = 0.2"]
genSeq.Pythia8.Commands += ["24:mMin = 0.2"]
genSeq.Pythia8.Commands += ["25:mMin = 0.2"]

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
  genSeq.Pythia8.Commands += [ "Merging:Process = guess", "1000024:spinType = 1", "1000023:spinType = 1" ] 
  genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]

testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #In mm                                                     
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000

