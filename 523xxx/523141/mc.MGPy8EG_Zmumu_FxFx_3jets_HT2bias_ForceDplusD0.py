evgenConfig.description = 'aMcAtNlo Wmunu+0,1,2,3j NLO FxFx HT2-biased D hadron filter'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch","mdshapiro@lbl.gov"]
evgenConfig.keywords += ['SM', 'Z', 'muon', 'jets', 'NLO']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 30.2%
# D+lepton filter eff: ~1.4%
# one LHE file contains 55000 events, taken up to 5 times

evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob = 1000

include("merge_LHE.py")
import glob
copies = 5 * glob.glob('*.events')
outputlhe = "replicatedLHE.lhe.events"
merge_lhe_files(copies,outputlhe)

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO                                                 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)
PYTHIA8_nJetMax=3
PYTHIA8_qCut=20.

include("Pythia8_i/Pythia8_FxFx_A14mod.py")

genSeq.Pythia8.LHEFile = outputlhe

genSeq.EvtInclusiveDecay.userDecayFile = "DplusAndMinusToKpipiAndD0ToKpi.dec"
evgenConfig.auxfiles += [ 'DplusAndMinusToKpipiAndD0ToKpi.dec']

include("GeneratorFilters/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut = 15000.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2

from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
DplusHadronFilter = HeavyFlavorHadronFilter("DplusHadronFilter")
filtSeq += DplusHadronFilter
DplusHadronFilter.RequestSpecificPDGID=True
DplusHadronFilter.RequestCharm = False
DplusHadronFilter.RequestBottom = False
DplusHadronFilter.Request_cQuark = False
DplusHadronFilter.Request_bQuark = False
DplusHadronFilter.PDGPtMin=7.0*GeV
DplusHadronFilter.PDGEtaMax=2.3
DplusHadronFilter.PDGID=411
DplusHadronFilter.PDGAntiParticleToo=True

DstarHadronFilter = HeavyFlavorHadronFilter("DstarHadronFilter")
filtSeq += DstarHadronFilter
DstarHadronFilter.RequestCharm = False
DstarHadronFilter.RequestBottom = False
DstarHadronFilter.Request_cQuark = False
DstarHadronFilter.Request_bQuark = False
DstarHadronFilter.RequestSpecificPDGID = True
DstarHadronFilter.RequireTruthJet = False
DstarHadronFilter.RequestCharm=False
DstarHadronFilter.PDGPtMin=7.0*GeV
DstarHadronFilter.PDGEtaMax=2.3
DstarHadronFilter.PDGID=413
DstarHadronFilter.PDGAntiParticleToo=True

filtSeq.Expression="MultiLeptonFilter and ((DplusHadronFilter) or (DstarHadronFilter))"
