import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
import os,subprocess,fileinput

# General settings 

evgenConfig.nEventsPerJob = 5000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*1.1
mode = 0
#gridpack_mode=True
#gridpack_dir='madevent/'

name = 'tqgammaSM_tchan_4fl_NLO'
runName='mc.MadGraph_'+str(name)

process = """
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~ 
define l+ = e+ mu+ ta+ 
define l- = e- mu- ta-
define vl = ve vm vt 
define vl~ = ve~ vm~ vt~
generate p p > t b~ j a $$ w+ w- [QCD]
add process p p > t~ b j a $$ w+ w- [QCD] 
output -f"""

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


extras = { 'lhe_version'  : '3.0',
           'parton_shower' :'PYTHIA8',
           'ptgmin'        :10,
           'r0gamma'       :0.2, 
           'maxjetflavor'  :4,
           'dynamical_scale_choice': '3', #sum of the transverse mass divided by 2
           'ptl'           :0.0,
           'ptj'           :0.0,
           'etal'          :5.0,
           'etagamma'      :5.0,
           'etaj'          :-1,
           'drll'          :0.0,
           'nevents'       : nevents,
           }


process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)      
set_top_params(process_dir,mTop=172.5,FourFS=True)
               
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')

mscard.write("""#************************************************************                                                                                          
#*                        MadSpin                           *                                                                                                              
#*                                                          *                                                                                                              
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *                                                                                                              
#*                                                          *                                                                                                              
#*    Part of the MadGraph5_aMC@NLO Framework:              *                                                                                                              
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *                                                                                                              
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *                                                                                                              
#*                                                          *                                                                                                              
#************************************************************                                                                                                              
#Some options (uncomment to apply)                                                                                                                                         
#                                                                                                                                                                          
# set seed 1                                                                                                                                                               
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight                                                                                     
 set BW_cut 50                # cut on how far the particle can be off-shell                                                                                              
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event                                                                                        
#                                                                                                                                                                          
set seed %i                                                                                                                                                                
# specify the decay for the final state particles                                                                                                                          
decay t > w+ b, w+ > l+ vl                                                                                                                                               
decay t~ > w- b~, w- > l- vl~                                                                                                                                                                                                                                                                                                      
# running the actual code                                                                                                                                                  
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()
print_cards()


generate(runArgs=runArgs,process_dir=process_dir)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


#### Shower
evgenConfig.description = 'aMC@NLO_'+str(name)
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.keywords+= ['SM', 'top',  'photon','singleTop','lepton'] 
evgenConfig.contact = ['nils.julius.abicht@cern.ch']
runArgs.inputGeneratorFile=outputDS


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")


check_reset_proc_number(opts)

## Enable MG5_aMC@NLO LHEF reading in Pythia8
include("Pythia8_i/Pythia8_LHEF.py")

#aMC@NLO default Pythia8 settings from http://amcatnlo.web.cern.ch/amcatnlo/list_detailed2.htm#showersettings
#plus MEC fix see https://arxiv.org/pdf/2308.06389.pdf
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",
                            "SpaceShower:pTmaxFudge = 1",
                            "SpaceShower:MEcorrections = off",
                            "TimeShower:pTmaxMatch = 1",
                            "TimeShower:pTmaxFudge = 1",
                            "TimeShower:MEcorrections = on",
                            "TimeShower:MEextended    = off",
                            "TimeShower:globalRecoil = on",
                            "TimeShower:limitPTmaxGlobal = on",
                            "TimeShower:nMaxGlobalRecoil = 1",
                            "TimeShower:globalRecoilMode = 2",
                            "TimeShower:nMaxGlobalBranch = 1.",
                            "TimeShower:weightGluonToQuark=1.",
                            "Check:epTolErr = 1e-2" ]
include("Pythia8_i/Pythia8_ShowerWeights.py")
