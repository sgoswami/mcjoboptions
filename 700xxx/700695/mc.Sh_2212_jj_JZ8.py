include("Sherpa_i/Base_Fragment.py")

evgenConfig.description = "QCD 2->2 JZ8"
evgenConfig.keywords = ["SM", "2jet", "LO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "javier.llorente.merino@cern.ch" ]
evgenConfig.nEventsPerJob = 1000
evgenConfig.tune = "CT18NNLO"

genSeq.Sherpa_i.RunCard="""
(run){
 ACTIVE[25]=0;
 PDF_LIBRARY LHAPDFSherpa; PDF_SET CT18NNLO;
 CORE_SCALE QCD;
}(run)

(processes){
 Process 93 93 -> 93 93;
 Enhance_Function: VAR{(PPerp2(p[2])+PPerp2(p[3]))/200};
 Order (*,0);
 Integration_Error 0.02 {2};
 End process;                 
}(processes)

(selector){
 NJetFinder  2  10.0  0.0  0.4  -1  9999.0  10.0
 NJetFinder  1  1650.0  0.0  0.4  -1  9999.0  10.0
}(selector)           
"""

genSeq.Sherpa_i.Parameters += [
    "PDF_VARIATIONS=CT18NNLO[all] NNPDF30_nnlo_as_0118_hessian[all] NNPDF30_nnlo_as_0117 NNPDF30_nnlo_as_0119 MSHT20nnlo_as118 CT18NNLO_as_0117 CT18NNLO_as_0119 PDF4LHC21_40_pdfas[all] NNPDF31_nnlo_as_0118_hessian NNPDF40_nnlo_as_01180_hessian CT18ANNLO CT18XNNLO CT18ZNNLO CT14nnlo",
]

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
JZSlice(8, filtSeq)
