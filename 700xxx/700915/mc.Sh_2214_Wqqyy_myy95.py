include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa W+/W- gammagamma + 0,1j@NLO + 2,3j@LO with pT_y>15 GeV and myy>95 GeV"
evgenConfig.keywords = ["SM","W","quark","photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "a.lancaster@cern.ch" ]
evgenConfig.nEventsPerJob = 200

genSeq.Sherpa_i.RunCard="""
(run){

  # Shower improvements
  NLO_SUBTRACTION_SCHEME=2;

  % Reduction in negative weights
  NLO_CSS_PSMODE=1

  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  EW_SCHEME=3
  GF=1.166397e-5
  METS_BBAR_MODE=5

  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=3; LJET:=3,4; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  % Onshell W's
  HARD_DECAYS=1
  STABLE[-24]=0
  WIDTH[-24]=0
  STABLE[24]=0
  WIDTH[24]=0

  % W->light flavor
  HDH_STATUS[24,2,-1]=2
  HDH_STATUS[24,4,-3]=2
  HDH_STATUS[-24,-2,1]=2
  HDH_STATUS[-24,-4,3]=2

  % massive b-quarks such that top-quark processes are not included by the 93 container
  MASSIVE[5]=1;
  OL_PARAMETERS=nq_nondecoupled 5 mass(5) 0.0
  MCATNLO_MASSIVE_SPLITTINGS=0

}(run)

(processes){
  Process 93 93 -> 22 22 -24 93{NJET};
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  PSI_ItMin 20000 {3};
  Integration_Error 0.99 {3};
  PSI_ItMin 50000 {4,5,6,7};
  Integration_Error 0.99 {4,5,6,7};
  End process;

  Process 93 93 -> 22 22 24 93{NJET};
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  PSI_ItMin 20000 {3};
  Integration_Error 0.99 {3};
  PSI_ItMin 50000 {4,5,6,7};
  Integration_Error 0.99 {4,5,6,7};
  End process;

}(processes)

(selector){
  PTNLO 22  15  E_CMS
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO  22  90  0.1 1000.0
  Mass  22  22  95.0 E_CMS 
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[-24]=0" ]
genSeq.Sherpa_i.Parameters += [ "WIDTH[24]=0" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1 nq_nondecoupled=5 mass(5)=0.0" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.NCores = 128
