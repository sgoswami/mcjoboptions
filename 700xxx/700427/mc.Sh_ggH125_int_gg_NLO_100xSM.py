include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
#os.environ["ATHENA_PROC_NUMBER"] = "16"

evgenConfig.description = "Sherpa 2.2.11 JO, H->yy and SM yy (gg) interference generation."
evgenConfig.keywords = [ "Higgs", "Photon" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch"]
evgenConfig.nEventsPerJob = 10000

genSeq.Sherpa_i.RunCard="""
(run){

  ME_SIGNAL_GENERATOR Amegic Higgs;

  QSF:=1.0; SCALES METS{MU_F2}{MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])};

  BEAM_1 2212; BEAM_ENERGY_1 6500;
  BEAM_2 2212; BEAM_ENERGY_2 6500;

  YUKAWA[4] 1.42; YUKAWA[5] 4.8;
  YUKAWA[15] 1.777; YUKAWA[6] 164;
  EW_SCHEME 3; RUN_MASS_BELOW_POLE 1;
  WIDTH[25] 0.407;
  MASS[25] 125.;
  
  # HIM: 1 -> include signal
  #      2 -> include loop background
  #      4 -> include tree background (for qg-channel)
  #  1|2=3 -> included signal and loop background
  #    etc... 
  
  # HIO = 1 : include only interferences
  # HIO = 0 : include interferences and signal^2, loop background^2, and tree^2
  # HIO = 2 : include interferences and signal^2
  
  PP_HPSMODE 4;
  NLO_SUBTRACTION_SCHEME 2;
  
  HIGGS_INTERFERENCE_MODE 3; HIGGS_INTERFERENCE_ONLY 1;
  AMEGIC_ALLOW_MAPPING 0; PSI_ITMIN 100000; ABS_ERROR 1.0;
  INTEGRATION_ERROR 0.99;

  MI_HANDLER None; ME_QED Off;
  SHOWER_GENERATOR CSS; CSS_IS_AS_FAC 1.0; REWEIGHT_MAXEM 0;  
 
  PDF_LIBRARY LHAPDFSherpa;
  PDF_SET NNPDF30_nnlo_as_0118_hessian;
  USE_PDF_ALPHAS 1;
  
}(run);

(processes){
  Process 93 93 -> 22 22;
  Order (*,2);
  NLO_QCD_Mode MC@NLO;
  Enable_MHV 12;
  Integrator PS2;
  RS_Integrator PS3;
  Loop_Generator Higgs;
  End process;
}(processes);

(selector){
  "PT" 22 20,E_CMS:18,E_CMS [PT_UP];
  RapidityNLO  22  -2.7  2.7;
  IsolationCut  22  0.1  2  0.10;
  Mass 22 22 90 175;
  DeltaRNLO 22 22 0.2 1000.0;
}(selector);

"""

genSeq.Sherpa_i.Parameters += ["PP_HPSMODE=4", "NLO_SUBTRACTION_SCHEME=2", "HIGGS_INTERFERENCE_MODE=3", "HIGGS_INTERFERENCE_ONLY=1"]
genSeq.Sherpa_i.OpenLoopsLibs = []
genSeq.Sherpa_i.ExtraFiles = []
#genSeq.Sherpa_i.NCores = 16
