include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Electroweak Sherpa gamma + 2,3j@LO using Min_N_TChannels option."
evgenConfig.keywords = ["SM", "photon", "jets", "VBF" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){
  # perturbative scales
  CORE_SCALE VAR{PPerp2(p[2])}
  ALPHAQED_DEFAULT_SCALE=0.0
  KFACTOR=VAR{132.17/137.03599976}

  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  EXCLUSIVE_CLUSTER_MODE=1;

  % tags for process setup
  NJET:=1; QCUT:=15.;

  % improve colour-flow treatment
  CSS_CSMODE=1
}(run)

(processes){
  Process 93 93 -> 22 93 93 93{NJET};
  Order (*,3);
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  Min_N_TChannels 1
  Integration_Error 0.05
  End process;
}(processes)

(selector){
  IsolationCut  22  0.1  2  0.10
  RapidityNLO  22  -2.7  2.7
  PTNLO 22  17  E_CMS
  NJetFinder 2 15.0 0.0 0.4 -1
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

