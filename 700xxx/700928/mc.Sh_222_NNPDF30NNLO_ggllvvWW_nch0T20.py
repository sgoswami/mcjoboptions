# aftertburner of 345718 
evgenConfig.description = "Sherpa+OpenLoops gg->(WW->)llvv + 0,1j, cf. arXiv:1309.0500, including the gg->h diagrams (+interference)."
evgenConfig.keywords = ["SM", "diboson", "2lepton", "neutrino" ]
evgenConfig.contact  = [ "oldrich.kepka@cern.ch" ]

evgenConfig.nEventsPerJob = 1000
evgenConfig.inputFilesPerJob = 17

if runArgs.trfSubstepName == 'generate' :
   print ("ERROR: These JO require an input file.  Please use the --afterburn option")
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   #--------------------------------------------------------------
   include('GeneratorFilters/MultiLeptonFilter.py')
   ### Default cut params
   filtSeq.MultiLeptonFilter.Ptcut = 3500.
   filtSeq.MultiLeptonFilter.Etacut = 2.7
   filtSeq.MultiLeptonFilter.NLeptons = 2

   include('GeneratorFilters/ChargedTrackWeightFilter.py')
   filtSeq.ChargedTracksWeightFilter.NchMin = 0
   filtSeq.ChargedTracksWeightFilter.NchMax = 20
   filtSeq.ChargedTracksWeightFilter.Ptcut = 500
   filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
   #filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 6, 10, 15, 20, 25 ]
   #filtSeq.ChargedTracksWeightFilter.SplineY =[ 0.0001, 0.0001, 0.0015, 0.005, 0.008, 0.01, 0.01 ]
   # reduce the relative weight of nch < 4 events. Spectrum is steeply falling and this keeps the filtering efficiency not too small 
   filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 4, 6, 10, 15, 20, 25 ]
   filtSeq.ChargedTracksWeightFilter.SplineY =[ 0.0006, 0.0006, 0.0006, 0.0015, 0.005, 0.008, 0.01, 0.01 ]


   postSeq.CountHepMC.CorrectRunNumber = True

