include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "W(->qq)W(->lv) with 0,1j@NLO + 2,3j@LO, QSF=0.25"
evgenConfig.keywords = ["SM", "WW","1lepton","quark","NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "stephen.jiggins@cern.ch","matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 5000

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=0.25;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])/4.0}

  NLO_SUBTRACTION_SCHEME=2

  %tags for process setup
  NJET:=3; LJET:=2,3; QCUT:=20.;

  % NLO EWK
  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  EW_SCHEME=3
  GF=1.166397e-5

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0

  % massive b-quarks such that top-quark processes are not included by the 93 container
  MASSIVE[5]=1;
  OL_PARAMETERS=nq_nondecoupled 5 mass(5) 0.0
  MCATNLO_MASSIVE_SPLITTINGS=0

  % decay setup
  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0

  % Negative weight reduction
  NLO_CSS_PSMODE=1
  METS_BBAR_MODE=5
  SOFT_SPIN_CORRELATIONS=1

}(run)

(processes){
  Process 93 93 -> 24 -24 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET}
  Enhance_Function VAR{(max(PPerp(p[2]),PPerp(p[3])))/20.0}
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[24]=0" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.NCores = 32

from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
decaysfilter = DecaysFinalStateFilter()
filtSeq += decaysfilter
filtSeq.DecaysFinalStateFilter.PDGAllowedParents = [ 24,-24 ]
filtSeq.DecaysFinalStateFilter.NQuarks  = 2
filtSeq.DecaysFinalStateFilter.NNeutrinos = 1
filtSeq.DecaysFinalStateFilter.NChargedLeptons = 1
