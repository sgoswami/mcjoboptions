include("Sherpa_i/Base_Fragment.py")

evgenConfig.description = "QCD 2->2 JZ7"
evgenConfig.keywords = ["SM", "2jet", "LO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "christopher.young@cern.ch" ]
evgenConfig.nEventsPerJob = 500
evgenConfig.tune = "CT14nnlo + Cluster hadronization v3"

genSeq.Sherpa_i.RunCard="""
(run){
 ACTIVE[25]=0;
 PDF_LIBRARY LHAPDFSherpa; PDF_SET CT14nnlo;
 CORE_SCALE QCD;

 STRANGE_FRACTION=0.535
 BARYON_FRACTION=1.48
 DECAY_OFFSET=1.29
 DECAY_EXPONENT=3.03
 P_qs_by_P_qq=0.26
 P_ss_by_P_qq=0.012
 P_di_1_by_P_di_0=0.93
 G2QQ_EXPONENT=0.60
 PT_MAX=1.48
 PT_MAX_FACTOR=1.34
 SPLIT_EXPONENT=0.24
 SPLIT_LEADEXPONENT=1.49
 SPECT_EXPONENT=1.49
 SPECT_LEADEXPONENT=10.32
}(run)

(processes){
 Process 93 93 -> 93 93;
 Enhance_Function: VAR{(PPerp2(p[2])+PPerp2(p[3]))/200};
 Order (*,0);
 Integration_Error 0.02 {2};
 End process;                 
}(processes)

(selector){
 NJetFinder  2  10.0  0.0  0.4  -1  9999.0  10.0
 NJetFinder  1  840.0  0.0  0.4  -1  9999.0  10.0
}(selector)           
"""

genSeq.Sherpa_i.Parameters += [
    "PDF_VARIATIONS=NNPDF30_nnlo_as_0118[all] NNPDF30_nnlo_as_0117 NNPDF30_nnlo_as_0119 MMHT2014nnlo68cl CT14nnlo[all] CT14nnlo_as_0117 CT14nnlo_as_0119 PDF4LHC15_nnlo_30_pdfas[all]",
    ]

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
JZSlice(7, filtSeq)


