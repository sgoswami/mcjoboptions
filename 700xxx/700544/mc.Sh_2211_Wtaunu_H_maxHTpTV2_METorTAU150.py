evgenConfig.description = "Sherpa W+/W- -> tau nu + 0,1,2j@NLO + 3,4,5j@LO with hadronic tau decaus and MET>150 GeV or pT(tau)>150 GeV taking input from existing unfiltered input file."
evgenConfig.keywords = ["SM", "W", "tau", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch", "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 16

if runArgs.trfSubstepName == 'generate' :
   print "ERROR: These JO require an input file.  Please use the --afterburn option"
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   include ( 'GeneratorFilters/MissingEtFilter.py' )
   filtSeq.MissingEtFilter.METCut = 150*GeV

   include("GeneratorFilters/MultiElecMuTauFilter.py")
   MultiElecMuTauFilter = filtSeq.MultiElecMuTauFilter
   MultiElecMuTauFilter.NLeptons       = 1
   MultiElecMuTauFilter.MinPt          = 1E9 # disable e or mu for this filter
   MultiElecMuTauFilter.IncludeHadTaus = True
   MultiElecMuTauFilter.MinVisPtHadTau = 150*GeV
   MultiElecMuTauFilter.MaxEta         = 2.6 
   filtSeq.Expression = "MissingEtFilter or MultiElecMuTauFilter"

   postSeq.CountHepMC.CorrectRunNumber = True

