include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/EW_scheme_sinthetaW_mZ.py")

evgenConfig.description = "Sherpa Z ->bb + 1,2j@NLO + 3-5j@LO with pTV > 200 GeV. Resummation scale 0.25."
evgenConfig.keywords = ["SM", "Z", "jets" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 2000

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=0.25;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % Shower improvements
  NLO_SUBTRACTION_SCHEME=2;

  %tags for process setup
  NJET:=4; LJET:=2,3; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  % Force Z->bb decay
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
  HDH_STATUS[23,5,-5]=2

  % EW corrections setup
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  METS_BBAR_MODE=5

  % speed and neg weight fraction improvements
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])+MPerp(p[2]))/4};
  NLO_CSS_PSMODE=1

}(run)

(processes){
  Process 93 93 -> 23 93 93{NJET};
  Enhance_Function VAR{PPerp2(p[2])/400.0} {2,3,4,5,6}
  Cut_Core 1;
  Order (*,1); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {2,3,4,5,6};
  End process;
}(processes)

(selector){
  PTNLO 23 200.0 E_CMS 
}(selector)

"""

genSeq.Sherpa_i.NCores = 32
genSeq.Sherpa_i.Parameters += [ "WIDTH[23]=0" ]
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppvj","ppzjj", "ppvj_ew","ppzjj_ew"]

