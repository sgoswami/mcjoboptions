include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO_nf_4.py")

evgenConfig.description = "WZZ (-> 3 charged leptons + 3 neutrinos) +Oj@NLO+1,2j@LO."
evgenConfig.keywords = ["SM", "triboson", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1; RSF:=1; QSF:=1;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[0]+p[1])};

  %tags for process setup
  NJET:=2; LJET:=3; QCUT:=30;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  %EW corrections setup
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;

  METS_BBAR_MODE=5
  SOFT_SPIN_CORRELATIONS=1

  EW_SCHEME=3
  GF=1.166397e-5

  % NWF improvements
  NLO_CSS_PSMODE=1

  PARTICLE_CONTAINER 901 lightflavs 1 -1 2 -2 3 -3 4 -4 21;

  %decay setup
  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0
  STABLE[23]=0
  WIDTH[23]=0
}(run);

(processes){
  Process 901 901 -> 23 23 24 901{NJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;

  Process 901 901 -> 23 23 -24 901{NJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes);
"""


genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5", "WIDTH[24]=0", "WIDTH[23]=0" ]
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppvvv_ew" ]

from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
filtSeq += DecaysFinalStateFilter()
filtSeq.DecaysFinalStateFilter.PDGAllowedParents = [23, 24, -24]
filtSeq.DecaysFinalStateFilter.NChargedLeptons = 3
filtSeq.Expression = "DecaysFinalStateFilter"

evgenConfig.nEventsPerJob = 5000

