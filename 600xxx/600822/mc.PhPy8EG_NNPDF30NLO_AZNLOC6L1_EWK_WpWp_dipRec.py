
#--------------------------------------------------------------
# Pythia8 showering with Photos
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += ['Powheg:NFinal = 4',
                            'SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBS WpWp + 2 jets using Pythia dipole recoil"
evgenConfig.keywords    = [ "SM", "VBS", "electroweak", "WW", "2jet", "NLO" ]
evgenConfig.contact     = [ "Christian Gutschow <chris.g@cern.ch>" ]
evgenConfig.process     = "VBS ssWW"
evgenConfig.inputFilesPerJob = 2
evgenConfig.nEventsPerJob = 10000

