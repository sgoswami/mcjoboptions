# EVGEN Configuration
evgenConfig.generators += ["Powheg", "Herwig7"]
evgenConfig.description = "SM diHiggs production, decay to bbZZ(2l2q), with Powheg-Box-V2 for ME and Herwig7 for shower"
evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "bbar", "ZZ"]
evgenConfig.contact = ['Xiaozhong Huang <xiaozhong.huang@cern.ch>']
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 20


# Herwig 7 showering setup                                    
# -- initialize Herwig7 generator configuration for showering
include("Herwig7_i/Herwig7_LHEF.py")

# -- configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop No")
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC15_nlo_30_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7.1-Default"

# -- add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# -- modify the BR to increase the filter efficiency
Herwig7Config.add_commands ("""
set /Herwig/Shower/ShowerHandler:SpinCorrelations Yes

# modify BR of Higgs 
do /Herwig/Particles/h0:SelectDecayModes h0->Z0,Z0; h0->b,bbar;
set /Herwig/Particles/h0/h0->Z0,Z0;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
do /Herwig/Particles/h0:PrintDecayModes

# modify BR of Z
set /Herwig/Particles/Z0/Z0->nu_e,nu_ebar;:OnOff Off
set /Herwig/Particles/Z0/Z0->nu_mu,nu_mubar;:OnOff Off
set /Herwig/Particles/Z0/Z0->nu_tau,nu_taubar;:OnOff Off
set /Herwig/Particles/Z0/Z0->e+,e-;:OnOff On
set /Herwig/Particles/Z0/Z0->mu+,mu-;:OnOff On
set /Herwig/Particles/Z0/Z0->tau+,tau-;:OnOff On
set /Herwig/Particles/Z0/Z0->u,ubar;:OnOff On
set /Herwig/Particles/Z0/Z0->d,dbar;:OnOff On
set /Herwig/Particles/Z0/Z0->c,cbar;:OnOff On
set /Herwig/Particles/Z0/Z0->s,sbar;:OnOff On
set /Herwig/Particles/Z0/Z0->b,bbar;:OnOff On 
do /Herwig/Particles/Z0:PrintDecayModes
""")


# Generator Filters
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("hZZFilter", PDGParent = [25], PDGChild = [23])
filtSeq += ParentChildFilter("ZllFilter", PDGParent = [23], PDGChild = [11,13,15])
filtSeq += ParentChildFilter("ZqqFilter", PDGParent = [23], PDGChild = [1,2,3,4,5])


# -- require HH->bbZZ
filtSeq.Expression = "hbbFilter and hZZFilter and ZllFilter and ZqqFilter"


# run Herwig7
Herwig7Config.run()
