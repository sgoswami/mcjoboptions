process="WmH"

#Higgs mass (in GeV)
H_Mass = 125.0

#Higgs width (in GeV)
H_Width = 0.00407

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
if process=="ggH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']
elif process=="VBF":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WpH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WmH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ggZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             '24:onMode = off',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                             ]


genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:addChannel = 1 0.002 100 313 22',
                             '313:mMax = 5' 
                             ]

evgenConfig.description = "PYTHIA8+EVTGEN, WmH, W->any, H->Kstar0Gamma"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WmHiggs", "mH125" ]
evgenConfig.contact     = [ 'g.s.virdee@cern.ch' ]
evgenConfig.generators  = [ "Pythia8", "EvtGen", "Powheg" ]
evgenConfig.inputFilesPerJob = 19 #Specify the number of LHEs files needed 
evgenConfig.nEventsPerJob = 10000
