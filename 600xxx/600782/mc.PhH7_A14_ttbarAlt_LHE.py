evgenConfig.description = 'POWHEG+Herwig7 ttbar production with Powheg hdamp equal 1.5*top mass, H7.1-Default tune, ME NNPDF30 NLO, mT = 172.5 GeV -> MPI off'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch','mshapiro@lbl.gov','steffen.henkelmann@cern.ch']
evgenConfig.generators += [ 'Powheg','Herwig7', 'EvtGen']
evgenConfig.tune = "H7.1-Default"
evgenConfig.inputFilesPerJob=180
evgenConfig.nEventsPerJob=500

#--------------------------------------------------------------
# Event-LHE filter
#--------------------------------------------------------------
include('MCJobOptionUtils/LHEFilter.py')
include('./LHEFilter_SubLeadingTopPt.py')
SubleadTopPt = LHEFilter_SubLeadingTopPt()
SubleadTopPt.Ptcut = 700.
lheFilters.addFilter(SubleadTopPt)
lheFilters.run_filters()

#--------------------------------------------------------------
# HERWIG7 showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")
# my Add: switch off MPI
Herwig7Config.add_commands(""" 
set /Herwig/Shower/ShowerHandler:MPIHandler    NULL 
""")
# run Herwig7
Herwig7Config.run()

#-------------------------------------------------------------
# Event-FinalState filter
#-------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

filtSeq.Expression = "(not TTbarWToLeptonFilter)"
