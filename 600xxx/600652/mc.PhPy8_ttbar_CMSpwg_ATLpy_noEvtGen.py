

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'ttbar production with CMS Powheg settings and ATLAS Pythia8 settings, WITHOUT EvtGen.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar']
evgenConfig.contact     = [ 'm.fenton@cern.ch']
evgenConfig.generators  = [ 'Powheg', 'Pythia8', ]

#--------------------------------------------
# Powheg box b2 input from cms
#-------------------------------------------
#                # ATLAS Keyword     # Powheg runcard keyword
include('PowhegControl/PowhegControl_tt_Common.py')

PowhegConfig.__setattr__("PDF" , 306000) #lhans1 (+lhans2)
#PowhegConfig.__setattr__("qmass", 172.5) # setting this crashes the job, but default is correct anyway
#PowhegConfig.__setattr__("beam_energy", 6500) # ebeam1 + ebeam2
PowhegConfig.__setattr__("mu_F", 1) #facscfact
PowhegConfig.__setattr__("mu_R", 1) #renscfact

PowhegConfig.hdamp = 237.8775

if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 22222 # inclusive top decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "t t~ > all"

PowhegConfig.__setattr__("mass_W", 80.4) #tdec/wmass
PowhegConfig.__setattr__("width_W", 2.141) #tdec/wwidth
PowhegConfig.__setattr__("mass_b", 4.8) #tdec/bmass
PowhegConfig.__setattr__("width_t", 1.31) #tdec/twidth
PowhegConfig.__setattr__("BR_W_to_enu", 0.108) #tdec/elbranching
PowhegConfig.__setattr__("mass_e", 0.00051) #tdec/emass
PowhegConfig.__setattr__("mass_mu", 0.1057) #tdec/mumass
PowhegConfig.__setattr__("mass_tau", 1.777) #tdec/taumass
PowhegConfig.__setattr__("mass_d", 0.100) #tdec/dmass
PowhegConfig.__setattr__("mass_u", 0.100) #tdec/umass
PowhegConfig.__setattr__("mass_s", 0.200) #tdec/smass
PowhegConfig.__setattr__("mass_c", 1.5) #tdec/cmass
PowhegConfig.__setattr__("sin2cabibbo", 0.051) #tdec/sin2cabibbo
#PowhegConfig.__setattr__("use-old-grid", 1) 
#PowhegConfig.__setattr__("use-old-ubound", 1)
PowhegConfig.__setattr__("ncall1", 10000)
PowhegConfig.__setattr__("itmx1", 5)
PowhegConfig.__setattr__("ncall2", 100000)
PowhegConfig.__setattr__("itmx2", 5)
PowhegConfig.__setattr__("foldcsi", 1)
PowhegConfig.__setattr__("foldy", 1)
PowhegConfig.__setattr__("foldphi", 1)
PowhegConfig.__setattr__("nubound", 100000)
PowhegConfig.__setattr__("iymax", 1)
PowhegConfig.__setattr__("xupbound", 2)

PowhegConfig.generate()

#--------------------------------------------------------------
# ATLAS Pythia8 showering WITHOUT EvtGen
#--------------------------------------------------------------
#include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'POWHEG:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]
#--------------------------------------------------------------                                                                                                                        \
                                                                                                                                                                                        
# Event filter                                                                                                                                                                         \
                                                                                                                                                                                        
#--------------------------------------------------------------                                                                                                                        \
                                                                                                                                                                                        
#include('GeneratorFilters/TTbarWToLeptonFilter.py')
#filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
#filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


