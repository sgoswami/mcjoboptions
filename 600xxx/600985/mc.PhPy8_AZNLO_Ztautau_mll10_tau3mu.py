# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG Ztautau production and exactly one BSM tau->3mu decay"
evgenConfig.keywords = ["BSM", "Z", "tau", "muon"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg","Pythia8"]

evgenConfig.nEventsPerJob = 5000

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg W_EW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_Z_EW_Common.py")

PowhegConfig.decay_mode = "z > tau+ tau-"

PowhegConfig.no_ew   = 1
PowhegConfig.ptsqmin = 4. # AZNLO setting
PowhegConfig.PHOTOS_enabled = False
PowhegConfig.mass_low = 10 # collect also the gamma* down to m(tautau)~10GeV

#Gmu EW scheme inputs matching what used in the Powheg V1 samples
PowhegConfig.scheme=0
PowhegConfig.alphaem=0.00781653
PowhegConfig.mass_W=79.958059

filterMultiplier = 15.
PowhegConfig.nEvents = runArgs.maxEvents*filterMultiplier if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMultiplier

# tune Powheg settings
PowhegConfig.rwl_group_events = 100000
PowhegConfig.ncall1       = 500000
PowhegConfig.ncall2       = 500000
PowhegConfig.nubound      = 2000000
PowhegConfig.itmx1        = 10
PowhegConfig.itmx2        = 20
PowhegConfig.storemintupb = 0 # smaller grids

# Fold parameter reducing the negative eventweight fraction
PowhegConfig.foldcsi      = 2
PowhegConfig.foldphi      = 2
PowhegConfig.foldy        = 1

### default PDF for AZNLO is CT10, then add the usuals, a few new ones and NNPDF3.0 replica
# CT10, NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118
PowhegConfig.PDF = [10800, 260000, 25200, 13165, 90900, 265000, 266000, 303400]
 # NNPDF31_nnlo_as_0118, CT18NLO, CT18NNLO, CT18ANNLO, MSHT20nlo_as118, MSHT20nnlo_as118
PowhegConfig.PDF.extend([303600, 14400, 14000, 14200, 27100, 27400])
PowhegConfig.PDF.extend(range(260001, 260101)) # Include the NNPDF3.0 error set
PowhegConfig.PDF.extend([331500]) # add NNPDF40_nnlo_as_01180_hessian

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# Pythia8 Shower & settings
# --------------------------------------------------------------    

include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#include('Pythia8_i/Pythia8_Photospp.py')

# force half of the taus to decay to three muons, keeping the SM decays
genSeq.Pythia8.Commands += [' 15:addChannel = 1 1.0 0 13 -13 13']

# --------------------------------------------------------------
# filter
# --------------------------------------------------------------    

# filter Z->tautau such that we have exactly one tau->3mu and one SM-like tau decay
# this filter is purely technical and has exactly 50% efficiency

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonWithParentFilter
filtSeq += MultiLeptonWithParentFilter("tau3mu")
filtSeq.tau3mu.NLeptonsMin = 3 # tau->3mu
filtSeq.tau3mu.NLeptonsMax = 4 # allows at most one extra leptonic tau, but vetoes a second tau->3mu
filtSeq.tau3mu.MinPt = 0.
filtSeq.tau3mu.MaxEta = 10.
filtSeq.tau3mu.PDGOrigin = [15]
filtSeq.tau3mu.IncludeLepTaus = False
filtSeq.tau3mu.IncludeHadTaus = False
#filtSeq.tau3mu.OutputLevel = DEBUG


# second filter step to ensure most of the tau->3mu decays are close to detector acceptance
# roughly 16% efficiency

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("TwoMuonsFilter")
filtSeq.TwoMuonsFilter.Ptcut = 3250.
filtSeq.TwoMuonsFilter.Etacut = 3.0
filtSeq.TwoMuonsFilter.NMuons = 2

filtSeq += MultiMuonFilter("ThreeMuonsFilter")
filtSeq.ThreeMuonsFilter.Ptcut = 2000.
filtSeq.ThreeMuonsFilter.Etacut = 3.0
filtSeq.ThreeMuonsFilter.NMuons = 3
