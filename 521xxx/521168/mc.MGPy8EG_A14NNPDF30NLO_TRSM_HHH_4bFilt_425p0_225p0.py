get_param_card = subprocess.Popen(['get_files', '-data', 'MadGraph_param_card_TRSM_HHH.dat'])
if get_param_card.wait():
        print("Could not get hold of MadGraph_param_card_TRSM_HHH.dat, exiting...")
        sys.exit(2)
get_run_card = subprocess.Popen(['get_files', '-data', 'MadGraph_run_card_TRSM_HHH.dat'])
if get_run_card.wait():
        print("Could not get hold of MadGraph_run_card_TRSM_HHH.dat, exiting...")
        sys.exit(2)

evgenConfig.nEventsPerJob  = 500

include('MadGraphControl_TRSM_HHH.py')
