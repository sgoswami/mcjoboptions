THDMparams = {}
THDMparams['gPXd'] = 1.0 # The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $y_\chi$ in (2.5) of arXiv:1701.07427.
THDMparams['tanbeta'] = 1.0 # The ratio of the vacuum expectation values $	an eta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['sinbma'] = 1.0 # The sine of the difference of the mixing angles $\sin (eta - lpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427.
THDMparams['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['sinp'] = 0.35 # The sine of the mixing angle $	heta$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['MXd'] = 150 # The mass of the fermionic DM candidate denoted by $m_\chi$ in arXiv:1701.07427.
THDMparams['mh1'] = 125.0 # The mass of the lightest scalar mass eigenstate $h$, which is identified in arXiv:1701.07427 with the Higgs-like resonance found at the LHC.
THDMparams['mh2'] = 1000.0 # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh3'] = 1000.0 # The mass of the heavy pseudoscalar mass eigenstate $A$. See Section 2.1 of arXiv:1701.07427 for further details. 
THDMparams['mhc'] = 1000.0 # The mass of the charged scalar eigenstate $H^\pm$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh4'] = 400.0 # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin 	heta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.

initialGluons = True # Determines initial state to generate from. True --> top loop induced production (gluon Fusion). False --> b b~ -initiated production (b-anti-b annihilation). See Sections 5.4 and 6.5 of arXiv:1701.07427 for further details

reweight = False # Determines whether to store alternative weights for tanb and sinp
reweights=[
'SINP_0.35-TANB_0.3', 
'SINP_0.35-TANB_0.5', 
'SINP_0.35-TANB_1.0', 
'SINP_0.35-TANB_2.0', 
'SINP_0.35-TANB_3.0', 
'SINP_0.35-TANB_5.0', 
'SINP_0.35-TANB_10.0', 
'SINP_0.35-TANB_20.0', 
'SINP_0.7-TANB_0.3', 
'SINP_0.7-TANB_0.5', 
'SINP_0.7-TANB_1.0', 
'SINP_0.7-TANB_2.0', 
'SINP_0.7-TANB_3.0', 
'SINP_0.7-TANB_5.0', 
'SINP_0.7-TANB_10.0', 
'SINP_0.7-TANB_20.0',
]

# Event multiplier
LHE_EventMultiplier = 2 #For Filter, Multiplier on maxEvents
evgenConfig.nEventsPerJob = 20000

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import re
import shutil

#----------------------------------------------------------------------------
# Process definition
#----------------------------------------------------------------------------
if initialGluons:
	# For the gluon-gluon fusion production use the 4FS to take into account the b-quark loops
	process="""
	import model Pseudoscalar_2HDM -modelname
	define p = g d u s c d~ u~ s~ c~
	define j = g d u s c d~ u~ s~ c~
	generate g g > z xd xd~ /h1 [QCD]
	output -f
	"""
else:
	# For b-initiated production use 5FS
	process="""
	import model Pseudoscalar_2HDM-bbMET_5FS -modelname
	define p = g d u s c b d~ u~ s~ c~ b~
	define j = g d u s c b d~ u~ s~ c~ b~
	generate p p > z xd xd~ / h1
	output -f
	"""   

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------
nevents=evgenConfig.nEventsPerJob
if LHE_EventMultiplier>0:
  nevents=runArgs.maxEvents*LHE_EventMultiplier

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {}
if initialGluons:
  extras = {
        'maxjetflavor'  : 4,
        'asrwgtflavor'  : 4,
        'lhe_version'	: '3.0',
        'cut_decays'	: 'F',
        'nevents'     	: nevents,
    }
else:
  extras = {
        'maxjetflavor'  : 5,
        'asrwgtflavor'  : 5,
        'lhe_version'	: '3.0',
        'cut_decays'	: 'F',
        'nevents'     	: nevents,
    }

# Build run_card
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------

# Build param_card.dat

params={}
## blocks might be modified
dict_blocks={
"mass": ["MB", "MXd", "mh2", "mh3", "mhc", "mh4"],
"DMINPUTS" : ["gPXd", ],
"FRBlock": ["tanbeta", "sinbma", ],
"Higgs": ["lam3", "laP1", "laP2", "sinp"],
}

for bl in dict_blocks.keys():
  for pa in dict_blocks[bl]:
    if pa in THDMparams.keys():
      if bl not in params: params[bl]={}
      if pa=="MB": 
        params[bl]["5"]=THDMparams[pa]
      else:
        params[bl][pa]=THDMparams[pa]

## auto calculation of decay width
THDMparams_decay={
"25": "Auto",
"35": "Auto",
"36": "Auto",
"37": "Auto",
"55": "Auto",
}

params["decay"]=THDMparams_decay

print("Updating parameters:")
print(params)

modify_param_card(process_dir=process_dir,params=params)

# Build reweight_card.dat
if reweight:
  # Create reweighting card
  reweight_card_loc=process_dir+'/Cards/reweight_card.dat'
  rwcard = open(reweight_card_loc,'w')

  for rw_name in reweights:
    params_rwt = params.copy()
    for param in rw_name.split('-'):
      param_name, value = param.split('_')
      if param_name == "SINP":
        params_rwt['HIGGS']['SINP'] = value # Capital letters keys used in params_rwt
      elif param_name == "TANB":
        params_rwt['FRBLOCK']['TANBETA'] = value # Capital letters keys used in params_rwt

    param_card_reweight = process_dir+'/Cards/param_card_reweight.dat'
    shutil.copy(process_dir+'/Cards/param_card.dat', param_card_reweight)
    param_card_rwt_new=process_dir+'/Cards/param_card_rwt_%s.dat' % rw_name
    modify_param_card(param_card_input=param_card_reweight, process_dir=process_dir,params=params_rwt, output_location=param_card_rwt_new)

    rwcard.write("launch --rwgt_info=%s\n" % rw_name)
    rwcard.write("%s\n" % param_card_rwt_new)
  rwcard.close()


print_cards()

#---------------------------------------------------------------------------
# Generate the events    
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True) 

#---------------------------------------------------------------------------                                                                                                                                                                       
# Metadata                                                                                                                                                          
#---------------------------------------------------------------------------
if initialGluons:
	evgenConfig.process = "g g > xd xd~ z"
	initialStateString = "gluon fusion"
else:
	evgenConfig.process = "p p > xd xd~ z"
	initialStateString = "b quark annihilation"

evgenConfig.description = "Pseudoscalar 2HDMa model for MonoVhad"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Timothy Knight <timothy.michael.knight@cern.ch>"]

#---------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------
from MadGraphControl.MadGraphUtils import check_reset_proc_number

check_reset_proc_number(opts)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Teach pythia about the dark matter particle
genSeq.Pythia8.Commands += ["SLHA:allowUserOverride = on",
                            "1000022:all = xd xd~ 2 0 0 %d 0.0 0.0 0.0 0.0" % (int(THDMparams['MXd'])),
                            "1000022:isVisible = false",
			    			"1000022:mayDecay = off"
                            ]


#particle data = name antiname spin=2s+1 3xcharge colour mass width (left out, so set to 0: mMin mMax tau0)
genSeq.Pythia8.Commands += ["1000022:all = xd xd~ 2 0 0 %d 0.0 0.0 0.0 0.0" % (int(THDMparams['MXd'])),
                            "1000022:isVisible = false"]

#---------------------------------------------------------------------------
# Filters
#---------------------------------------------------------------------------

# Force Z->had decay in Pythia
genSeq.Pythia8.Commands += ["23:onIfAny= 21 1 -1 2 -2 3 -3 4 -4 5 -5"]



# Generator filter
	
include("GeneratorFilters/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 100*GeV

