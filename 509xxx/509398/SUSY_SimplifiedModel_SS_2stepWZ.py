include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )
from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

# setup mass information
masses['1000001'] = float(JOName.split('_')[4]) #sdown L
masses['1000002'] = float(JOName.split('_')[4]) #sup L
masses['1000003'] = float(JOName.split('_')[4]) #sstrange L
masses['1000004'] = float(JOName.split('_')[4]) #scharm L

masses['2000001'] = float(JOName.split('_')[4]) #sdown R
masses['2000002'] = float(JOName.split('_')[4]) #sup R
masses['2000003'] = float(JOName.split('_')[4]) #sstrange R
masses['2000004'] = float(JOName.split('_')[4]) #scharm R

masses['1000024'] = float(JOName.split('_')[5]) #chi1+
masses['1000023'] = float(JOName.split('_')[6]) #chi20
masses['1000022'] = float(JOName.split('_')[7]) #chi10

# setup gen & decay type
gentype = 'SS'
decaytype = '2stepWZ'

# setup process
process = ''' 
generate p p > susylq susylq~  @1
add process p p > susylq susylq~ j  @2
add process p p > susylq susylq~ j j  @3
''' 

# setup decay
decays["1000001"]="""DECAY   1000001     1.31836627E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2    -1000024      2   # BR(~d_L -> ~chi_1- u)"""
decays["2000001"]="""DECAY   2000001     3.95203891E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2    -1000024      2   # BR(~d_R -> ~chi_1- u)"""

decays["1000002"]="""DECAY   1000002     1.31301150E+01   # sup_L decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2     1000024      1   # BR(~u_L -> ~chi_1+ d)"""
decays["2000002"]="""DECAY   2000002     5.45626401E+00   # sup_R decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2     1000024      1   # BR(~u_R -> ~chi_1+ d)"""

decays["1000003"]="""DECAY   1000003     1.31836627E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2    -1000024      4   # BR(~s_L -> ~chi_1- c)"""
decays["2000003"]="""DECAY   2000003     3.95203891E+00   # sstrange_R decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2    -1000024      4   # BR(~s_R -> ~chi_1- c)"""

decays["1000004"]="""DECAY   1000004     1.31301150E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2     1000024      3   # BR(~c_L -> ~chi_1+ s)"""
decays["2000004"]="""DECAY   2000004     5.45626401E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2     1000024      3   # BR(~c_R -> ~chi_1+ s)"""

decays["1000024"]="""DECAY   1000024     7.00367294E-03   # chargino1+ decays                                                                                            
#          BR         NDA      ID1       ID2
1.00000000E+00         2     1000023      24  # BR(~chi_1+ -> ~chi_20  W+)
0.0000000              3     1000023      -11       12 # BR(~chi_1+ -> ~chi_20 e+ v)"""

decays["1000023"] = """DECAY   1000023     9.37327589E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
1.00000000E+00         2     1000022      23  # BR(~chi_20 -> ~chi_10   Z )
0.0000000              3     1000022       11      -11 # BR(~chi_20 -> ~chi_10 e- e+) """

# setup basic information
evgenConfig.contact  = [ "liuya@cern.ch" ]
evgenConfig.description = 'SUSY Simplified Model with squark production and decays via WZ with MadGraph/Pythia8, m_sq = %s GeV, m_N2 = %s GeV, m_C1 = %s GeV, m_N1 = %s GeV'%(masses['1000001'],masses['1000023'],masses['1000024'],masses['1000022'])
evgenConfig.keywords += ['simplifiedModel','squark']


njets = 2
include( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

#--------------------------------------------------------------
# Pythia configuration
#--------------------------------------------------------------
# This comes after all Simplified Model setup files
evgenLog.info('Will use Pythia8...')

pythia = genSeq.Pythia8

pythia.Commands += ["24:mMin = 0.2"]
pythia.Commands += ["23:mMin = 0.2"]


#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
    else:
        genSeq.Pythia8.UserHook = "JetMergingaMCatNLO"
