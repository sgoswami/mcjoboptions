#Job bookkeping infos
evgenConfig.description = 'aMcAtNlo Zvv+0,1,2,3j NLO FxFx HT2-biased, pT(vv)>70 GeV, BFilter'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch","jan.kretzschmar@cern.ch"]
evgenConfig.keywords += ['Z','jets', 'neutrino', 'invisible', 'monojet']
evgenConfig.generators += ["aMcAtNlo"]

# FxFx match+merge eff: 33.4%
# BFilter - eff ~9.6%
# one LHE file contains 50000 events
evgenConfig.nEventsPerJob = 2000
evgenConfig.inputFilesPerJob = 2

# Shower/merging settings                                                                                                                   
parton_shower='PYTHIA8'
nJetMax=3
qCut=20.

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO                                                 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)                                                     
PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut

include("Pythia8_i/Pythia8_FxFx_A14mod.py")

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
include("GeneratorFilters/BHadronFilter.py")
HeavyFlavorBHadronFilter.BottomEtaMax = 2.9
HeavyFlavorBHadronFilter.BottomPtMin = 5*GeV
HeavyFlavorBHadronFilter.RequireTruthJet = True
HeavyFlavorBHadronFilter.JetPtMin = 10*GeV
HeavyFlavorBHadronFilter.JetEtaMax = 2.9
HeavyFlavorBHadronFilter.TruthContainerName = "AntiKt4TruthJets"
HeavyFlavorBHadronFilter.DeltaRFromTruth = 0.4
filtSeq += HeavyFlavorBHadronFilter
