import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={     
'central_pdf':13300,     
'pdf_variations': [13300,26400,26500,26100,26200],#None, #[26400,26500,26100,26200],     
'alternative_pdfs':[13300,26500,26400,26100,26200],#  None,# [26400,26500,26100,26200],     
'scale_variations':[0.5,1.,2.]
}

# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=4*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
#mode=0

process = """
    import model sm
    define p = a g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate a a > l+ l-  
    output -f"""


process_dir = new_process(process) 
#Fetch default LO run_card.dat and set parameters 
settings = {'lhe_version':'3.0', 
            'cut_decays' :'F', 
            'lpp1'      : '1',
            'lpp2'      : '1',
            'fixed_fac_scale1' : 'F',
            'fixed_fac_scale2' : 'F',
            'ptl' : '3.5',
            'mmll':'60',
            'nevents'   : int(nevents) } 

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings) 

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=False)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

evgenConfig.generators = ["MadGraph"]

runName='PROC_sm_0/run_01'  
############################
# Shower JOs will go here
#runName=

include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_DD_Common.py") 
include("Pythia8_i/Pythia8_ShowerWeights.py")
    
include("Pythia8_i//Pythia8_MadGraph.py")

include('GeneratorFilters/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3500.
MultiLeptonFilter.Etacut = 2.5
MultiLeptonFilter.NLeptons = 2

include('GeneratorFilters/ChargedTrackFilter.py')
chtrkfilter = filtSeq.ChargedTracksFilter
chtrkfilter.NTracks=-1
chtrkfilter.NTracksMax=20
chtrkfilter.Ptcut=500.
chtrkfilter.Etacut=2.5

#-------------------------------------------------------------- 
# Evgen 
#-------------------------------------------------------------- 
evgenConfig.description = 'MadGraphPy8EG_yyWW'
evgenConfig.keywords+=["SM", "Z"]
evgenConfig.contact = ["kristin.lohwasser@cern.ch"]
