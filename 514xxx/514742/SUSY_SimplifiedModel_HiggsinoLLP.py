import math

def GetDeltaMassHiggsino(m_chi):
  # input : Chargino mass [GeV]
  # output: Chargino mass - Neutralino mass [MeV]
  M = m_chi # GeV
  mZ = 91.1876 # GeV
  if False: # old (PUB NOTE version)
    GF = 1.16637e-5 # GeV^-2
    sinW2 = 0.23126
    mW = 80.385 # GeV
    a2 = GF * mW * mW * 4 * math.sqrt(2) / (4*math.pi)
    c = a2 / (4*math.pi)
    deltaM = c * M * sinW2 * LoopFunction(mZ/M) # GeV
    return deltaM
  else: # I think this formula is better.
    alpha = 1./128.952 # at Z mass
    return M * alpha/2 * LoopFunction(mZ/M) / (2*math.pi)

def frange(start, end, step):
  l = [start]
  n = start
  if step < 0:
    while n - step > end:
      n = n + step
      l.append(n)
  else:
    while n + step < end:
      n = n + step
      l.append(n)
  return l

import bisect
def GetDeltaMassHiggsinoFromLifetime(lifetime):
  # input : Chargino lifetime [nsec]
  # output: Chargino mass - Neutralino mass [MeV]
  dmmin = 140
  dmmax = 2000
  lifemin = 3e-05
  lifemax = 3
  dmstep = 0.1
  if lifetime > lifemax:
    print 'Lifetime exceeds the upper limit, setting deltaM for the lifetime of '+str(lifemax)+' ns'
    lifetime = lifemax
  if lifetime < lifemin:
    print 'Lifetime exceeds the lower limit, setting deltaM for the lifetime of '+str(lifemin)+' ns'
    lifetime = lifemin
  lifelist = [GetCharginoLifetimeFromDeltaM(dm) for dm in frange(dmmax, dmmin, -dmstep)]
  index = bisect.bisect_left(lifelist, lifetime)
  key1 = lifelist[index-1]
  key2 = lifelist[index]
  value1 = dmmax - (index-1) * dmstep
  value2 = dmmax - (index-0) * dmstep
  return (value1 + (lifetime - key1) / (key2 - key1) * (value2 - value1)) / 1e3 # MeV -> GeV

def GetDeltaMassHiggsino_1703_09675(m_chi):
  M = m_chi # GeV
  mZ = 91.1876 # GeV
  alpha_2_mZ_sin2 = 355 # MeV
  return alpha_2_mZ_sin2 * (1 - (3 * mZ) / (2 * math.pi * M)) / 1e3 # MeV -> GeV

def LoopFunction(x):
  x2 = x*x
  A = math.sqrt(1-x2/4)
  C = -x2 + x2*x2*math.log(x) + 4*x*(1+x2/2)*A*math.atan(2/x*A)
  return C

def GetDecayWidthPion(deltaM):
  # input : Chargino mass - Neutralino mass [MeV]
  # output: DecayWidth (c1 -> n1 + pion) [1/nsec]
  dm      = deltaM # MeV
  m_pi    = 139.57018 # MeV
  m_mu    = 105.6583715 # MeV
  dl_pimu = 1 / 26.033 # nsec^-1
  dl = dl_pimu * (16 * math.pow(dm,3)/(m_pi*math.pow(m_mu,2)))*math.sqrt(1-math.pow(m_pi/dm,2))*math.pow(1-math.pow(m_mu/m_pi,2),-2)
  dl *= 0.5
  return dl

def GetDecayWidthLepton(deltaM, m_lep):
  # input : Chargino mass - Neutralino mass [MeV], Lepton mass [MeV]
  # output: DecayWidth (c1 -> n1 + lep + nu) [1/nsec]
  dm      = deltaM # MeV
  Gf      = 1.1663787*(1e-5)*(1e-6) # Fermi Coupling Constant (MeV ^-2)
  hbarc   = 1.0545718e-34*2.9979246e+08*(1e-6)/1.6021766e-19 # MeV*m
  dl_el   = 2.*math.pow(Gf,2)/(15.*math.pow(3.1415927,3))*math.pow(dm,5) # MeV
  dl_el *= 1 / hbarc*2.9979246e+08*(1e-9) # MeV -> nsec
  dl_el *= 0.5
  ml = m_lep # MeV
  bl = ml / dm
  dl_el *= CoefficientForLepton(bl)
  return dl_el

def GetDecayWidthElectron(deltaM):
  # input : Chargino mass - Neutralino mass [MeV]
  # output: DecayWidth (c1 -> n1 + ele + nu) [1/nsec]
  m_ele = 0.511; # MeV
  return GetDecayWidthLepton(deltaM, m_ele)

def GetDecayWidthMuon(deltaM):
  # input : Chargino mass - Neutralino mass [MeV]
  # output: DecayWidth (c1 -> n1 + mu + nu) [1/nsec]
  m_mu = 105.6583715 # MeV
  return GetDecayWidthLepton(deltaM, m_mu)

def CoefficientForLepton(bl):
  # bl = m_l / delta m
  # ref : Scott Thomas and James D. Wells. Phenomenology of massive vertorlike doublet leptons. Phys. Rev. Lett., Vol. 81, pp. 34-37, Jul 1998.
  bl2 = bl  * bl
  bl4 = bl2 * bl2
  return math.sqrt(1-bl2) * (1 - 9./2*bl2 - 4*bl4 + 15*bl4/2/math.sqrt(1-bl2)*math.atanh(math.sqrt(1-bl2)))

def GetCharginoLifetimeFromDeltaM(deltaM):
  # input : Chargino mass - Neutralino mass [MeV]
  # output: Chargino lifetime [nsec]
  totalWidth = GetDecayWidthPion(deltaM)
  totalWidth += GetDecayWidthElectron(deltaM) + GetDecayWidthMuon(deltaM)
  return 1. / totalWidth


include ('MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

# get the file name and split it on _ to extract relavent information
jobConfigParts = JOName.split("_")

gentype = str(jobConfigParts[2])
decaytype = str(jobConfigParts[3])
masses['1000022'] = float(jobConfigParts[4])

if gentype == "CpN":
    gentype = 'C1pN1'
elif gentype == 'CmN':
    gentype = 'C1mN1'
elif gentype == 'CC':
    gentype = 'C1C1'

if masses['1000022']<0.5: masses['1000022']=0.5

# ----------------------------------------------
#   delta m (C1,N1) [GeV] and lifetime [ns]
# ----------------------------------------------

mN = int (masses['1000022'])
deltaM = -1.000 
lifetime = -1.000

# ---------------------------------------------
# pure higgsino mass, deltaM, lifetime 
# for more information: arxiv/hep-ph/9804359 and arXiv:1703.09675
#  95 GeV: (254,0.067), 100 GeV: (257,0.064), 120 GeV: (269,0.055), 140 GeV: (278, 0.049), 
# 150 GeV: (281,0.048), 160 GeV: (284,0.046), 200 GeV: (296,0.040), 240 GeV: (304, 0.037),
# 1000 GeV: (340, 0.026)
# ---------------------------------------------
  
deltaM =  GetDeltaMassHiggsino(mN)
lifetime = GetCharginoLifetimeFromDeltaM(GetDeltaMassHiggsino(mN)*1e3) # GeV->MeV

if re.match('\d+p\d+', jobConfigParts[5]):
  lifetime = float(jobConfigParts[5].replace('p', '.'))
else:
  raise RunTimeError("ERROR: lifetime is not correctly set")

# deltaM = GetDeltaMassHiggsinoFromLifetime(lifetime)

masses['1000024'] = masses['1000022'] + deltaM
masses['1000023'] = masses['1000022'] * (-1)

# ----------------------------------------------
#   process lines
# ----------------------------------------------

if gentype == "C1pN1":
  process = '''
generate p p > x1+ n1 $ susystrong @1
add process p p > x1+ n1 j $ susystrong @2
add process p p > x1+ n1 j j $ susystrong @3
'''
  mergeproc = "{x1+,1000024}{n1,1000022}"
elif gentype == "C1mN1":
  process = '''
generate p p > x1- n1 $ susystrong @1
add process p p > x1- n1 j $ susystrong @2
add process p p > x1- n1 j j $ susystrong @3
'''
  mergeproc = "{x1-,-1000024}{n1,1000022}"
elif gentype == "C1C1":
  process = '''
generate p p > x1+ x1- $ susystrong @1
add process p p > x1+ x1- j $ susystrong @2
add process p p > x1+ x1- j j $ susystrong @3
'''
  mergeproc = "{x1+,1000024}{x1-,-1000024}"
njets = 2

# ----------------------------------------------
#   generator filter 
# ----------------------------------------------

evt_multiplier = 4

if len(jobConfigParts) > 6 and 'MET' in jobConfigParts[6]:
  include ('GeneratorFilters/MissingEtFilter.py')
  metFilter = jobConfigParts[6]
  metFilter = int(metFilter.split("MET")[1].split(".")[0])
  print "Using MET Filter: " + str(metFilter)
  filtSeq.MissingEtFilter.METCut = metFilter*GeV
  filtSeq.MissingEtFilter.UseChargedNonShowering = True
  evt_multiplier = 10
else:
  print "No MET Filter applied"

# ----------------------------------------------
#   evgen configs 
# ----------------------------------------------

EleBR = 0.030
MuBR  = 0.015

dsid = int((runArgs.jobConfig[0])[-6:])
evgenLog.info('Registered generation of ~chi1+/- ~chi20 production (long-lived higgsinos)'+str(dsid))

evgenConfig.contact  = ["keisuke.yoshihara@cern.ch"]
evgenConfig.keywords += ['simplifiedModel','gaugino', 'neutralino', 'chargino', 'longLived']
evgenConfig.description = '~chi1+/- ~chi20 production (long-lived higgsinos), m_C1 = %s GeV, m_N1 = %s GeV, lifetime = %s ns' %(masses['1000024'],masses['1000022'], lifetime)

if lifetime != 0:
   evgenConfig.specialConfig = 'AMSBC1Mass=%s*GeV;AMSBN1Mass=%s*GeV;AMSBC1ToEleBR=%s;AMSBC1ToMuBR=%s;AMSBC1Lifetime=%s*ns;preInclude=SimulationJobOptions/preInclude.AMSB.py' % (masses['1000024'],masses['1000022'],EleBR,MuBR,lifetime)

gentype = "C1N2" # dummy gentype to set xqcut correctly

# AGENE-1542: disable syscalc and set event_norm explicitly
# extras['event_norm']='sum'
# extras['use_syst']='F'

include ('MadGraphControl/SUSY_SimplifiedModel_PostInclude.py')

# ----------------------------------------------
#   pythia8 commands etc 
# ----------------------------------------------

genSeq.Pythia8.Commands += ["1000022:all = Chi Chi~ 1 0 0 %s" % (masses['1000022'])]

if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = pp>%s" % mergeproc,
                                 "1000022:spinType = 1",
                                 "1000024:spinType = 1" ]

bonus_file = open('pdg_extras.dat','w')
bonus_file.write( '1000024 Chargino 100.0 (MeV/c) boson Chargino 1\n')
bonus_file.write( '-1000024 Anti-chargino 100.0 (MeV/c) boson Chargino -1\n')
bonus_file.close()
 
testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'

import os
os.system("get_files %s" % testSeq.TestHepMC.G4ExtraWhiteFile)
