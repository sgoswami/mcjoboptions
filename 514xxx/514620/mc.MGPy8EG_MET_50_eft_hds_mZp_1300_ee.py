model="InelasticVectorEFT"
fs = "ee"
mDM1 = 650.
mDM2 = 2600.
mZp = 1300.
mHD = 125.

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Jared Little <jared.little@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")
