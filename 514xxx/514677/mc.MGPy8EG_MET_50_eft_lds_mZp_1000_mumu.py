model="InelasticVectorEFT"
fs = "mumu"
mDM1 = 5.
mDM2 = 1030.
mZp = 1000.
mHD = 125.

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Jared Little <jared.little@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")
