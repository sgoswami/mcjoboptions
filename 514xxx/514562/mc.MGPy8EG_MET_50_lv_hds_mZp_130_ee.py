model="LightVector"
fs = "ee"
mDM1 = 65.
mDM2 = 260.
mZp = 130.
mHD = 125.

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")

