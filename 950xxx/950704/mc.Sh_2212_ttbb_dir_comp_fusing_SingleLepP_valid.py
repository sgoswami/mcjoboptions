include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa 2.2.12, Fused direct component l(positive)+jets"
evgenConfig.keywords = ["SM", "top" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "lars.ferencz@cern.ch" ]
evgenConfig.nEventsPerJob = 20000

genSeq.Sherpa_i.RunCard="""
(run){
  # ME and clustering options
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  QSF:=1;
  SCALES METS{H_TM2/16}{0.25*P_TM2}{H_TM2/16};
  CORE_SCALE QCD; 
  PP_RS_SCALE VAR{H_TM2/16}{0.25*P_TM2}{H_TM2/16};
  EXCLUSIVE_CLUSTER_MODE 1;
  CSS_RESPECT_Q2=1
  CSS_KMODE=34
  CSS_KIN_SCHEME=0
  EVENT_GENERATION_MODE Weighted;
  PP_HPSMODE 0;
  MI_HANDLER=None
  ABS_ERROR=2
 
  % fusing settings 
  SHERPA_LDADD=SherpaFusing; 
  USERHOOK = Fusing_Direct;
  FUSING_DIRECT_FACTOR 1.;

  # massive b parameters
  MASSIVE[5]=1
  MASS[5]=4.75
  CSS_SCALE_SCHEME 2
  CSS_EVOLUTION_SCHEME 30

  # decays
  HARD_DECAYS=1
  STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;
  HDH_STATUS[24,2,-1]=0
  HDH_STATUS[24,4,-3]=0
  HDH_STATUS[-24,-12,11]=0
  HDH_STATUS[-24,-14,13]=0 
  HDH_STATUS[-24,-16,15]=0
  SPECIAL_TAU_SPIN_CORRELATIONS=1
  SOFT_SPIN_CORRELATIONS=1

}(run)

(processes){
  Process 93 93 -> 6 -6 5 -5
  Order (*,0)
  CKKW sqr(100);
  NLO_QCD_Mode MC@NLO
  Loop_Generator LOOPGEN
  ME_Generator Amegic
  PSI_ItMin 20000
  RS_ME_Generator Comix
  End process
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]

genSeq.Sherpa_i.CleanupGeneratedFiles = 1

# Variations
genSeq.Sherpa_i.Parameters += [ "SCALE_VARIATIONS=0.25,0.25 0.25,1. 1.,0.25 1.,1. 1.,4. 4.,1. 4.,4." ]
