include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/PDF4LHC21.py")

evgenConfig.description = "Sherpa Z/gamma* -> e e + 0,1,2j@NLO + 3,4j@LO."
evgenConfig.keywords = ["SM", "Z", "2electron", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]

genSeq.Sherpa_i.RunCard="""

# me generator settings
ME_SIGNAL_GENERATOR:
  - Comix
  - Amegic
  - OpenLoops

EW_SCHEME: Gmu
GF: 1.166397e-5

PROCESSES:
  - 93 93 -> 11 -11 93{4}:
      Order: {QCD: 0, EW: 2}
      CKKW: 20
      2->2-4:
        NLO_QCD_Mode: MC@NLO
        ME_Generator: Amegic
        RS_ME_Generator: Comix
        Loop_Generator: OpenLoops
      2->6:
        Max_N_Quarks: 4
        Max_Epsilon: 0.01
      2->3-6:
        Integration_Error: 0.99

SELECTORS:
  - [Mass, 11, -11, 40.0, E_CMS]
"""

genSeq.Sherpa_i.NCores = 48

evgenConfig.nEventsPerJob = 10000
