evgenConfig.description = 'MG5_aMC@NLO+MadSpin+Pythia8+EvtGen ttbar production, pp collisions'
evgenConfig.generators    = ['aMcAtNlo', 'Pythia8', 'EvtGen']
evgenConfig.keywords = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact  = [ "dominic.hirschbuehl@cern.ch" ]
evgenConfig.inputFilesPerJob=1

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
