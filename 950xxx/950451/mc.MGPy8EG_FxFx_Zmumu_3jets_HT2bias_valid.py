import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

#Job bookkeping infos
evgenConfig.description = 'aMcAtNlo Zmumu+3Np FxFx HT2-biased'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch"]
evgenConfig.keywords += ['Z','jets']
evgenConfig.generators += ["aMcAtNlo"]

# General settings                                                                                                                               
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob=1

# Shower/merging settings                                                                                                                   
parton_shower='PYTHIA8'
nJetMax=3
qCut=20.

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO                                                 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)                                                     
PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut
print "PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax
print "PYTHIA8_qCut = %i"%PYTHIA8_qCut

include("Pythia8_i/Pythia8_FxFx_A14mod.py")
