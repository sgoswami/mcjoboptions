#--------------------------------------------------------------
# MadGraph
#--------------------------------------------------------------
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

process_def = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > t t~
output -f"""

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0', 
             'cut_decays':'F', 
             'nevents':int(nevents)}

process_dir = new_process(process_def)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

lhe_filename = outputDS.split(".tar.gz")[0] + ".events"

# configure Herwig7
Herwig7Config.me_pdf_commands(order="LO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=lhe_filename, me_pdf_order="NLO")
# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'MadGraph+Herwig7+EvtGen ttbar'
evgenConfig.keywords    = [ 'SM', 'top']
evgenConfig.tune        = "H7.1-Default"
evgenConfig.contact     = [ 'jens.roggel@cern.ch' ]
evgenConfig.generators += ["MadGraph", "Herwig7", "EvtGen"]
evgenConfig.nEventsPerJob = 10000

