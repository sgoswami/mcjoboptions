#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 dib production with A14 NNPDF2.3 tune JZ5"
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet", "bbbar", "bottom"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg", "Pythia8"]

evgenConfig.nEventsPerJob = 500
filterMulti = 3.7  # filter efficiency 8~31% 
if hasattr(runArgs,'ecmEnergy') and runArgs.ecmEnergy < 13000.:
    filterMulti *= 4

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg jj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_bb_Common.py")

# go hard on Born suppression factor - very hard to reach TeV level!
PowhegConfig.bornsuppfact = 3600

# use default factorisation+renormalisation scales
# PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
# PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          = [14000, 304400, 25300, 14200, 14100, 14300, 14400, 25100, 91500, 42560, 317500, 303600, 319300, 319500, 322500, 322700, 322900, 323100, 323300, 323500, 323700, 323900, 27400, 331500]
PowhegConfig.rwl_group_events = 100000

PowhegConfig.foldcsi      = 10
PowhegConfig.foldphi      = 10
PowhegConfig.foldy        = 10
PowhegConfig.ncall1       = 100000
PowhegConfig.ncall2       = 100000
PowhegConfig.nubound      = 10000000
PowhegConfig.itmx1        = 5
PowhegConfig.itmx2        = 5

PowhegConfig.storemintupb = 0 # smaller grids

PowhegConfig.nEvents = runArgs.maxEvents*filterMulti if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMulti

# PDF variations with nominal scale variation:
# CT18NNLO,	 
# NNPDF31_nnlo_as_0118_hessian, 
# MMHT2014nnlo68cl, 
# CT18ANNLO, 
# CT18ZNNLO, 
# CT18XNNLO, 
# CT18NLO, 
# MMHT2014nlo68cl, 
# PDF4LHC15_nnlo_mc, 
# ABMP16_5_nnlo, 
# NNPDF31_nnlo_as_0118_nojets, 
# NNPDF31_nnlo_as_0118
# NNPDF31_nnlo_as_0116
# NNPDF31_nnlo_as_0120
# NNPDF31_nnlo_as_0108
# NNPDF31_nnlo_as_0110
# NNPDF31_nnlo_as_0112
# NNPDF31_nnlo_as_0114
# NNPDF31_nnlo_as_0117
# NNPDF31_nnlo_as_0119
# NNPDF31_nnlo_as_0122
# NNPDF31_nnlo_as_0124
# MSHT20nnlo_as118

# full PDF set variations cost a lot of time
#PowhegConfig.PDF.extend(range(14001, 14059))                            # Include the CT18NNLO error set
#PowhegConfig.PDF.extend(range(304401, 304501))                          # Include the NNPDF31_nnlo_as_0118_hessian error set
#PowhegConfig.PDF.extend(range(27401, 27465))                            # Include the MSHT20nnlo_as118 error set
# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.6)
include("GeneratorFilters/JetFilter_JZX.py")
from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MinPt = minDict[5]*GeV
