include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/EW_scheme_sinthetaW_mZ.py")
include("Sherpa_i/Fusing_Direct.py")
genSeq.Sherpa_i.Parameters += [ "FUSING_DIRECT_FACTOR=1." ]

evgenConfig.description = "Sherpa Z -> vv + bb"
evgenConfig.keywords = ["SM", "Z", "2neutrinos", "b-jets", "NLO","4FS" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch" ]
evgenConfig.nEventsPerJob = 500

genSeq.Sherpa_i.RunCard="""                             
(run){
  %scales, tags for scale variations                              
  FSF:=1.; RSF:=1.; QSF:=1.;                                                                                                                           
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};                          
  
  % Speed Setup Increase (HtPrime)
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};
  % Negative Weight reduction 
  NLO_CSS_PMODE=1

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
  
  % Fusing directing component for 
  FUSING_DIRECT_FACTOR 1.;
  MASSIVE[5]=1

  % EW corrections setup
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  METS_BBAR_MODE=5

}(run)                               

(processes){   
  Process 93 93 -> 91 91 5 -5;
  Order (*,2); 
  CKKW 100000.0;	
  Associated_Contributions EW|LO1|LO2|LO3 {2};
  Enhance_Observable VAR{log10(PPerp(p[2]+p[3]))}|2|3.3 {4}
  NLO_QCD_Mode MC@NLO; 
  ME_Generator Amegic; 
  RS_ME_Generator Comix; 
  Loop_Generator LOOPGEN;  
  Integration_Error 0.99 {4};
  End process;                                         
}(processes)   

(selector){ 
  Mass 91 91 2.0 E_CMS
  PT2NLO 91 91 100.0 E_CMS
}(selector) 
"""

genSeq.Sherpa_i.NCores = 16
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppllj","pplljj", "ppllj_ew", "pplljj_ew"]

