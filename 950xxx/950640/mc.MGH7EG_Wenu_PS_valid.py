### Metadata
evgenConfig.description = 'MadGraph+Herwig7+EvtGen Wenu from LHE files'
evgenConfig.generators    = ['MadGraph', 'Herwig7', 'EvtGen']
evgenConfig.keywords+=['W','jets']
evgenConfig.contact  = ["jens.roggel@cern.ch", "dominic.hirschbuehl@cern.ch" ]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

#--------------------------------------------------------------
# Herwig7 (H72UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files

include("Herwig7_i/Herwig72_LHEF.py")

# @Dominic Please check/change the H7 settings 
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118_nf_4", max_flav=4)
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

