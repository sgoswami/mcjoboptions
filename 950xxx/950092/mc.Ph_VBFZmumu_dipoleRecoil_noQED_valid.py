#--------------------------------------------------------------
# Pythia8 showering with the AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

include('Pythia8_i/Pythia8_Powheg.py')
genSeq.Pythia8.Commands += ['Powheg:NFinal = 3']
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']
genSeq.Pythia8.Commands += ['TimeShower:QEDshowerByL = off' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 VBF Z production with AZNLO CTEQ6L1 tune and dipole recoil shower and QED switched off.'
evgenConfig.keywords    = [ 'SM', 'VBF', 'Z' ]
evgenConfig.contact     = [ 'chris.g@cern.ch' ]
evgenConfig.generators  = [ "Powheg", "Pythia8", "EvtGen"] 
evgenConfig.nEventsPerJob = 50000
evgenConfig.inputFilesPerJob = 10

