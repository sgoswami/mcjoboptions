from MadGraphControl.MadGraphUtils import *

if hasattr(runArgs, 'ecmEnergy'):
        beamEnergy = runArgs.ecmEnergy / 2.
else:
        raise RuntimeError("No center-of-mass energy provided in the Gen_tf.py command, exiting now.")

if ((charge*10)%1) == 0:
        X = int(charge * 10)
        Y = 10
elif ((charge*3)%1) == 0:
        X = int(charge *3)
        Y = 3
else :
	X = int(charge *4)
	Y = 4

PdgId = int(20000000+X*1000+Y*10)
charge_nume = X
charge_deno = Y
print 'Numerator of charge is', charge_nume, ', denominator part is', charge_deno

run_card_extras = {}

print 'DY, mass is', mass, 'GeV, charge is +/-', charge, 'e'
evgenConfig.description="Drell-Yan fcp generation for mass = %i GeV, charge = +/- %.1fe with MadGraph5_aMC@NLO+Pythia8, NNPDF23LO pdf and A14 tune in MC16" % (mass, charge)
runName = 'MGPy8.Fraccharges_DY_M%iZ%iv%ip0' % (mass, charge_nume, charge_deno)
process_str="import model LFCP_photonplusZ_UFO\ngenerate p p > fcp%iv%i fcp%iv%i~\noutput -f" % (charge_nume, charge_deno, charge_nume, charge_deno)
evgenConfig.keywords += ['exotic','drellYan','BSM','longLived']
run_card_extras['lhaid'] = '247000' # NNDF23_lo_as_0130_qed pdf set
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

run_card_extras['pdlabel'] = 'lhapdf'
run_card_extras['lhe_version'] = '2.0'
run_card_extras['nevents'] = str(int(evgenConfig.nEventsPerJob*1.1)) # 10% safety margin because some events may fail Pythia hadronization

process_dir = new_process(process_str)

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=run_card_extras)

param_card_extras = { "MASS": { 'Mfcp%iv%i' %(charge_nume, charge_deno): mass}} # changing mass in the param_card
modify_param_card(process_dir=process_dir, params=param_card_extras)

print_cards()

generate(process_dir=process_dir, grid_pack=False, runArgs=runArgs)

arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=2, saveProcDir=True)

evgenConfig.contact = ['quanyin.li@cern.ch']

evgenConfig.specialConfig = "MASS=%i;CHARGE=%.2f;X=%i;Y=%i;preInclude=SimulationJobOptions/preInclude.fcp.py;MDT_QballConfig=True;InteractingPDGCodes=[%i,%i]" % (mass, charge, X, Y, PdgId, (-1)*PdgId)

import os, sys

# Edit G4particle_whitelist.txt with MCPs

ALINE3="%i   fcp%iv%i  %i (Mev/c) lepton 0" %(PdgId,charge_nume,charge_deno,1000*mass)
ALINE4="%i   fcp%iv%ibar  %i (Mev/c) lepton 0" %((-1)*PdgId,charge_nume,charge_deno,1000*mass)

pdgmod = os.path.isfile('G4particle_whitelist.txt')
if pdgmod is True:
	os.remove('G4particle_whitelist.txt')
os.system('get_files -data G4particle_whitelist.txt')
f=open('G4particle_whitelist.txt','a')
f.writelines(str(ALINE3))
f.writelines('\n')
f.writelines(str(ALINE4))
f.writelines('\n')
f.close()

f=open('pdg_extras.dat','w')
#  The most important number is the first: the PDGID of the particle
f.writelines(str(ALINE3))
f.writelines('\n')
f.writelines(str(ALINE4))
f.writelines('\n')
f.close()
testSeq.TestHepMC.UnknownPDGIDFile='pdg_extras.dat'

del ALINE3
del ALINE4

include("Pythia8_i/Pythia8_MadGraph.py")

# These two lines below fix an issue with Pythia 8.205 not recognizing multi-charged particles. For details please check the https://groups.cern.ch/group/hn-atlas-Generators/Lists/Archive/Flat.aspx?RootFolder=%2Fgroup%2Fhn-atlas-Generators%2FLists%2FArchive%2FError%20in%20Pythiacheck%20unknown%20particle%20code&FolderCTID=0x012002006E9F14B8795719469C62A1525BB20B42
exec('genSeq.Pythia8.Commands += [\"'+str(int(PdgId))+':all = fcp'+str(int(charge_nume))+'v'+str(int(charge_deno))+' fcp'+str(int(charge_nume))+'v'+str(int(charge_deno))+'bar 2 '+str(charge*3)+' 0 '+str(mass)+'\"]')
exec('genSeq.Pythia8.Commands += [\"-'+str(int(PdgId))+':all = fcp'+str(int(charge_nume))+'v'+str(int(charge_deno))+'bar fcp'+str(int(charge_nume))+'v'+str(int(charge_deno))+' 2 '+str(charge*(-3))+' 0 '+str(mass)+'\"]')
