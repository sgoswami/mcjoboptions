## pythia shower
keyword=['SM','top', 'ttgamma', 'photon']
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1
evgenConfig.keywords += keyword
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.description = 'aMcAtNlo+Py8_ttgamma_nonallhad_GamFromProd_A14_var3cUp'
evgenConfig.contact = ["amartya.rej@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
include("GeneratorFilters/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
