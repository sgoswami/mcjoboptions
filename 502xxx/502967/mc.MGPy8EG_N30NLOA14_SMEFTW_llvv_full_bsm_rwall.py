# definitions that will be used for process
definitions="""define l+ = e+ mu+ ta+ 
define l- = e- mu- ta-
"""

# process (EFTORDER: NP<=1)
processes={'llvv' : 'generate p p > l+ l- vl vl~ SMHLOOP==0 EFTORDER',
           'evmv' : 'generate p p > e+ ve mu- vm~ SMHLOOP==0 EFTORDER\n add process p p > e- ve~ mu+ vm SMHLOOP==0 EFTORDER\n',
           'WWlvlv' : 'generate p p > w- | w- > l+ l- vl vl~ SMHLOOP==0 EFTORDER',
}

# cuts and other run card settings
settings={'dynamical_scale_choice':'4',
          'ptl':'15', 
          'etal':'3', 
          'mmnl':'20',
          'drll':'0.1', 
}

# param card settings 
params={}

# EFT parameters with non-zero values for initial sample
smeftpars_bsm={'cW': 1, 
               'cHDD': 1, 
               'cHWB': 1, 
               'cHj1': 1, 
               'cHj3': 1, 
               'cHu': 1, 
               'cHd': 1, 
               'clj1': 1, 
               'clj3': 1, 
               'clu': 1, 
               'cld': 1}

# parameters used in reweighting:
relevant_coeffs = 'cW cHDD cHWB cHj1 cHj3 cHu cHd cHl1 cHl3 cll1 clj1 clj3 clu cld'.split()

evgenConfig.description = 'lvlv production with SMEFTsim'
evgenConfig.keywords+=['SM','diboson','WW','ZZ','electroweak']
evgenConfig.generators = ["MadGraph","Pythia8","EvtGen"]
evgenConfig.contact = ['Hannes Mildner <hannes.mildner@cern.ch>']

include("MadGraphControl_SMEFT.py")
