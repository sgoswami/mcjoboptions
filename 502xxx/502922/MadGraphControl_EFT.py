from MadGraphControl.MadGraphUtils import *
import fnmatch
import os
import sys

nevents=int(1.2*runArgs.maxEvents)
fixed_scale = 345. # ~ m(top)+m(top)

process_string=str(0)

# Input process comes from individual JO
if input_process == 'sstt':
    process_string='generate p p > t t NP=1 NPcpv=0, (t > b w+ NP=0 NPcpv=0, w+ > l+ vl NP=0 NPcpv=0)'
elif input_process == 'sstbartbar':
    process_string='generate p p > t~ t~ NP=1 NPcpv=0, (t~ > b~ w- NP=0 NPcpv=0, w- > l- vl~ NP=0 NPcpv=0)'

else: 
    raise RuntimeError("Process %s not recognised in these jobOptions."%input_process)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#not sure about some of these but let's keep them for now
process_str="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~       
define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ g u c d s b u~ c~ d~ s~ b~   
define zdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ g u c d s b u~ c~ d~ s~ b~   
define qdec = u c u~ c~
import model SMEFTsim_general_MwScheme_UFO-massless_sstt           
"""+process_string+"""
output -f
"""
###
# Parameters in the model parsed via JO
# cuuRe1313, cuuRe1323, cuuRe2323, cqu1Re1313, cqu1Re1323, cqu1Re2313, cqu1Re2323, cqu8Re1313, cqu8Re1323, cqu8Re2313, cqu8Re2323 
model_pars_str = str(jofile) 
cuuRe1313=float(0)
cuuRe1323=float(0)
cuuRe2323=float(0)
cqu1Re1313=float(0)
cqu1Re1323=float(0)
cqu1Re2313=float(0)
cqu1Re2323=float(0)
cqu8Re1313=float(0)
cqu8Re1323=float(0)
cqu8Re2313=float(0)
cqu8Re2323=float(0)

for s in model_pars_str.split("_"):
    print('jobConfig fragment used to extract the model configuration '+s)
    if 'cuu' in s:
        ss=s.replace("cuu", "")
        if 'p' in ss:
            sss=ss.replace("p",".")
        else:
            sss = ss
        cuuRe1313 = float(sss)
        cuuRe1323 = float(sss)
        cuuRe2323 = float(sss)
        print "cuu WC set to %f"%cuuRe1313
    if 'cqu1' in s:
        ss=s.replace("cqu1", "")
        if 'p' in ss:
            sss=ss.replace("p",".")
        else:
            sss = ss
        cqu1Re1313 = float(sss)
        cqu1Re1323 = float(sss)
        cqu1Re2313 = float(sss)
        cqu1Re2323 = float(sss)
        print "cqu1 WC set to %f"%cqu1Re1313
    if 'cqu8' in s:
        ss=s.replace("cqu8", "")
        if 'p' in ss:
            sss=ss.replace("p",".")
        else:
            sss = ss
        cqu8Re1313 = float(sss)
        cqu8Re1323 = float(sss)
        cqu8Re2313 = float(sss)
        cqu8Re2323 = float(sss)
        print "cqu8 WC set to %f"%cqu8Re1313
    

#skip for now except nevents
extras  = { 'lhe_version':'3.0',
            'cut_decays':'F',
    'maxjetflavor'          : 5,
    'pdlabel'               : 'lhapdf',
    'lhaid'                 : 262000,
    'use_syst'              : 'False',
    'ptj'                   : '0.0',
    'ptl'                   : '0.0',
    'etaj'                  : '-1.0',
    'etal'                  : '-1.0',
    'drjj'                  : '0.0',
    'drll'                  : '0.0',
    'drjl'                  : '0.0',
    'dynamical_scale_choice': '0',
    'fixed_ren_scale'       : 'True',
    'fixed_fac_scale'       : 'True',
    'scale'                 : fixed_scale,
    'dsqrt_q2fact1'         : fixed_scale,
    'dsqrt_q2fact2'         : fixed_scale,
            'nevents' :int(nevents) }

process_dir = new_process(process_str)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

# SM param card settings
params = dict()
params['mass'] = dict()
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
   3.377000e-01   2   -1   2
   3.377000e-01   2   -3   4
   1.082000e-01   2  -11  12
   1.082000e-01   2  -13  14
   1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'

#EFT param card is modified 
parameters={'cuuRe1313': cuuRe1313, 'cuuRe1323': cuuRe1323, 'cuuRe2323': cuuRe2323, 'cqu1Re1313': cqu1Re1313, 'cqu1Re1323': cqu1Re1323, 'cqu1Re2313': cqu1Re2313, 'cqu1Re2323': cqu1Re2323,'cqu8Re1313': cqu8Re1313, 'cqu8Re1323': cqu8Re1323, 'cqu8Re2313': cqu8Re2313, 'cqu8Re2323': cqu8Re2323}
params={}
params['SMEFT']=parameters
modify_param_card(process_dir=process_dir,params=params)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Lepton filter
if not hasattr(filtSeq,"LeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()   
    filtSeq.LeptonFilter.Ptcut = 10000.0#MeV

check_reset_proc_number(opts)
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_MadGraph.py")
#new lines to avoid crach when no pythia
#theApp.finalize()
#theApp.exit()

evgenConfig.description = 'MadGraph control same-sign top EFT'
evgenConfig.keywords+=['Top']
evgenConfig.contact = ['Noemi Cavalli <noemi.cavalli@cern.ch>']
