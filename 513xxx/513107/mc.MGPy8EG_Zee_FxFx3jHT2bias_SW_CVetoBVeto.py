evgenConfig.description = 'aMcAtNlo Zee+0,1,2,3j NLO FxFx HT2-biased CVetoBVeto Py8 ShowerWeights'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch","jan.kretzschmar@cern.ch"]
evgenConfig.keywords += ['SM', 'Z', 'electron', 'jets', 'NLO']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 29.6%
# CVetoBVeto - eff ~72.5%
# one LHE file contains 50000 events
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

include("MadGraphControl_Vjets_FxFx_shower.py")
