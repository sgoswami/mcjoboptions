evgenConfig.description = 'aMcAtNlo Wmunu+0,1,2,3j NLO FxFx HT2-biased, m>105GeV CVetoBVeto Py8 ShowerWeights'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch","jan.kretzschmar@cern.ch"]
evgenConfig.keywords += ['SM', 'W', 'muon', 'jets', 'NLO']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 30.2%
# CVetoBVeto - eff ~71.5%
# one LHE file contains 55000 events
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

include("MadGraphControl_Vjets_FxFx_shower.py")
