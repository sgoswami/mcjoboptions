evgenConfig.description = 'aMcAtNlo Ztautau+0,1,2,3j NLO FxFx HT2-biased, low mass 10<m<40GeV, BFilter Py8 ShowerWeights'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch","jan.kretzschmar@cern.ch"]
evgenConfig.keywords += ['SM', 'Z', 'tau', 'jets', 'NLO']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 27.4%
# BFilter+dilep eff ~2.5%
# one LHE file contains 32000 events
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 23

# restore the resonances (23)->(-15)+(15) in LHE files, if not present
restoreResonances = [[-15, 15, 23]]

include("MadGraphControl_Vjets_FxFx_shower.py")

include("GeneratorFilters/MultiElecMuTauFilter.py")
MultiElecMuTauFilter = filtSeq.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons       = 2
MultiElecMuTauFilter.MinPt          = 5000.
MultiElecMuTauFilter.IncludeHadTaus = True
MultiElecMuTauFilter.MinVisPtHadTau = 5000.
MultiElecMuTauFilter.MaxEta         = 10.
