gKKtR=4.348788447
gKKtL=4.348788447
MKK=2500
WGKK=2500*0.3

decay_str='decay t > w+ b, w+ >  fall fall\ndecay t~ > w- b~, w- > fall fall\n'
evgenConfig.nEventsPerJob = 10000

include ( "MadGraphControl_KKgluon_tt.py" )
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
# Semi-leptonic decay filter
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #no-allhad
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
