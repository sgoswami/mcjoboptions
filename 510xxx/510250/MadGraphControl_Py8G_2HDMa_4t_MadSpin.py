import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import re

# to check lepton filter
import os
### get MC job-options filename
FIRST_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
jofiles = [f for f in os.listdir(FIRST_DIR) if (f.startswith('mc') and f.endswith('.py'))]
filter_string=jofiles[0].split('_')[-1].replace(".py","")

#----------------------------------------------------------------------------
# Process definition
#----------------------------------------------------------------------------
initialGluons=False
#
# Specical syntax:  see the 2HDM+a white paper (https://arxiv.org/pdf/1810.09420.pdf)
# QCD<=2: Four-top production from NP processes, including interference terms amongH,A,a
# QED<=2: Four-top production including both SM and NP contributions and their interference.
# QED<=0: Four-top production within the SM.
#
process="""
import model Pseudoscalar_2HDM -modelname
define p = g d u s c b d~ u~ s~ c~ b~
define j = g d u s c b d~ u~ s~ c~ b~
generate p p > t t~ t t~ / a z h1 QCD<=2
output -f
"""

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------
nevents=evgenConfig.nEventsPerJob
if LHE_EventMultiplier>0:
  nevents=runArgs.maxEvents*LHE_EventMultiplier

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {}
if initialGluons:
  extras = {
        'maxjetflavor'  : 4,
        'asrwgtflavor'  : 4,
        'lhe_version'   : '3.0',
        'cut_decays'    : 'F',
        'nevents'       : nevents,
    }
else:
  extras = {
        'maxjetflavor'  : 5,
        'asrwgtflavor'  : 5,
        'lhe_version'   : '3.0',
        'cut_decays'    : 'F',
        'nevents'       : nevents,
    }

# Build run_card
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------

# Build param_card.dat

params={}
## blocks might be modified
dict_blocks={
"mass": ["MB", "MXd", "mh2", "mh3", "mhc", "mh4"],
"DMINPUTS" : ["gPXd", ],
"FRBlock": ["tanbeta", "sinbma", ],
"Higgs": ["lam3", "laP1", "laP2", "sinp"],
}

for bl in dict_blocks.keys():
  for pa in dict_blocks[bl]:
    if pa in THDMparams.keys():
      if bl not in params: params[bl]={}
      if pa=="MB": 
        params[bl]["5"]=THDMparams[pa]
      else:
        params[bl][pa]=THDMparams[pa]

## auto calculation of decay width
THDMparams_decay={
"25": "Auto",
"35": "Auto",
"36": "Auto",
"37": "Auto",
"55": "Auto",
}

params["decay"]=THDMparams_decay

print("Updating parameters:")
print(params)

modify_param_card(process_dir=process_dir,params=params)

# Build reweight_card.dat
if reweight:
  # Create reweighting card
  reweight_card_loc=process_dir+'/Cards/reweight_card.dat'
  rwcard = open(reweight_card_loc,'w')

  for rw_name in reweights:
    params_rwt = params.copy()
    for param in rw_name.split('-'):
      param_name, value = param.split('_')
      if param_name == "SINP":
        params_rwt['HIGGS']['SINP'] = value
      elif param_name == "TANB":
        params_rwt['FRBLOCK']['TANBETA'] = value

    param_card_reweight = process_dir+'/Cards/param_card_reweight.dat'
    #shutil.copy(process_dir+'/Cards/param_card_default.dat', param_card_reweight)
    shutil.copy(process_dir+'/Cards/param_card.dat', param_card_reweight)
    param_card_rwt_new=process_dir+'/Cards/param_card_rwt_%s.dat' % rw_name
    modify_param_card(param_card_input=param_card_reweight, process_dir=process_dir,params=params_rwt, output_location=param_card_rwt_new)

    rwcard.write("launch --rwgt_info=%s\n" % rw_name)
    rwcard.write("%s\n" % param_card_rwt_new)
  rwcard.close()

print_cards()

# Build MadSpin card
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')                                                                                                                                    
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
decay t > w+ b
decay t~ > w- b~
decay w+ > all all
decay w- > all all
# running the actual code
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()


#---------------------------------------------------------------------------
# Generate the events    
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True) 

#---------------------------------------------------------------------------                                                                                                                                                                       
# Metadata                                                                                                                                                          
#---------------------------------------------------------------------------
if initialGluons:
  evgenConfig.process = "p p > t t~ t t~ / a z h1 QCD<=2"
  initialStateString = "gluon fusion"

evgenConfig.description = "Pseudoscalar 2HDMa model for 4-top events"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Johanna Gramling <jgramlin@cern.ch>", "Lailin Xu <lailin.xu@cern.ch>"]

#---------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Teach pythia about the dark matter particle
genSeq.Pythia8.Commands += ["SLHA:allowUserOverride = on",
                            "1000022:all = xd xd~ 2 0 0 %d 0.0 0.0 0.0 0.0" % (int(THDMparams['MXd'])),
                            "1000022:isVisible = false",
                            "1000022:mayDecay = off"
                            ]


#particle data = name antiname spin=2s+1 3xcharge colour mass width (left out, so set to 0: mMin mMax tau0)
genSeq.Pythia8.Commands += ["1000022:all = xd xd~ 2 0 0 %d 0.0 0.0 0.0 0.0" % (int(THDMparams['MXd'])),
                            "1000022:isVisible = false"]

#---------------------------------------------------------------------------
# Filters
#---------------------------------------------------------------------------

# Generator filter
if (filter_string == "1L"):
    evgenLog.info('1lepton filter applied')

    include('GeneratorFilters/LeptonFilter.py' )
    filtSeq.LeptonFilter.Ptcut  = 20000.
    filtSeq.LeptonFilter.Etacut = 2.8
