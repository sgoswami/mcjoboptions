import os

from AthenaCommon.Include import include
from MadGraphControl.MadGraphUtils import *

lplus = {
    "mu" : "mu+",
    "e" : "e+",
}
lminus = {
    "mu" : "mu-",
    "e" : "e-",
}
nplus= {
    "mu" : "vm",
    "e" : "ve"
}
nminus = {
    "mu" : "vm~",
    "e" : "ve~",
}

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
model_string = phys_short.split('_')[2]
mhnl = float(phys_short.split('_')[2].replace("HNL",""))
#W -> lep1 N, N -> lep2 W*, W* -> lep3 v, tested for HNLs produced in on-shell W decay
channel = phys_short.split('_')[4]
LNCV = phys_short.split('_')[5]

lep = [None]*3
digit = 0
for i in range(3):
    if channel[digit:digit+2]=='mu':
        lep[i] = 'mu'
        print(lep[i])
        digit+=2
    elif channel[digit:digit+1]=='e':
        lep[i] = 'e'
        digit+=1
    else:
        evgenLog.error("lepton %i does not have correct type, "%i)

if len(channel[digit:])!=0:
    evgenLog.error("channel name too long, check")
processline = "pp>mue mue mue nx"

evgenLog.info('process line: %s' %processline)
ctaustring = phys_short.split('_')[3].replace("ctau","")
if int(str(ctaustring)) == 0:
    ctau = -1.0
elif int(str(ctaustring[:1])) != 0:
    ctau = float(str(ctaustring))
else:
    ctau = 0.1*float(str(ctaustring))
if ctau > 0:
    tofoption = 0
else:
    tofoption= -1

evgenLog.info('physics short: %s' %phys_short)

evgenLog.info('Processing model with HNL mass and average lifetime: (mhnl, ctau) = (%e,%e)' %(mhnl, ctau))

genline1 = "import model SM_HeavyN_Gen3Mass_NLO"
genline2 = "define p = u c d s b u~ c~ d~ s~ b g"
genline3 = "define j = p"
genline4 = "define mue = mu+ mu- e+ e-"
genline5 = "define nx = vm vm~ ve ve~"
if LNCV == "LNV":
    genline6 = "generate p p > "+lplus[lep[0]]+" n1, n1 > "+lplus[lep[1]]+" "+lminus[lep[2]]+" "+nminus[lep[2]]+" @ 0"
    genline7 = "add process p p > "+lplus[lep[0]]+" n1 j, n1 > "+lplus[lep[1]]+" "+lminus[lep[2]]+" "+nminus[lep[2]]+" @ 1"
    genline8 = "add process p p > "+lplus[lep[0]]+" n1 j j, n1 > "+lplus[lep[1]]+" "+lminus[lep[2]]+" "+nminus[lep[2]]+" @ 2"
    genline9 = "add process p p > "+lminus[lep[0]]+" n1, n1 > "+lminus[lep[1]]+" "+lplus[lep[2]]+" "+nplus[lep[2]]+" @ 3"
    genline10 = "add process p p > "+lminus[lep[0]]+" n1 j, n1 > "+lminus[lep[1]]+" "+lplus[lep[2]]+" "+nplus[lep[2]]+" @ 4"
    genline11 = "add process p p > "+lminus[lep[0]]+" n1 j j, n1 > "+lminus[lep[1]]+" "+lplus[lep[2]]+" "+nplus[lep[2]]+" @ 5"
elif LNCV == "LNC":
    genline6 = "generate p p > "+lplus[lep[0]]+" n1, n1 > "+lminus[lep[1]]+" "+lplus[lep[2]]+" "+nplus[lep[2]]+" @ 0"
    genline7 = "add process p p > "+lplus[lep[0]]+" n1 j, n1 > "+lminus[lep[1]]+" "+lplus[lep[2]]+" "+nplus[lep[2]]+" @ 1"
    genline8 = "add process p p > "+lplus[lep[0]]+" n1 j j, n1 > "+lminus[lep[1]]+" "+lplus[lep[2]]+" "+nplus[lep[2]]+" @ 2"
    genline9 = "add process p p > "+lminus[lep[0]]+" n1, n1 > "+lplus[lep[1]]+" "+lminus[lep[2]]+" "+nminus[lep[2]]+" @ 3"
    genline10 = "add process p p > "+lminus[lep[0]]+" n1 j, n1 > "+lplus[lep[1]]+" "+lminus[lep[2]]+" "+nminus[lep[2]]+" @ 4"
    genline11 = "add process p p > "+lminus[lep[0]]+" n1 j j, n1 > "+lplus[lep[1]]+" "+lminus[lep[2]]+" "+nminus[lep[2]]+" @ 5"
else:
    evgenLog.error("you did not specify correctly if you are interested in LNC or LNV, please check")
genline12 = "output -f"
gen_process = genline1+"\n"+genline2+"\n"+genline3+"\n"+genline4+"\n"+genline5+"\n"+genline6+"\n"+genline7+"\n"+genline8+"\n"+genline9+"\n"+genline10+"\n"+genline11+"\n"+genline12

# General settings
gridpack_mode=False
# Merging settings
maxjetflavor=5
ickkw=0
nJetMax=2
ktdurham=30
dparameter=0.4

if mhnl/4 > 30:
    ktdurham = mhnl/4
    
evgenLog.info('ktdurham set to %i' %ktdurham)    
process=processline
evgenConfig.process = processline
evgenLog.info('process for jet merging set to %s' %processline)

if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevents=runArgs.maxEvents*evt_multiplier
    else:    
        nevents=500*evt_multiplier

run_settings = {'lhe_version':'3.0',
                'pdlabel'    : "'lhapdf'",
                'lhaid'      : 260000,
                'ickkw'      : '0',
                'ktdurham'   : ktdurham,
                'maxjetflavor':5, # 5 flavor scheme
                'asrwgtflavor':5, # 5 flavor scheme
                'xptb':0,
                'etal':10,
                'ptl':0,
                'ptj':20.,
                'drjj':0.0,
                'xqcut':0.,
                'nhel' : 0,   #helicity
                'bwcutoff': 15.0,
                'time_of_flight': tofoption
}

run_settings['use_syst']='F'
run_settings['nevents'] = nevents

# Set up the process
process_dir = new_process(gen_process)
# Set up the run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

# this is a dictionary
params = {}

params['mass']={'9900012':mhnl,'9900014':1e10,'9900016':1e10}
params['numixing']={'1':0,'2':0,'3':0,'4':0,'5':0,'6':0,'7':0,'8':0,'9':0}

if ctau > 0:
    width= 197.4635212e-15/ctau # hbar*c [GeV*mm] / c*tau [mm]                 
    params['DECAY']={'9900012':'DECAY 9900012 {} # WN1'.format(width)}
else: 
    params['DECAY']={'9900012':'DECAY  9900012 Auto # WN1'}

if lep[0]=="mu" and lep[1]=="mu":
    params['numixing']={'1':0,'2':0,'3':0,'4':1,'5':0,'6':0,'7':0,'8':0,'9':0}
elif lep[0]=="e" and lep[1]=="e":
    params['numixing']={'1':1,'2':0,'3':0,'4':0,'5':0,'6':0,'7':0,'8':0,'9':0}
elif (lep[0]=="mu" and lep[1]=="e") or (lep[0]=="e" and lep[1]=="mu"):       
    params['numixing']={'1':1,'2':0,'3':0,'4':1,'5':0,'6':0,'7':0,'8':0,'9':0}
else:
    egenLog.error("This is not a valid HNL process")

modify_param_card(process_dir=process_dir,params=params)

# Generate the events
generate(process_dir=process_dir,runArgs=runArgs)

# Remember to set saveProcDir to FALSE before sending for production!!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Set the evgen metadata
evgenConfig.description = 'HNL %s (with matching), m_hnl = %s GeV, channel = %s '%(model_string, mhnl, channel)
evgenConfig.keywords = ["exotic","BSM","neutrino"]
evgenConfig.contact = ["Monika Wielers <monika.wielers@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Reset the number of processes for Pythia8
check_reset_proc_number(opts)

if nJetMax>0:
    genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on",
                                "Merging:doKTMerging = on",
                                "Merging:ktType = 1",
                                "Merging:nJetMax = %i"%nJetMax,
                                "Merging:Process = %s"%process,
                                "Merging:TMS = %f"%run_settings['ktdurham'],
                                "Merging:nQuarksMerge = %s" %run_settings['maxjetflavor'],
                                "Merging:Dparameter = %f"%dparameter
                                ]

testSeq.TestHepMC.MaxTransVtxDisp = 200000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 1000000 #in mm
