#--------------------------------------------------------------
#EVGENconfiguration
#--------------------------------------------------------------

#Provideconfiginformation
evgenConfig.generators+=["aMcAtNlo","Herwig7"]
evgenConfig.tune="H7.1-Default"
evgenConfig.description='MG5_aMC@NLO+Herwig7+EvtGen,H7p1defaulttune,MENNPDF3.0NLO,withtwoleptonfilterfromDSID412121LHEfiles'
evgenConfig.keywords=['SM','top','ttbar','lepton']
evgenConfig.contact=['aknue@cern.ch']
evgenConfig.nEventsPerJob=10000
evgenConfig.inputFilesPerJob=6

#initializeHerwig7generatorconfigurationforshoweringofLHEfiles
include("Herwig7_i/Herwig7_LHEF.py")

#configureHerwig7
Herwig7Config.me_pdf_commands(order="NLO",name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile,me_pdf_order="NLO")


#addEvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

#runHerwig7
Herwig7Config.run()

##Dileptonfilter
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons=2
filtSeq.TTbarWToLeptonFilter.Ptcut=0.

filtSeq.Expression="TTbarWToLeptonFilter"
