# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:50
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.74089217E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.86491810E+03  # scale for input parameters
    1    2.25432030E+02  # M_1
    2    2.16881551E+02  # M_2
    3    3.04058602E+03  # M_3
   11    3.18654628E+03  # A_t
   12   -1.99350905E+03  # A_b
   13   -1.15651825E+03  # A_tau
   23   -9.97037509E+02  # mu
   25    4.71651081E+01  # tan(beta)
   26    4.93610967E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.43830917E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.21540497E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.16242433E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.86491810E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.86491810E+03  # (SUSY scale)
  1  1     8.38625643E-06   # Y_u(Q)^DRbar
  2  2     4.26021827E-03   # Y_c(Q)^DRbar
  3  3     1.01312714E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.86491810E+03  # (SUSY scale)
  1  1     7.94820793E-04   # Y_d(Q)^DRbar
  2  2     1.51015951E-02   # Y_s(Q)^DRbar
  3  3     7.88213165E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.86491810E+03  # (SUSY scale)
  1  1     1.38702040E-04   # Y_e(Q)^DRbar
  2  2     2.86791829E-02   # Y_mu(Q)^DRbar
  3  3     4.82336677E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.86491810E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.18654628E+03   # A_t(Q)^DRbar
Block Ad Q=  2.86491810E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.99350901E+03   # A_b(Q)^DRbar
Block Ae Q=  2.86491810E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.15651825E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.86491810E+03  # soft SUSY breaking masses at Q
   1    2.25432030E+02  # M_1
   2    2.16881551E+02  # M_2
   3    3.04058602E+03  # M_3
  21    2.32099274E+07  # M^2_(H,d)
  22   -8.24613410E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.43830917E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.21540497E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.16242433E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.24229940E+02  # h0
        35     4.93138180E+03  # H0
        36     4.93610967E+03  # A0
        37     4.93027640E+03  # H+
   1000001     1.01000366E+04  # ~d_L
   2000001     1.00749936E+04  # ~d_R
   1000002     1.00996651E+04  # ~u_L
   2000002     1.00784307E+04  # ~u_R
   1000003     1.01000493E+04  # ~s_L
   2000003     1.00750156E+04  # ~s_R
   1000004     1.00996774E+04  # ~c_L
   2000004     1.00784325E+04  # ~c_R
   1000005     2.52917443E+03  # ~b_1
   2000005     4.20976524E+03  # ~b_2
   1000006     2.52375978E+03  # ~t_1
   2000006     3.25219372E+03  # ~t_2
   1000011     1.00215969E+04  # ~e_L-
   2000011     1.00085486E+04  # ~e_R-
   1000012     1.00208263E+04  # ~nu_eL
   1000013     1.00216570E+04  # ~mu_L-
   2000013     1.00086521E+04  # ~mu_R-
   1000014     1.00208817E+04  # ~nu_muL
   1000015     1.00342531E+04  # ~tau_1-
   2000015     1.00425815E+04  # ~tau_2-
   1000016     1.00366909E+04  # ~nu_tauL
   1000021     3.45858629E+03  # ~g
   1000022     2.27920244E+02  # ~chi_10
   1000023     2.40599291E+02  # ~chi_20
   1000025     1.01707224E+03  # ~chi_30
   1000035     1.01890828E+03  # ~chi_40
   1000024     2.40752974E+02  # ~chi_1+
   1000037     1.02018181E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.11078256E-02   # alpha
Block Hmix Q=  2.86491810E+03  # Higgs mixing parameters
   1   -9.97037509E+02  # mu
   2    4.71651081E+01  # tan[beta](Q)
   3    2.43301821E+02  # v(Q)
   4    2.43651787E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.94870459E-01   # Re[R_st(1,1)]
   1  2     1.01157152E-01   # Re[R_st(1,2)]
   2  1    -1.01157152E-01   # Re[R_st(2,1)]
   2  2    -9.94870459E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99946545E-01   # Re[R_sb(1,1)]
   1  2     1.03396049E-02   # Re[R_sb(1,2)]
   2  1    -1.03396049E-02   # Re[R_sb(2,1)]
   2  2    -9.99946545E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -7.90221301E-01   # Re[R_sta(1,1)]
   1  2     6.12821585E-01   # Re[R_sta(1,2)]
   2  1    -6.12821585E-01   # Re[R_sta(2,1)]
   2  2    -7.90221301E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.97395194E-01   # Re[N(1,1)]
   1  2    -5.07587858E-02   # Re[N(1,2)]
   1  3    -5.01970510E-02   # Re[N(1,3)]
   1  4    -1.03261540E-02   # Re[N(1,4)]
   2  1     5.48779155E-02   # Re[N(2,1)]
   2  2     9.95082378E-01   # Re[N(2,2)]
   2  3     8.06691033E-02   # Re[N(2,3)]
   2  4     1.70871691E-02   # Re[N(2,4)]
   3  1     2.57951826E-02   # Re[N(3,1)]
   3  2    -4.64289967E-02   # Re[N(3,2)]
   3  3     7.04976315E-01   # Re[N(3,3)]
   3  4    -7.07239247E-01   # Re[N(3,4)]
   4  1    -3.90621386E-02   # Re[N(4,1)]
   4  2     7.12667883E-02   # Re[N(4,2)]
   4  3    -7.02837924E-01   # Re[N(4,3)]
   4  4    -7.06692328E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.93078585E-01   # Re[U(1,1)]
   1  2    -1.17451798E-01   # Re[U(1,2)]
   2  1     1.17451798E-01   # Re[U(2,1)]
   2  2    -9.93078585E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99690352E-01   # Re[V(1,1)]
   1  2     2.48837314E-02   # Re[V(1,2)]
   2  1     2.48837314E-02   # Re[V(2,1)]
   2  2     9.99690352E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02349893E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.94840066E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.01135858E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     6.52461537E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.49603924E-03    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44726941E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     7.07558114E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.19468806E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.18234161E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     7.58256774E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.00463353E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     8.23553804E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.05563622E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.88534541E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.01330246E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.22515166E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.05370410E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     3.12866524E-03    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44888454E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     7.06802103E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.19121958E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     8.68834106E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.30498946E-03    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     5.99797599E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     8.22640897E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.72223277E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.54565284E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.89758027E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.20934547E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.14888383E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.58853688E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     6.10000717E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.59685324E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     2.12918533E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     9.00580547E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.53374524E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.58828680E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.62016815E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.22803394E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44731695E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.03682662E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.83913157E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.08212227E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.53373790E-03    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08418675E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     3.69644850E-04    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44893129E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.03567718E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.83598406E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.08092285E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.53092951E-03    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.07759768E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     1.46225601E-03    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.90472690E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.89088131E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.16075264E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     8.23612339E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.92844963E-03    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.66404970E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.35858891E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.24484094E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.95107750E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.91002095E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.55817380E+02   # ~d_L
#    BR                NDA      ID1      ID2
     3.06435601E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.70377058E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.50111991E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     3.52687555E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.15881149E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.58983196E-03    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.21924157E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.24575512E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.94997073E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.90860363E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.55867099E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.06431053E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.70344191E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.79542215E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     3.81923897E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.15873671E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.59445817E-03    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.21871675E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     9.09119843E+01   # ~b_1
#    BR                NDA      ID1      ID2
     7.25987462E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.19256767E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.20174165E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.19506834E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.35662619E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     3.98139740E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.32605105E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.83701608E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.22938820E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.73100955E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.72085071E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.59727610E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     3.42470415E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.64761807E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.06096581E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     5.31478028E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     6.43333446E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.41671858E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.48571689E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.05512025E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.64962022E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.55802097E+02   # ~u_L
#    BR                NDA      ID1      ID2
     9.45467929E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.93219595E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.00864811E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     2.38333423E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.17427372E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.21894480E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.41679130E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.48567793E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.05512503E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.64951302E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.55851741E+02   # ~c_L
#    BR                NDA      ID1      ID2
     9.45407471E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.93181346E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.03221949E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     2.40680098E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.17420636E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.29928961E-04    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.21841991E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     9.60495570E+01   # ~t_1
#    BR                NDA      ID1      ID2
     8.34532381E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     2.11992263E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     5.22691401E-02    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.11632804E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.86228086E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.91311545E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.27296071E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.26932561E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.37533736E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.48541947E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.76973454E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.93620109E-03    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.80704671E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.77094957E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.71206528E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.60557459E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     9.76425739E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     4.90314522E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     8.16232750E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     5.01693560E-09   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.42088513E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.26059354E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.14019789E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.13981483E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.03850850E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     7.16790389E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     7.69992171E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.20255942E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.00162836E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.91673676E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.50838917E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.23085211E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     5.15225957E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.54150079E-11   # chi^0_2
#    BR                NDA      ID1      ID2
     4.53531380E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     7.53832544E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     6.59699950E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     9.69294806E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     9.68663007E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     2.66555720E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.12697642E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.12482372E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.59674270E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.26178589E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     8.58428861E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.54839629E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.54839629E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     6.58643338E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.68869553E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     6.42533516E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.78462555E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.21995592E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.60387967E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     5.29685213E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     5.29685213E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     7.20586020E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     3.02444210E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.02444210E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.39123313E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.47497474E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     6.98810553E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.94088136E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.69392421E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.73923538E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     5.02453300E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.02453300E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     5.60952647E+01   # ~g
#    BR                NDA      ID1      ID2
     9.09197136E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     9.09197136E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.05418291E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.05418291E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     5.63745700E-03    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     5.63745700E-03    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.97527916E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.97527916E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.14215233E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     1.75305070E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.74053910E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     1.70374899E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.70374899E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.31723170E-03   # Gamma(h0)
     2.66792965E-03   2        22        22   # BR(h0 -> photon photon)
     1.63289487E-03   2        22        23   # BR(h0 -> photon Z)
     2.93560442E-02   2        23        23   # BR(h0 -> Z Z)
     2.46257154E-01   2       -24        24   # BR(h0 -> W W)
     8.34410660E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.90109132E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.18008904E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.28588762E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.39738161E-07   2        -2         2   # BR(h0 -> Up up)
     2.71210815E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.67916640E-07   2        -1         1   # BR(h0 -> Down down)
     2.05402056E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.46240830E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.19854603E+02   # Gamma(HH)
     1.55806218E-08   2        22        22   # BR(HH -> photon photon)
     3.60473780E-08   2        22        23   # BR(HH -> photon Z)
     2.09696071E-08   2        23        23   # BR(HH -> Z Z)
     3.51840321E-09   2       -24        24   # BR(HH -> W W)
     4.24643718E-06   2        21        21   # BR(HH -> gluon gluon)
     6.28992003E-09   2       -11        11   # BR(HH -> Electron electron)
     2.80103062E-04   2       -13        13   # BR(HH -> Muon muon)
     8.09034232E-02   2       -15        15   # BR(HH -> Tau tau)
     2.53626102E-14   2        -2         2   # BR(HH -> Up up)
     4.92029179E-09   2        -4         4   # BR(HH -> Charm charm)
     5.00930222E-04   2        -6         6   # BR(HH -> Top top)
     4.57579373E-07   2        -1         1   # BR(HH -> Down down)
     1.65529987E-04   2        -3         3   # BR(HH -> Strange strange)
     6.42067876E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.55419348E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     8.26993020E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     8.26993020E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     9.09393696E-05   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.45646876E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.92074549E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.25294416E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.40607038E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     6.01705632E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     8.61384373E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     8.90912622E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.50159651E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     9.59692172E-04   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     6.01774968E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.52921204E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.02128393E+02   # Gamma(A0)
     5.16561552E-10   2        22        22   # BR(A0 -> photon photon)
     4.56914358E-09   2        22        23   # BR(A0 -> photon Z)
     7.33326680E-06   2        21        21   # BR(A0 -> gluon gluon)
     6.07587399E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.70567801E-04   2       -13        13   # BR(A0 -> Muon muon)
     7.81493687E-02   2       -15        15   # BR(A0 -> Tau tau)
     2.25104898E-14   2        -2         2   # BR(A0 -> Up up)
     4.36692285E-09   2        -4         4   # BR(A0 -> Charm charm)
     4.51548273E-04   2        -6         6   # BR(A0 -> Top top)
     4.41962240E-07   2        -1         1   # BR(A0 -> Down down)
     1.59880968E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.20331289E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.75664843E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     9.00774380E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     9.00774380E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     7.68104555E-05   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.57172128E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     6.38883903E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.86081605E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     4.57073389E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     6.49244132E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     9.82904460E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     9.25727581E-03   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     3.54944388E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     8.87767574E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     7.21096105E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     6.22183551E-08   2        23        25   # BR(A0 -> Z h0)
     1.28469394E-11   2        23        35   # BR(A0 -> Z HH)
     2.57528146E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.54438191E+02   # Gamma(Hp)
     7.20227646E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.07919814E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     8.70972713E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.50155201E-07   2        -1         2   # BR(Hp -> Down up)
     7.64131440E-06   2        -3         2   # BR(Hp -> Strange up)
     7.13415058E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.13295209E-08   2        -1         4   # BR(Hp -> Down charm)
     1.62264544E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.99036559E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.11569826E-08   2        -1         6   # BR(Hp -> Down top)
     7.03869172E-07   2        -3         6   # BR(Hp -> Strange top)
     6.73046041E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.62383079E-06   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.76344882E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.25684145E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.55304102E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     7.16538881E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     4.67408078E-04   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     7.15189143E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.03864323E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     4.92628462E-08   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.91423817E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.22455600E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.22454742E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000386E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.45674391E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    4.49529639E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.91423817E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.22455600E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.22454742E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999992E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.30144937E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999992E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.30144937E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26159869E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.02750350E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.24791291E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.30144937E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999992E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.12383504E-04   # BR(b -> s gamma)
    2    1.59052407E-06   # BR(b -> s mu+ mu-)
    3    3.52318375E-05   # BR(b -> s nu nu)
    4    2.71186526E-15   # BR(Bd -> e+ e-)
    5    1.15846310E-10   # BR(Bd -> mu+ mu-)
    6    2.41649828E-08   # BR(Bd -> tau+ tau-)
    7    9.24177555E-14   # BR(Bs -> e+ e-)
    8    3.94803050E-09   # BR(Bs -> mu+ mu-)
    9    8.34307773E-07   # BR(Bs -> tau+ tau-)
   10    9.63849922E-05   # BR(B_u -> tau nu)
   11    9.95620069E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41942311E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93631891E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15723293E-03   # epsilon_K
   17    2.28165777E-15   # Delta(M_K)
   18    2.47864289E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28725127E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.06675070E-16   # Delta(g-2)_electron/2
   21   -1.31115648E-11   # Delta(g-2)_muon/2
   22   -3.72798942E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.18454259E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.79121921E-01   # C7
     0305 4322   00   2    -3.07535455E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.20613700E-02   # C8
     0305 6321   00   2    -4.92757968E-05   # C8'
 03051111 4133   00   0     1.61561307E+00   # C9 e+e-
 03051111 4133   00   2     1.61601651E+00   # C9 e+e-
 03051111 4233   00   2     5.74211489E-04   # C9' e+e-
 03051111 4137   00   0    -4.43830517E+00   # C10 e+e-
 03051111 4137   00   2    -4.43475388E+00   # C10 e+e-
 03051111 4237   00   2    -4.31712974E-03   # C10' e+e-
 03051313 4133   00   0     1.61561307E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61601632E+00   # C9 mu+mu-
 03051313 4233   00   2     5.74211142E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43830517E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43475407E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.31713038E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50458239E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     9.35854967E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50458239E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     9.35854988E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50458240E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     9.35860970E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85860968E-07   # C7
     0305 4422   00   2    -1.80191441E-05   # C7
     0305 4322   00   2     4.67815762E-07   # C7'
     0305 6421   00   0     3.30515438E-07   # C8
     0305 6421   00   2     8.74652777E-06   # C8
     0305 6321   00   2     4.75398188E-07   # C8'
 03051111 4133   00   2     1.80998579E-06   # C9 e+e-
 03051111 4233   00   2     1.07050670E-05   # C9' e+e-
 03051111 4137   00   2     9.84634907E-07   # C10 e+e-
 03051111 4237   00   2    -8.04871522E-05   # C10' e+e-
 03051313 4133   00   2     1.80997869E-06   # C9 mu+mu-
 03051313 4233   00   2     1.07050626E-05   # C9' mu+mu-
 03051313 4137   00   2     9.84642484E-07   # C10 mu+mu-
 03051313 4237   00   2    -8.04871654E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.75658004E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.74477739E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.75657625E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.74477739E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.75551933E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.74477738E-05   # C11' nu_3 nu_3
