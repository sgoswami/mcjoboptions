## Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")
include("Herwig7_i/Herwig71_EvtGen.py")

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.description = "DPS with WW decaying leptonically with H71UE default tune"
evgenConfig.keywords = [ "electroweak","MPI", "W"]
evgenConfig.contact  = [ "oleg.kuprash@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

## hard process setup
Herwig7Config.tune_commands()
Herwig7Config.me_pdf_commands(order="LO", name="NNPDF30_lo_as_0130")
command = """
cd /Herwig/MatrixElements
insert SubProcess:MatrixElements[0] MEqq2W2ff
set MEqq2W2ff:Process Leptons
cd /Herwig/Cuts
set LeptonKtCut:MinKT 5.0*GeV
set LeptonKtCut:MaxEta 3.0
set MassCut:MinM 0.*GeV
cd /Herwig/UnderlyingEvent/
create ThePEG::SimpleKTCut DPKtCut SimpleKTCut.so
set DPKtCut:MinKT 5
set DPKtCut:MaxEta 3.0
create ThePEG::Cuts DP1Cuts
set DP1Cuts:MHatMin 10
insert DP1Cuts:OneCuts 0 DPKtCut
cd /Herwig/UnderlyingEvent/
create ThePEG::SubProcessHandler DP1
insert DP1:MatrixElements 0 /Herwig/MatrixElements/MEqq2W2ff
set DP1:PartonExtractor /Herwig/Partons/PPExtractor
cd /Herwig/UnderlyingEvent/
insert MPIHandler:SubProcessHandlers 1 DP1
insert MPIHandler:Cuts 1 DP1Cuts
insert MPIHandler:additionalMultiplicities 0 1
"""

print(command)

Herwig7Config.add_commands(command)

## run the generator
Herwig7Config.run()
