## Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")
include("Herwig7_i/Herwig7_EvtGen.py")

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "H7.2-Default"
evgenConfig.description = "Herwig7 dijet LO"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact  = [ "yoran.yeh@cern.ch" ]
evgenConfig.nEventsPerJob = 5000

## Configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="LO", name="NNPDF30_lo_as_0130")

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6 )
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
include("GeneratorFilters/JetFilter_JZX.py")
from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MinPt = minDict[9]*GeV

command = """
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 2200.0*GeV

##  - - -   bias to high pt.
create Herwig::MergingReweight MPreWeight HwDipoleShower.so
insert /Herwig/MatrixElements/SubProcess:Preweights 0  MPreWeight
set MPreWeight:MaxPTPower 4
##

"""

print (command)

Herwig7Config.add_commands(command)

## run the generator
Herwig7Config.run()
