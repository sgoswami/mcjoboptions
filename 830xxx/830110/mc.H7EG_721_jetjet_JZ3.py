evgenConfig.description = 'Herwig7.2.1 NLO dijets with MMHT2014 and angular ordered shower, slice JZ1'
evgenConfig.keywords+=['SM','QCD','jets']
evgenConfig.contact = ['javier.llorente.merino@cern.ch']

from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigMatchbox import Hw7ConfigMatchbox

name = runArgs.jobConfig[0]
genSeq += Herwig7()

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "MMHT2014"

## initialize generator configuration object
Herwig7Config = Hw7ConfigMatchbox(genSeq, runArgs, run_name="HerwigMatchbox", beams="pp")

## configure generator
include("Herwig7_i/Herwig71_EvtGen.py")
Herwig7Config.me_pdf_commands(order="NLO", name="MMHT2014nlo68cl")
Herwig7Config.tune_commands()
 
Herwig7Config.add_commands("""
## Model assumptions
read Matchbox/StandardModelLike.in
read Matchbox/DiagonalCKM.in

## Set the order of the couplings
cd /Herwig/MatrixElements/Matchbox
set Factory:OrderInAlphaS 2
set Factory:OrderInAlphaEW 0

## Select the process
## You may use identifiers such as p, pbar, j, l, mu+, h0 etc.
do Factory:Process p p -> j j

#read Matchbox/MadGraph-NJet.in
read Matchbox/MadGraph-OpenLoops.in

## cuts on additional jets
cd /Herwig/Cuts/
read Matchbox/DefaultPPJets.in

insert JetCuts:JetRegions 0 FirstJet
set FirstJet:PtMin 50*GeV

## Scale choice
cd /Herwig/MatrixElements/Matchbox
set Factory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/MaxJetPtScale

##  - - -   bias to high pt.
cd /Herwig/MatrixElements/Matchbox
create Herwig::MergingReweight MPreWeight HwDipoleShower.so
insert Factory:Preweighters 0  MPreWeight
set MPreWeight:MaxPTPower 4
set /Herwig/Samplers/MonacoSampler:Kappa 0.02
##

## Matching and shower selection
read Matchbox/MCatNLO-DefaultShower.in

## PDF choice
read Matchbox/FiveFlavourNoBMassScheme.in
read Matchbox/MMHT2014.in

do /Herwig/MatrixElements/Matchbox/Factory:ProductionMode
""")

#Jet filter
include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(3,filtSeq);

# Using author's default
Herwig7Config.sampler_commands("MonacoSampler", 20000, 4, 50000, 1, 100)

if runArgs.generatorRunMode == 'build':
  Herwig7Config.do_build(10)

elif runArgs.generatorRunMode == 'integrate':
  Herwig7Config.do_integrate(runArgs.generatorJobNumber)

elif runArgs.generatorRunMode == 'run':
  Herwig7Config.do_run()
