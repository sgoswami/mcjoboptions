# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.05.2022,  16:00
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.36198777E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.41904893E+03  # scale for input parameters
    1    2.12757459E+02  # M_1
    2   -6.90276940E+02  # M_2
    3    2.98992100E+03  # M_3
   11   -1.71991897E+02  # A_t
   12    1.32242350E+03  # A_b
   13   -1.28921402E+03  # A_tau
   23   -3.73874675E+02  # mu
   25    5.35759778E+01  # tan(beta)
   26    3.60282581E+03  # m_A, pole mass
   31    1.23763490E+03  # M_L11
   32    1.23763490E+03  # M_L22
   33    1.02237489E+03  # M_L33
   34    1.67845364E+02  # M_E11
   35    1.67845364E+02  # M_E22
   36    1.64390403E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.89538417E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.87064191E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.41208294E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.41904893E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.41904893E+03  # (SUSY scale)
  1  1     8.38583250E-06   # Y_u(Q)^DRbar
  2  2     4.26000291E-03   # Y_c(Q)^DRbar
  3  3     1.01307592E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.41904893E+03  # (SUSY scale)
  1  1     9.02810366E-04   # Y_d(Q)^DRbar
  2  2     1.71533970E-02   # Y_s(Q)^DRbar
  3  3     8.95304983E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.41904893E+03  # (SUSY scale)
  1  1     1.57547011E-04   # Y_e(Q)^DRbar
  2  2     3.25757251E-02   # Y_mu(Q)^DRbar
  3  3     5.47870106E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.41904893E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.71991899E+02   # A_t(Q)^DRbar
Block Ad Q=  3.41904893E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.32242360E+03   # A_b(Q)^DRbar
Block Ae Q=  3.41904893E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.28921393E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.41904893E+03  # soft SUSY breaking masses at Q
   1    2.12757459E+02  # M_1
   2   -6.90276940E+02  # M_2
   3    2.98992100E+03  # M_3
  21    1.33855135E+07  # M^2_(H,d)
  22    1.16184193E+05  # M^2_(H,u)
  31    1.23763490E+03  # M_(L,11)
  32    1.23763490E+03  # M_(L,22)
  33    1.02237489E+03  # M_(L,33)
  34    1.67845364E+02  # M_(E,11)
  35    1.67845364E+02  # M_(E,22)
  36    1.64390403E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.89538417E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.87064191E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.41208294E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21519111E+02  # h0
        35     3.59932083E+03  # H0
        36     3.60282581E+03  # A0
        37     3.59794171E+03  # H+
   1000001     1.01062007E+04  # ~d_L
   2000001     1.00814115E+04  # ~d_R
   1000002     1.01058339E+04  # ~u_L
   2000002     1.00841251E+04  # ~u_R
   1000003     1.01062100E+04  # ~s_L
   2000003     1.00814286E+04  # ~s_R
   1000004     1.01058432E+04  # ~c_L
   2000004     1.00841263E+04  # ~c_R
   1000005     2.49545263E+03  # ~b_1
   2000005     2.98013545E+03  # ~b_2
   1000006     2.98267935E+03  # ~t_1
   2000006     3.91925991E+03  # ~t_2
   1000011     1.24265775E+03  # ~e_L-
   2000011     2.14664585E+02  # ~e_R-
   1000012     1.23968475E+03  # ~nu_eL
   1000013     1.24261054E+03  # ~mu_L-
   2000013     2.14179926E+02  # ~mu_R-
   1000014     1.23963624E+03  # ~nu_muL
   1000015     1.00590919E+03  # ~tau_1-
   2000015     1.62036750E+03  # ~tau_2-
   1000016     1.00580898E+03  # ~nu_tauL
   1000021     3.42319212E+03  # ~g
   1000022     2.05960055E+02  # ~chi_10
   1000023     3.72785766E+02  # ~chi_20
   1000025     3.88262822E+02  # ~chi_30
   1000035     7.47052529E+02  # ~chi_40
   1000024     3.74163670E+02  # ~chi_1+
   1000037     7.47209937E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.86883810E-02   # alpha
Block Hmix Q=  3.41904893E+03  # Higgs mixing parameters
   1   -3.73874675E+02  # mu
   2    5.35759778E+01  # tan[beta](Q)
   3    2.43117288E+02  # v(Q)
   4    1.29803538E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99968743E-01   # Re[R_st(1,1)]
   1  2     7.90649638E-03   # Re[R_st(1,2)]
   2  1    -7.90649638E-03   # Re[R_st(2,1)]
   2  2     9.99968743E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.94387045E-02   # Re[R_sb(1,1)]
   1  2     9.99811051E-01   # Re[R_sb(1,2)]
   2  1    -9.99811051E-01   # Re[R_sb(2,1)]
   2  2    -1.94387045E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.99776690E-01   # Re[R_sta(1,1)]
   1  2     2.11322029E-02   # Re[R_sta(1,2)]
   2  1    -2.11322029E-02   # Re[R_sta(2,1)]
   2  2    -9.99776690E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.84004381E-01   # Re[N(1,1)]
   1  2     6.96570712E-03   # Re[N(1,2)]
   1  3    -1.57318060E-01   # Re[N(1,3)]
   1  4    -8.32939733E-02   # Re[N(1,4)]
   2  1     5.29768121E-02   # Re[N(2,1)]
   2  2     1.56484987E-01   # Re[N(2,2)]
   2  3     7.04000368E-01   # Re[N(2,3)]
   2  4    -6.90716576E-01   # Re[N(2,4)]
   3  1     1.69948759E-01   # Re[N(3,1)]
   3  2    -4.96364027E-02   # Re[N(3,2)]
   3  3     6.88381210E-01   # Re[N(3,3)]
   3  4     7.03409522E-01   # Re[N(3,4)]
   4  1     6.80116044E-03   # Re[N(4,1)]
   4  2    -9.86407702E-01   # Re[N(4,2)]
   4  3     7.59329937E-02   # Re[N(4,3)]
   4  4    -1.45560192E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.07612028E-01   # Re[U(1,1)]
   1  2    -9.94192965E-01   # Re[U(1,2)]
   2  1    -9.94192965E-01   # Re[U(2,1)]
   2  2     1.07612028E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     2.06472257E-01   # Re[V(1,1)]
   1  2     9.78452455E-01   # Re[V(1,2)]
   2  1     9.78452455E-01   # Re[V(2,1)]
   2  2    -2.06472257E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     6.59321793E-03   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     8.29615272E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.76910329E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.86425916E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     9.09257500E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.59675461E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.26264506E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.31235907E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.90749067E-03   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     8.31796660E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.76512531E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.98876202E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.12384400E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.58969309E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.25926685E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.29777011E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
     1.37015346E-04    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.41885071E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.55200044E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.78447855E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     2.43757763E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.02431685E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.21803633E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.07982289E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     2.56895880E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.04910131E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.67116345E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.67835940E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.06643744E-03    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.31649002E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.13633990E-03    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     1.13724600E-02    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     5.63690118E-03    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     5.27644323E-03    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     8.33886235E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     1.66473224E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.89505428E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.04961845E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.60133011E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     4.60890049E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.07913521E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
DECAY   1000014     8.35943755E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.66056245E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.87267546E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.04697621E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.59459428E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     4.85280219E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.06613868E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
DECAY   1000016     8.48484504E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.28859719E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     6.37955963E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     7.45109737E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.02800844E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.52047683E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.02461098E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
DECAY   2000001     6.28463349E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.66433346E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.58022140E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.91051779E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.58588704E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.65876245E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.26466645E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     3.73346141E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.65723558E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.35439373E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.14655504E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.24120972E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.28582331E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.66502355E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     3.02207181E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.90866743E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.58652168E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.66054827E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.30299823E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.10055461E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.65681190E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.35887179E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.14646225E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.24053182E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     7.69293978E+01   # ~b_1
#    BR                NDA      ID1      ID2
     3.07794682E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.46020355E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.32918128E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.85806345E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.84748724E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     3.67526099E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.39662196E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.04182744E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.64778831E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.57036047E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     8.25904589E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.01642942E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.82042770E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     6.15359162E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     8.75317155E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.45594032E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.37467810E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.00449577E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.65149497E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.58574067E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.94477628E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.60985057E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.62941966E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.98606321E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.11051715E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.24091456E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.45601375E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.37464386E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.00311929E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.00726752E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.65138646E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.58637495E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.94464883E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.61200690E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.62896384E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.06229918E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.11043427E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.24023666E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.40329678E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.40153956E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     6.20469950E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.13715438E-03    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.02051877E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.05826883E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     8.59930361E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.31372369E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.61626033E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.25909126E-03    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     3.12731689E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.81613230E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.89991463E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.05961587E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.14907971E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     8.20739685E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.15088603E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.65948818E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     8.85130031E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     1.04599503E-03    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     5.17784169E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.62517546E-04    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.41053981E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     1.25104600E-02    2    -2000013        14   # BR(chi^+_1 -> ~mu^+_R nu_mu)
     9.87487628E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     5.92542180E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.43977774E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.79274823E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.33557263E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.45883782E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.24612587E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.28504632E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.16095857E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     8.98379362E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.30452138E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     6.22340390E-03    2     2000013       -13   # BR(chi^0_2 -> ~mu^-_R mu^+)
     6.22340390E-03    2    -2000013        13   # BR(chi^0_2 -> ~mu^+_R mu^-)
     6.52526730E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.35001124E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.00190500E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.78439284E-02    2     2000011       -11   # BR(chi^0_3 -> ~e^-_R e^+)
     6.78439284E-02    2    -2000011        11   # BR(chi^0_3 -> ~e^+_R e^-)
     7.28368666E-02    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     7.28368666E-02    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     1.14314074E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     6.04296243E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     7.36710318E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.27024873E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.27024873E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.04048731E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.94197372E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.93668236E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     7.74137195E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.60805944E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.52882681E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.11452266E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.11452266E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.64459169E+01   # ~g
#    BR                NDA      ID1      ID2
     3.67311541E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     3.67311541E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     7.43866898E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     7.43866898E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.05281498E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.05281498E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     8.14445350E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     8.14445350E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.30049500E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     9.10929662E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     9.69914694E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     9.29169961E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     9.29169961E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.07060880E-03   # Gamma(h0)
     2.59961520E-03   2        22        22   # BR(h0 -> photon photon)
     1.37083155E-03   2        22        23   # BR(h0 -> photon Z)
     2.22900050E-02   2        23        23   # BR(h0 -> Z Z)
     1.97520378E-01   2       -24        24   # BR(h0 -> W W)
     8.45355907E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.31376284E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.36363895E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.81789137E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.51542761E-07   2        -2         2   # BR(h0 -> Up up)
     2.94098936E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.17551676E-07   2        -1         1   # BR(h0 -> Down down)
     2.23353315E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.93634281E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.77609186E+02   # Gamma(HH)
     3.25616002E-09   2        22        22   # BR(HH -> photon photon)
     1.95370000E-08   2        22        23   # BR(HH -> photon Z)
     7.51348746E-08   2        23        23   # BR(HH -> Z Z)
     1.79990404E-08   2       -24        24   # BR(HH -> W W)
     7.13307305E-06   2        21        21   # BR(HH -> gluon gluon)
     8.00750537E-09   2       -11        11   # BR(HH -> Electron electron)
     3.56551131E-04   2       -13        13   # BR(HH -> Muon muon)
     1.10375984E-01   2       -15        15   # BR(HH -> Tau tau)
     1.72495740E-14   2        -2         2   # BR(HH -> Up up)
     3.34656703E-09   2        -4         4   # BR(HH -> Charm charm)
     2.83084009E-04   2        -6         6   # BR(HH -> Top top)
     5.55682770E-07   2        -1         1   # BR(HH -> Down down)
     2.00998027E-04   2        -3         3   # BR(HH -> Strange strange)
     6.29960803E-01   2        -5         5   # BR(HH -> Bottom bottom)
     7.64844219E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     7.05988022E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     7.05988022E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.72528987E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.15159143E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.18963628E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     9.17838641E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.06421350E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.56742420E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.29407166E-07   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     3.22150749E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.61042615E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.86881287E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     8.80401680E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     3.13350621E-07   2        25        25   # BR(HH -> h0 h0)
     9.43949551E-09   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     1.59171670E-14   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     1.59171670E-14   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     1.06336301E-08   2  -2000011   2000011   # BR(HH -> Selectron2 selectron2)
     9.26179687E-09   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     6.80503253E-10   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     6.80503253E-10   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     1.04998109E-08   2  -2000013   2000013   # BR(HH -> Smuon2 smuon2)
     1.07340941E-05   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
     4.98776186E-03   2  -2000015   1000015   # BR(HH -> Stau2 stau1)
     4.98776186E-03   2  -1000015   2000015   # BR(HH -> Stau1 stau2)
     4.56706116E-06   2  -2000015   2000015   # BR(HH -> Stau2 stau2)
DECAY        36     1.64128926E+02   # Gamma(A0)
     4.45770078E-08   2        22        22   # BR(A0 -> photon photon)
     7.07233587E-08   2        22        23   # BR(A0 -> photon Z)
     1.08907925E-05   2        21        21   # BR(A0 -> gluon gluon)
     7.77438013E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.46173464E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.07167108E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.62413463E-14   2        -2         2   # BR(A0 -> Up up)
     3.15081575E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.69717149E-04   2        -6         6   # BR(A0 -> Top top)
     5.39445976E-07   2        -1         1   # BR(A0 -> Down down)
     1.95124115E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.11626219E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     8.99560151E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     7.57971672E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     7.57971672E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.53720826E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.21392725E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.34436540E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     9.42083321E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     9.84050577E-04   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.90504454E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.32908424E-06   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     4.07621378E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.69246552E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.57255377E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.29126166E-03   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.54386120E-07   2        23        25   # BR(A0 -> Z h0)
     3.54024811E-12   2        23        35   # BR(A0 -> Z HH)
     7.40144581E-40   2        25        25   # BR(A0 -> h0 h0)
     1.71785121E-14   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     1.71785121E-14   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     7.34435096E-10   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     7.34435096E-10   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
     5.40728015E-03   2  -2000015   1000015   # BR(A0 -> Stau2 stau1)
     5.40728015E-03   2  -1000015   2000015   # BR(A0 -> Stau1 stau2)
DECAY        37     1.98024653E+02   # Gamma(Hp)
     8.63867559E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.69330362E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.04467650E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.52244841E-07   2        -1         2   # BR(Hp -> Down up)
     9.34101016E-06   2        -3         2   # BR(Hp -> Strange up)
     7.01066982E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.59888191E-08   2        -1         4   # BR(Hp -> Down charm)
     1.99029671E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.81744715E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.60112503E-08   2        -1         6   # BR(Hp -> Down top)
     6.44989195E-07   2        -3         6   # BR(Hp -> Strange top)
     6.62130600E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.25015271E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.74622481E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     4.28549262E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     6.89244623E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     4.30455093E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     6.32116645E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     6.56192765E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     7.57352666E-08   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.27493389E-07   2        24        25   # BR(Hp -> W h0)
     2.80325010E-14   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     3.74587403E-08   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     1.19852663E-09   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     3.72900139E-08   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
     4.25540099E-06   2  -1000015   1000016   # BR(Hp -> Stau1 snu_tau1)
     8.96735104E-03   2  -2000015   1000016   # BR(Hp -> Stau2 snu_tau1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.00273064E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.87038267E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.87038540E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99999049E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.49336588E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.48385273E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.00273064E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.87038267E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.87038540E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999999E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    6.48539857E-10        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999999E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    6.48539857E-10        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26413287E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.41351251E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.45297261E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    6.48539857E-10        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999999E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.29285919E-04   # BR(b -> s gamma)
    2    1.58855993E-06   # BR(b -> s mu+ mu-)
    3    3.52368528E-05   # BR(b -> s nu nu)
    4    2.21422123E-15   # BR(Bd -> e+ e-)
    5    9.45897301E-11   # BR(Bd -> mu+ mu-)
    6    1.98436609E-08   # BR(Bd -> tau+ tau-)
    7    7.64484482E-14   # BR(Bs -> e+ e-)
    8    3.26589687E-09   # BR(Bs -> mu+ mu-)
    9    6.94024617E-07   # BR(Bs -> tau+ tau-)
   10    9.56210261E-05   # BR(B_u -> tau nu)
   11    9.87728591E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41993590E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93574457E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15753830E-03   # epsilon_K
   17    2.28166181E-15   # Delta(M_K)
   18    2.47896767E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28803595E-11   # BR(K^+ -> pi^+ nu nu)
   20    4.88404429E-15   # Delta(g-2)_electron/2
   21    2.07631668E-10   # Delta(g-2)_muon/2
   22    1.27082371E-07   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.19516886E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.99246615E-01   # C7
     0305 4322   00   2    -1.11364826E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.02950934E-01   # C8
     0305 6321   00   2    -1.14926129E-04   # C8'
 03051111 4133   00   0     1.62218349E+00   # C9 e+e-
 03051111 4133   00   2     1.62250142E+00   # C9 e+e-
 03051111 4233   00   2     6.92793191E-04   # C9' e+e-
 03051111 4137   00   0    -4.44487559E+00   # C10 e+e-
 03051111 4137   00   2    -4.44179151E+00   # C10 e+e-
 03051111 4237   00   2    -5.15882335E-03   # C10' e+e-
 03051313 4133   00   0     1.62218349E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62249895E+00   # C9 mu+mu-
 03051313 4233   00   2     6.92789499E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44487559E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44179397E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.15883092E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50469651E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.11664409E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50469652E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.11664420E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50469671E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.11667369E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85816683E-07   # C7
     0305 4422   00   2     1.28821340E-05   # C7
     0305 4322   00   2    -2.39317710E-07   # C7'
     0305 6421   00   0     3.30477504E-07   # C8
     0305 6421   00   2     2.04753752E-05   # C8
     0305 6321   00   2     2.25509478E-07   # C8'
 03051111 4133   00   2    -1.27363728E-07   # C9 e+e-
 03051111 4233   00   2     1.33338436E-05   # C9' e+e-
 03051111 4137   00   2     9.13410727E-07   # C10 e+e-
 03051111 4237   00   2    -9.92952575E-05   # C10' e+e-
 03051313 4133   00   2    -1.27427987E-07   # C9 mu+mu-
 03051313 4233   00   2     1.33337868E-05   # C9' mu+mu-
 03051313 4137   00   2     9.13502264E-07   # C10 mu+mu-
 03051313 4237   00   2    -9.92954017E-05   # C10' mu+mu-
 03051212 4137   00   2     5.24800065E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.14928074E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     5.24825042E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.14928074E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     5.59366900E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.14927984E-05   # C11' nu_3 nu_3
