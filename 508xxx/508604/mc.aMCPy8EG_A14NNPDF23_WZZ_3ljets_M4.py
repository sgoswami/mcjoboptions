import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# WZZ -> 3ljets

evgenConfig.contact = ["thomas.glyn.hitchings@cern.ch"]
runName = 'test'
evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

process = """
import model SM_Ltotal_Ind5v2020v2_UFO
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > w+ z z NP=1 QCD=1 QED=3, w+ > l+ vl, z > l+ l-, z > j j
add process p p > w- z z NP=1 QCD=1 QED=3, w- > l- vl~, z > l+ l-, z > j j
output -f"""
process_dir = new_process(process)


settings = {'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


#update param card to include new couplings
param_card_name = 'param_card_mod.dat'
modify_param_card(param_card_input=param_card_name,process_dir=process_dir)

rcard = open('reweight_card.dat','w')
reweightCommand="""
launch --rwgt_name=test_rw_SM
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 0
  set anoinputs 8 0
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

launch --rwgt_name=M4_30
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 0
  set anoinputs 8 30e-12
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

launch --rwgt_name=M4_20
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 0
  set anoinputs 8 20e-12
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

launch --rwgt_name=M4_10
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 0
  set anoinputs 8 10e-12
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

launch --rwgt_name=M4_m10
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 0
  set anoinputs 8 -10e-12
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

launch --rwgt_name=M4_m20
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 0
  set anoinputs 8 -20e-12
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

launch --rwgt_name=M4_m30
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 0
  set anoinputs 8 -30e-12
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

launch --rwgt_name=M4_m40
  set anoinputs 1 0
  set anoinputs 2 0
  set anoinputs 3 0
  set anoinputs 4 0
  set anoinputs 5 0
  set anoinputs 6 0
  set anoinputs 7 0
  set anoinputs 8 -40e-12
  set anoinputs 9 0
  set anoinputs 10 0
  set anoinputs 11 0
  set anoinputs 12 0
  set anoinputs 13 0
  set anoinputs 14 0
  set anoinputs 15 0
  set anoinputs 16 0
  set anoinputs 17 0
  set anoinputs 18 0
  set anoinputs 19 0
  set anoinputs 20 0
  set anoinputs 21 0

"""
rcard.write(reweightCommand)
rcard.close()

subprocess.call('cp reweight_card.dat ' + process_dir+'/Cards/', shell=True)

generate(process_dir=process_dir,runArgs=runArgs)
outDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower
evgenConfig.generators = ["aMcAtNlo"]
evgenConfig.description = 'aMcAtNlo_WWZ'
evgenConfig.keywords+=['lepton']
#evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=outDS #'test_lhe_events.events'

# SHOWERING
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#LO shower
include("Pythia8_i/Pythia8_MadGraph.py")

#### Finalize
#theApp.finalize()
#theApp.exit:()
