evt_multiplier = 20
reweight = True
reweights=[
           "SINP_0.1-TANB_1.0",
           "SINP_0.2-TANB_1.0",
           "SINP_0.3-TANB_1.0",
           "SINP_0.35-TANB_1.0",
           "SINP_0.4-TANB_1.0",
           "SINP_0.5-TANB_1.0",
           "SINP_0.6-TANB_1.0",
           "SINP_0.7-TANB_1.0",
           "SINP_0.8-TANB_1.0",
           "SINP_0.9-TANB_1.0",
           "SINP_0.35-TANB_0.3",
           "SINP_0.35-TANB_0.5",
           "SINP_0.35-TANB_2.0",
           "SINP_0.35-TANB_3.0",
           "SINP_0.7-TANB_0.3",
           "SINP_0.7-TANB_0.5",
           "SINP_0.7-TANB_2.0",
           "SINP_0.7-TANB_3.0"
           ]
include("MadGraphControl_2HDMa_DMtW.py")

                
evgenConfig.nEventsPerJob = 10000 
