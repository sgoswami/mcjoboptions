evgenConfig.description = "MadGraph+Pythia8 showered samples for diphoton+0-2j at NLO, myy=0-50 GeV"
evgenConfig.keywords = ["SM","diphoton","NLO"]
evgenConfig.contact = ["yanwen.hong@cern.ch","ana.cueto@cern.ch"]
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# one LHE file contains 220000 events
# FxFx match+merge eff: 51%
evgenConfig.nEventsPerJob = 100000
evgenConfig.inputFilesPerJob = 1

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
# Pythia8 shower weights should be ok to use in versions >=8.307
include("Pythia8_i/Pythia8_ShowerWeights.py")

# FxFx Matching settings
PYTHIA8_nJetMax=2
PYTHIA8_qCut=20.
include("Pythia8_i/Pythia8_FxFx_A14mod.py")

#avoid ME photons to decay y->ffbar in the shower
genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]
