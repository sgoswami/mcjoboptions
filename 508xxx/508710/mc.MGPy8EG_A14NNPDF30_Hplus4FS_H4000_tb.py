#-------------------------------------------------------------------------------------------------
### JO file to generate charged Higgs events
#-------------------------------------------------------------------------------------------------

from MadGraphControl.MadGraphUtils import *

# Define number of generated LHE events
nevents=10*(runArgs.maxEvents)
mode=0

##Getting the Physics short, as while running in release 21, need to pass the directory itself##
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short() 
           
ss=str(phys_short.split('_')[4][0:])
print ss

process="""
    set group_subprocesses Auto
    set ignore_six_quark_processes False   
    set loop_optimized_output True
    set complex_mass_scheme False
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    import model 2HDMtypeII                     
    generate p p > t~ h+ b [QCD]               
    output -f
    """

#---------------------------------------------------------------------------------------------------
# Set masses
#---------------------------------------------------------------------------------------------------
mhc=0
mhc = float(phys_short.split('_')[3][1:])
print "mhc", mhc

import math
mh1=1.250e+02                 
mh2=math.sqrt(math.pow(mhc,2)+math.pow(8.0399e+01,2)) 
mh3=mh2

extras = { 'lhe_version':'3.0',
           'pdlabel':"'lhapdf'",
           'lhaid':' 260400',
           'parton_shower':'PYTHIA8',
           'fixed_ren_scale':'F',
           'fixed_fac_scale':'F',
           'reweight_scale' :'T',
           'rw_rscale'      :'1.0 0.5 2.0',
           'rw_fscale'      :'1.0 0.5 2.0',
           'reweight_PDF'   :'T',
           'PDF_set_min'    :'260401',
           'PDF_set_max'    :'260500',
           'store_rwgt_info':'T'
           }


extras['nevents'] = nevents

#scales set as per the dynamical scale choice
extras['dynamical_scale_choice']=2
extras["muR_over_ref"]=0.333
extras["muF1_over_ref"]=1
extras["muF2_over_ref"]=1   

# set up process
process_dir = new_process(process)
print "process_dir", process_dir

#------------------------------------------------
# Run Card
#--------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

masses = {'25':str(mh1)+'  #  mh1',
          '35':str(mh2)+'  #  mh2',
          '36':str(mh3)+'  #  mh3',
          '37':str(mhc)+'  #  mhc'}

params = {}
params['mass'] = masses
print masses

#------------------------------------------------
# Param Card
#--------------------------------------------------
modify_param_card(process_dir=process_dir,params=params)

print_cards()

runName='run_01'     
    
generate(process_dir=process_dir, runArgs=runArgs)
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Showering via Pythia8
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

evgenConfig.description = 'aMcAtNlo High mass charged Higgs NLO4FS'
evgenConfig.keywords+=['Higgs','MSSM','BSMHiggs','chargedHiggs']
evgenConfig.contact = ['shubham.bansal@cern.ch, Dominik.Duda@cern.ch, mir@ifae.es']
        
runArgs.inputGeneratorFile=outputDS

genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
                            "37:onMode = off",                   # turn off all mhc decays
                            "37:onIfMatch = 5 6"]                # switch on H+ to tb

evgenConfig.keywords+=['top','bottom']


from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter   # lep filter
filtSeq += TTbarWToLeptonFilter("TTbarWToLeptonFilter")

TTbarWToLeptonFilter = filtSeq.TTbarWToLeptonFilter

TTbarWToLeptonFilter.NumLeptons = -1
TTbarWToLeptonFilter.Ptcut = 0.

filtSeq.Expression = "(TTbarWToLeptonFilter)"

