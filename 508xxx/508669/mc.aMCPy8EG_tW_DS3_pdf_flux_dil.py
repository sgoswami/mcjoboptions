import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.DiagramRemoval import do_MadSpin_DRX


# General settings
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob


MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
        'central_pdf':260000,
        'scale_variations':[0.5,1.,2.],
    }

process_dir = MADGRAPH_GRIDPACK_LOCATION

#Fetch default run_card.dat and set parameters
settings = {'parton_shower':'PYTHIA8',
            'nevents'      :int(nevents)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=False)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# MadSpin settings
madspin_dir = process_dir +"/MadSpin"
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)

fMadSpinCard = open(madspin_card,'w')
fMadSpinCard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************

#Some options (uncomment to apply)
#

 set Nevents_for_max_weight 250 # number of events for the estimate of the max. weight
 set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event
 set seed %i
 set ms_dir %s
 #set use_old_dir True
 set bw_cut 50

# specify the decay for the final state particles
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
decay w+ > l+ vl
decay w- > l- vl~
# running the actual code
launch"""%(int(runArgs.randomSeed),madspin_dir))
fMadSpinCard.close()

# run MadSpin to create MadSpin files
if os.path.isfile("tmp_LHE_events.events"):
    madspin_on_lhe("tmp_LHE_events.events",madspin_card,runArgs=runArgs,keep_original=True)
else:
    raise RuntimeError("tmp_LHE_events.events not found!")

# apply hacks on MadSpin files
do_MadSpin_DRX(1,madspin_dir)

# Make sure the code is taken from the madspin directory and not overwritten when you redo the decays
msfile=madspin_card
mstilde=madspin_card+"~"
shutil.copyfile(msfile,mstilde)
with open(msfile,"w") as myfile, open(mstilde,'r') as f:
    for line in f:
        if '#set use_old_dir True' in line:
            line = line.replace('#',' ') #uncomment set use_old_dir True
        myfile.write(line)
os.remove(mstilde)

# hack to be able to redecay the events
if os.path.isfile("tmp_LHE_events.events"):
    os.remove("tmp_LHE_events.events")
else:
    raise RuntimeError("tmp_LHE_events.events not found!")
subprocess.call(["cp", "tmp_LHE_events.events.original", "tmp_LHE_events.events"])
subprocess.call(["rm", "tmp_LHE_events.events.original"])
subprocess.call(["cp", process_dir+"/madspin_makefile", madspin_dir+"/makefile"])
subprocess.call(["make","clean"],cwd=madspin_dir)
subprocess.call(["make"],cwd=madspin_dir)

# re-run MadSpin on undecayed lhe files with hack applied
if os.path.isfile("tmp_LHE_events.events"):
    madspin_on_lhe("tmp_LHE_events.events",madspin_card,runArgs=runArgs,keep_original=False)
else:
    raise RuntimeError("tmp_LHE_events.events not found!")

# remove process dir and madspin dir
shutil.rmtree(process_dir,ignore_errors=True)
if os.path.isdir('MGC_LHAPDF/'):
    shutil.rmtree('MGC_LHAPDF/',ignore_errors=True)
shutil.rmtree(madspin_dir,ignore_errors=True)

# Shower Pythia8
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# Metadata
evgenConfig.generators += ["aMcAtNlo", "Pythia8", "EvtGen"]
evgenConfig.description = "MG5aMCatNLO+MadSpin+Pythia8+EvtGen tW DS3 pdf flux dilepton"
evgenConfig.keywords    = ["SM","top"]
evgenConfig.contact = ['dominic.hirschbuehl@cern.ch','jens.roggel@cern.ch']

