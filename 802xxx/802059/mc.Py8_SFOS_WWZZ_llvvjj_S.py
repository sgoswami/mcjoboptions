evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'Py8EG_SFOS_WWZZ_llvvjj_S  LHE input'
evgenConfig.keywords = ['SM','diboson','VBS','WW','ZZ','electroweak','2lepton','2jet']
evgenConfig.contact = ["bryan.kortman@cern.ch"]

# Job parameters
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob = 10000


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include('Pythia8_i/Pythia8_LHEF.py')
include("Pythia8_i/Pythia8_ShowerWeights.py")


genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]
