####################################################################
# Job Options for B-->D0(->a_1+(->rho0(->pi+pi-)pi+)K-)e-anti-nu_e #
####################################################################

### Meta Information ###
### ---------------- ### 
evgenConfig.description   = "Exclusive B-->D0(->a_1+(->rho0(->pi+pi-)pi+)K-)e-anti-nu_e Decay Production"
evgenConfig.process       = "B-->D0(->a_1+(->rho0(->pi+pi-)pi+)K-)e-anti-nu_e"
# The list of allowed keywords (for the Athena 21.6.83):
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/EvgenJobTransforms/share/file/evgenkeywords.txt
evgenConfig.keywords      = [ "bottom", "Bminus", "exclusive", "1electron", "neutrino" ]
evgenConfig.contact       = [ "giannis.maniatis@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

### Create EvtGen Decay File ###
### ------------------------ ###

### Create EvtGen Decay File -> Generating the File ###
### ----------------------------------------------- ###
userDecayFileName = "Bm_D0_a1Plus_rho0pipi_pi_K_enue_USER.DEC"
f = open( userDecayFileName, "w" )

### Create EvtGen Decay File -> Writing Decay Rules ###
### ----------------------------------------------- ###
f.write( "Define dm_incohMix_B_s0 0.0e12\n"                                   )
f.write( "Define dm_incohMix_B0 0.0e12\n"                                     )
f.write( "Alias my_D0 D0\n"                                         )
f.write( "Alias my_a_1+ a_1+\n"                                               )
f.write( "Alias my_rho0 rho0\n"                                               )
f.write( "Decay B-\n"                                                         )
f.write( "1.0000  my_D0 e- anti-nu_e  PHOTOS  HQET2 1.185 1.081;\n"           )
f.write( "Enddecay\n"                                                         )
f.write( "Decay my_D0\n"                                                 )
f.write( "1.0000  my_a_1+ K-  SVS;\n"                                         )
f.write( "Enddecay\n"                                                         )
f.write( "Decay my_a_1+\n"                                                    )
f.write( "1.0000 my_rho0 pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;\n"            )
f.write( "Enddecay\n"                                                         )
f.write( "Decay my_rho0\n"                                                    )
f.write( "1.0000 pi+ pi- VSS;\n"                                              )
f.write( "Enddecay\n"                                                         )

### Create EvtGen Decay File -> Closing the File ###
### -------------------------------------------- ###
f.write( "End\n" )
f.close()

### Inheritance from Pythia8B_i ###
### --------------------------- ###
include( "Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py" )
include( "Pythia8B_i/Pythia8B_BPDGCodes.py"                 )

### Process Configuration ###
### --------------------- ###

genSeq.Pythia8B.Commands                 += [ "HardQCD:all         = on"  ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ "ParticleDecays:mixB = off" ]
genSeq.Pythia8B.Commands                 += [ "HadronLevel:all     = off" ]
genSeq.Pythia8B.Commands                 += [ "PhaseSpace:pTHatMin = 7"   ]
genSeq.Pythia8B.NHadronizationLoops       = 4
genSeq.Pythia8B.NDecayLoops               = 1

### Intermediate Cuts ###
### ----------------- ###

genSeq.Pythia8B.QuarkPtCut                = 7.5
genSeq.Pythia8B.AntiQuarkPtCut            = 0.0
genSeq.Pythia8B.QuarkEtaCut               = 3.0
genSeq.Pythia8B.AntiQuarkEtaCut           = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.TriggerPDGCode            = 0
genSeq.Pythia8B.SignalPDGCodes            = [ -521 ]
genSeq.EvtInclusiveDecay.userDecayFile    = userDecayFileName

### Final State Cuts ###
### ---------------- ###

# The filters are implemented here: 
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/GeneratorFilters/src

if not hasattr(filtSeq, "ParentsTracksFilterHard"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
    filtSeq += ParentsTracksFilter( "ParentsTracksFilterHard" )
if not hasattr(filtSeq, "ParentsTracksFilterSoft"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
    filtSeq += ParentsTracksFilter( "ParentsTracksFilterSoft" )

filtSeq.ParentsTracksFilterHard.AllowChargeConjParent = False
filtSeq.ParentsTracksFilterSoft.AllowChargeConjParent = False

filtSeq.ParentsTracksFilterHard.PDGParent       = [ -521 ]
filtSeq.ParentsTracksFilterHard.PtMinLeptons    = 4000.0
filtSeq.ParentsTracksFilterHard.EtaRangeLeptons = 2.7
filtSeq.ParentsTracksFilterHard.PtMinHadrons    = 4000.0
filtSeq.ParentsTracksFilterHard.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilterHard.NumMinTracks    = 2
filtSeq.ParentsTracksFilterHard.NumMinLeptons   = 1
filtSeq.ParentsTracksFilterHard.NumMaxLeptons   = 1

filtSeq.ParentsTracksFilterSoft.PDGParent       = [ -521 ]
filtSeq.ParentsTracksFilterSoft.PtMinHadrons    = 400.0
filtSeq.ParentsTracksFilterSoft.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilterSoft.NumMinTracks    = 4
