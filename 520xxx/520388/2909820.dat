# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  20:28
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.41729308E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.44223798E+03  # scale for input parameters
    1    2.71391481E+02  # M_1
    2    8.68327385E+02  # M_2
    3    3.38903889E+03  # M_3
   11    1.55137562E+03  # A_t
   12   -1.60512903E+03  # A_b
   13   -1.98941100E+03  # A_tau
   23   -7.98526621E+02  # mu
   25    2.32011000E+01  # tan(beta)
   26    5.29889542E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.25675170E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.90214683E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.79073969E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.44223798E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.44223798E+03  # (SUSY scale)
  1  1     8.39215647E-06   # Y_u(Q)^DRbar
  2  2     4.26321549E-03   # Y_c(Q)^DRbar
  3  3     1.01383991E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.44223798E+03  # (SUSY scale)
  1  1     3.91257248E-04   # Y_d(Q)^DRbar
  2  2     7.43388772E-03   # Y_s(Q)^DRbar
  3  3     3.88004588E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.44223798E+03  # (SUSY scale)
  1  1     6.82772509E-05   # Y_e(Q)^DRbar
  2  2     1.41175700E-02   # Y_mu(Q)^DRbar
  3  3     2.37434303E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.44223798E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.55137563E+03   # A_t(Q)^DRbar
Block Ad Q=  3.44223798E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.60512883E+03   # A_b(Q)^DRbar
Block Ae Q=  3.44223798E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.98941097E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.44223798E+03  # soft SUSY breaking masses at Q
   1    2.71391481E+02  # M_1
   2    8.68327385E+02  # M_2
   3    3.38903889E+03  # M_3
  21   -4.36032601E+05  # M^2_(H,d)
  22   -4.87453272E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.25675170E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.90214683E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.79073969E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21788927E+02  # h0
        35     5.30002785E+02  # H0
        36     5.29889542E+02  # A0
        37     5.43196268E+02  # H+
   1000001     1.01008917E+04  # ~d_L
   2000001     1.00763259E+04  # ~d_R
   1000002     1.01005237E+04  # ~u_L
   2000002     1.00802170E+04  # ~u_R
   1000003     1.01008941E+04  # ~s_L
   2000003     1.00763293E+04  # ~s_R
   1000004     1.01005260E+04  # ~c_L
   2000004     1.00802181E+04  # ~c_R
   1000005     2.39426551E+03  # ~b_1
   2000005     2.91857224E+03  # ~b_2
   1000006     2.39698898E+03  # ~t_1
   2000006     4.94328609E+03  # ~t_2
   1000011     1.00213765E+04  # ~e_L-
   2000011     1.00081810E+04  # ~e_R-
   1000012     1.00206091E+04  # ~nu_eL
   1000013     1.00213861E+04  # ~mu_L-
   2000013     1.00081976E+04  # ~mu_R-
   1000014     1.00206180E+04  # ~nu_muL
   1000015     1.00129543E+04  # ~tau_1-
   2000015     1.00241149E+04  # ~tau_2-
   1000016     1.00231541E+04  # ~nu_tauL
   1000021     3.82603682E+03  # ~g
   1000022     2.72836017E+02  # ~chi_10
   1000023     7.89858065E+02  # ~chi_20
   1000025     8.11476462E+02  # ~chi_30
   1000035     9.49175024E+02  # ~chi_40
   1000024     7.89989051E+02  # ~chi_1+
   1000037     9.49130805E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.46095989E-02   # alpha
Block Hmix Q=  3.44223798E+03  # Higgs mixing parameters
   1   -7.98526621E+02  # mu
   2    2.32011000E+01  # tan[beta](Q)
   3    2.43125376E+02  # v(Q)
   4    2.80782927E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99945770E-01   # Re[R_st(1,1)]
   1  2     1.04143060E-02   # Re[R_st(1,2)]
   2  1    -1.04143060E-02   # Re[R_st(2,1)]
   2  2    -9.99945770E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99891601E-01   # Re[R_sb(1,1)]
   1  2     1.47236559E-02   # Re[R_sb(1,2)]
   2  1    -1.47236559E-02   # Re[R_sb(2,1)]
   2  2    -9.99891601E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.32897747E-01   # Re[R_sta(1,1)]
   1  2     9.91129754E-01   # Re[R_sta(1,2)]
   2  1    -9.91129754E-01   # Re[R_sta(2,1)]
   2  2    -1.32897747E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.97994979E-01   # Re[N(1,1)]
   1  2     1.86092256E-03   # Re[N(1,2)]
   1  3     6.05814374E-02   # Re[N(1,3)]
   1  4     1.82331451E-02   # Re[N(1,4)]
   2  1     5.28909695E-02   # Re[N(2,1)]
   2  2     3.68406027E-01   # Re[N(2,2)]
   2  3     6.65161374E-01   # Re[N(2,3)]
   2  4     6.47332906E-01   # Re[N(2,4)]
   3  1    -2.98177930E-02   # Re[N(3,1)]
   3  2     3.43100358E-02   # Re[N(3,2)]
   3  3    -7.05208686E-01   # Re[N(3,3)]
   3  4     7.07541116E-01   # Re[N(3,4)]
   4  1     1.78736044E-02   # Re[N(4,1)]
   4  2    -9.29029794E-01   # Re[N(4,2)]
   4  3     2.37846474E-01   # Re[N(4,3)]
   4  4     2.82866101E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -3.33975055E-01   # Re[U(1,1)]
   1  2    -9.42581913E-01   # Re[U(1,2)]
   2  1     9.42581913E-01   # Re[U(2,1)]
   2  2    -3.33975055E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -3.97765021E-01   # Re[V(1,1)]
   1  2     9.17487323E-01   # Re[V(1,2)]
   2  1     9.17487323E-01   # Re[V(2,1)]
   2  2     3.97765021E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02104169E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.96039989E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.76696180E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     8.78804315E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     3.14227040E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.42624021E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.71675436E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.80579544E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.03125019E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.56800869E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.81367579E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.39733750E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02888304E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.94491450E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.10743614E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.26471830E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.57577060E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     6.92378892E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.42663380E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.71445993E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.81664528E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.39798776E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.56745743E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.81180267E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.39585380E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.38594618E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     6.71572278E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     8.76529415E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     7.24414493E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.68262799E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.52301497E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.50420213E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     6.78869792E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.76779230E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.48850329E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     4.69036522E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.13639095E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42628548E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.83834998E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.53361936E-02    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     7.70306218E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.67549019E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     9.66404037E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.11320578E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42667893E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.83592037E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.53264806E-02    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     7.70094481E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.67475479E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     9.68582165E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.11210526E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.53761271E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.20051840E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.27862925E-02    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     7.14720271E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.48243074E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.53821492E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.82429237E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.88164495E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.51228951E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.90449784E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.17449690E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.99545076E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     7.88866854E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.29050907E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.36552670E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.08177370E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.15282110E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.88186636E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.51200354E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.90413059E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.17464491E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.99546666E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     7.89525530E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.03618659E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.29048683E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.36592104E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.08175967E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.15265613E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.65280928E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.48772676E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     5.48418306E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     4.20786372E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.01418417E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.01605648E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.94567741E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.67293441E+01   # ~b_2
#    BR                NDA      ID1      ID2
     9.66020615E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.95166226E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.21853576E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.71040717E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.89878549E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.32940769E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     7.28706385E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     3.65779064E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     5.15658460E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.05400778E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.69801816E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.02745433E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.62872771E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.17433684E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.91356680E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     8.73811278E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.21842236E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.93695548E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.02492404E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.15250693E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.05408050E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.69797419E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.05236813E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.62861331E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.17448443E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.91352952E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     8.74003585E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.21835681E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.93827107E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.02492008E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.15234189E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.67517131E+01   # ~t_1
#    BR                NDA      ID1      ID2
     4.91617141E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.51002805E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.83968685E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.45983834E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.12932702E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.00175810E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.00059315E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.77191887E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.94609581E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.45630831E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.72457817E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.61558576E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.93199162E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.18429500E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.23450772E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.43930286E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     5.95843104E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     7.17877907E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.97506990E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     1.43178143E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     2.95413281E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     4.19697215E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.88206587E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.12047231E-02    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
     5.84892585E-04    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     2.67328036E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.82222260E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.49197655E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.88352614E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     3.05689390E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     2.04150986E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.26638864E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.74847146E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.97550443E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     2.41811635E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     7.47076192E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.07486604E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
     8.92220595E-03    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     1.10552921E-03    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
DECAY   1000025     8.72213822E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     4.63903663E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     6.15109230E-04    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     4.85405738E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     4.89303273E-02    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.06741476E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     3.17177126E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     1.99833414E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.99833414E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.17207270E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.23590966E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     3.38834823E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     2.76361102E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.66408712E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.03023162E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.30349005E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     2.64989721E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.00127131E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
DECAY   1000021     1.03569365E+02   # ~g
#    BR                NDA      ID1      ID2
     1.72515250E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.72515250E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.01512486E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.01512486E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.02394212E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.02394212E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     9.55620836E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     9.55620836E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.24776969E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.44612237E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.26205647E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.26205647E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.39584881E-03   # Gamma(h0)
     2.40293585E-03   2        22        22   # BR(h0 -> photon photon)
     1.26644981E-03   2        22        23   # BR(h0 -> photon Z)
     2.08918006E-02   2        23        23   # BR(h0 -> Z Z)
     1.84124268E-01   2       -24        24   # BR(h0 -> W W)
     7.63161997E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.44719540E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.42299080E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.98583191E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.33227883E-07   2        -2         2   # BR(h0 -> Up up)
     2.58563354E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.34494727E-07   2        -1         1   # BR(h0 -> Down down)
     2.29482628E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.18811137E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.88301495E+00   # Gamma(HH)
     1.05989003E-07   2        22        22   # BR(HH -> photon photon)
     2.52082456E-08   2        22        23   # BR(HH -> photon Z)
     4.73296993E-05   2        23        23   # BR(HH -> Z Z)
     9.90672592E-05   2       -24        24   # BR(HH -> W W)
     1.79988771E-04   2        21        21   # BR(HH -> gluon gluon)
     8.58673120E-09   2       -11        11   # BR(HH -> Electron electron)
     3.82119127E-04   2       -13        13   # BR(HH -> Muon muon)
     1.10324704E-01   2       -15        15   # BR(HH -> Tau tau)
     5.99664729E-13   2        -2         2   # BR(HH -> Up up)
     1.16259769E-07   2        -4         4   # BR(HH -> Charm charm)
     4.52066608E-03   2        -6         6   # BR(HH -> Top top)
     8.08019206E-07   2        -1         1   # BR(HH -> Down down)
     2.92254707E-04   2        -3         3   # BR(HH -> Strange strange)
     8.83742486E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.10320330E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.83025453E+00   # Gamma(A0)
     2.50977945E-07   2        22        22   # BR(A0 -> photon photon)
     1.20580870E-07   2        22        23   # BR(A0 -> photon Z)
     2.41130759E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.56833685E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.81300670E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.10093481E-01   2       -15        15   # BR(A0 -> Tau tau)
     5.18119520E-13   2        -2         2   # BR(A0 -> Up up)
     1.00305405E-07   2        -4         4   # BR(A0 -> Charm charm)
     6.95297898E-03   2        -6         6   # BR(A0 -> Top top)
     8.06310925E-07   2        -1         1   # BR(A0 -> Down down)
     2.91636891E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.81955890E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     8.22941720E-05   2        23        25   # BR(A0 -> Z h0)
     1.60694821E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.65449944E+00   # Gamma(Hp)
     1.12772466E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.82137479E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.36373082E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     9.14402956E-07   2        -1         2   # BR(Hp -> Down up)
     1.50862557E-05   2        -3         2   # BR(Hp -> Strange up)
     1.08832773E-05   2        -5         2   # BR(Hp -> Bottom up)
     4.74934509E-08   2        -1         4   # BR(Hp -> Down charm)
     3.29660154E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.52403225E-03   2        -5         4   # BR(Hp -> Bottom charm)
     5.11536235E-07   2        -1         6   # BR(Hp -> Down top)
     1.15401149E-05   2        -3         6   # BR(Hp -> Strange top)
     8.61156404E-01   2        -5         6   # BR(Hp -> Bottom top)
     9.54559275E-05   2        24        25   # BR(Hp -> W h0)
     1.14526457E-07   2        24        35   # BR(Hp -> W HH)
     1.19522287E-07   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.07248627E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.38218555E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.38291041E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99865340E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.99239107E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.85773108E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.07248627E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.38218555E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.38291041E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99997644E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.35577737E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99997644E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.35577737E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26033336E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.13596476E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.17365719E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.35577737E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99997644E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.02387306E-04   # BR(b -> s gamma)
    2    1.58855136E-06   # BR(b -> s mu+ mu-)
    3    3.52559881E-05   # BR(b -> s nu nu)
    4    2.53839947E-15   # BR(Bd -> e+ e-)
    5    1.08437509E-10   # BR(Bd -> mu+ mu-)
    6    2.26983106E-08   # BR(Bd -> tau+ tau-)
    7    8.63231302E-14   # BR(Bs -> e+ e-)
    8    3.68771981E-09   # BR(Bs -> mu+ mu-)
    9    7.82105016E-07   # BR(Bs -> tau+ tau-)
   10    8.56133611E-05   # BR(B_u -> tau nu)
   11    8.84353244E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41969366E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93201670E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15774223E-03   # epsilon_K
   17    2.28167635E-15   # Delta(M_K)
   18    2.48012452E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29080665E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.55744039E-16   # Delta(g-2)_electron/2
   21   -1.09338830E-11   # Delta(g-2)_muon/2
   22   -3.09496831E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.97573594E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.69330272E-01   # C7
     0305 4322   00   2    -1.58861570E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.81433962E-01   # C8
     0305 6321   00   2    -1.69589127E-03   # C8'
 03051111 4133   00   0     1.62122475E+00   # C9 e+e-
 03051111 4133   00   2     1.62152885E+00   # C9 e+e-
 03051111 4233   00   2     1.04079106E-05   # C9' e+e-
 03051111 4137   00   0    -4.44391685E+00   # C10 e+e-
 03051111 4137   00   2    -4.44257517E+00   # C10 e+e-
 03051111 4237   00   2    -7.60887037E-05   # C10' e+e-
 03051313 4133   00   0     1.62122475E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62152882E+00   # C9 mu+mu-
 03051313 4233   00   2     1.03939847E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44391685E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44257520E+00   # C10 mu+mu-
 03051313 4237   00   2    -7.60748390E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50506149E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.64725430E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50506149E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.64755564E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50506150E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.73247435E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85828105E-07   # C7
     0305 4422   00   2    -1.21921669E-05   # C7
     0305 4322   00   2    -2.28608238E-07   # C7'
     0305 6421   00   0     3.30487288E-07   # C8
     0305 6421   00   2     4.96067012E-06   # C8
     0305 6321   00   2     9.22932736E-08   # C8'
 03051111 4133   00   2     7.38297232E-07   # C9 e+e-
 03051111 4233   00   2     3.21482795E-06   # C9' e+e-
 03051111 4137   00   2     8.60340447E-08   # C10 e+e-
 03051111 4237   00   2    -2.39653154E-05   # C10' e+e-
 03051313 4133   00   2     7.38295749E-07   # C9 mu+mu-
 03051313 4233   00   2     3.21482763E-06   # C9' mu+mu-
 03051313 4137   00   2     8.60355831E-08   # C10 mu+mu-
 03051313 4237   00   2    -2.39653164E-05   # C10' mu+mu-
 03051212 4137   00   2     1.45437561E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     5.18855882E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     1.45438254E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     5.18855882E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     1.45636630E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     5.18855881E-06   # C11' nu_3 nu_3
