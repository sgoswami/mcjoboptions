# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 20.12.2022,  14:30
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    6.22810357E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.90696182E+03  # scale for input parameters
    1   -1.77116601E+02  # M_1
    2   -6.37039873E+02  # M_2
    3    3.27637131E+03  # M_3
   11    2.43674925E+02  # A_t
   12    1.34689455E+03  # A_b
   13    1.00795139E+03  # A_tau
   23    8.04569763E+02  # mu
   25    5.90985164E+00  # tan(beta)
   26    1.55330389E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.30984956E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.40387293E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.28521850E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.90696182E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.90696182E+03  # (SUSY scale)
  1  1     8.50355439E-06   # Y_u(Q)^DRbar
  2  2     4.31980563E-03   # Y_c(Q)^DRbar
  3  3     1.02729767E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.90696182E+03  # (SUSY scale)
  1  1     1.00985105E-04   # Y_d(Q)^DRbar
  2  2     1.91871699E-03   # Y_s(Q)^DRbar
  3  3     1.00145580E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.90696182E+03  # (SUSY scale)
  1  1     1.76226392E-05   # Y_e(Q)^DRbar
  2  2     3.64380288E-03   # Y_mu(Q)^DRbar
  3  3     6.12827700E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.90696182E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.43674802E+02   # A_t(Q)^DRbar
Block Ad Q=  3.90696182E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.34689965E+03   # A_b(Q)^DRbar
Block Ae Q=  3.90696182E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.00795166E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.90696182E+03  # soft SUSY breaking masses at Q
   1   -1.77116601E+02  # M_1
   2   -6.37039873E+02  # M_2
   3    3.27637131E+03  # M_3
  21    1.67264188E+06  # M^2_(H,d)
  22   -2.37404584E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.30984956E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.40387293E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.28521850E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.19024276E+02  # h0
        35     1.55349907E+03  # H0
        36     1.55330389E+03  # A0
        37     1.56093969E+03  # H+
   1000001     1.01074487E+04  # ~d_L
   2000001     1.00826841E+04  # ~d_R
   1000002     1.01070984E+04  # ~u_L
   2000002     1.00859726E+04  # ~u_R
   1000003     1.01074491E+04  # ~s_L
   2000003     1.00826842E+04  # ~s_R
   1000004     1.01070989E+04  # ~c_L
   2000004     1.00859733E+04  # ~c_R
   1000005     3.41732430E+03  # ~b_1
   2000005     3.42605917E+03  # ~b_2
   1000006     3.42811516E+03  # ~t_1
   2000006     4.45269483E+03  # ~t_2
   1000011     1.00213031E+04  # ~e_L-
   2000011     1.00087915E+04  # ~e_R-
   1000012     1.00205508E+04  # ~nu_eL
   1000013     1.00213035E+04  # ~mu_L-
   2000013     1.00087921E+04  # ~mu_R-
   1000014     1.00205511E+04  # ~nu_muL
   1000015     1.00089700E+04  # ~tau_1-
   2000015     1.00214082E+04  # ~tau_2-
   1000016     1.00206467E+04  # ~nu_tauL
   1000021     3.74705792E+03  # ~g
   1000022     1.78777453E+02  # ~chi_10
   1000023     6.77435044E+02  # ~chi_20
   1000025     8.16109422E+02  # ~chi_30
   1000035     8.29303552E+02  # ~chi_40
   1000024     6.77691274E+02  # ~chi_1+
   1000037     8.30873479E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.60581556E-01   # alpha
Block Hmix Q=  3.90696182E+03  # Higgs mixing parameters
   1    8.04569763E+02  # mu
   2    5.90985164E+00  # tan[beta](Q)
   3    2.42990497E+02  # v(Q)
   4    2.41275297E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99995229E-01   # Re[R_st(1,1)]
   1  2     3.08886631E-03   # Re[R_st(1,2)]
   2  1    -3.08886631E-03   # Re[R_st(2,1)]
   2  2     9.99995229E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.38521732E-01   # Re[R_sb(1,1)]
   1  2     9.90359394E-01   # Re[R_sb(1,2)]
   2  1    -9.90359394E-01   # Re[R_sb(2,1)]
   2  2     1.38521732E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     2.69789829E-02   # Re[R_sta(1,1)]
   1  2     9.99636001E-01   # Re[R_sta(1,2)]
   2  1    -9.99636001E-01   # Re[R_sta(2,1)]
   2  2     2.69789829E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.98526960E-01   # Re[N(1,1)]
   1  2    -1.02121148E-03   # Re[N(1,2)]
   1  3    -5.41633627E-02   # Re[N(1,3)]
   1  4    -3.03262342E-03   # Re[N(1,4)]
   2  1    -1.31606361E-02   # Re[N(2,1)]
   2  2    -9.49168493E-01   # Re[N(2,2)]
   2  3     2.49822206E-01   # Re[N(2,3)]
   2  4     1.91036213E-01   # Re[N(2,4)]
   3  1     3.61223469E-02   # Re[N(3,1)]
   3  2    -4.36382221E-02   # Re[N(3,2)]
   3  3    -7.04707782E-01   # Re[N(3,3)]
   3  4     7.07232510E-01   # Re[N(3,4)]
   4  1    -3.82868549E-02   # Re[N(4,1)]
   4  2     3.11727180E-01   # Re[N(4,2)]
   4  3     6.61847518E-01   # Re[N(4,3)]
   4  4     6.80674772E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.35077659E-01   # Re[U(1,1)]
   1  2     3.54442902E-01   # Re[U(1,2)]
   2  1    -3.54442902E-01   # Re[U(2,1)]
   2  2    -9.35077659E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.62393114E-01   # Re[V(1,1)]
   1  2     2.71660623E-01   # Re[V(1,2)]
   2  1     2.71660623E-01   # Re[V(2,1)]
   2  2    -9.62393114E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02565723E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.97093060E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.71735062E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.28840134E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.44680190E-03    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.43632467E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.76638303E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.78223103E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.79543403E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.56985182E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     5.32131293E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     7.61037123E-02    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02617932E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.96989702E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.74970026E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.31404314E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.46937743E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
DECAY   1000013     1.43635085E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.76622899E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.78219181E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.88571193E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.57060148E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     5.32121615E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     7.61023282E-02    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.18018572E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.67053538E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     3.01387450E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     8.06221405E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     6.39573189E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     5.64394499E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     9.83069638E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.44304436E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.73993499E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.76548383E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.81565659E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.82689796E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.28276320E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     7.66913102E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43635893E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.69864805E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.70073369E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.20017040E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.34203650E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.63618043E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     4.47015720E-02    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43638510E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.69848986E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.70068458E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.20014858E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.34197573E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.63610091E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     4.47166472E-02    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.44376160E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.65412995E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.68691226E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.19402844E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.32493340E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.61380054E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.89440575E-02    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.96831860E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.39793889E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.90574650E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.27194982E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.91089835E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.43528849E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.51385082E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.12447051E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.06024350E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.51644609E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.16271550E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.96833323E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.39791955E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.90572238E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.27199457E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.91088963E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.43526150E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.51881810E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.12487217E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.06024068E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.51690947E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.16266579E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     5.06353383E+00   # ~b_1
#    BR                NDA      ID1      ID2
     3.73584035E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     9.26714515E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     5.31701555E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.40529187E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     1.97340878E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.59180561E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.04364683E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.53131069E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.15844083E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.54543260E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.69403107E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.65506857E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.93632006E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
DECAY   2000002     6.14031644E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.65506963E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.63342723E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.27179482E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.95527973E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.48915824E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.61127913E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.12307616E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.90803071E-03    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.16242628E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.14039072E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.65502554E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.63331130E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.27183936E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.95526782E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.48914373E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.61359811E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.12307061E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.90885160E-03    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.16237652E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.06451874E+02   # ~t_1
#    BR                NDA      ID1      ID2
     4.48790735E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.42343684E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.99627699E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.85066664E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.42549546E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.41317493E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.79274476E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.13026779E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.62575988E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.54479843E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.04563655E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.88564221E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.13426702E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.78214394E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.35282565E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.77497023E-04    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
DECAY   1000024     2.74849865E-02   # chi^+_1
#    BR                NDA      ID1      ID2
     9.86962645E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.30370064E-02    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.95427742E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.27955219E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.46590542E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.77924548E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.44007702E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.52150968E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.61373363E-02   # chi^0_2
#    BR                NDA      ID1      ID2
     5.63622762E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     4.35379460E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     9.50031046E-04    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     2.65707704E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     1.15914079E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     1.15914079E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     9.92795082E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.83083633E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.71052131E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.13947002E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     1.35472960E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     6.72789785E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     2.37397372E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     3.47201856E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.47201856E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     8.22040169E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     5.13765076E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     9.59066718E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.21688042E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.49936079E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
DECAY   1000021     8.73738057E+00   # ~g
#    BR                NDA      ID1      ID2
     1.42906022E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.42906022E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.79010492E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.79010492E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.71323223E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.71323223E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     3.61730937E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
     3.47478935E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     8.44195621E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     2.30882457E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.90044029E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.63230237E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.37003303E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.37003303E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     2.66725300E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     2.66725300E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.27517182E-03   # Gamma(h0)
     2.26542275E-03   2        22        22   # BR(h0 -> photon photon)
     9.89471525E-04   2        22        23   # BR(h0 -> photon Z)
     1.48608831E-02   2        23        23   # BR(h0 -> Z Z)
     1.38341297E-01   2       -24        24   # BR(h0 -> W W)
     7.44774505E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.87728655E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.61428342E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.53697156E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.66103533E-07   2        -2         2   # BR(h0 -> Up up)
     3.22289672E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.86334664E-07   2        -1         1   # BR(h0 -> Down down)
     2.48231243E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.60956275E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     6.40294738E+00   # Gamma(HH)
     6.22892839E-07   2        22        22   # BR(HH -> photon photon)
     1.26562786E-06   2        22        23   # BR(HH -> photon Z)
     2.67444249E-04   2        23        23   # BR(HH -> Z Z)
     2.03461308E-04   2       -24        24   # BR(HH -> W W)
     1.24989258E-04   2        21        21   # BR(HH -> gluon gluon)
     1.53037416E-09   2       -11        11   # BR(HH -> Electron electron)
     6.81255963E-05   2       -13        13   # BR(HH -> Muon muon)
     1.96734264E-02   2       -15        15   # BR(HH -> Tau tau)
     1.81717395E-11   2        -2         2   # BR(HH -> Up up)
     3.52444785E-06   2        -4         4   # BR(HH -> Charm charm)
     2.44521807E-01   2        -6         6   # BR(HH -> Top top)
     1.21649907E-07   2        -1         1   # BR(HH -> Down down)
     4.40011196E-05   2        -3         3   # BR(HH -> Strange strange)
     1.12957241E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.11562319E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.43121730E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.43121730E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.48059350E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.82829735E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     5.53591611E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     6.55414815E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.02833304E-02   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.09796348E-01   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.24905861E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.49980197E-03   2        25        25   # BR(HH -> h0 h0)
DECAY        36     6.33159354E+00   # Gamma(A0)
     6.58654031E-07   2        22        22   # BR(A0 -> photon photon)
     2.16422882E-06   2        22        23   # BR(A0 -> photon Z)
     2.35865423E-04   2        21        21   # BR(A0 -> gluon gluon)
     1.52287294E-09   2       -11        11   # BR(A0 -> Electron electron)
     6.77917419E-05   2       -13        13   # BR(A0 -> Muon muon)
     1.95771230E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.74011717E-11   2        -2         2   # BR(A0 -> Up up)
     3.37454291E-06   2        -4         4   # BR(A0 -> Charm charm)
     2.47030849E-01   2        -6         6   # BR(A0 -> Top top)
     1.21055521E-07   2        -1         1   # BR(A0 -> Down down)
     4.37861331E-05   2        -3         3   # BR(A0 -> Strange strange)
     1.12409963E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.67270494E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.23838645E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.23838645E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.51811330E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.66932616E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     7.74904560E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     4.66805136E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.73401754E-02   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.21083769E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     8.40345532E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     3.58510987E-04   2        23        25   # BR(A0 -> Z h0)
     6.60384052E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     6.49180096E+00   # Gamma(Hp)
     1.54238650E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     6.59418397E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.86520669E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.06285680E-07   2        -1         2   # BR(Hp -> Down up)
     1.77967859E-06   2        -3         2   # BR(Hp -> Strange up)
     1.11167155E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.61784107E-07   2        -1         4   # BR(Hp -> Down charm)
     4.16772339E-05   2        -3         4   # BR(Hp -> Strange charm)
     1.55678837E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.68532763E-05   2        -1         6   # BR(Hp -> Down top)
     3.67512408E-04   2        -3         6   # BR(Hp -> Strange top)
     3.64726325E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.42811610E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.11250173E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     3.36972061E-06   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.63482788E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.54242666E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.52354844E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.55771998E-04   2        24        25   # BR(Hp -> W h0)
     4.77404843E-09   2        24        35   # BR(Hp -> W HH)
     5.43432110E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.18476806E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.50078696E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.49263464E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00233415E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.62975347E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.86316807E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.18476806E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.50078696E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.49263464E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99950443E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.95566200E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99950443E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.95566200E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27448327E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.25160258E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.82516829E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.95566200E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99950443E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.38826790E-04   # BR(b -> s gamma)
    2    1.58915930E-06   # BR(b -> s mu+ mu-)
    3    3.52700633E-05   # BR(b -> s nu nu)
    4    2.56311662E-15   # BR(Bd -> e+ e-)
    5    1.09493432E-10   # BR(Bd -> mu+ mu-)
    6    2.29213773E-08   # BR(Bd -> tau+ tau-)
    7    8.62996911E-14   # BR(Bs -> e+ e-)
    8    3.68672006E-09   # BR(Bs -> mu+ mu-)
    9    7.81985632E-07   # BR(Bs -> tau+ tau-)
   10    9.67098722E-05   # BR(B_u -> tau nu)
   11    9.98975955E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42820148E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93949049E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16073360E-03   # epsilon_K
   17    2.28169372E-15   # Delta(M_K)
   18    2.48117296E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29328161E-11   # BR(K^+ -> pi^+ nu nu)
   20   -7.37716510E-17   # Delta(g-2)_electron/2
   21   -3.15396925E-12   # Delta(g-2)_muon/2
   22   -8.92141681E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -1.71588644E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.07378519E-01   # C7
     0305 4322   00   2    -3.73731968E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.18514963E-01   # C8
     0305 6321   00   2    -4.61085991E-04   # C8'
 03051111 4133   00   0     1.62538852E+00   # C9 e+e-
 03051111 4133   00   2     1.62576901E+00   # C9 e+e-
 03051111 4233   00   2     8.35295303E-06   # C9' e+e-
 03051111 4137   00   0    -4.44808062E+00   # C10 e+e-
 03051111 4137   00   2    -4.44812670E+00   # C10 e+e-
 03051111 4237   00   2    -6.12069260E-05   # C10' e+e-
 03051313 4133   00   0     1.62538852E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62576901E+00   # C9 mu+mu-
 03051313 4233   00   2     8.35295112E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.44808062E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44812671E+00   # C10 mu+mu-
 03051313 4237   00   2    -6.12069244E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50536176E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.32382103E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50536176E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.32382107E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50536176E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.32383227E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822429E-07   # C7
     0305 4422   00   2    -9.93769551E-07   # C7
     0305 4322   00   2     1.55504169E-08   # C7'
     0305 6421   00   0     3.30482426E-07   # C8
     0305 6421   00   2    -1.41122911E-06   # C8
     0305 6321   00   2    -2.17044567E-08   # C8'
 03051111 4133   00   2     5.64995014E-07   # C9 e+e-
 03051111 4233   00   2     1.98636454E-07   # C9' e+e-
 03051111 4137   00   2     5.59885229E-07   # C10 e+e-
 03051111 4237   00   2    -1.46003154E-06   # C10' e+e-
 03051313 4133   00   2     5.64994923E-07   # C9 mu+mu-
 03051313 4233   00   2     1.98636453E-07   # C9' mu+mu-
 03051313 4137   00   2     5.59885325E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.46003154E-06   # C10' mu+mu-
 03051212 4137   00   2    -9.12647527E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     3.15790011E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -9.12647480E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     3.15790011E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -9.12634344E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     3.15790011E-07   # C11' nu_3 nu_3
