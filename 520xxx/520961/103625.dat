# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 20.12.2022,  15:40
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.95875185E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.72608109E+03  # scale for input parameters
    1   -1.79056613E+02  # M_1
    2   -5.54767446E+02  # M_2
    3    2.34465862E+03  # M_3
   11   -1.29145255E+02  # A_t
   12    8.50164247E+02  # A_b
   13   -1.81238645E+03  # A_tau
   23    1.46155633E+03  # mu
   25    5.67669700E+00  # tan(beta)
   26    1.45812670E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.66944919E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.64985890E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.55587060E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.72608109E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.72608109E+03  # (SUSY scale)
  1  1     8.51346981E-06   # Y_u(Q)^DRbar
  2  2     4.32484266E-03   # Y_c(Q)^DRbar
  3  3     1.02849553E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.72608109E+03  # (SUSY scale)
  1  1     9.71141608E-05   # Y_d(Q)^DRbar
  2  2     1.84516906E-03   # Y_s(Q)^DRbar
  3  3     9.63068163E-02   # Y_b(Q)^DRbar
Block Ye Q=  2.72608109E+03  # (SUSY scale)
  1  1     1.69471312E-05   # Y_e(Q)^DRbar
  2  2     3.50412924E-03   # Y_mu(Q)^DRbar
  3  3     5.89336893E-02   # Y_tau(Q)^DRbar
Block Au Q=  2.72608109E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.29145304E+02   # A_t(Q)^DRbar
Block Ad Q=  2.72608109E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     8.50164967E+02   # A_b(Q)^DRbar
Block Ae Q=  2.72608109E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.81238621E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.72608109E+03  # soft SUSY breaking masses at Q
   1   -1.79056613E+02  # M_1
   2   -5.54767446E+02  # M_2
   3    2.34465862E+03  # M_3
  21   -1.34282094E+05  # M^2_(H,d)
  22   -1.93090566E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.66944919E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.64985890E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.55587060E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.16127957E+02  # h0
        35     1.45844019E+03  # H0
        36     1.45812670E+03  # A0
        37     1.46377439E+03  # H+
   1000001     1.01088333E+04  # ~d_L
   2000001     1.00840922E+04  # ~d_R
   1000002     1.01084840E+04  # ~u_L
   2000002     1.00870297E+04  # ~u_R
   1000003     1.01088345E+04  # ~s_L
   2000003     1.00840925E+04  # ~s_R
   1000004     1.01084851E+04  # ~c_L
   2000004     1.00870316E+04  # ~c_R
   1000005     2.74398137E+03  # ~b_1
   2000005     4.60756588E+03  # ~b_2
   1000006     2.70012296E+03  # ~t_1
   2000006     2.75228878E+03  # ~t_2
   1000011     1.00210211E+04  # ~e_L-
   2000011     1.00090431E+04  # ~e_R-
   1000012     1.00202731E+04  # ~nu_eL
   1000013     1.00210221E+04  # ~mu_L-
   2000013     1.00090446E+04  # ~mu_R-
   1000014     1.00202740E+04  # ~nu_muL
   1000015     1.00094630E+04  # ~tau_1-
   2000015     1.00213367E+04  # ~tau_2-
   1000016     1.00205222E+04  # ~nu_tauL
   1000021     2.74677915E+03  # ~g
   1000022     1.81972298E+02  # ~chi_10
   1000023     6.06125009E+02  # ~chi_20
   1000025     1.47693148E+03  # ~chi_30
   1000035     1.47698375E+03  # ~chi_40
   1000024     6.06163903E+02  # ~chi_1+
   1000037     1.47886726E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.67864982E-01   # alpha
Block Hmix Q=  2.72608109E+03  # Higgs mixing parameters
   1    1.46155633E+03  # mu
   2    5.67669700E+00  # tan[beta](Q)
   3    2.43384542E+02  # v(Q)
   4    2.12613347E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     3.17140468E-01   # Re[R_st(1,1)]
   1  2     9.48378576E-01   # Re[R_st(1,2)]
   2  1    -9.48378576E-01   # Re[R_st(2,1)]
   2  2     3.17140468E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99999263E-01   # Re[R_sb(1,1)]
   1  2     1.21386805E-03   # Re[R_sb(1,2)]
   2  1    -1.21386805E-03   # Re[R_sb(2,1)]
   2  2     9.99999263E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     7.55985545E-02   # Re[R_sta(1,1)]
   1  2     9.97138335E-01   # Re[R_sta(1,2)]
   2  1    -9.97138335E-01   # Re[R_sta(2,1)]
   2  2     7.55985545E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99557606E-01   # Re[N(1,1)]
   1  2    -1.29479941E-03   # Re[N(1,2)]
   1  3    -2.96752181E-02   # Re[N(1,3)]
   1  4     1.51558330E-03   # Re[N(1,4)]
   2  1     4.33402949E-04   # Re[N(2,1)]
   2  2     9.98160387E-01   # Re[N(2,2)]
   2  3    -5.88870248E-02   # Re[N(2,3)]
   2  4    -1.44212548E-02   # Re[N(2,4)]
   3  1     2.20761164E-02   # Re[N(3,1)]
   3  2    -3.14516038E-02   # Re[N(3,2)]
   3  3    -7.06111839E-01   # Re[N(3,3)]
   3  4     7.07056938E-01   # Re[N(3,4)]
   4  1     1.99260964E-02   # Re[N(4,1)]
   4  2    -5.18166212E-02   # Re[N(4,2)]
   4  3    -7.05023241E-01   # Re[N(4,3)]
   4  4    -7.07007933E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.96531956E-01   # Re[U(1,1)]
   1  2     8.32109454E-02   # Re[U(1,2)]
   2  1    -8.32109454E-02   # Re[U(2,1)]
   2  2    -9.96531956E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.99788422E-01   # Re[V(1,1)]
   1  2     2.05696616E-02   # Re[V(1,2)]
   2  1     2.05696616E-02   # Re[V(2,1)]
   2  2    -9.99788422E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02566575E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99152911E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     4.66689285E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     3.80211876E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.43911700E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.77593024E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.03275695E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.13351417E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     4.97210063E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.04292964E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     4.06147758E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02613451E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.99059918E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.89855917E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.03314912E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
DECAY   1000013     1.43914058E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.77578884E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.03270815E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.21466114E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     5.05293692E-04    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.04283125E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     4.06141148E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.21189718E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.59632775E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     5.75764774E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     6.31418068E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     6.01147699E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.14745448E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.08093749E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.44042147E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.90889449E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     3.00957968E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.57651298E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     3.02539769E-03    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.99641197E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.70997863E-03    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43914713E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.69039090E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.02965144E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.48496156E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.14425693E-03    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08190033E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     2.48161100E-04    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43917069E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.69024942E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.02960212E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.48487230E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.14423831E-03    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.08180249E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     2.64318886E-04    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.44581069E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.65055332E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.01576423E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.45982985E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.13901402E-03    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     6.05435303E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.79774374E-03    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.87864811E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.17200200E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.91821060E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.18354848E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.69650048E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.37734204E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.59621727E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.07212696E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     7.21044413E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.36371523E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.87866142E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.17198729E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.91819179E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.18359227E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.69649234E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.37731419E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.60018209E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.07212132E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     7.25445299E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.36367177E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.20155019E+01   # ~b_1
#    BR                NDA      ID1      ID2
     6.03592772E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.75562831E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.17763473E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.35003035E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.46607235E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     4.67266341E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.55151223E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.65171602E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.20781971E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.20049255E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.37285674E-03    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     9.74020543E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.50811494E-04    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     1.40326498E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     7.05011812E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.19022865E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.68070648E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.18341493E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.74655852E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.37890463E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.21320572E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.07912518E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.36347056E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.05019143E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.19019596E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.68060780E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.18345830E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.74654940E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.37887663E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.23519083E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.07911964E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.36342706E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     5.91817904E+01   # ~t_1
#    BR                NDA      ID1      ID2
     9.09500239E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.47978585E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.14371335E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.22825431E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.98188397E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.27105685E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.30827151E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     6.44408425E+01   # ~t_2
#    BR                NDA      ID1      ID2
     1.44572222E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.54849578E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.41705108E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.30599181E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.12619299E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.57696114E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
DECAY   1000024     2.19550705E-04   # chi^+_1
#    BR                NDA      ID1      ID2
     9.84523368E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.54623427E-02    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     8.10307645E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.14866722E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.88318633E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.83596447E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.92236775E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.38703192E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.58534460E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.09978201E-02    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.19097943E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     8.52477988E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     1.46928099E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     1.12653497E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     2.02935935E-04    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
     2.48835677E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
DECAY   1000025     1.11729745E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.07164997E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.07164997E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     3.71443290E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.53255035E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     6.72813481E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     3.14224831E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.47887156E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     3.54177156E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     4.36643513E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     4.36643513E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     8.16250137E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.83546838E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.83546838E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.22193894E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     7.67728610E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.66983394E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.16941277E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.82174751E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.77116278E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.28317881E-02    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.28317881E-02    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     5.72064938E-01   # ~g
#    BR                NDA      ID1      ID2
     2.56312894E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
     2.66699479E-03    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     4.36881570E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.71234040E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     6.28767213E-02    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     9.57124948E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.18851631E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     7.80378420E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.14562928E-01    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     8.48361921E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     1.18628906E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.18628906E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.57775270E-01    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.57775270E-01    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.05430619E-03   # Gamma(h0)
     2.20600599E-03   2        22        22   # BR(h0 -> photon photon)
     7.67941208E-04   2        22        23   # BR(h0 -> photon Z)
     1.05025574E-02   2        23        23   # BR(h0 -> Z Z)
     1.03011194E-01   2       -24        24   # BR(h0 -> W W)
     7.44561203E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.15965715E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.73986455E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.89847637E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.74529121E-07   2        -2         2   # BR(h0 -> Up up)
     3.38618783E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.22500420E-07   2        -1         1   # BR(h0 -> Down down)
     2.61311265E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.95673338E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.32450615E+00   # Gamma(HH)
     1.64148578E-06   2        22        22   # BR(HH -> photon photon)
     1.12557552E-06   2        22        23   # BR(HH -> photon Z)
     8.08315568E-04   2        23        23   # BR(HH -> Z Z)
     6.71711243E-04   2       -24        24   # BR(HH -> W W)
     3.88300252E-04   2        21        21   # BR(HH -> gluon gluon)
     3.59202006E-09   2       -11        11   # BR(HH -> Electron electron)
     1.59898191E-04   2       -13        13   # BR(HH -> Muon muon)
     4.61751813E-02   2       -15        15   # BR(HH -> Tau tau)
     5.14179637E-11   2        -2         2   # BR(HH -> Up up)
     9.97235667E-06   2        -4         4   # BR(HH -> Charm charm)
     6.60381477E-01   2        -6         6   # BR(HH -> Top top)
     2.87362729E-07   2        -1         1   # BR(HH -> Down down)
     1.03939877E-04   2        -3         3   # BR(HH -> Strange strange)
     2.64580045E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.02921457E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.05896665E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.85903216E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     5.14629866E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.36165811E-03   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.35234841E+00   # Gamma(A0)
     1.96178073E-06   2        22        22   # BR(A0 -> photon photon)
     2.06083501E-06   2        22        23   # BR(A0 -> photon Z)
     6.87645107E-04   2        21        21   # BR(A0 -> gluon gluon)
     3.48714674E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.55229779E-04   2       -13        13   # BR(A0 -> Muon muon)
     4.48273222E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.82053558E-11   2        -2         2   # BR(A0 -> Up up)
     9.34814419E-06   2        -4         4   # BR(A0 -> Charm charm)
     6.57255254E-01   2        -6         6   # BR(A0 -> Top top)
     2.78981483E-07   2        -1         1   # BR(A0 -> Down down)
     1.00908426E-04   2        -3         3   # BR(A0 -> Strange strange)
     2.56887956E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.07209683E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.14930242E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     6.78479289E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.03517412E-02   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.06522562E-03   2        23        25   # BR(A0 -> Z h0)
     1.43041588E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.46128635E+00   # Gamma(Hp)
     3.49207238E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.49297000E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     4.22295867E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.42210234E-07   2        -1         2   # BR(Hp -> Down up)
     4.05216822E-06   2        -3         2   # BR(Hp -> Strange up)
     2.51832910E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.38240792E-07   2        -1         4   # BR(Hp -> Down charm)
     9.64805584E-05   2        -3         4   # BR(Hp -> Strange charm)
     3.52669917E-04   2        -5         4   # BR(Hp -> Bottom charm)
     4.57238829E-05   2        -1         6   # BR(Hp -> Down top)
     9.97057213E-04   2        -3         6   # BR(Hp -> Strange top)
     9.42964923E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.21240603E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     8.73620959E-10   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.03294075E-03   2        24        25   # BR(Hp -> W h0)
     2.38278728E-09   2        24        35   # BR(Hp -> W HH)
     3.17019898E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.27471636E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.22974172E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.22248888E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00225069E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.87812206E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.10319147E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.27471636E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.22974172E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.22248888E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99957688E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.23119122E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99957688E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.23119122E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27051533E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.50113269E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.88890699E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.23119122E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99957688E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.39516164E-04   # BR(b -> s gamma)
    2    1.58846936E-06   # BR(b -> s mu+ mu-)
    3    3.52556352E-05   # BR(b -> s nu nu)
    4    2.54722250E-15   # BR(Bd -> e+ e-)
    5    1.08814454E-10   # BR(Bd -> mu+ mu-)
    6    2.27792294E-08   # BR(Bd -> tau+ tau-)
    7    8.57994016E-14   # BR(Bs -> e+ e-)
    8    3.66534770E-09   # BR(Bs -> mu+ mu-)
    9    7.77451921E-07   # BR(Bs -> tau+ tau-)
   10    9.67089118E-05   # BR(B_u -> tau nu)
   11    9.98966035E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43336191E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94134085E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16288253E-03   # epsilon_K
   17    2.28171568E-15   # Delta(M_K)
   18    2.48026853E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29111252E-11   # BR(K^+ -> pi^+ nu nu)
   20   -7.66538530E-17   # Delta(g-2)_electron/2
   21   -3.27719221E-12   # Delta(g-2)_muon/2
   22   -9.27096741E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -1.52539921E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.07959655E-01   # C7
     0305 4322   00   2    -3.99362365E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.19632602E-01   # C8
     0305 6321   00   2    -4.90635857E-04   # C8'
 03051111 4133   00   0     1.61280954E+00   # C9 e+e-
 03051111 4133   00   2     1.61330956E+00   # C9 e+e-
 03051111 4233   00   2     6.34356965E-06   # C9' e+e-
 03051111 4137   00   0    -4.43550164E+00   # C10 e+e-
 03051111 4137   00   2    -4.43412740E+00   # C10 e+e-
 03051111 4237   00   2    -4.74364280E-05   # C10' e+e-
 03051313 4133   00   0     1.61280954E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61330956E+00   # C9 mu+mu-
 03051313 4233   00   2     6.34356770E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.43550164E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43412740E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.74364263E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50505370E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.02890436E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50505370E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.02890440E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50505370E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.02891592E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85823650E-07   # C7
     0305 4422   00   2    -1.50949950E-06   # C7
     0305 4322   00   2     2.97847760E-08   # C7'
     0305 6421   00   0     3.30483472E-07   # C8
     0305 6421   00   2    -1.52594560E-06   # C8
     0305 6321   00   2    -1.53012839E-08   # C8'
 03051111 4133   00   2     7.59301561E-07   # C9 e+e-
 03051111 4233   00   2     1.61599761E-07   # C9' e+e-
 03051111 4137   00   2     9.16774090E-07   # C10 e+e-
 03051111 4237   00   2    -1.21220413E-06   # C10' e+e-
 03051313 4133   00   2     7.59301484E-07   # C9 mu+mu-
 03051313 4233   00   2     1.61599760E-07   # C9' mu+mu-
 03051313 4137   00   2     9.16774173E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.21220413E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.68818114E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.62933771E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.68818110E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.62933771E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.68816985E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.62933770E-07   # C11' nu_3 nu_3
