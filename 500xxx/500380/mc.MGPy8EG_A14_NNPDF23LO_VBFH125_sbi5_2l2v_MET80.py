evgenConfig.inputconfcheck="sbi5_2l2v"
evgenConfig.nEventsPerJob = 2000

proc_name="VBF2l2v_SBI5"
include("MadGraphControl_Pythia8EvtGen_2l2vjj_EW6.py")

# MET filter
include("GeneratorFilters/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 80*GeV
