from MadGraphControl.MadGraphUtils import *

avgtau = 125
#Zdmass = 45
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 6500.

# MadGraph info
rcard='MadGraph_run_card_ZpMuMu.dat'
#pcard='param_card35.dat'
pcard='MadGraph_param_card_HAHMv3_ZpMuMu_mZd'+str(Zdmass)+'K_mH125.dat'
rname = 'run_ZpMuMu'+str(runArgs.jobConfig).strip("[]'./")

# initialise random number generator/sequence
import random
random.seed(runArgs.randomSeed)

# do not run MadGraph if config only is requested
if not opts.config_only:
    # writing proc card for MG
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""

import model --modelname HAHM_variableMW_v3_UFO
define j =  d d~ s s~ u u~ c c~ b b~
generate j j > zp, (zp > m+ m-) 
output -f
""")
#define f = u c d s u~ c~ d~ s~ b b~ e+ e- m+ m- tt+ tt- ve vm vt ve~ vm~ vt~
#generate zp > f f
    fcard.close()

    # getting run cards
    from PyJobTransformsCore.trfutil import get_files
    import subprocess
    get_dat_file = subprocess.Popen(['get_files','-jo', rcard])
    get_dat_file.wait()
    get_dat_file = subprocess.Popen(['get_files','-jo', pcard])
    get_dat_file.wait()
    #get_files( rcard, keepDir=False, errorIfNotFound=True )
    #get_files( pcard, keepDir=False, errorIfNotFound=True )

    # generating events in MG
    process_dir = new_process()
    generate(run_card_loc=rcard,param_card_loc=pcard,mode=0,njobs=1,run_name=rname,proc_dir=process_dir)

    # hacking LHE file
    unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
    unzip1.wait()
    oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
    newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')

    init = True

    for line in oldlhe:
        if init==True:
            if '30000016' in line:
                line = line.replace('30000016','3000016')
            elif '30000015' in line:
                line = line.replace('30000015','1000022')
            newlhe.write(line)
            if '</init>' in line:
                init = False
        else:
            newline=line.rstrip('\n')
            columns=newline.split()
            pdgid=columns[0]

            if (pdgid == '3000001') :
                part1 = line[:-22]
                part2 = "%.5e" % (0)
                part3 = line[-12:]
                newlhe.write(part1+part2+part3)

            elif (pdgid == '-30000016') :
                part1 = ' -3000016'
                part2 = line[10:]
                newlhe.write(part1+part2)
            elif (pdgid == '30000016') :
                part1 = '  3000016'
                part2 = line[10:]
                newlhe.write(part1+part2)
            elif (pdgid == '-30000015') :
                part1 = ' -1000022'
                part2 = line[10:]
                newlhe.write(part1+part2)
            elif (pdgid == '30000015') :
                part1 = '  1000022'
                part2 = line[10:]
                newlhe.write(part1+part2)
            else:
                newlhe.write(line)
        
    oldlhe.close()
    newlhe.close()
    
    # re-zipping hacked LHE
    zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
    zip1.wait()
    shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
    os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

    arrange_output(run_name=rname,proc_dir=process_dir,outputDS=rname+'._00001.events.tar.gz')

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    opts.nprocs = 0
runArgs.inputGeneratorFile=rname+'._00001.events.tar.gz'


#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
#include ( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")


from GeneratorFilters.GeneratorFiltersConf import DiLeptonMassFilter
filtSeq += DiLeptonMassFilter()
filtSeq.DiLeptonMassFilter.MinPt = 4000.
filtSeq.DiLeptonMassFilter.MaxEta = 3.0
#filtSeq.DiLeptonMassFilter.MinMass = 20000.
#filtSeq.DiLeptonMassFilter.MaxMass = 14000000.
#filtSeq.DiLeptonMassFilter.AllowSameCharge = False
#filtSeq.DiLeptonMassFilter.MinDilepPt = 150000.


#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]


#relax the cuts on displaced vertices and non G4 particles
testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = 'HAHM_v3 pp -> Zp -> mu+ mu-'
evgenConfig.contact  = ['damiano.vannicola@cern.ch']
evgenConfig.process="SuperDarkPhoton"
evgenConfig.inputfilecheck = rname
evgenConfig.nEventsPerJob=10000
runArgs.inputGeneratorFile=rname+'._00001.events.tar.gz'

