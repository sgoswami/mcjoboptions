import subprocess
retcode = subprocess.call(['get_files', '-jo', 'HeavyNCommon.py'])
if retcode != 0:
    raise IOError('could not locate HeavyNCommon.py')

import HeavyNCommon

HeavyNCommon.process = HeavyNCommon.available_processes['tautauchannel']
HeavyNCommon.parameters_paramcard['mass']['mN3'] = 1000
HeavyNCommon.parameters_paramcard['numixing']['VtaN1'] = 1
HeavyNCommon.parameters_paramcard['numixing']['VtaN2'] = 1
HeavyNCommon.parameters_paramcard['numixing']['VtaN3'] = 1

HeavyNCommon.run_evgen(runArgs, evgenConfig, opts)
