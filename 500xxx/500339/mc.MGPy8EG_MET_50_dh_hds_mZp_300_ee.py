model="darkHiggs"
fs = "ee"
mDM1 = 5.
mDM2 = 5.
mZp = 300.
mHD = 300.

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Jared Little <jared.little@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")
