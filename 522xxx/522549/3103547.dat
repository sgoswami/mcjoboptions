# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:51
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.20903713E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.62689398E+03  # scale for input parameters
    1    2.12753862E+02  # M_1
    2    2.69095777E+02  # M_2
    3    2.96255436E+03  # M_3
   11   -7.90963279E+02  # A_t
   12    5.39766697E+02  # A_b
   13    1.16145111E+03  # A_tau
   23   -2.88333692E+02  # mu
   25    1.15135630E+01  # tan(beta)
   26    2.60305157E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.07219865E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.09834672E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.37108139E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.62689398E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.62689398E+03  # (SUSY scale)
  1  1     8.41593703E-06   # Y_u(Q)^DRbar
  2  2     4.27529601E-03   # Y_c(Q)^DRbar
  3  3     1.01671279E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.62689398E+03  # (SUSY scale)
  1  1     1.94711887E-04   # Y_d(Q)^DRbar
  2  2     3.69952586E-03   # Y_s(Q)^DRbar
  3  3     1.93093178E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.62689398E+03  # (SUSY scale)
  1  1     3.39786482E-05   # Y_e(Q)^DRbar
  2  2     7.02570679E-03   # Y_mu(Q)^DRbar
  3  3     1.18160830E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.62689398E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -7.90963300E+02   # A_t(Q)^DRbar
Block Ad Q=  3.62689398E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     5.39766694E+02   # A_b(Q)^DRbar
Block Ae Q=  3.62689398E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.16145110E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.62689398E+03  # soft SUSY breaking masses at Q
   1    2.12753862E+02  # M_1
   2    2.69095777E+02  # M_2
   3    2.96255436E+03  # M_3
  21   -3.06664608E+04  # M^2_(H,d)
  22    2.19516318E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.07219865E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.09834672E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.37108139E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21865716E+02  # h0
        35     2.61032217E+02  # H0
        36     2.60305157E+02  # A0
        37     2.73744871E+02  # H+
   1000001     1.01083419E+04  # ~d_L
   2000001     1.00831410E+04  # ~d_R
   1000002     1.01079774E+04  # ~u_L
   2000002     1.00864556E+04  # ~u_R
   1000003     1.01083427E+04  # ~s_L
   2000003     1.00831417E+04  # ~s_R
   1000004     1.01079782E+04  # ~c_L
   2000004     1.00864565E+04  # ~c_R
   1000005     2.51523198E+03  # ~b_1
   2000005     4.14479411E+03  # ~b_2
   1000006     3.17192437E+03  # ~t_1
   2000006     4.14712282E+03  # ~t_2
   1000011     1.00216181E+04  # ~e_L-
   2000011     1.00087757E+04  # ~e_R-
   1000012     1.00208484E+04  # ~nu_eL
   1000013     1.00216199E+04  # ~mu_L-
   2000013     1.00087791E+04  # ~mu_R-
   1000014     1.00208502E+04  # ~nu_muL
   1000015     1.00097435E+04  # ~tau_1-
   2000015     1.00221333E+04  # ~tau_2-
   1000016     1.00213504E+04  # ~nu_tauL
   1000021     3.41569287E+03  # ~g
   1000022     2.06020220E+02  # ~chi_10
   1000023     2.58608397E+02  # ~chi_20
   1000025     3.10410669E+02  # ~chi_30
   1000035     3.58063024E+02  # ~chi_40
   1000024     2.54904582E+02  # ~chi_1+
   1000037     3.57991704E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.16032632E-01   # alpha
Block Hmix Q=  3.62689398E+03  # Higgs mixing parameters
   1   -2.88333692E+02  # mu
   2    1.15135630E+01  # tan[beta](Q)
   3    2.43057890E+02  # v(Q)
   4    6.77587748E+04  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     2.00995670E-02   # Re[R_st(1,1)]
   1  2     9.99797983E-01   # Re[R_st(1,2)]
   2  1    -9.99797983E-01   # Re[R_st(2,1)]
   2  2     2.00995670E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -7.42031593E-04   # Re[R_sb(1,1)]
   1  2     9.99999725E-01   # Re[R_sb(1,2)]
   2  1    -9.99999725E-01   # Re[R_sb(2,1)]
   2  2    -7.42031593E-04   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -3.18887102E-02   # Re[R_sta(1,1)]
   1  2     9.99491426E-01   # Re[R_sta(1,2)]
   2  1    -9.99491426E-01   # Re[R_sta(2,1)]
   2  2    -3.18887102E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.20538017E-01   # Re[N(1,1)]
   1  2    -1.45427186E-01   # Re[N(1,2)]
   1  3    -3.06262486E-01   # Re[N(1,3)]
   1  4    -1.94072104E-01   # Re[N(1,4)]
   2  1    -3.49727232E-01   # Re[N(2,1)]
   2  2    -7.14248719E-01   # Re[N(2,2)]
   2  3    -4.67215038E-01   # Re[N(2,3)]
   2  4    -3.86328537E-01   # Re[N(2,4)]
   3  1    -6.50762418E-02   # Re[N(3,1)]
   3  2     1.01665424E-01   # Re[N(3,2)]
   3  3    -6.93784592E-01   # Re[N(3,3)]
   3  4     7.09994482E-01   # Re[N(3,4)]
   4  1    -1.61448767E-01   # Re[N(4,1)]
   4  2     6.77025733E-01   # Re[N(4,2)]
   4  3    -4.54506697E-01   # Re[N(4,3)]
   4  4    -5.55872391E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.50791803E-01   # Re[U(1,1)]
   1  2    -7.59256234E-01   # Re[U(1,2)]
   2  1     7.59256234E-01   # Re[U(2,1)]
   2  2    -6.50791803E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -7.90328152E-01   # Re[V(1,1)]
   1  2     6.12683779E-01   # Re[V(1,2)]
   2  1     6.12683779E-01   # Re[V(2,1)]
   2  2     7.90328152E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02423646E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.47482503E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.22262765E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.23082121E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     2.60239066E-02    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44650093E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     3.65484152E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.47269544E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.36221488E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     1.06159570E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.57973033E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.50687224E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02620024E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     8.47170001E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.22257663E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.32310972E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.60540373E-02    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.12580887E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.44659942E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     3.65523084E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.47267585E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.39480385E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.06166376E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.57955515E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.50663411E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.58917726E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.68340052E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.24403582E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     2.65087963E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.95327308E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.80978362E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.31170023E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.47342133E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.69760357E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.45499423E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.08670001E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.09679770E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.49680435E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.47297337E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44654442E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.23696364E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.47043661E-02    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.66454392E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.77185534E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.80415818E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     2.28333373E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44664290E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.23687966E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.46986149E-02    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.66415931E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.77173504E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.80429154E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     2.28346602E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.47440215E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.21365304E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     8.31081144E-02    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.55779605E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.73846499E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.84117327E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.32004961E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.29336946E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.57347913E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.09260454E-03    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     2.32569467E-04    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.91063520E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.60705042E+02   # ~d_L
#    BR                NDA      ID1      ID2
     5.59252675E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.48287821E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     7.48782236E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     2.90609234E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     4.94798543E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     6.72641100E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.23025021E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.29342477E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.57382089E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.09354333E-03    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.33463811E-04    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.91054913E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.60711535E+02   # ~s_L
#    BR                NDA      ID1      ID2
     5.59281768E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.48293590E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     7.50509238E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     2.90614206E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     4.94812413E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     6.72665581E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.23018094E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     4.98943753E+00   # ~b_1
#    BR                NDA      ID1      ID2
     2.70215817E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.13896273E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.75564178E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     8.07148815E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.10012252E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.49596599E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.77094300E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.14186202E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     4.71961483E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     9.57413645E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.41001988E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.63647831E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.14454065E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.90202610E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     7.78024738E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.40615423E-03    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
DECAY   2000002     6.46527796E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.94981219E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.25560283E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.47263557E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.05831739E-04    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.65193180E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.60690308E+02   # ~u_L
#    BR                NDA      ID1      ID2
     3.52210808E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.73661939E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     2.45182104E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     7.29712715E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     4.37998019E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.22995981E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.46535172E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.94979994E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.25640092E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.50115796E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.07570728E-04    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.65182252E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.60696771E+02   # ~c_L
#    BR                NDA      ID1      ID2
     3.52215009E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.76088459E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     2.45194961E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     7.29727355E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     4.38009567E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.22989048E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.34415857E+02   # ~t_1
#    BR                NDA      ID1      ID2
     6.27031233E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     7.95466030E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.39317310E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.43170101E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.85904882E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.89337528E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.75814517E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.70729174E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.30958298E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.40794070E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.94913116E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.32627893E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     9.02521080E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.84447685E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.64203608E-04    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     3.93061193E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.13139116E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     3.28361723E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     8.04109449E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     8.36892637E-05   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.33784430E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.32839091E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11257538E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11258061E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10860880E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.87694898E-01   # chi^+_2
#    BR                NDA      ID1      ID2
     4.29147130E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     7.03394049E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.50096331E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
#    BR                NDA      ID1      ID2       ID3
     1.19234539E-03    3     1000025        -1         2   # BR(chi^+_2 -> chi^0_3 d_bar u)
     1.18889991E-03    3     1000025        -3         4   # BR(chi^+_2 -> chi^0_3 s_bar c)
     3.97446667E-04    3     1000025       -11        12   # BR(chi^+_2 -> chi^0_3 e^+ nu_e)
     3.97440807E-04    3     1000025       -13        14   # BR(chi^+_2 -> chi^0_3 mu^+ nu_mu)
     3.95181249E-04    3     1000025       -15        16   # BR(chi^+_2 -> chi^0_3 tau^+ nu_tau)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     6.83396881E-06   # chi^0_2
#    BR                NDA      ID1      ID2
     1.32935007E-02    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.17990729E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.17212732E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.51315817E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.51311452E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.44120043E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.41006769E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.41000121E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.39143541E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.02178049E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     2.59888861E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     9.86536167E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
#    BR                NDA      ID1      ID2       ID3
     2.33584553E-04    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     2.31976935E-04    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     2.99493694E-04    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     2.99483922E-04    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     2.81239678E-04    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     4.00520054E-04    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     1.88917235E-03    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     1.88917235E-03    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     1.88566066E-03    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     1.88566066E-03    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     6.29726976E-04    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     6.29726976E-04    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     6.29721761E-04    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     6.29721761E-04    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     6.27656526E-04    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     6.27656526E-04    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     3.10813842E-01   # chi^0_4
#    BR                NDA      ID1      ID2
     4.43404750E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.43404750E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.95083372E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     9.20486252E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.79678798E-04    3     1000025         2        -2   # BR(chi^0_4 -> chi^0_3 u u_bar)
     1.78168287E-04    3     1000025         4        -4   # BR(chi^0_4 -> chi^0_3 c c_bar)
     2.30390044E-04    3     1000025         1        -1   # BR(chi^0_4 -> chi^0_3 d d_bar)
     2.30380970E-04    3     1000025         3        -3   # BR(chi^0_4 -> chi^0_3 s s_bar)
     2.13619434E-04    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     3.08088302E-04    3     1000025        12       -12   # BR(chi^0_4 -> chi^0_3 nu_e nu_bar_e)
DECAY   1000021     2.27044958E+01   # ~g
#    BR                NDA      ID1      ID2
     2.93481453E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.93481453E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     4.66539098E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.66539098E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.43000520E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     5.57340823E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.49985512E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.07852289E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     9.87237127E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.87725168E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     9.09314695E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     9.09314695E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.32187556E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.32187556E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     5.42025211E-03   # Gamma(h0)
     1.49520933E-03   2        22        22   # BR(h0 -> photon photon)
     7.91179706E-04   2        22        23   # BR(h0 -> photon Z)
     1.32082456E-02   2        23        23   # BR(h0 -> Z Z)
     1.16226688E-01   2       -24        24   # BR(h0 -> W W)
     4.60364792E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.36456809E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.83105194E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.16237075E-02   2       -15        15   # BR(h0 -> Tau tau)
     8.97291291E-08   2        -2         2   # BR(h0 -> Up up)
     1.74132158E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.41099597E-07   2        -1         1   # BR(h0 -> Down down)
     2.68041768E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.22653291E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     6.73476696E-01   # Gamma(HH)
     4.11346309E-07   2        22        22   # BR(HH -> photon photon)
     1.19771401E-06   2        22        23   # BR(HH -> photon Z)
     2.42338036E-03   2        23        23   # BR(HH -> Z Z)
     5.63134379E-03   2       -24        24   # BR(HH -> W W)
     6.62092963E-04   2        21        21   # BR(HH -> gluon gluon)
     8.50973059E-09   2       -11        11   # BR(HH -> Electron electron)
     3.78611886E-04   2       -13        13   # BR(HH -> Muon muon)
     1.09278536E-01   2       -15        15   # BR(HH -> Tau tau)
     1.71549967E-11   2        -2         2   # BR(HH -> Up up)
     3.32532603E-06   2        -4         4   # BR(HH -> Charm charm)
     8.75373968E-07   2        -1         1   # BR(HH -> Down down)
     3.16615568E-04   2        -3         3   # BR(HH -> Strange strange)
     8.61991568E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.93120332E-02   2        25        25   # BR(HH -> h0 h0)
DECAY        36     6.57050278E-01   # Gamma(A0)
     3.67726117E-07   2        22        22   # BR(A0 -> photon photon)
     9.35435321E-07   2        22        23   # BR(A0 -> photon Z)
     2.21726894E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.73056148E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.88436923E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.12135205E-01   2       -15        15   # BR(A0 -> Tau tau)
     8.35864850E-12   2        -2         2   # BR(A0 -> Up up)
     1.61633993E-06   2        -4         4   # BR(A0 -> Charm charm)
     8.98469358E-07   2        -1         1   # BR(A0 -> Down down)
     3.24969008E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.85089404E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.83643107E-03   2        23        25   # BR(A0 -> Z h0)
     8.46187973E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     3.35685867E-01   # Gamma(Hp)
     1.97130070E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     8.42792368E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     2.38369754E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.75728014E-06   2        -1         2   # BR(Hp -> Down up)
     2.86592917E-05   2        -3         2   # BR(Hp -> Strange up)
     1.85899461E-05   2        -5         2   # BR(Hp -> Bottom up)
     2.55723399E-07   2        -1         4   # BR(Hp -> Down charm)
     6.37168635E-04   2        -3         4   # BR(Hp -> Strange charm)
     2.60315113E-03   2        -5         4   # BR(Hp -> Bottom charm)
     6.98845448E-06   2        -1         6   # BR(Hp -> Down top)
     1.52689022E-04   2        -3         6   # BR(Hp -> Strange top)
     7.50682244E-01   2        -5         6   # BR(Hp -> Bottom top)
     6.65298098E-03   2        24        25   # BR(Hp -> W h0)
     1.27201118E-06   2        24        35   # BR(Hp -> W HH)
     1.67767194E-06   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.79016766E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.31771965E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.32562133E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.94039266E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.35043667E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.54363239E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.79016766E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.31771965E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.32562133E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99136126E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.63873616E-04        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99136126E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.63873616E-04        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.21477406E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.46628314E-01        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.32002646E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.63873616E-04        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99136126E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    5.03856165E-04   # BR(b -> s gamma)
    2    1.60706734E-06   # BR(b -> s mu+ mu-)
    3    3.52876790E-05   # BR(b -> s nu nu)
    4    2.49586523E-15   # BR(Bd -> e+ e-)
    5    1.06620537E-10   # BR(Bd -> mu+ mu-)
    6    2.23204015E-08   # BR(Bd -> tau+ tau-)
    7    8.42604398E-14   # BR(Bs -> e+ e-)
    8    3.59960355E-09   # BR(Bs -> mu+ mu-)
    9    7.63521770E-07   # BR(Bs -> tau+ tau-)
   10    8.51471574E-05   # BR(B_u -> tau nu)
   11    8.79537537E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43968333E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94057373E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16639432E-03   # epsilon_K
   17    2.28176685E-15   # Delta(M_K)
   18    2.48229739E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29599061E-11   # BR(K^+ -> pi^+ nu nu)
   20   -9.94431839E-17   # Delta(g-2)_electron/2
   21   -4.25151025E-12   # Delta(g-2)_muon/2
   22   -1.20291121E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    1.88259436E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -3.61719279E-01   # C7
     0305 4322   00   2    -3.35015975E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -2.58835806E-01   # C8
     0305 6321   00   2    -3.17873930E-03   # C8'
 03051111 4133   00   0     1.62282592E+00   # C9 e+e-
 03051111 4133   00   2     1.62361259E+00   # C9 e+e-
 03051111 4233   00   2    -5.39550107E-05   # C9' e+e-
 03051111 4137   00   0    -4.44551803E+00   # C10 e+e-
 03051111 4137   00   2    -4.44728331E+00   # C10 e+e-
 03051111 4237   00   2     4.03131769E-04   # C10' e+e-
 03051313 4133   00   0     1.62282592E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62361257E+00   # C9 mu+mu-
 03051313 4233   00   2    -5.39620717E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44551803E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44728332E+00   # C10 mu+mu-
 03051313 4237   00   2     4.03138843E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50573362E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -8.72491740E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50573362E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -8.72476424E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50573362E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -8.68160350E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821798E-07   # C7
     0305 4422   00   2    -1.99047996E-06   # C7
     0305 4322   00   2    -2.55422777E-07   # C7'
     0305 6421   00   0     3.30481886E-07   # C8
     0305 6421   00   2     4.05361437E-06   # C8
     0305 6321   00   2     9.83825764E-10   # C8'
 03051111 4133   00   2     7.62485602E-07   # C9 e+e-
 03051111 4233   00   2     5.95021531E-07   # C9' e+e-
 03051111 4137   00   2     1.72546633E-07   # C10 e+e-
 03051111 4237   00   2    -4.40605227E-06   # C10' e+e-
 03051313 4133   00   2     7.62485310E-07   # C9 mu+mu-
 03051313 4233   00   2     5.95021515E-07   # C9' mu+mu-
 03051313 4137   00   2     1.72546925E-07   # C10 mu+mu-
 03051313 4237   00   2    -4.40605232E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.14357661E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     9.53561623E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.14357509E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     9.53561623E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.14314349E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     9.53561619E-07   # C11' nu_3 nu_3
