import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':303600, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[303600], # list of pdfs ids for which all variations (error sets) will be included as weights
    'alternative_pdfs':None, # list of pdfs ids for which only the central set will be included as weights
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}
from MadGraphControl.MadGraphUtils import *


#Job bookkeping infos
evgenConfig.description = 'aMcAtNlo ttW+0,1j@NLO FxFx'
evgenConfig.contact = ["marcos.miralles.lopez@cern.ch"]
evgenConfig.keywords+=['ttW','jets']
#evgenConfig.inputconfcheck='/afs/cern.ch/work/m/mmiralle/ATHENA/QT/MG5_ttX_FxFx_myGridpack/forMarcos/999999/mc_13TeV.aMCPy8EG_A14NNPDF23LO_ttW_FxFx01jNLO.GRID.tar.gz'

# General settings
evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents*2.0 if runArgs.maxEvents>0 else 2.0*evgenConfig.nEventsPerJob

#Madgraph run card and shower settings
# Shower/merging settings
maxjetflavor=5
parton_shower='PYTHIA8'
nJetMax=1
qCut=30.

gridpack_mode=True

if not is_gen_from_gridpack():
    process = """
    import model loop_sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define w = w+ w-
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    generate p p > t t~ w [QCD] @0
    add process p p > t t~ w j [QCD] @1
    output -f
    """

    
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

#Fetch default run_card.dat and set parameters
settings = {
            'maxjetflavor'  : int(maxjetflavor),
            'parton_shower' : parton_shower,
            'nevents'       : int(nevents),
            'ickkw'         : 3,
            'jetradius'     : 1.0,
            'ptj'           : 10,
            'etaj'          : 10,
            'bwcutoff'      : 50,
        }
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Add madspin card
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)

mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set ms_dir %s
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
set BW_cut 50 # cut on how far the particle can be off-shell
# specify the decay for the final state particles
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all
# running the actual code
launch"""%(process_dir+'/MadSpin',runArgs.randomSeed))
mscard.close() 

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Helper for resetting process number
check_reset_proc_number(opts)

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
# include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)                                                     
PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut
print "PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax
print "PYTHIA8_qCut = %i"%PYTHIA8_qCut

include("Pythia8_i/Pythia8_FxFx_A14mod.py")
