from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

frblock = {}
masses = {}
decays = {}

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
joparts = get_physics_short().split('_')
scalar_mass = float(joparts[2])
#frblock['8'] = scalar_mass
masses['200002100'] = scalar_mass
zprime_mass = float(joparts[3])
#frblock['1'] = zprime_mass
masses['32'] = zprime_mass
zprime_ctau = float(joparts[4].replace('p','.')) 
zprime_width = 3.*1e11*6.581e-25/zprime_ctau

decays = {'32':"""DECAY  32  """+str(zprime_width)+""" # WZP
	#          BR          NDA       ID1       ID2
        0.5000  2    11 -13  
        0.5000  2    13 -11	
	#"""}#,
	#'200002100':"""DECAY 200002100 1.000000e+00 # WS0
	##	   BR	       NDA	 ID1	   ID2
	#1.0000  2    32 32
	## """}

#nevents=40000

my_process="""
import model AnoFree_NLO_simp_16_mt
generate g g > s0 [QCD]
output -f"""

process_dir = new_process(my_process)

frblock = {'1':str(zprime_mass),'8':str(scalar_mass)}

modify_param_card(process_dir=process_dir,params={'MASS':masses,'DECAY':decays,'FRBLOCK':frblock})

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'use_syst':"False",
	   'time_of_flight':'0',
           'nevents':nevents}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

# Now try running MadSpin
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 0                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set spinmode none # Required for loop-induced processes
set seed %i
# specify the decay for the final state particles
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all
decay s0 > zp zp
# running the actual code
launch"""%runArgs.randomSeed)
mscard.close()

generate(process_dir=process_dir,runArgs=runArgs)
add_lifetimes(process_dir=process_dir,threshold=0)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower  
evgenConfig.contact  = [ "benjamin.michael.cote@cern.ch" ]
evgenConfig.description = 'MadGraph_zprime'                                                                                                                                
evgenConfig.keywords = ["BSM","exotic","longLived"]
#evgenConfig.nEventsPerJob = 200

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")                                                                                                               
include("Pythia8_i/Pythia8_MadGraph.py")

#genSeq.Pythia8.Commands += ["Merging:Process = pp>{zp,32}{zp,32}"]

# Turn off checks for displaced vertices. 
# Other checks are fine.
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #In mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000 
