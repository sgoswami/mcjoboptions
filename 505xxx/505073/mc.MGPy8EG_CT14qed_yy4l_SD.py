import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

#MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={     
#'central_pdf':13300,     
#'pdf_variations':None,     
#'alternative_pdfs':None,     
#'use_syst':False ,
#'scale_variations':[0.5,1.,2.]
#}

# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=8*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
#mode=0

process = """
    import model sm
    define p = a g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate a a > l+ l- l+ l- 
    output -f"""

#l+ l- l+ l- #w+ w- > l+ vl l- vl~


process_dir = new_process(process) 
#Fetch default LO run_card.dat and set parameters 
settings = {'lhe_version':'3.0', 
        'cut_decays' :'F', 
        'lpp1'      : '2',
        'lpp2'      : '1',
        'dsqrt_q2fact1' : 2., 
#        'dsqrt_q2fact2' : 2.,
#        'fixed_fac_scale' : 'T', 
        'use_syst':'F',
        'ptl':'2',
        'nevents'   : int(nevents) } 

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings) 

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=False)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

evgenConfig.generators = ["MadGraph"]

runName='PROC_sm_0/run_01'  
############################
# Shower JOs will go here
#runName=

include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_SD_Common.py") 
include("Pythia8_i/Pythia8_ShowerWeights.py")
    
include("Pythia8_i//Pythia8_MadGraph.py")

include('GeneratorFilters/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3000.
MultiLeptonFilter.Etacut = 2.6
MultiLeptonFilter.NLeptons = 4

#-------------------------------------------------------------- 
# Evgen 
#-------------------------------------------------------------- 
evgenConfig.description = 'MadGraphPy8EG_yyWW'
evgenConfig.keywords+=["SM", "Z"]
evgenConfig.contact = ["kristin.lohwasser@cern.ch"]
