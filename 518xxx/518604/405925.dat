# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  12:00
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.39588154E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.87505664E+03  # scale for input parameters
    1   -2.69712546E+01  # M_1
    2   -3.41327001E+02  # M_2
    3    1.86102360E+03  # M_3
   11   -6.92911091E+03  # A_t
   12   -1.52912348E+03  # A_b
   13   -2.62524648E+03  # A_tau
   23    3.53280238E+03  # mu
   25    3.26989990E+01  # tan(beta)
   26    1.58387645E+03  # m_A, pole mass
   31    8.54012651E+02  # M_L11
   32    8.54012651E+02  # M_L22
   33    7.95514404E+02  # M_L33
   34    9.31616840E+02  # M_E11
   35    9.31616840E+02  # M_E22
   36    1.27025144E+03  # M_E33
   41    3.23355039E+03  # M_Q11
   42    3.23355039E+03  # M_Q22
   43    2.19159970E+03  # M_Q33
   44    1.45571783E+03  # M_U11
   45    1.45571783E+03  # M_U22
   46    3.80794817E+03  # M_U33
   47    3.00414102E+03  # M_D11
   48    3.00414102E+03  # M_D22
   49    1.45607461E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.87505664E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.87505664E+03  # (SUSY scale)
  1  1     8.38829199E-06   # Y_u(Q)^DRbar
  2  2     4.26125233E-03   # Y_c(Q)^DRbar
  3  3     1.01337305E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.87505664E+03  # (SUSY scale)
  1  1     5.51173394E-04   # Y_d(Q)^DRbar
  2  2     1.04722945E-02   # Y_s(Q)^DRbar
  3  3     5.46591294E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.87505664E+03  # (SUSY scale)
  1  1     9.61837877E-05   # Y_e(Q)^DRbar
  2  2     1.98877567E-02   # Y_mu(Q)^DRbar
  3  3     3.34479351E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.87505664E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.92911159E+03   # A_t(Q)^DRbar
Block Ad Q=  2.87505664E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.52912236E+03   # A_b(Q)^DRbar
Block Ae Q=  2.87505664E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -2.62524645E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.87505664E+03  # soft SUSY breaking masses at Q
   1   -2.69712546E+01  # M_1
   2   -3.41327001E+02  # M_2
   3    1.86102360E+03  # M_3
  21   -1.00329021E+07  # M^2_(H,d)
  22   -1.24099126E+07  # M^2_(H,u)
  31    8.54012651E+02  # M_(L,11)
  32    8.54012651E+02  # M_(L,22)
  33    7.95514404E+02  # M_(L,33)
  34    9.31616840E+02  # M_(E,11)
  35    9.31616840E+02  # M_(E,22)
  36    1.27025144E+03  # M_(E,33)
  41    3.23355039E+03  # M_(Q,11)
  42    3.23355039E+03  # M_(Q,22)
  43    2.19159970E+03  # M_(Q,33)
  44    1.45571783E+03  # M_(U,11)
  45    1.45571783E+03  # M_(U,22)
  46    3.80794817E+03  # M_(U,33)
  47    3.00414102E+03  # M_(D,11)
  48    3.00414102E+03  # M_(D,22)
  49    1.45607461E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.25491962E+02  # h0
        35     1.58355531E+03  # H0
        36     1.58387645E+03  # A0
        37     1.58713949E+03  # H+
   1000001     3.29852345E+03  # ~d_L
   2000001     3.06383233E+03  # ~d_R
   1000002     3.29767600E+03  # ~u_L
   2000002     1.57587422E+03  # ~u_R
   1000003     3.29852326E+03  # ~s_L
   2000003     3.06382848E+03  # ~s_R
   1000004     3.29767411E+03  # ~c_L
   2000004     1.57587440E+03  # ~c_R
   1000005     1.56006730E+03  # ~b_1
   2000005     2.25157563E+03  # ~b_2
   1000006     2.22692778E+03  # ~t_1
   2000006     3.71181803E+03  # ~t_2
   1000011     8.60678318E+02  # ~e_L-
   2000011     9.28876621E+02  # ~e_R-
   1000012     8.56784707E+02  # ~nu_eL
   1000013     8.59944358E+02  # ~mu_L-
   2000013     9.29378390E+02  # ~mu_R-
   1000014     8.56728069E+02  # ~nu_muL
   1000015     7.56045787E+02  # ~tau_1-
   2000015     1.25189604E+03  # ~tau_2-
   1000016     7.81278992E+02  # ~nu_tauL
   1000021     2.08014127E+03  # ~g
   1000022     2.66189197E+01  # ~chi_10
   1000023     3.64414633E+02  # ~chi_20
   1000025     3.51309454E+03  # ~chi_30
   1000035     3.51342486E+03  # ~chi_40
   1000024     3.64617098E+02  # ~chi_1+
   1000037     3.51408752E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.97741869E-02   # alpha
Block Hmix Q=  2.87505664E+03  # Higgs mixing parameters
   1    3.53280238E+03  # mu
   2    3.26989990E+01  # tan[beta](Q)
   3    2.43261311E+02  # v(Q)
   4    2.50866461E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.93933653E-01   # Re[R_st(1,1)]
   1  2     1.09981329E-01   # Re[R_st(1,2)]
   2  1    -1.09981329E-01   # Re[R_st(2,1)]
   2  2     9.93933653E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     7.00314226E-02   # Re[R_sb(1,1)]
   1  2     9.97544786E-01   # Re[R_sb(1,2)]
   2  1    -9.97544786E-01   # Re[R_sb(2,1)]
   2  2     7.00314226E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.77985298E-01   # Re[R_sta(1,1)]
   1  2     2.08673806E-01   # Re[R_sta(1,2)]
   2  1    -2.08673806E-01   # Re[R_sta(2,1)]
   2  2     9.77985298E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99921373E-01   # Re[N(1,1)]
   1  2    -2.99616357E-04   # Re[N(1,2)]
   1  3    -1.25300481E-02   # Re[N(1,3)]
   1  4     3.94346435E-04   # Re[N(1,4)]
   2  1    -2.01494672E-05   # Re[N(2,1)]
   2  2     9.99749465E-01   # Re[N(2,2)]
   2  3    -2.23410658E-02   # Re[N(2,3)]
   2  4    -1.37244935E-03   # Re[N(2,4)]
   3  1     9.14161439E-03   # Re[N(3,1)]
   3  2    -1.48262075E-02   # Re[N(3,2)]
   3  3    -7.06909924E-01   # Re[N(3,3)]
   3  4     7.07089085E-01   # Re[N(3,4)]
   4  1     8.58358118E-03   # Re[N(4,1)]
   4  2    -1.67660683E-02   # Re[N(4,2)]
   4  3    -7.06839610E-01   # Re[N(4,3)]
   4  4    -7.07123035E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.99501931E-01   # Re[U(1,1)]
   1  2     3.15577222E-02   # Re[U(1,2)]
   2  1    -3.15577222E-02   # Re[U(2,1)]
   2  2    -9.99501931E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.99998097E-01   # Re[V(1,1)]
   1  2     1.95083488E-03   # Re[V(1,2)]
   2  1     1.95083488E-03   # Re[V(2,1)]
   2  2    -9.99998097E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     8.72657825E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.23773850E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.92262213E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     5.83963937E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     4.65887036E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000013     8.71343183E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.23854599E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.92235595E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.83909806E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.66140414E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.99998317E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.73717101E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.60125927E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.80199149E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.59674925E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     3.18883855E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.90293347E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     5.39795801E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.07874667E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.95878566E-01    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     2.11615561E-01    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.86027102E-01    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     8.65950936E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     1.23887788E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.92040369E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.84071843E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     8.65849867E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.23894027E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.92038093E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.84067880E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     7.29148311E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.34112818E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.88593857E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.77290784E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.19816727E+01   # ~d_R
#    BR                NDA      ID1      ID2
     2.37558832E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.76244116E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     1.37375007E+02   # ~d_L
#    BR                NDA      ID1      ID2
     3.33907516E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.03070354E-01    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.06031504E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     6.87559057E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.19812891E+01   # ~s_R
#    BR                NDA      ID1      ID2
     2.37559942E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.76243871E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     1.37375841E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.33906287E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.03069746E-01    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.06030164E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     6.87554727E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.04215956E+00   # ~b_1
#    BR                NDA      ID1      ID2
     8.40570056E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     5.41937163E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.05236227E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     4.28402818E+01   # ~b_2
#    BR                NDA      ID1      ID2
     7.21358689E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.17531813E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     4.29066157E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.86837894E-02    2     1000021         5   # BR(~b_2 -> ~g b)
     6.05067962E-02    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.96997857E-01    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     3.51663553E+00   # ~u_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> chi^0_1 u)
DECAY   1000002     1.37306077E+02   # ~u_L
#    BR                NDA      ID1      ID2
     3.36244855E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.03092814E-01    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     2.06284007E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     6.87260730E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     3.51663035E+00   # ~c_R
#    BR                NDA      ID1      ID2
     9.99999999E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
DECAY   1000004     1.37306153E+02   # ~c_L
#    BR                NDA      ID1      ID2
     3.36244354E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.03092657E-01    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     2.06283821E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     6.87258227E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     3.81893553E+01   # ~t_1
#    BR                NDA      ID1      ID2
     3.73192462E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     9.56058590E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.11088501E-01    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.36862826E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     4.80313711E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.41990266E-02    2     1000021         4   # BR(~t_1 -> ~g c)
     1.23071170E-01    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.17057255E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.87945664E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.10220127E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     5.24118520E-04    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     6.09500744E-04    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     5.94187742E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.05430155E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.08828414E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.46159907E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     2.96258795E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.71266601E-01    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     1.25048460E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     1.39672582E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.12795432E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.54683300E-05   # chi^+_1
#    BR                NDA      ID1      ID2
     1.89056973E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.88831869E-04    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     2.88842278E-04    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     4.24376791E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
     2.19968603E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     2.20406732E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     3.69565640E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.17224231E+02   # chi^+_2
#    BR                NDA      ID1      ID2
     1.16112086E-04    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     1.16137352E-04    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     1.01918017E-04    2    -2000013        14   # BR(chi^+_2 -> ~mu^+_R nu_mu)
     6.57399923E-04    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     2.49539599E-02    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     1.04679126E-04    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     3.01147953E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     9.86804047E-02    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     1.78439311E-01    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     3.10715758E-01    2    -2000005         6   # BR(chi^+_2 -> ~b^*_2 t)
     1.98111169E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     6.08016112E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.18906091E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     4.08647512E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     6.08748445E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     4.21982161E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     6.49245695E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     4.21868008E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     6.23814041E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.27932104E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.01465998E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     8.78452689E-04    3     1000021        -5         6   # BR(chi^+_2 -> ~g b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.74610297E-05   # chi^0_2
#    BR                NDA      ID1      ID2
     7.40470054E-02    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     6.30060944E-03    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     3.30370874E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.67516236E-04    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.67483159E-04    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.66223238E-04    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.66257482E-04    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.02968222E-03    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     1.24287946E-01    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.24747950E-01    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.26511864E-01    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     4.42077091E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.20250597E+02   # chi^0_3
#    BR                NDA      ID1      ID2
     1.45518553E-02    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     1.45518553E-02    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     1.25790243E-02    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     1.25790243E-02    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     1.64108666E-01    2     1000006        -6   # BR(chi^0_3 -> ~t_1 t_bar)
     1.64108666E-01    2    -1000006         6   # BR(chi^0_3 -> ~t^*_1 t)
     8.36394588E-02    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     8.36394588E-02    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     4.53278906E-02    2     2000005        -5   # BR(chi^0_3 -> ~b_2 b_bar)
     4.53278906E-02    2    -2000005         5   # BR(chi^0_3 -> ~b^*_2 b)
     5.95123318E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     5.95123318E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.10266587E-02    2          37  -1000024   # BR(chi^0_3 -> H^+ chi^-_1)
     4.10266587E-02    2         -37   1000024   # BR(chi^0_3 -> H^- chi^+_1)
     9.02626426E-03    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.33843135E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     6.04331657E-03    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     1.59376532E-02    2     1000023        36   # BR(chi^0_3 -> chi^0_2 A^0)
     9.82894140E-03    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     4.09290943E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     5.56458809E-03    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     2.38959243E-02    2     1000023        35   # BR(chi^0_3 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.61841575E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     3.11346374E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     3.82551515E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     3.82551515E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
     1.88801593E-03    3     1000021         6        -6   # BR(chi^0_3 -> ~g t t_bar)
DECAY   1000035     1.19274165E+02   # chi^0_4
#    BR                NDA      ID1      ID2
     1.46524600E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     1.46524600E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     1.26976285E-02    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     1.26976285E-02    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     1.67453289E-01    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     1.67453289E-01    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     8.42994579E-02    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     8.42994579E-02    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     4.57223789E-02    2     2000005        -5   # BR(chi^0_4 -> ~b_2 b_bar)
     4.57223789E-02    2    -2000005         5   # BR(chi^0_4 -> ~b^*_2 b)
     5.99522268E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     5.99522268E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.00868311E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     4.00868311E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     1.03227027E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.63168654E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.38975752E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.42148955E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     8.79965800E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.66333452E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     6.32014676E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     2.71399394E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     8.05558862E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     3.17375655E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.35228552E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.35228552E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
     1.93125842E-03    3     1000021         6        -6   # BR(chi^0_4 -> ~g t t_bar)
DECAY   1000021     3.42137516E+01   # ~g
#    BR                NDA      ID1      ID2
     1.63350234E-01    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     1.63350234E-01    2    -2000002         2   # BR(~g -> ~u^*_R u)
     1.63349749E-01    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     1.63349749E-01    2    -2000004         4   # BR(~g -> ~c^*_R c)
     1.72027256E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.72027256E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     3.80629730E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     4.64890922E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     5.86363560E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     5.86363560E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.98579281E-03   # Gamma(h0)
     2.33089562E-03   2        22        22   # BR(h0 -> photon photon)
     1.52400878E-03   2        22        23   # BR(h0 -> photon Z)
     2.86197501E-02   2        23        23   # BR(h0 -> Z Z)
     2.34139932E-01   2       -24        24   # BR(h0 -> W W)
     7.07003101E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.11744583E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.27633481E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.56441768E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.42341385E-07   2        -2         2   # BR(h0 -> Up up)
     2.76219868E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.89315909E-07   2        -1         1   # BR(h0 -> Down down)
     2.13143282E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.68956011E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     2.14153108E-05   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.33153796E+01   # Gamma(HH)
     2.01443509E-07   2        22        22   # BR(HH -> photon photon)
     6.95570989E-08   2        22        23   # BR(HH -> photon Z)
     8.08042567E-06   2        23        23   # BR(HH -> Z Z)
     5.98321179E-06   2       -24        24   # BR(HH -> W W)
     2.38933187E-05   2        21        21   # BR(HH -> gluon gluon)
     2.06301126E-08   2       -11        11   # BR(HH -> Electron electron)
     9.18368438E-04   2       -13        13   # BR(HH -> Muon muon)
     2.64111399E-01   2       -15        15   # BR(HH -> Tau tau)
     2.33939529E-13   2        -2         2   # BR(HH -> Up up)
     4.53434608E-08   2        -4         4   # BR(HH -> Charm charm)
     4.44611554E-03   2        -6         6   # BR(HH -> Top top)
     1.06163843E-06   2        -1         1   # BR(HH -> Down down)
     3.84058025E-04   2        -3         3   # BR(HH -> Strange strange)
     7.14464598E-01   2        -5         5   # BR(HH -> Bottom bottom)
     7.51166599E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     4.11878600E-05   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.61876122E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.75489951E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     7.53378396E-05   2        25        25   # BR(HH -> h0 h0)
     1.41310464E-02   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
DECAY        36     1.27943679E+01   # Gamma(A0)
     5.65965602E-08   2        22        22   # BR(A0 -> photon photon)
     5.82120967E-08   2        22        23   # BR(A0 -> photon Z)
     5.32587706E-05   2        21        21   # BR(A0 -> gluon gluon)
     2.09358507E-08   2       -11        11   # BR(A0 -> Electron electron)
     9.31978262E-04   2       -13        13   # BR(A0 -> Muon muon)
     2.68025020E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.80557685E-13   2        -2         2   # BR(A0 -> Up up)
     3.49945793E-08   2        -4         4   # BR(A0 -> Charm charm)
     3.76953822E-03   2        -6         6   # BR(A0 -> Top top)
     1.07737545E-06   2        -1         1   # BR(A0 -> Down down)
     3.89751504E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.25069957E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     9.52740640E-04   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     4.30356185E-05   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.76095830E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.76249052E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.11270229E-05   2        23        25   # BR(A0 -> Z h0)
     6.88992421E-36   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.45147458E+01   # Gamma(Hp)
     2.08531354E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     8.91536661E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     2.52176811E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.74576886E-07   2        -1         2   # BR(Hp -> Down up)
     1.46564935E-05   2        -3         2   # BR(Hp -> Strange up)
     7.87042657E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.29260319E-08   2        -1         4   # BR(Hp -> Down charm)
     3.15383070E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.10214037E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.57566112E-07   2        -1         6   # BR(Hp -> Down top)
     6.07113901E-06   2        -3         6   # BR(Hp -> Strange top)
     7.39882902E-01   2        -5         6   # BR(Hp -> Bottom top)
     4.84958198E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.07338816E-12   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     9.89240427E-06   2        24        25   # BR(Hp -> W h0)
     5.53782292E-11   2        24        35   # BR(Hp -> W HH)
     3.46379317E-11   2        24        36   # BR(Hp -> W A0)
     5.10658271E-03   2  -1000015   1000016   # BR(Hp -> Stau1 snu_tau1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.48476086E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.06927606E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.06922454E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00004819E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    8.87069137E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    9.35257251E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.48476086E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.06927606E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.06922454E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999363E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    6.37219468E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999363E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    6.37219468E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25191428E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.76448992E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.22690691E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    6.37219468E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999363E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.24798341E-04   # BR(b -> s gamma)
    2    1.58992196E-06   # BR(b -> s mu+ mu-)
    3    3.52634938E-05   # BR(b -> s nu nu)
    4    1.61963218E-15   # BR(Bd -> e+ e-)
    5    6.91888585E-11   # BR(Bd -> mu+ mu-)
    6    1.44845765E-08   # BR(Bd -> tau+ tau-)
    7    5.43193698E-14   # BR(Bs -> e+ e-)
    8    2.32052235E-09   # BR(Bs -> mu+ mu-)
    9    4.92235536E-07   # BR(Bs -> tau+ tau-)
   10    9.51787344E-05   # BR(B_u -> tau nu)
   11    9.83159887E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41600517E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93439730E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15618786E-03   # epsilon_K
   17    2.28164974E-15   # Delta(M_K)
   18    2.48136532E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29243857E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.66184388E-15   # Delta(g-2)_electron/2
   21   -2.42023428E-10   # Delta(g-2)_muon/2
   22   -6.54667615E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.97663128E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.88139047E-01   # C7
     0305 4322   00   2    -8.90296241E-05   # C7'
     0305 6421   00   0    -9.52278349E-02   # C8
     0305 6421   00   2    -1.18880694E-01   # C8
     0305 6321   00   2    -4.52589461E-04   # C8'
 03051111 4133   00   0     1.61517914E+00   # C9 e+e-
 03051111 4133   00   2     1.61528399E+00   # C9 e+e-
 03051111 4233   00   2     2.50469367E-04   # C9' e+e-
 03051111 4137   00   0    -4.43787124E+00   # C10 e+e-
 03051111 4137   00   2    -4.43690996E+00   # C10 e+e-
 03051111 4237   00   2    -1.88470225E-03   # C10' e+e-
 03051313 4133   00   0     1.61517914E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61528293E+00   # C9 mu+mu-
 03051313 4233   00   2     2.50467710E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43787124E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43691106E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.88470150E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50523713E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     4.08584211E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50523713E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     4.08584522E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50523766E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     4.08671964E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85911504E-07   # C7
     0305 4422   00   2     5.58874214E-05   # C7
     0305 4322   00   2     4.68723994E-06   # C7'
     0305 6421   00   0     3.30558725E-07   # C8
     0305 6421   00   2    -2.29490712E-04   # C8
     0305 6321   00   2    -3.26873674E-07   # C8'
 03051111 4133   00   2    -2.25869824E-06   # C9 e+e-
 03051111 4233   00   2     5.84319509E-06   # C9' e+e-
 03051111 4137   00   2     2.28602123E-05   # C10 e+e-
 03051111 4237   00   2    -4.39752329E-05   # C10' e+e-
 03051313 4133   00   2    -2.25835364E-06   # C9 mu+mu-
 03051313 4233   00   2     5.84319120E-06   # C9' mu+mu-
 03051313 4137   00   2     2.28598954E-05   # C10 mu+mu-
 03051313 4237   00   2    -4.39752450E-05   # C10' mu+mu-
 03051212 4137   00   2    -3.22827970E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     9.53347117E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -3.22829173E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     9.53347117E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -3.21891628E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     9.53346781E-06   # C11' nu_3 nu_3
