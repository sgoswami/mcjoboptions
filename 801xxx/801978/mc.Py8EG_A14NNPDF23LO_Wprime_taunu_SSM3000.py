evgenConfig.description = 'Pythia 8 Zprime decaying to tau nu'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'BSM', 'Wprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak', 'lepton', 'neutrino' ]
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.process = "pp>Zprime>taunu"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",  # create W' bosons
                            "34:onMode = off",                     # switch off all W' decays
                            "34:onIfAny = 15,16",                  # switch on W'->enu decays
                            "34:m0 = 3000"]
