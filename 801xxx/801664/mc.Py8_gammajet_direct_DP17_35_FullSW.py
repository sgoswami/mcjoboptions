include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Pythia8 gamma+jet events with prompt (direct) photons in 17 < pT_ylead < 35"
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.contact = ["frank.siegert@cern.ch", "ana.cueto@cern.ch", "javier.llorente.merino@cern.ch"]

genSeq.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 8.5"]

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 17000. ]
filtSeq.DirectPhotonFilter.Ptmax = [ 35000. ]
filtSeq.DirectPhotonFilter.OrderPhotons = True
