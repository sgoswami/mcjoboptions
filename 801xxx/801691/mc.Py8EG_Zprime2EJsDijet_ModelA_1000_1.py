###########################################################                                                                                                                                             
# Emerging Jets Event Generation                                                                                                                                                                       
# Pythia 8: Zd --> Qd Qd_bar --> 2EJ                                                                                                                                                                     
# contact: Aaron Kilgallon (aaron.joseph.kilgallon@cern.ch)                                                                                                                                              
#==========================================================                                                                                                                                               

evgenConfig.description = "emerging jet events from pair-produced dark quarks"
evgenConfig.keywords = ["exotic", "hiddenValley", "4jet"]
evgenConfig.process = "p p --> Zd --> Qd Qd_bar --> 2EJ"
evgenConfig.contact = ["aaron.joseph.kilgallon@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")


# set sample / model parameters automatically based on jo name
print("ARGS: ", runArgs.jobConfig[0])
print("JO ARGS: ", jofile.rstrip('.py').split('_'))

m_Xd = float(jofile.rstrip('.py').split('_')[3])
ctau_pi_d = float(jofile.rstrip('.py').split('_')[4])

print("SCALAR MEDIATOR MASS: %f " % m_Xd)
print("DARK PION LIFETIME: %f " % ctau_pi_d)

mod = jofile.rstrip('.py').split('_')[2]
print("MODEL: %s " % mod)
if mod == "ModelA":
    m_pi_d = 5.0
elif mod == "ModelB":
    m_pi_d = 2.0
elif mod == "ModelC":
    m_pi_d = 10.0
elif mod == "ModelD":
    m_pi_d = 20.0
elif mod == "ModelE":
    m_pi_d = 0.8
print("DARK PION MASS: %f " % m_pi_d)
print("DARK RHO MASS: %f " % (m_pi_d*4))
print("LAMBDA / DARK QUARK MASS: %f " % (m_pi_d*2))
print("PT MIN FSR: %f " % (m_pi_d*2*1.1))

# show 5 events for testing
genSeq.Pythia8.Commands += ["Next:numberShowEvent = 1"]

## OVERRIDE STANDARD ATLAS TAU0 LIMIT ##
genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"]

# settings for dark sector 
genSeq.Pythia8.Commands += ["HiddenValley:spinFV = 0",
                            "HiddenValley:Ngauge = 3", # n dark QCD colors
                            "HiddenValley:alphaFSR = 0.7"] # fixed dark coupling

# Model settings
genSeq.Pythia8.Commands += ["4900101:m0 = " + str(m_pi_d*2), # qd mass
                            "4900111:m0 = " + str(m_pi_d), # pi_d mass
                            "4900113:m0 = " + str(m_pi_d*4), # rho_d mass
                            "4900211:m0 = " + str(m_pi_d), # pi_d off-diag mass
                            "4900213:m0 = " + str(m_pi_d*4), # rho_d off-diag mass
                            "HiddenValley:Lambda = " + str(m_pi_d*2),
                            "HiddenValley:pTminFSR = " + str(m_pi_d*2*1.1)] # pT cutoff for dark shower

# dark pion lifetime
genSeq.Pythia8.Commands += ["4900111:tau0 = " + str(ctau_pi_d)] # pi_d lifetime -- variable
# off-diagonal dark pion lifetime
genSeq.Pythia8.Commands += ["4900211:tau0 = " + str(ctau_pi_d)]

# non-model dependent settings
genSeq.Pythia8.Commands += ["PartonLevel:MPI = on",
                            "PartonLevel:ISR = on"]

# emerging jet event processes
genSeq.Pythia8.Commands += ["HiddenValley:ffbar2Zv = on"]
genSeq.Pythia8.Commands += ["4900023:m0 = " + str(m_Xd)]
genSeq.Pythia8.Commands += ["4900023:tau0 = 1E-20",
                            "4900023:mWidth=10",
                            "4900023:isResonance = on",
                            "4900023:mayDecay = on",
                            "4900023:0:bRatio = 1",
                            "4900023:0:meMode = 102"]

genSeq.Pythia8.Commands += ["4900023:onMode = off",
                            "4900023:offIfAny 1 2 3 4 5 6 -1 -2 -3 -4 -5 -6 7 8 9 10 11 12 13 14 15 16 -7 -8 -9 -10 -11 -12 -13 -14 -15 -16",
                            "4900023:onIfAny 4900101 -4900101",
                            "4900023:oneChannel 1 0.999 102 4900101 -4900101",
                            "4900023:addChannel 1 0.001 102 1 -1"]

# dark meson decays
genSeq.Pythia8.Commands += ["4900111:0:all on 1.0 102 1 -1", # dark pion to down quarks
                            "4900113:0:all on 0.999 102 4900111 4900111", # dark vector to dark pions 99.9%
                            "4900113:addchannel on 0.001 102 1 -1"] # dark vector to down quarks 0.1%
# dark meson off-diagonal decays
genSeq.Pythia8.Commands += ["4900211:oneChannel on 1.0 91 1 -1", # dark pion to down quarks
                            "4900213:oneChannel on 0.999 102 4900211 4900211", # dark vector to dark pions 99.9%
                            "4900213:addchannel on 0.001 102 1 -1"] # dark vector to down quarks 0.1%


# dark QCD coupling (alphaHV) running
genSeq.Pythia8.Commands += ["HiddenValley:alphaOrder = 1",
                            "HiddenValley:nFlav = 7"]

# HV parton shower settings
genSeq.Pythia8.Commands += ["HiddenValley:FSR = on",
                            "HiddenValley:fragment = on"]


genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 500"]


genSeq.Pythia8.Commands += ["ProcessLevel:all = on",
"ProcessLevel:resonanceDecays = on",
"PartonLevel:all = on",
"PartonLevel:ISR = on",
"HadronLevel:all = on",
"PhaseSpace:useBreitWigners = on"]


# workarounds for TestHepMC
testSeq.TestHepMC.MaxVtxDisp=5000000.
testSeq.TestHepMC.MaxTransVtxDisp = 5000000.


## JET FILTERING ##
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

if not hasattr( filtSeq, "TruthJetFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter()
    pass

filtSeq.TruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.TruthJetFilter.Njet = 2
filtSeq.TruthJetFilter.NjetMinPt = 125*GeV
filtSeq.TruthJetFilter.NjetMaxEta = 2.4



