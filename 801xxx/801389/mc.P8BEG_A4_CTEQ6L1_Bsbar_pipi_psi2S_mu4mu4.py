##############################################################
# Job options for Bsbar -> pi pi psi(2S) (mumu)
##############################################################

evgenConfig.description   = "Exclusive Bsbar -> pi pi psi(2S) (mumu) decay production"
evgenConfig.process       = "Bsbar -> pi pi Psi(2S) (mumu)"
evgenConfig.keywords      = [ "bottom", "Bs", "2muon", "exclusive" ]
evgenConfig.contact       = [ "kin.yip.fung@cern.ch" ]
evgenConfig.nEventsPerJob = 100

# Create EvtGen decay rules
f = open("Bsbar_pipi_PSI2S_MUMU_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Alias my_psi(2S) psi(2S)\n")
f.write("Decay  my_psi(2S)\n")
f.write("1.0000   mu+ mu-   PHOTOS VLL;\n")
f.write("Enddecay\n")
f.write("Decay anti-B_s0\n")
f.write("1.0000   my_psi(2S) pi+ pi- PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("GeneratorFilters/BSignalFilter.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py") # list of B-species

genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 7' ]
genSeq.Pythia8B.QuarkPtCut                = 7.5
genSeq.Pythia8B.AntiQuarkPtCut            = 0.0
genSeq.Pythia8B.QuarkEtaCut               = 3.0
genSeq.Pythia8B.AntiQuarkEtaCut           = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.NHadronizationLoops = 4
genSeq.Pythia8B.NDecayLoops         = 1

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ -531 ]
genSeq.EvtInclusiveDecay.userDecayFile = "Bsbar_pipi_PSI2S_MUMU_USER.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn             = True
filtSeq.BSignalFilter.LVL2MuonCutOn             = True
filtSeq.BSignalFilter.LVL2MuonCutEta            = 2.7
filtSeq.BSignalFilter.LVL1MuonCutEta            = 2.7
filtSeq.BSignalFilter.LVL1MuonCutPT             = 4000.0
filtSeq.BSignalFilter.LVL2MuonCutPT             = 4000.0

filtSeq.BSignalFilter.B_PDGCode                 = -531
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.7

