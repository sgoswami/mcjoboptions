include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Pythia8 gamma+jet events with prompt (direct) photons in 1000 < pT_ylead < 1500"
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.nEventsPerJob = 5000
evgenConfig.contact = ["frank.siegert@cern.ch", "ana.cueto@cern.ch", "javier.llorente.merino@cern.ch"]

genSeq.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 500"]

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 1000000. ]
filtSeq.DirectPhotonFilter.Ptmax = [ 1500000. ]
filtSeq.DirectPhotonFilter.OrderPhotons = True
