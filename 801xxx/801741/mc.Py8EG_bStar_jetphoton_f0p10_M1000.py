evgenConfig.description = "PYTHIA 8 b* -> gamma+jet, b* mass = lambda = 1000. GeV, f = 0.10"
evgenConfig.keywords    = ["exotic", "excitedQuark", "photon", "jets"]
evgenConfig.contact     = ["francisco.alonso@cern.ch"]
evgenConfig.process     = "b* -> gamma+jet"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [
    "ExcitedFermion:bg2bStar = on",             # switch on bg -> b*
    "ExcitedFermion:Lambda = 1000.",           # Compositness scale
    "4000005:m0 = 1000.",                      # b* mass
    "4000005:onMode = off",                     # switch off all b* decays
    "4000005:onIfAny = 22",                     # switch on b*->gamma+X decays
    
    "ExcitedFermion:coupF = 0.10",        # coupling strength of SU(2)
    "ExcitedFermion:coupFprime = 0.10",   # coupling strength of U(1)
    "ExcitedFermion:coupFcol = 0.10"      # coupling strength of SU(3)
]
