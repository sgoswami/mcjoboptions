##############################################################
# Job options for Bs -> pi K psi(2S) (ee)
##############################################################

evgenConfig.description   = "Exclusive Bs -> pi K psi(2S) (ee) decay production"
evgenConfig.process       = "Bs -> pi K Psi(2S) (ee)"
evgenConfig.keywords      = [ "bottom", "Bs", "2electron", "exclusive" ]
evgenConfig.contact       = [ "ann-kathrin.perrevoort@cern.ch" ]
evgenConfig.nEventsPerJob = 100

# Create EvtGen decay rules
f = open("Bs_piK_PSI2S_EE_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Alias my_psi(2S) psi(2S)\n")
f.write("Decay  my_psi(2S)\n")
f.write("1.0000   e+ e-   PHOTOS VLL;\n")
f.write("Enddecay\n")
f.write("Decay B_s0\n")
f.write("1.0000   my_psi(2S) pi+ K- PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("GeneratorFilters/BSignalFilter.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py") # list of B-species

genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 7' ]
genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 7.5
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.NHadronizationLoops = 4
genSeq.Pythia8B.NDecayLoops         = 1

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ 531 ]
genSeq.EvtInclusiveDecay.userDecayFile = "Bs_piK_PSI2S_EE_USER.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn             = False
filtSeq.BSignalFilter.LVL2MuonCutOn             = False
# filtSeq.BSignalFilter.LVL2MuonCutEta            = 2.7
# filtSeq.BSignalFilter.LVL1MuonCutEta            = 2.7
# filtSeq.BSignalFilter.LVL1MuonCutPT             = 10000.0
# filtSeq.BSignalFilter.LVL2MuonCutPT             = 10000.0
if not hasattr( filtSeq, "MultiElectronFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
  filtSeq += MultiElectronFilter()
  pass
filtSeq.MultiElectronFilter.NElectrons = 2
filtSeq.MultiElectronFilter.Ptcut      = 4000.0
filtSeq.MultiElectronFilter.Etacut     = 2.7
filtSeq.BSignalFilter.B_PDGCode                 = 531
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.7

