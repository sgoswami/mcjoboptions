# JO for Pythia 8 jet jet JZ1 slice

#evgenConfig.generators += ["Pythia8"]
evgenConfig.description = "Dijet truth jet slice JZ1, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["amoroso@cern.ch"]
evgenConfig.nEventsPerJob = 10000

# Keep truth jets collection needed for pileup truth studies
evgenConfig.saveJets = True

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.6)
from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MinPt = 0*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 7000*GeV

CreateJets(prefiltSeq, 0.4)
