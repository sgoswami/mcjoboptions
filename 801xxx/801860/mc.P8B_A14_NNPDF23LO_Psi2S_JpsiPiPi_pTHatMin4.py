#######################################################################
# Job options fragment for pp->X Psi(2S)(->pi,pi,Jpsi(mu3p5mu3p5))
#######################################################################
evgenConfig.description = "pp->X Psi(2S)(->pi,pi,Jpsi(mu3p5mu3p5))"
evgenConfig.keywords = ["charmonium","2muon"]
evgenConfig.generators = ["Pythia8B"]
evgenConfig.contact  = ["Xin.Chen@cern.ch, Qipeng.Hu@cern.ch"]
evgenConfig.nEventsPerJob = 200

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")
include("Pythia8B_i/Pythia8B_Photospp.py")
include("Pythia8B_i/Pythia8B_Charmonium_Common.py")

# Hard process
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 4.'] # Equivalent of CKIN3

# Turn on colour-octet decays into Psi(2S)
genSeq.Pythia8B.Commands += ['9940103:0:products = 100443 21']
genSeq.Pythia8B.Commands += ['9941103:0:products = 100443 21']
genSeq.Pythia8B.Commands += ['9942103:0:products = 100443 21']

genSeq.Pythia8B.Commands += ['100443:0:products = 443 211 -211',
                             '100443:onMode = off',
                             '100443:0:onMode = on']
genSeq.Pythia8B.Commands += ['443:onMode = off',
                             '443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [100443,443,-13,13,-211,211] #mu+ mu- pi- pi+
genSeq.Pythia8B.SignalPtCuts = [0.0, 0.0, 2.8, 2.8, 0.38, 0.38]
genSeq.Pythia8B.SignalEtaCuts = [100.0, 100.0, 2.7, 2.7, 2.7, 2.7]
