# QCD multi-jet background with 4 b's
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "2->2 QCD process with 4b filter and pTHat_min of 125 GeV"
evgenConfig.contact = ["sven.menke@cern.ch"]
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.saveJets = True

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 125."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

if not hasattr( filtSeq, "MultiBjetFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
    filtSeq += MultiBjetFilter()
    pass

MultiBjetFilter = filtSeq.MultiBjetFilter
MultiBjetFilter.LeadJetPtMin = 0
MultiBjetFilter.LeadJetPtMax = 50000000
MultiBjetFilter.BottomPtMin = 5000
MultiBjetFilter.BottomEtaMax = 3.0
MultiBjetFilter.NBJetsMin = 4
MultiBjetFilter.JetPtMin = 25000
MultiBjetFilter.JetEtaMax = 2.7
MultiBjetFilter.DeltaRFromTruth = 0.3
MultiBjetFilter.TruthContainerName = "AntiKt4TruthJets" 

print filtSeq
evgenConfig.nEventsPerJob = 50
