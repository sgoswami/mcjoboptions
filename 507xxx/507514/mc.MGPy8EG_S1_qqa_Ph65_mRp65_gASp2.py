mR          = 650
mDM         = 10000
gVSM        = 0.00
gASM        = 0.20
gVDM        = 0.00
gADM        = 1.00
filteff     = 0.239000
phminpt     = 65
quark_decays= ['u', 'd', 's', 'c']

evgenConfig.nEventsPerJob = 10000

include("MadGraphControl_MGPy8EG_DMS1_dijetgamma.py")

evgenConfig.description = "Zprime with ISR - mR650 - model DMsimp_s_spin1"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Chris Delitzsch <chris.malena.delitzsch>, Karol Krizka <kkrizka@cern.ch>"]

