pol_state = "WTZT" # Options W0Z0, W0ZT, WTZ0, WTZT   
PtZRegion = "PtZlt150GeV" # PtZge150GeV, PtZlt150GeV

input_safefactor = 15.0

include("MadGraphControl_Pythia8EvtGen_WZ_Pol_lvll_lvllj_LO_min1tau_CKKWL.py")
