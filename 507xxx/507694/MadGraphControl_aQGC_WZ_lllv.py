from MadGraphControl.MadGraphUtils import *

mode=0
nevents=5000
njobs=8
gridpack_mode=True
gridpack_dir='madevent/'
stringy=""

def aQGC_Generation(
                   version="S0",
                   aqgcorder="QUAD",
                   aqgctypes=["FS0"],
                   aqgcvalue=1.0,
                   rand_seed=1234,
                   nevts=5000,
                   beamEnergy=6500.,
                   pdf='lhapdf',
                   lhaid=260000):

  #---------------------------------------------------------------------------
  # MG5 Proc card
  #---------------------------------------------------------------------------
  if version=="S0": 
      procname="""
      generate p p > l+ l- l+ vl j j QCD=0 S0^2==2 S2=0
      add process p p > l+ l- l- vl~ j j QCD=0 S0^2==2 S2=0
      """  
  elif version=="INTRF":
      procname="""
      generate p p > l+ l- l+ vl j j QCD=0 S0^2==1 S2^2==1
      add process p p > l+ l- l- vl~ j j QCD=0 S0^2==1 S2^2==1
      """  
  elif version=="S2":
      procname="""
      generate p p > l+ l- l+ vl j j QCD=0 S2^2==2 S0=0
      add process p p > l+ l- l- vl~ j j QCD=0 S2^2==2 S0=0
      """  

  process_dir = new_process("""import model SM_Ltotal_Ind5v2020v2_UFO
define l+ = e+ mu+ ta+
define vl = ve vm vt
define l- = e- mu- ta-
define vl~ = ve~ vm~ vt~
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
%s
output -f""" % (procname))


  #---------------------------------------------------------------------------
  # Number of Events
  #---------------------------------------------------------------------------
  safefactor = 1.1
  if nevts>0: nevents = nevts*safefactor
  else: nevents = 20000 * safefactor
  nevents=int(nevents) ## !! must be an integral
  
  #---------------------------------------------------------------------------
  # MG5 Run Card
  #---------------------------------------------------------------------------

  extras = {
        'nevents' : nevents,
    'ebeam1' : beamEnergy,
    'ebeam2' : beamEnergy,
    'gridpack': '.true.',
    'pdlabel': pdf,
    'lhaid': lhaid,
    'dynamical_scale_choice': 2,
    'maxjetflavor': 5,
    'asrwgtflavor': 5,
    'lhe_version':"3.0",
    'auto_ptj_mjj': 'F',
    'cut_decays': 'T',
    'ptl': "4.0",
    'ptj': "15.0",
    'ptb': "15.0",
    'drbb': "0.2",
    'etal': "3.0",
    'etaj': "5.5",
    'etab': "5.5",
    'mmll': "0.0",
    'dral': "0.1",
    'drbj': "0.2",
    'drll': "0.2",
    'drbl': "0.2",
    'drjl': "0.2",
    'drjj': "0.2",
    'use_syst': "T",
    'systematics_program': 'systematics',
    'systematics_arguments': "['--mur=0.5,1,2', '--muf=0.5,1,2', '--dyn=-1,1,2,3,4', '--pdf=errorset,NNPDF30_nlo_as_0119@0,NNPDF30_nlo_as_0117@0,CT14nlo@0,MMHT2014nlo68clas118@0,PDF4LHC15_nlo_30_pdfas']"
  }
  modify_run_card(process_dir=process_dir,settings=extras)

  #---------------------------------------------------------------------------
  # MG5 param Card
  #---------------------------------------------------------------------------
  ## params is a dictionary of dictionaries (each dictionary is a separate block)
  params={}

  ## default aQGC couplings, set all as 0
  anoinputs={}
  anoinputs['FS0']="82e-12"
  anoinputs['FS1']="0e-12"
  anoinputs['FS2']="82e-12"
  anoinputs['FM0']="0e-12"
  anoinputs['FM1']="0e-12"
  anoinputs['FM2']="0e-12"
  anoinputs['FM3']="0e-12"
  anoinputs['FM4']="0e-12"
  anoinputs['FM5']="0e-12"
  anoinputs['FM6']="0e-12"
  anoinputs['FM7']="0e-12"
  anoinputs['FT0']="0e-12"
  anoinputs['FT1']="0e-12"
  anoinputs['FT2']="0e-12"
  anoinputs['FT3']="0e-12"
  anoinputs['FT4']="0e-12"
  anoinputs['FT5']="0e-12"
  anoinputs['FT6']="0e-12"
  anoinputs['FT7']="0e-12"
  anoinputs['FT8']="0e-12"
  anoinputs['FT9']="0e-12"

  ## update with user defined aQGC couplings
  for aqgc in aqgctypes:
    if aqgc=="FS02" : 
      anoinputs["FS0"]=str(aqgcvalue)+"e-12"   
      anoinputs["FS2"]=str(aqgcvalue)+"e-12"   
    elif aqgc in anoinputs.keys(): anoinputs[aqgc]=str(aqgcvalue)+"e-12"
  params['anoinputs']=anoinputs

  #Update SM parameters to match SM model
  sminputs={'aEWM1':"1.325070e+02", 'Gf':"1.166390e-05",'aS':"1.180000e-01"}
  params["SMINPUTS"]=sminputs

  massinputs={'MMU':"0.0","MT":"1.720000e+02","MZ":"9.118800e+01","W+":"80.419000"}
  params["MASS"]=massinputs

  yukawainputs={"ymt":"1.645000e+02"}
  params["YUKAWA"]=yukawainputs

  widthinputs={"WT":"1.320000e+00","WZ":"2.495200e+00","TA-":"0.000000e+00 ","WW":"2.085000e+00 ","WH":"4.070000e-03" }
  params["DECAY"]=widthinputs

  widthinputs={"cabi":"2.277360e-01"}
  params["CKMBLOCK"]=widthinputs


  modify_param_card(process_dir=process_dir,params=params)
  
  #---------------------------------------------------------------------------
  # MG5 + Pythia8 setup and process (lhe) generation
  #---------------------------------------------------------------------------
  generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
  opts.nprocs = 0 #multi-core cleanup after generation


  #--------------------------------------------------------------------------------------------------------------------
  # arrange output file
  #--------------------------------------------------------------------------------------------------------------------
  outputDS=""
  try:
      outputDS=arrange_output(runArgs=runArgs)
  except:
      mglog.error('Error arranging output dataset!')
      return -1

  mglog.info('All done generating events!!')
  return outputDS

#--------------------------------------------------------------------------------------------------------------------
# call the aQGC_Generation
#--------------------------------------------------------------------------------------------------------------------

#shortname=runArgs.jobConfig[0].split('/')[-1].split('.')[1]
shortname=jofile.split('/')[-1].split('.')[1]
aqgcorder="QUAD"
version="S0"
version=shortname.split("_")[6]

aqgctypes=[]
if aqgcorder is "QUAD" or aqgcorder is "INT" or aqgcorder is "FULL":  
  aqgctypes.append(shortname.split("_")[1].replace("aQGC",""))
elif aqgcorder is "CROSS":
  aqgctypetmp = shortname.split("_")[1].replace("aQGC","")
  aqgctypes=aqgctypetmp.split("vs")

aqgcvalue=shortname.split("_")[3]
if aqgcvalue[0] == "0":
  aqgcvalue=aqgcvalue.replace("0","0.",1)

# PDF information, in MadGraph's PDF naming scheme.  
# Note that if you change these numbers, you'll probably want to 
# change the "sys_pdf" tag in the run card too.  That's not done
# automatically yet.
pdf='lhapdf'
lhaid=260000 # NNPDF30_nlo_as_0118

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

# Run MadGraph!
outputDS=aQGC_Generation( version,
                          aqgcorder,             # QUAD, INT, CROSS
                          aqgctypes,              # [FT0], [FT1,FT2],,,,
                          aqgcvalue,             # Value if operrator point
                          randomSeed,            # random seed
                          runArgs.maxEvents,     # number of events for MadGraph to generate
                          runArgs.ecmEnergy/2.,  # Beam energy
                          pdf,                   # PDF information
                          lhaid
                          )


#--------------------------------------------------------------------------------------------------------------------
# multi-core cleanup
#--------------------------------------------------------------------------------------------------------------------
# multi-core running, if allowed!
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts


#--------------------------------------------------------------------------------------------------------------------
# Shower
#--------------------------------------------------------------------------------------------------------------------
runArgs.inputGeneratorFile=outputDS
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil=on"]
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph aQGC leptonic final states'
evgenConfig.keywords+=['SM','WZ','2jet','VBS']
evgenConfig.inputfilecheck = outputDS
evgenConfig.contact = ['Despoina Sampsonidou <despoina.sampsonidou@cern.ch>']
