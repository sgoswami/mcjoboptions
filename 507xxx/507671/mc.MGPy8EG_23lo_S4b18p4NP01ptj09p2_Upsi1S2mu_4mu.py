##
## 18.4GeV tetrab (4b bound state) scalar --> Upsilon+2mu --> 4mu with 0 or 1 jet
##
m4b=18.4
cVV4=0.001
cGG6=0.001
tcVV6=0.0
tcGG6=0.0
ktdurham=9.2
include("./MGCtrl_Py8EG_A14NNPDF23LO_tetrab_4mu.py")
