import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *


#import fileinput
#import shutil
#import subprocess
#import os

mode=0

#gridpack_dir='madevent/'
#gridpack_mode=True
gridpack_dir=None
gridpack_mode=False

masses={'25': '1.250000e+02'}        ## Higgs mass 
decays={'25': 'DECAY  25 4.07e-03'}  ## Higgs width
extras_cuts = {} ## mass filters
#os.environ['MADPATH'] = "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/sw/lcg/external/MCGenerators_lcgcmt67c/madgraph5amc/2.6.2.atlas/x86_64-slc6-gcc47-opt"



# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
model="import model sm"
nevents=1.2*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
process_str="""
"""+model+"""
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define v = ve vm vt
define v~ = ve~ vm~ vt~
generate p p > W+ W- j j QCD=0, w+ > l+ v,  w- > l- v~
output -f
"""

name = "osWW"

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


maxjetflavor = 4
dynamic=-1
#dynamic=1 #Total transverse energy of the event.
#dynamic=2 #sum of the transverse mass
#dynamic=3 #sum of the transverse mass divide by 2
#dynamic=4 #\sqrt(s), partonic energy
#dynamic=5 #\decaying particle mass, for decays



#process_dir = new_process(grid_pack="madevent/")
process_dir = new_process(process_str)

extras = { 
'asrwgtflavor':"5",     
'lhe_version':"3.0",     
'etal':"5",    
'drll':"0",    
'drjl':"0",    
'maxjetflavor':"5" ,    
'auto_ptj_mjj': 'F',   
'nevents'      : nevents
 }

extras.update(extras_cuts)
#build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
#build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', xqcut=0,nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
print_cards()
params={}
params['MASS']=masses 
params['DECAY']=decays
modify_param_card(process_dir=process_dir,params=params)
#os.system("cp setscales.f  "+process_dir+"/SubProcesses/setscales.f")

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

###shower

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


if "StoreLHE" in genSeq.Pythia8.__slots__.keys():
   print "Enablihg ABLING storage of LHE record in HepMC by default."
   genSeq.Pythia8.StoreLHE = False


genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_osWWevmvjj_EW6_LO'
evgenConfig.keywords = ['SM','diboson','VBS','WW','electroweak','2lepton','2jet']
evgenConfig.contact = ["yunju@cern.ch"]
