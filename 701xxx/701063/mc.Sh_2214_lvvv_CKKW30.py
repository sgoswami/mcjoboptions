include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "1 charged lepton + 3 neutrinos with 0,1j@NLO + 2,3j@LO and pTl1>5 GeV, pTl2>5 GeV."
evgenConfig.keywords = ["SM", "diboson", "lepton", "neutrino", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 2000

genSeq.Sherpa_i.RunCard="""
(run){
  # scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  # tags for process setup
  NJET:=3; LJET:=4,5; QCUT:=30.;

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  # EW corrections setup
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  METS_BBAR_MODE=5

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  SOFT_SPIN_CORRELATIONS=1

  EW_SCHEME=3
  GF=1.166397e-5
  ME_QED_CLUSTERING_THRESHOLD=5.

  PP_RS_SCALE VAR{0.25*sqr(PPerp(p[2])+PPerp(p[3])+PPerp(p[4])+PPerp(p[5]))};

  # NWF improvements
  NLO_CSS_PSMODE=1
}(run)

(processes){
  Process 93 93 -> 90 91 91 91 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  Enhance_Factor  3 {5};
  Enhance_Factor 10 {6};
  Enhance_Factor 10 {7};
  End process;
}(processes)

(selector){
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=write_parameters=1 ew_renorm_scheme=1 ew_scheme=2" ]
genSeq.Sherpa_i.Parameters += ["OL_PREFIX=Process/OpenLoops"]
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppllll" ,"ppllll_ew", "pp3lnj", "pp3lnj_ew"]
genSeq.Sherpa_i.NCores = 128

