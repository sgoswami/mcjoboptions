include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# Set masses based on physics short
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

# phys_short: [event generation]_[pdf]_[channel]_[decay flavor]_[gluino mass]_[neutralino mass]_[neutralino_fraction]_[prompt_fraction]
# ex: MGPy8EG_A14NNPDF23LO_GG_rpvLF_900_200_0p25_0p75
phys_short = get_physics_short()

decayflavor = phys_short.split('_')[3]

masses['1000021'] = float(phys_short.split('_')[4]) #go
masses['1000022'] = float(phys_short.split('_')[5].split('.')[0]) #N1

neutralino_frac = float(phys_short.split('_')[6].replace("p","."))
direct_frac = float(phys_short.split('_')[7].replace("p","."))

process = '''
import model RPVMSSM_UFO
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate    p p > go go QED=0 RPV=0     / susysq susysq~ @1
add process p p > go go j QED=0 RPV=0   / susysq susysq~ @2
add process p p > go go j j QED=0 RPV=0 / susysq susysq~ @3
'''

# Set up the decays
if decayflavor == "rpvHF":
  decays['1000021'] = """DECAY   1000021     7.40992706E-02   # gluino decays
  #          BR          NDA       ID1       ID2       ID3
        {0:.4f}  3    1000022  6  -6
        {1:.4f}  3    6        3   5
        {1:.4f}  3   -6       -3  -5
  """.format(neutralino_frac, direct_frac/2.)
  decays['1000022'] = """DECAY   1000022     1.0  # neutralino decays
  #          BR          NDA       ID1       ID2       ID3
        0.5000  3    6   3   5
        0.5000  3   -6  -3  -5
  #"""

elif decayflavor == "rpvLF":  
  decays['1000021'] = """DECAY   1000021     7.40992706E-02   # gluino decays
  #          BR          NDA       ID1       ID2       ID3
        {0:.4f}  3    1000022  1  -1 
        {0:.4f}  3    1000022  2  -2 
        {0:.4f}  3    1000022  3  -3 
        {0:.4f}  3    1000022  4  -4 
        {1:.4f}  3    2        1   3
        {1:.4f}  3    4        1   3
        {1:.4f}  3   -2       -1  -3
        {1:.4f}  3   -4       -1  -3
  """.format(neutralino_frac/4., direct_frac/4.)
  decays['1000022'] = """DECAY   1000022     1.0   # neutralino decays
  #          BR          NDA       ID1       ID2       ID3
        0.2500  3    2  1  3
        0.2500  3    4  1  3
        0.2500  3   -2 -1 -3
        0.2500  3   -4 -1 -3
  #"""         

# Set up a default event multiplier
evt_multiplier = 2

njets = 2

evgenConfig.contact  = ["zubair.bhatti@cern.ch"]

if decayflavor == "rpvHF":
  evgenConfig.description = 'gluino pair production and decay via RPV lamba323, or decay to top quarks and neutralino, which then decays via RPV lambda323, m_gluino = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000022'])

elif decayflavor == "rpvLF":
  evgenConfig.description = 'gluino pair production and decay via RPV lamba112, or decay to LF quarks and neutralino, which then decays via RPV lambda112, m_gluino = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000022'])

evgenConfig.keywords += [ 'SUSY', 'RPV', 'gluino', 'simplifiedModel', 'neutralino']

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

genSeq.Pythia8.Commands += ["Merging:Process = pp>{go,1000021}{go,1000021}"]

