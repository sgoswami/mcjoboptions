# --------------------------------------------------------------
# Standard pre-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# uncomment this if you want to keep MadGraph tarball, e.g. for Feynman diagrams
keepOutput=False
# --------------------------------------------------------------
from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

# --------------------------------------------------------------
# Some options for local testing.  Shouldn't hurt anything in production.
#
# Nominal configuration for production: MadSpin decays, with 0,1,2+ parton emissions in the matrix element
#

madgraph_pre_exec = "madgraphdecays" in dir()
pythia_pre_exec = "pythiadecays" in dir()
madspin_pre_exec = "madspindecays" in dir()
njets_pre_exec = "newnjets" in dir()
madspin_filename = "MS" in JOName
noguess_filename = "noG" in JOName
fixLHE_pre_exec = "fixLHE" in dir()
fixLHE_pre_exec = "WZ" in JOName


# pythia decays by default -- all can be enabled via preExec
#   - if MadSpin via filename, enable it and disable pythia
pythiadecays = not madspin_filename
madgraphdecays = False
madspindecays = madspin_filename
noguess = noguess_filename
fixLHE = fixLHE_pre_exec
if fixLHE: evgenLog.info('Fix LHE weights after MadSpin use (fixEventWeightsForBridgeMode).')

if sum([madgraph_pre_exec, pythia_pre_exec, madspin_pre_exec]) > 1:
    raise RuntimeError(
        "Too many decays specified in preExec. Pick pythiadecays, madgraphdecays, or madspindecays."
    )

if madgraph_pre_exec:
    madgraphdecays = True
    pythiadecays = False
    madspindecays = False
elif pythia_pre_exec:
    madgraphdecays = False
    pythiadecays = True
    madspindecays = False
elif madspin_pre_exec:
    madgraphdecays = False
    pythiadecays = False
    madspindecays = True
else:
    evgenLog.info("No preExec override for decays.")

# will be two, unless otherwise specified.
njets = 2 if not njets_pre_exec else newnjets
# if we're doing madgraph decays, don't do multijets.  For your own good.
njets = 0 if madgraphdecays else njets

# --------------------------------------------------------------

# --------------------------------------------------------------
# Interpret the name of the JO file to figure out the mass spectrum
#
def MassToFloat(s):
    return float(s.replace("p", "."))


# split up the JO file input name to interpret it
# assumes names are like: 
#   - MC15.xxxxxx.MGPy8EG_N2N1_302_300_2LMET50_MadSpin.py
#   - MC15.xxxxxx.MGPy8EG_C1pN2_180_150_3L3_MadSpin_noG.py
splitConfig = JOName.split("_")
config_after_UEtune = "_".join(splitConfig[1:])

# interpret the generation type, so we know which processes to run.
gentype = splitConfig[1]

# Tells MGC which param card to use.
decaytype = "N2_ZN1"
if "2L8" in config_after_UEtune and gentype == "C1C1":
    evgenLog.info("Setting decay type to WW in Wino-like model")
    decaytype = "WW"
else:
    evgenLog.info("Keeping default Higgsino decay type: {0:s}".format(decaytype))

# interpret the mass splittings, C1/N2 nearly degenerate
dM = MassToFloat(splitConfig[2]) - MassToFloat(splitConfig[3])
masses["1000023"] = -1.0 * MassToFloat(splitConfig[2])
masses["1000024"] = (MassToFloat(splitConfig[2]) + MassToFloat(splitConfig[3])) / 2.0
masses["1000022"] = MassToFloat(splitConfig[3])
if "C1M" in config_after_UEtune:
    C1M = (
        re.search("C1M[0-9]*p?[0-9]*", config_after_UEtune).group(0).replace("C1M", "")
    )
    masses["1000024"] = MassToFloat(C1M)
    evgenLog.info(
        "Using custom mass splitting. C1M = {0:s}, masses['1000024'] = {1:0.2f}".format(
            C1M, masses["1000024"]
        )
    )
elif "2L8" in config_after_UEtune and gentype == "C1C1":
    masses["1000024"] = MassToFloat(splitConfig[2])
    evgenLog.info(
        "Using non-default C1 mass. masses[1000024] = {0:0.2f}".format(
            masses["1000024"]
        )
    )
else:
    evgenLog.info(
        "Using default mass splitting. masses['1000024'] = {0:0.2f}".format(
            masses["1000024"]
        )
    )


# Force C1/N2 BR to VN1 --------------------------
evgenLog.info('Force BR(C1->WN1)=BR(N2->ZN1)=100%')
decays['1000024']="""DECAY   1000024     7.00367294E-03   # chargino1+ decays
     1.00000000E+00    2     1000022        24            # BR(~chi_1+ -> ~chi_10 w+)
     0.00000000E+00    3     1000022       -11        12  # BR(~chi_1+ -> ~chi_10 e+ nu_e), dummy on-shell decay to force Pythia to decay off-shell"""
decays['1000023']="""DECAY   1000023     9.37327589E-04   # neutralino2 decays
     1.00000000E+00    2     1000022        23            # BR(~chi_20 -> ~chi_10   Z )
     0.00000000E+00    3     1000022       -11        11  # BR(~chi_1+ -> ~chi_10 e+ e-), dummy on-shell decay to force Pythia to decay off-shell"""



# Add prompt N1 PRV decays (WHAT WIDTH TO USE? STEALING IT FROM C1 FOR NOW):
if "uds" in JOName:
    evgenLog.info('Force BR(N1->uds)=100%')
    decays['1000022'] = """DECAY   1000022  7.00367294E-03   # neutralino1 decays
    #          BR         NDA      ID1           ID2        ID3
    1.00000000E+00    3     1         2          3   # BR(Chi_10 -> u d s )
    """
elif "csb" in JOName:
    evgenLog.info('Force BR(N1->csb)=100%')
    decays['1000022'] = """DECAY   1000022  7.00367294E-03   # neutralino1 decays
    #          BR         NDA      ID1           ID2        ID3
    1.00000000E+00    3     3         4          5   # BR(Chi_10 -> c s b )
    """
else:
    raise RuntimeError("No (or incorrect) RPV decays specified in JO name! Must specify either uds or csb.")


# higgsino mixing matrix settings ----------------
# see also: https://twiki.cern.ch/twiki/bin/view/LHCPhysics/SUSYCrossSections13TeVhinosplit
# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='0.00E+00'
param_blocks['VMIX']['1 2']='1.00E+00'
param_blocks['VMIX']['2 1']='1.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'
# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='0.00E+00'
param_blocks['UMIX']['1 2']='1.00E+00'
param_blocks['UMIX']['2 1']='1.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'
# Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
param_blocks['NMIX']={}
param_blocks['NMIX']['1  1']=' 0.00E+00'   # N_11 bino
param_blocks['NMIX']['1  2']=' 0.00E+00'   # N_12
param_blocks['NMIX']['1  3']=' 7.07E-01'   # N_13
param_blocks['NMIX']['1  4']='-7.07E-01'   # N_14
param_blocks['NMIX']['2  1']=' 0.00E+00'   # N_21
param_blocks['NMIX']['2  2']=' 0.00E+00'   # N_22
param_blocks['NMIX']['2  3']='-7.07E-01'   # N_23 higgsino
param_blocks['NMIX']['2  4']='-7.07E-01'   # N_24 higgsino
param_blocks['NMIX']['3  1']=' 1.00E+00'   # N_31
param_blocks['NMIX']['3  2']=' 0.00E+00'   # N_32
param_blocks['NMIX']['3  3']=' 0.00E+00'   # N_33 higgsino
param_blocks['NMIX']['3  4']=' 0.00E+00'   # N_34 higgsino
param_blocks['NMIX']['4  1']=' 0.00E+00'   # N_41
param_blocks['NMIX']['4  2']='-1.00E+00'   # N_42 wino
param_blocks['NMIX']['4  3']=' 0.00E+00'   # N_43
param_blocks['NMIX']['4  4']=' 0.00E+00'   # N_44

if "WZ" in config_after_UEtune:

    dM=MassToFloat(splitConfig[2])-MassToFloat(splitConfig[3])
    masses['1000023'] = MassToFloat(splitConfig[2])
    masses['1000024'] = MassToFloat(splitConfig[2])
    masses['1000022'] = MassToFloat(splitConfig[3])

    # see also: https://twiki.cern.ch/twiki/pub/LHCPhysics/SUSYCrossSections13TeVn2x1wino/wino.dat
    # Off-diagonal chargino mixing matrix V
    param_blocks['VMIX']={}
    param_blocks['VMIX']['1 1']='9.72557835E-01'
    param_blocks['VMIX']['1 2']='-2.32661249E-01'
    param_blocks['VMIX']['2 1']='2.32661249E-01'
    param_blocks['VMIX']['2 2']='9.72557835E-01'
    # Off-diagonal chargino mixing matrix U
    param_blocks['UMIX']={}
    param_blocks['UMIX']['1 1']='9.16834859E-01'
    param_blocks['UMIX']['1 2']='-3.99266629E-01'
    param_blocks['UMIX']['2 1']='3.99266629E-01'
    param_blocks['UMIX']['2 2']='9.16834859E-01'
    # Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
    param_blocks['NMIX']={}
    param_blocks['NMIX']['1  1']='9.86364430E-01'   # N_11 bino
    param_blocks['NMIX']['1  2']='-5.31103553E-02'   # N_12
    param_blocks['NMIX']['1  3']='1.46433995E-01'   # N_13
    param_blocks['NMIX']['1  4']='-5.31186117E-02'   # N_14
    param_blocks['NMIX']['2  1']='9.93505358E-02'   # N_21
    param_blocks['NMIX']['2  2']='9.44949299E-01'   # N_22 wino
    param_blocks['NMIX']['2  3']='-2.69846720E-01'   # N_23 
    param_blocks['NMIX']['2  4']='1.56150698E-01'   # N_24 
    param_blocks['NMIX']['3  1']='-6.03388002E-02'   # N_31
    param_blocks['NMIX']['3  2']='8.77004854E-02'   # N_32
    param_blocks['NMIX']['3  3']='6.95877493E-01'   # N_33 higgsino
    param_blocks['NMIX']['3  4']='7.10226984E-01'   # N_34 higgsino
    param_blocks['NMIX']['4  1']='-1.16507132E-01'   # N_41
    param_blocks['NMIX']['4  2']='3.10739017E-01'   # N_42 
    param_blocks['NMIX']['4  3']='6.49225960E-01'   # N_43 higgsino
    param_blocks['NMIX']['4  4']='-6.84377823E-01'   # N_44 higgisno
# --------------------------------------------------------------
# MadGraph options
#
bwcutoff = 1000
xqcut = 15                             # low matching scale, following DM group recommendations
run_settings['ptj'] = 10               # low matching scale, following DM group recommendations
run_settings['ptl'] = 0
if 'MET' in config_after_UEtune:               # leading jet pT > 50 GeV for generation with MET filter
   run_settings['ptj1min'] = 50
run_settings['etal'] = '-1.0'
run_settings['drll'] = 0.0
run_settings['drjl'] = 0.0
run_settings['auto_ptj_mjj'] = 'F'
run_settings['bwcutoff'] = bwcutoff    # to allow very low-mass W* and Z*
run_settings['ktdurham'] = xqcut       # doesn't seem to work with just xqcut variable as in L160
run_settings['lhe_version'] = '3.0'    # default
run_settings['event_norm'] = 'sum'     # default

import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING['central_pdf'] = 247000
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING['pdf_variations'] = [247000] #247000:NNPDF23_lo_as_0130_qed, 260000:NNPDF30_nlo_as_0118
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING['alternative_pdfs'] = []
evgenLog.info('Update PDF settings: %r',MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING)

predefined_process = """
"""
#
# --------------------------------------------------------------

# --------------------------------------------------------------
# Configure the process definitions.

gentypes = {
    "incl": {"processes": ["n2 n1", "n2 c1", "c1 c1", "c1 n1"]},
    "N2C1": {"processes": ["n2 c1"]},
    "C1N2": {"processes": ["n2 c1"]},
    "N2C1p": {"processes": ["n2 x1+"]},
    "C1pN2": {"processes": ["n2 x1+"]},
    "N2C1m": {"processes": ["n2 x1-"]},
    "C1mN2": {"processes": ["n2 x1-"]},
    "C1C1": {"processes": ["x1+ x1-"]},
    "N2N1": {"processes": ["n2 n1"]},
    "N1N2": {"processes": ["n2 n1"]},
    "C1N1": {"processes": ["c1 n1"]},
    "N1C1": {"processes": ["c1 n1"]},
}
if not any(process_type == gentype for process_type in gentypes.keys()):
    raise RuntimeError("Unknown process type {0:s}, aborting.".format(gentype))

if madspindecays and gentype in ["C1N1", "N1C1", "N2C1", "C1N2", "incl"]:
    raise RuntimeError(
        "We can only have two particle species for MadSpin to decay. Three or more particle species in the same job can't use MadSpin! This happens, for example, when you request N1C1: N1C1+ and N1C1-."
    )

# build mgprocstrings (note: space at end important)
mgprocstrings = [
    "p p > {0:s} ".format(process) for process in gentypes[gentype]["processes"]
]

mergeprocs = {
    "N2C1p": "w+",
    "C1pN2": "w+",
    "N2C1m": "w-",
    "C1mN2": "w-",
    "C1C1": "z",
    "N2N1": "z",
    "N1N2": "z"
}
if noguess and not gentype in mergeprocs:
    raise RunTimeError("NoGuess option active and merge process not defined for {0:s}. Define merge process or use guess option.".format(gentype))

# build multijetstrings
# skip the 0 jet events when applying a MET filter
skip0Jet = 'MET' in config_after_UEtune
multijetstrings = ["j " * i for i in range(int(skip0Jet), njets + 1)]

# additional strings at the end of each process
mgdecaystring = ""
msdecaystring = ""
if noguess:
    mergeproc = "pp>{0:s}".format(mergeprocs[gentype])
else:
    mergeproc = "guess"

if gentype == "incl":
    pass  # shortcircuit

elif gentype == "N2C1" or gentype == "C1N2":
    if madgraphdecays:
        mgdecaystring = ", (n2 > l+ l- n1 $ susystrong sleptons), (c1 > all all n1 $ susystrong sleptons)"

elif gentype == "N2C1p" or gentype == "C1pN2":
    if madspindecays:
        if "3L" in config_after_UEtune:
            msdecaystring = "decay n2 > l+ l- n1\ndecay x1+ > l+ vl n1\n"
        else:
            msdecaystring = "decay n2 > l+ l- n1\ndecay x1+ > f f n1\n"
    elif madgraphdecays:
        if "3L" in config_after_UEtune:
            mgdecaystring = ", (n2 > l+ l- n1 $ susystrong sleptons), (x1+ > l+ vl n1 $ susystrong sleptons)"
        else:
            mgdecaystring = ", (n2 > l+ l- n1 $ susystrong sleptons), (x1+ > all all n1 $ susystrong sleptons)"

elif gentype == "N2C1m" or gentype == "C1mN2":
    if madspindecays:
        if "3L" in config_after_UEtune:
            msdecaystring = "decay n2 > l+ l- n1\ndecay x1- > l- vl~ n1\n"
        else:
            msdecaystring = "decay n2 > l+ l- n1\ndecay x1- > f f n1\n"
    elif madgraphdecays:
        if "3L" in config_after_UEtune:
            mgdecaystring = ", (n2 > l+ l- n1 $ susystrong sleptons), (x1- > l- vl~ n1 $ susystrong sleptons)"
        else:
            mgdecaystring = ", (n2 > l+ l- n1 $ susystrong sleptons), (x1- > all all n1 $ susystrong sleptons)"

elif gentype == "C1C1":
    if madspindecays:
        msdecaystring = "decay x1+ > l+ vl  n1\ndecay x1- > l- vl~ n1\n"
    elif madgraphdecays:
        mgdecaystring = ", (x1+ > l+ vl n1 $ susystrong sleptons), (x1- > l- vl~ n1 $ susystrong sleptons)"

elif gentype == "N2N1" or gentype == "N1N2":
    if madspindecays:
        msdecaystring = "decay n2 > l+ l-  n1"
    elif madgraphdecays:
        mgdecaystring = ", (n2 > l+ l- n1 $ susystrong sleptons)"

elif gentype == "C1N1" or gentype == "N1C1":
    if madgraphdecays:
        mgdecaystring = ", (c1 > lv lv n1 $ susystrong sleptons)"

# first build up all the process strings
process_strings = [
    "{0:s}{1:s}{2:s} RPV=0 / susystrong @".format(mgprocstring, multijetstring, mgdecaystring)
    for mgprocstring in mgprocstrings
    for multijetstring in multijetstrings
]
# then add the susystrong @# at the end (starting from #=1)
process_strings = [
    "{0:s}{1:d}".format(proc_string, i + 1)
    for i, proc_string in enumerate(process_strings)
]
# build entire process string
process = """
import model RPVMSSM_UFO
define c1 = x1+ x1-
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lv = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~ g
define sleptons = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ h+ h- svt svm sve svt~ svm~ sve~
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate {0:s}
""".format(
    "\nadd process ".join(process_strings)
)

# print the process, just to confirm we got everything right
evgenLog.info("Final process card:")
evgenLog.info(process)

# change this back, to use a single parameter card for the generation
if "2L8" in config_after_UEtune and gentype == "C1C1":
    gentype = "C1C1"
else:
    gentype = "C1N1"

# --------------------------------------------------------------
# Madspin configuration
#
if madspindecays == True:
    fixEventWeightsForBridgeMode=fixLHE
    if msdecaystring == "":
        raise RuntimeError("Asking for MadSpin decays, but no decay string provided!")
    madspin_card = "madspin_card_higgsino.dat"

    mscard = open(madspin_card, "w")

    mscard.write(
        """#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
set BW_cut %i                # cut on how far the particle can be off-shell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
set spinmode none
# specify the decay for the final state particles

%s

# running the actual code
launch"""
        % (bwcutoff, runArgs.randomSeed, msdecaystring)
    )
    mscard.close()
#
# --------------------------------------------------------------


# --------------------------------------------------------------
# Pythia options
#
pythia = genSeq.Pythia8
pythia.Commands += ["23:mMin = 0.2"]
pythia.Commands += ["24:mMin = 0.2"]
pythia.Commands += ["-24:mMin = 0.2"]

# information about this generation
evgenLog.info("p p > c1n1/c1n2/c1c1/n2n1 productions. ")
evgenConfig.contact = ["michael.hance@cern.ch","sara.alderweireldt@cern.ch","jeffrey.david.shahinian@cern.ch"]
evgenConfig.keywords += ["gaugino", "chargino", "neutralino", "RPV"]
evgenConfig.description = "SUSY Simplified Model with compressed chargino/neutralino production and decays via W/Z with MadGraph/MadSpin/Pythia8/EvtGen. The N1 decays promptly via RPV UDD couplings."
# --------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------
# add some filters here
#
# include check on dM in case we just want to ask MadGraph to generate completely degenerate Higgsinos,
# e.g. to check cross sections.
#
filters = []
evt_multiplier = 12
if gentype == "C1C1":
    evt_multiplier = 24

# N1 filter
try:
    N1pT = float(splitConfig[8].replace("N1pT", ""))
except:
    raise RuntimeError("Invalid N1pT cut specified! Must be of the form N1pT150 (or an equivalent number).")
evgenLog.info('Filter: requiring at least N1 with pT > %s GeV' % N1pT)
if not hasattr(filtSeq, "ParticleFilter"):
    from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
    filtSeq += ParticleFilter("ParticleFilter")
    filtSeq.ParticleFilter.Ptcut = N1pT * 1000.
    filtSeq.ParticleFilter.Etacut = 10.0
    filtSeq.ParticleFilter.StatusReq = -1
    filtSeq.ParticleFilter.PDG = 1000022
    filtSeq.ParticleFilter.MinParts = 1
    filtSeq.ParticleFilter.Exclusive = False
    filters.append("ParticleFilter")

# lepton filter, interpreted from jobOptions
nleptonsfilterreq = 0
if "1L" in config_after_UEtune:
    nleptonsfilterreq = 1
elif "2L" in config_after_UEtune:
    nleptonsfilterreq = 2
elif "3L" in config_after_UEtune:
    nleptonsfilterreq = 3

is1L  = True if '1L'  in config_after_UEtune else False
is2L2 = True if '2L2' in config_after_UEtune else False
is2L8 = True if '2L8' in config_after_UEtune else False
is3L2 = True if '3L2' in config_after_UEtune else False
is3L3 = True if '3L3' in config_after_UEtune else False

evgenLog.info('nleptonsfilterreq: {}'.format(nleptonsfilterreq))
evgenLog.info('is1L: {}'.format(is1L))
evgenLog.info('is2L2: {}'.format(is2L2))
evgenLog.info('is2L8: {}'.format(is2L8))
evgenLog.info('is3L2: {}'.format(is3L2))
evgenLog.info('is3L3: {}'.format(is3L3))

# MultiElecMuTauFilter - general --------------------------------------
if nleptonsfilterreq > 0:
    if not hasattr(filtSeq, "MultiElecMuTauFilter"):
       from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
       filtSeq += MultiElecMuTauFilter("MultiElecMuTauFilter")
    ElecMuTauFilter = filtSeq.MultiElecMuTauFilter
    ElecMuTauFilter.MinPt = 2000.0
    ElecMuTauFilter.MaxEta = 2.8
    ElecMuTauFilter.NLeptons = nleptonsfilterreq
    ElecMuTauFilter.IncludeHadTaus = 0  # don't include hadronic taus

    if is2L2:
        evgenLog.info("2lepton2 filter is applied")
        ElecMuTauFilter.MinPt = 2000.0
    if is2L8:
        evgenLog.info("2lepton8 filter is applied")
        ElecMuTauFilter.MinPt = 8000.0
    if is3L2:
        evgenLog.info("3lepton2 filter is applied")
        ElecMuTauFilter.MinPt = 2000.0
    if is3L3:
        evgenLog.info("3lepton3 filter is applied")
        ElecMuTauFilter.MinPt = 3000.0

    filters.append("MultiElecMuTauFilter")

# Z->ll/W->lv setting -----------------------------------------
if nleptonsfilterreq > 1:
    # if there's a Z in the event, and we have pythia decaying things, the only way to get 2 leptons is to have Z->ll
    pythia.Commands += [
        "23:onMode = off",  # switch off all Z decays
        "23:onIfAny = 11 13 15",  # switch on Z->ll
    ]
    # if there's a W in the event, and we have pythia decaying things, force W->lv (on top of Z->ll)
    pythia.Commands += [
        "24:onMode = off",  # switch off all W decays
        "24:onIfAny = 11 12 13 14 15 16",  # switch on W->lv
    ]

if is3L3:
    # if there's a W in the event, and we have pythia decaying things, force W->lv (on top of Z->ll)
    pythia.Commands += [
        "24:onMode = off",  # switch off all W decays
        "24:onIfAny = 11 12 13 14 15 16",  # switch on W->lv
    ]

# multipliers ------------------------------------------------
if nleptonsfilterreq > 0 and dM > 0:
    evt_multiplier *= 2.5
    if not is1L: evt_multiplier *= 1.6
    if dM < 10: evt_multiplier *= 2

    # some samples with very small mass splittings
    # need more events to converge
    if splitConfig[1] == "C1C1":
        if int(dM) == 5:
            evt_multiplier *= 3
            
        if int(dM) == 3:
            evt_multiplier *= 6
            
        if int(dM) == 2:
            evt_multiplier *= 10
            
    else:
        if int(dM) == 3:
            evt_multiplier *= 1 if is1L else 2
            
        elif int(dM) <= 2:
            evt_multiplier *= 1.5 if is1L else 4
            
            if abs(masses["1000023"]) >= 200:
                evt_multiplier *= 1.3

        if int(dM) <= 1:
            evt_multiplier *= 5

    if is2L8: 
        evt_multiplier = 20  # temporary, should be set based on dM, as above

    if is3L3:
        if int(dM)>50: evt_multiplier *= 2
        elif int(dM)>20: evt_multiplier *= 3
        elif int(dM)>15: evt_multiplier *= 4
        elif int(dM)>10: evt_multiplier *= 5
        elif int(dM)>5:  
            evt_multiplier *= 10
            evgenConfig.nEventsPerJob = 5000
        elif int(dM)<=5: 
            evt_multiplier *= 19
            evgenConfig.nEventsPerJob = 1000
            if abs(masses["1000023"]) >= 300: 
                evt_multiplier *= 1.5
            elif abs(masses["1000023"]) >= 200: 
                evt_multiplier *= 1.25
        evt_multiplier = int(evt_multiplier)

#------------------------------------------------------------
# MET filter, interpreted from jobOptions
if 'MET' in config_after_UEtune and dM > 0:
    evt_multiplier *= 10
    if not hasattr(filtSeq, "MissingEtFilter"):
        from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter

        filtSeq += MissingEtFilter("MissingEtFilter")

    if not hasattr(filtSeq, "MissingEtFilterUpperCut"):
        filtSeq += MissingEtFilter("MissingEtFilterUpperCut")

    lowercut = int(
        JOName[
            JOName.find("MET") + 3 : JOName.find("MET") + 5
        ]
    )

    filtSeq.MissingEtFilter.METCut = lowercut * GeV
    filtSeq.MissingEtFilterUpperCut.METCut = 100000 * GeV
    filters.append("MissingEtFilter and not MissingEtFilterUpperCut")

#------------------------------------------------------------
# no filters (e.g. inclusive)
if len(filters) == 0:
    evt_multiplier *= 3

#------------------------------------------------------------
evgenLog.info("Event Multiplier: {0:0.2f}".format(evt_multiplier))

# only add these filters if we have some non-zero mass splitting.  dM=0 is a special case used to test
# output cross sections against standard benchmarks
if dM > 0:
    for i in filters:
        if filtSeq.Expression == "":
            filtSeq.Expression = i
        else:
            filtSeq.Expression = "(%s) and (%s)" % (filtSeq.Expression, i)
# --------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------
# Merging options
#
if not noguess:
   mergeproc = 'Merging:process = guess'

if njets > 0:
    if not noguess:
       genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
       
    genSeq.Pythia8.Commands += [
        "Merging:Process = {0:s}".format(mergeproc),
        "1000024:spinType = 1",
        "1000023:spinType = 1",
        "1000022:spinType = 1",
    ]

# ----------------------------------------------------------------------------------------------------------------
# Standard post-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )
# --------------------------------------------------------------
