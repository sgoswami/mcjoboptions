# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 10.04.2021,  08:03
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.53660658E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.17338280E+03  # scale for input parameters
    1   -1.01110889E+03  # M_1
    2   -4.26438509E+02  # M_2
    3    3.40959286E+03  # M_3
   11   -3.20017301E+03  # A_t
   12   -1.03145346E+03  # A_b
   13   -4.11491807E+02  # A_tau
   23    8.25201776E+02  # mu
   25    3.45144712E+01  # tan(beta)
   26    5.29351188E+02  # m_A, pole mass
   31    8.30892783E+02  # M_L11
   32    8.30892783E+02  # M_L22
   33    5.47282032E+02  # M_L33
   34    7.58207999E+02  # M_E11
   35    7.58207999E+02  # M_E22
   36    9.52857260E+02  # M_E33
   41    2.13151369E+03  # M_Q11
   42    2.13151369E+03  # M_Q22
   43    1.89820867E+03  # M_Q33
   44    1.11975428E+03  # M_U11
   45    1.11975428E+03  # M_U22
   46    9.69351075E+02  # M_U33
   47    1.50504384E+03  # M_D11
   48    1.50504384E+03  # M_D22
   49    1.83268294E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.17338280E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.17338280E+03  # (SUSY scale)
  1  1     8.38789055E-06   # Y_u(Q)^DRbar
  2  2     4.26104840E-03   # Y_c(Q)^DRbar
  3  3     1.01332455E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.17338280E+03  # (SUSY scale)
  1  1     5.81747099E-04   # Y_d(Q)^DRbar
  2  2     1.10531949E-02   # Y_s(Q)^DRbar
  3  3     5.76910829E-01   # Y_b(Q)^DRbar
Block Ye Q=  1.17338280E+03  # (SUSY scale)
  1  1     1.01519123E-04   # Y_e(Q)^DRbar
  2  2     2.09909348E-02   # Y_mu(Q)^DRbar
  3  3     3.53032992E-01   # Y_tau(Q)^DRbar
Block Au Q=  1.17338280E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -3.20017300E+03   # A_t(Q)^DRbar
Block Ad Q=  1.17338280E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.03145349E+03   # A_b(Q)^DRbar
Block Ae Q=  1.17338280E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -4.11491807E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  1.17338280E+03  # soft SUSY breaking masses at Q
   1   -1.01110889E+03  # M_1
   2   -4.26438509E+02  # M_2
   3    3.40959286E+03  # M_3
  21   -4.02542526E+05  # M^2_(H,d)
  22   -6.97174588E+05  # M^2_(H,u)
  31    8.30892783E+02  # M_(L,11)
  32    8.30892783E+02  # M_(L,22)
  33    5.47282032E+02  # M_(L,33)
  34    7.58207999E+02  # M_(E,11)
  35    7.58207999E+02  # M_(E,22)
  36    9.52857260E+02  # M_(E,33)
  41    2.13151369E+03  # M_(Q,11)
  42    2.13151369E+03  # M_(Q,22)
  43    1.89820867E+03  # M_(Q,33)
  44    1.11975428E+03  # M_(U,11)
  45    1.11975428E+03  # M_(U,22)
  46    9.69351075E+02  # M_(U,33)
  47    1.50504384E+03  # M_(D,11)
  48    1.50504384E+03  # M_(D,22)
  49    1.83268294E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23361790E+02  # h0
        35     5.29178788E+02  # H0
        36     5.29351188E+02  # A0
        37     5.36837383E+02  # H+
   1000001     2.11438472E+03  # ~d_L
   2000001     1.43531315E+03  # ~d_R
   1000002     2.11298771E+03  # ~u_L
   2000002     9.91071907E+02  # ~u_R
   1000003     2.11438375E+03  # ~s_L
   2000003     1.43531184E+03  # ~s_R
   1000004     2.11298670E+03  # ~c_L
   2000004     9.91072090E+02  # ~c_R
   1000005     1.77946945E+03  # ~b_1
   2000005     1.83371272E+03  # ~b_2
   1000006     7.43936878E+02  # ~t_1
   2000006     1.85073121E+03  # ~t_2
   1000011     8.35630511E+02  # ~e_L-
   2000011     7.66560714E+02  # ~e_R-
   1000012     8.31534000E+02  # ~nu_eL
   1000013     8.35677026E+02  # ~mu_L-
   2000013     7.66496092E+02  # ~mu_R-
   1000014     8.31529183E+02  # ~nu_muL
   1000015     5.49099862E+02  # ~tau_1-
   2000015     9.58854741E+02  # ~tau_2-
   1000016     5.47687120E+02  # ~nu_tauL
   1000021     3.24335274E+03  # ~g
   1000022     4.42369844E+02  # ~chi_10
   1000023     8.30372879E+02  # ~chi_20
   1000025     8.30996495E+02  # ~chi_30
   1000035     1.00046773E+03  # ~chi_40
   1000024     4.42628431E+02  # ~chi_1+
   1000037     8.38373753E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.10329300E-02   # alpha
Block Hmix Q=  1.17338280E+03  # Higgs mixing parameters
   1    8.25201776E+02  # mu
   2    3.45144712E+01  # tan[beta](Q)
   3    2.44157309E+02  # v(Q)
   4    2.80212680E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     1.61369132E-01   # Re[R_st(1,1)]
   1  2     9.86894120E-01   # Re[R_st(1,2)]
   2  1    -9.86894120E-01   # Re[R_st(2,1)]
   2  2     1.61369132E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     3.73248458E-01   # Re[R_sb(1,1)]
   1  2     9.27731421E-01   # Re[R_sb(1,2)]
   2  1    -9.27731421E-01   # Re[R_sb(2,1)]
   2  2     3.73248458E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.96422778E-01   # Re[R_sta(1,1)]
   1  2     8.45082692E-02   # Re[R_sta(1,2)]
   2  1    -8.45082692E-02   # Re[R_sta(2,1)]
   2  2     9.96422778E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     4.73414901E-03   # Re[N(1,1)]
   1  2    -9.89726227E-01   # Re[N(1,2)]
   1  3     1.27634733E-01   # Re[N(1,3)]
   1  4     6.42569700E-02   # Re[N(1,4)]
   2  1     1.75743289E-01   # Re[N(2,1)]
   2  2     1.34472468E-01   # Re[N(2,2)]
   2  3     6.88551293E-01   # Re[N(2,3)]
   2  4     6.90600151E-01   # Re[N(2,4)]
   3  1     1.78154406E-02   # Re[N(3,1)]
   3  2    -4.49359415E-02   # Re[N(3,2)]
   3  3    -7.05217392E-01   # Re[N(3,3)]
   3  4     7.07341361E-01   # Re[N(3,4)]
   4  1     9.84263427E-01   # Re[N(4,1)]
   4  2    -1.84366992E-02   # Re[N(4,2)]
   4  3    -1.10792242E-01   # Re[N(4,3)]
   4  4    -1.36420940E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.83854174E-01   # Re[U(1,1)]
   1  2     1.78971964E-01   # Re[U(1,2)]
   2  1    -1.78971964E-01   # Re[U(2,1)]
   2  2    -9.83854174E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.95941238E-01   # Re[V(1,1)]
   1  2     9.00058315E-02   # Re[V(1,2)]
   2  1     9.00058315E-02   # Re[V(2,1)]
   2  2    -9.95941238E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     3.84052954E-05   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99941755E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
DECAY   1000011     5.54229490E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     3.35043671E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     6.64950781E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     1.82667301E-04   # ~mu^-_R
#    BR                NDA      ID1      ID2
     4.76580022E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     5.23419978E-01    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     5.54314388E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     3.35050460E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     6.64943680E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.73773862E-01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.37229103E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     6.62770897E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     2.68026848E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.50391757E-04    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.99247965E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.79228516E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.57900125E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.00733049E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     4.59042636E-01    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     2.21946271E-01    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     2.10681848E-01    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     5.58151277E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     3.31892184E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     6.68107804E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     5.58155699E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     3.31884597E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     6.68115392E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     8.68911831E-01   # ~nu_tau
#    BR                NDA      ID1      ID2
     3.30062609E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     6.69937391E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
DECAY   2000001     2.16296918E-01   # ~d_R
#    BR                NDA      ID1      ID2
     5.06561176E-02    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     5.29521063E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.48725817E-01    2     1000035         1   # BR(~d_R -> chi^0_4 d)
DECAY   1000001     2.55422512E+01   # ~d_L
#    BR                NDA      ID1      ID2
     3.26903822E-01    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.77476964E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     6.03082377E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     8.23508077E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     6.44904288E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.65789347E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
DECAY   2000003     2.19401194E-01   # ~s_R
#    BR                NDA      ID1      ID2
     2.79174400E-04    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     5.32596374E-02    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     4.00802059E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.35355655E-01    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     4.17044187E-04    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     6.68046885E-03    2    -1000037         4   # BR(~s_R -> chi^-_2 c)
DECAY   1000003     2.55467390E+01   # ~s_L
#    BR                NDA      ID1      ID2
     3.26849106E-01    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.84240035E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.74345183E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     8.23513802E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     6.44790027E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.65977921E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
DECAY   1000005     2.76057462E+01   # ~b_1
#    BR                NDA      ID1      ID2
     6.71048547E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.20261824E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.23931267E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.03973761E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     1.27661630E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.48900696E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     2.80144325E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
     2.15980282E-02    2     1000006       -37   # BR(~b_1 -> ~t_1 H^-)
DECAY   2000005     1.05129698E+02   # ~b_2
#    BR                NDA      ID1      ID2
     5.18320266E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.63047243E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.80230363E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     4.52646743E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     9.82474978E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.20875061E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.16444547E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     3.37466400E-02    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
DECAY   2000002     6.16421909E-03   # ~u_R
#    BR                NDA      ID1      ID2
     5.15995145E-03    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.84791533E-01    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.00484965E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
DECAY   1000002     2.56202762E+01   # ~u_L
#    BR                NDA      ID1      ID2
     3.24545827E-01    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.13194045E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.52442044E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     5.37419044E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     6.58319787E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     4.17581280E-03    2     1000037         1   # BR(~u_L -> chi^+_2 d)
DECAY   2000004     6.23180992E-03   # ~c_R
#    BR                NDA      ID1      ID2
     5.25884114E-03    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.77279958E-01    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.25539574E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     2.98232719E-04    2     1000024         3   # BR(~c_R -> chi^+_1 s)
     4.60901051E-03    2     1000037         3   # BR(~c_R -> chi^+_2 s)
DECAY   1000004     2.56245450E+01   # ~c_L
#    BR                NDA      ID1      ID2
     3.24491489E-01    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.14056043E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.63101006E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     5.37368435E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     6.58215773E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     4.31230628E-03    2     1000037         3   # BR(~c_L -> chi^+_2 s)
DECAY   1000006     4.24964726E-02   # ~t_1
#    BR                NDA      ID1      ID2
     8.81131608E-01    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     3.09653171E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     8.27035726E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
#    BR                NDA      ID1      ID2       ID3
     8.31609260E-04    3     1000016       -15         5   # BR(~t_1 -> ~nu_tau tau^+ b)
     4.31576584E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.32692658E+02   # ~t_2
#    BR                NDA      ID1      ID2
     5.50265029E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     8.55336491E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     9.29531075E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.44995830E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.12832752E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.69398639E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.40019365E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.60718419E-02    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     3.23999436E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.41735238E-02    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     4.48608955E-14   # chi^+_1
#    BR                NDA      ID1      ID2
     9.30202466E-01    2     1000022       211   # BR(chi^+_1 -> chi^0_1 pi^+)
#    BR                NDA      ID1      ID2       ID3
     4.82053364E-02    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     2.15921972E-02    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
DECAY   1000037     5.85215503E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     3.70326677E-03    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     5.83127236E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.98529592E-01    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     2.60971597E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.36288197E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.39477065E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.26884427E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     6.81659914E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.77220888E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.82477940E+00   # chi^0_2
#    BR                NDA      ID1      ID2
     2.91686292E-04    2     2000011       -11   # BR(chi^0_2 -> ~e^-_R e^+)
     2.91686292E-04    2    -2000011        11   # BR(chi^0_2 -> ~e^+_R e^-)
     3.00244130E-04    2     2000013       -13   # BR(chi^0_2 -> ~mu^-_R mu^+)
     3.00244130E-04    2    -2000013        13   # BR(chi^0_2 -> ~mu^+_R mu^-)
     3.76701541E-02    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     3.76701541E-02    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     1.99154173E-04    2     1000016       -16   # BR(chi^0_2 -> ~nu_tau nu_bar_tau)
     1.99154173E-04    2    -1000016        16   # BR(chi^0_2 -> ~nu^*_tau nu_tau)
     3.13642974E-01    2     1000024       -24   # BR(chi^0_2 -> chi^+_1 W^-)
     3.13642974E-01    2    -1000024        24   # BR(chi^0_2 -> chi^-_1 W^+)
     3.00680047E-02    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     2.60501337E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.48341086E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.31382934E-03    3     1000024         5        -6   # BR(chi^0_2 -> chi^+_1 b t_bar)
     2.31382934E-03    3    -1000024        -5         6   # BR(chi^0_2 -> chi^-_1 b_bar t)
DECAY   1000025     6.03310010E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.66148519E-02    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     2.66148519E-02    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     2.87564234E-04    2     1000016       -16   # BR(chi^0_3 -> ~nu_tau nu_bar_tau)
     2.87564234E-04    2    -1000016        16   # BR(chi^0_3 -> ~nu^*_tau nu_tau)
     2.28696917E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.28696917E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     2.33997616E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.51719022E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.62364435E-04    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     1.33328409E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     1.33328409E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     6.95536942E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     5.96954619E-02    2     2000011       -11   # BR(chi^0_4 -> ~e^-_R e^+)
     5.96954619E-02    2    -2000011        11   # BR(chi^0_4 -> ~e^+_R e^-)
     7.45088573E-03    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     7.45088573E-03    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     5.97251106E-02    2     2000013       -13   # BR(chi^0_4 -> ~mu^-_R mu^+)
     5.97251106E-02    2    -2000013        13   # BR(chi^0_4 -> ~mu^+_R mu^-)
     7.44786585E-03    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     7.44786585E-03    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     4.06136115E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     4.06136115E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     2.33097736E-03    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     2.33097736E-03    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     8.96449766E-03    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     8.96449766E-03    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     8.96496175E-03    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     8.96496175E-03    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     4.59881450E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     4.59881450E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     1.63137583E-04    2     2000002        -2   # BR(chi^0_4 -> ~u_R u_bar)
     1.63137583E-04    2    -2000002         2   # BR(chi^0_4 -> ~u^*_R u)
     1.61725924E-04    2     2000004        -4   # BR(chi^0_4 -> ~c_R c_bar)
     1.61725924E-04    2    -2000004         4   # BR(chi^0_4 -> ~c^*_R c)
     1.33922739E-01    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     1.33922739E-01    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     5.74378407E-03    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     5.74378407E-03    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.32683941E-02    2     1000037       -24   # BR(chi^0_4 -> chi^+_2 W^-)
     5.32683941E-02    2    -1000037        24   # BR(chi^0_4 -> chi^-_2 W^+)
     2.04326466E-04    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     2.04326466E-04    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     7.61107241E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.27825504E-04    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.90574812E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     5.01644321E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.07833793E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.39781861E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     2.57164798E-04    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000021     6.32678970E+02   # ~g
#    BR                NDA      ID1      ID2
     6.23633826E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     6.23633826E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     2.51344587E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     2.51344587E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     6.23633757E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     6.23633757E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     2.51344843E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     2.51344843E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     1.90582442E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.90582442E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     6.56365689E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     6.56365689E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.60890017E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     3.60890017E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     4.90631809E-02    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     4.90631809E-02    2    -2000001         1   # BR(~g -> ~d^*_R d)
     2.50854503E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     2.50854503E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     4.90632245E-02    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     4.90632245E-02    2    -2000003         3   # BR(~g -> ~s^*_R s)
     2.50854842E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     2.50854842E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     3.69736945E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.69736945E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     3.52107066E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     3.52107066E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     3.16180956E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     3.10306025E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.23655065E-04    3     1000035         2        -2   # BR(~g -> chi^0_4 u u_bar)
     1.23655518E-04    3     1000035         4        -4   # BR(~g -> chi^0_4 c c_bar)
     4.01478969E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     4.01478969E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.23325128E-03   # Gamma(h0)
     2.04606250E-03   2        22        22   # BR(h0 -> photon photon)
     1.18052878E-03   2        22        23   # BR(h0 -> photon Z)
     2.05870941E-02   2        23        23   # BR(h0 -> Z Z)
     1.75747372E-01   2       -24        24   # BR(h0 -> W W)
     6.17294494E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.65484760E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.51536881E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.23593821E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.33038861E-07   2        -2         2   # BR(h0 -> Up up)
     2.58160019E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.42881535E-07   2        -1         1   # BR(h0 -> Down down)
     2.32526302E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.40049265E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.00572975E+01   # Gamma(HH)
     7.76418554E-08   2        22        22   # BR(HH -> photon photon)
     5.35881660E-09   2        22        23   # BR(HH -> photon Z)
     1.63889623E-05   2        23        23   # BR(HH -> Z Z)
     3.43181608E-05   2       -24        24   # BR(HH -> W W)
     1.57685949E-04   2        21        21   # BR(HH -> gluon gluon)
     9.77183428E-09   2       -11        11   # BR(HH -> Electron electron)
     4.34857151E-04   2       -13        13   # BR(HH -> Muon muon)
     1.21989971E-01   2       -15        15   # BR(HH -> Tau tau)
     1.52867548E-13   2        -2         2   # BR(HH -> Up up)
     2.96384944E-08   2        -4         4   # BR(HH -> Charm charm)
     1.00802839E-03   2        -6         6   # BR(HH -> Top top)
     7.08556066E-07   2        -1         1   # BR(HH -> Down down)
     2.56370324E-04   2        -3         3   # BR(HH -> Strange strange)
     8.75988560E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.12989303E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     9.91536625E+00   # Gamma(A0)
     2.06163151E-07   2        22        22   # BR(A0 -> photon photon)
     1.81614477E-07   2        22        23   # BR(A0 -> photon Z)
     1.85267301E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.76884567E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.34724262E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.21961737E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.18541382E-13   2        -2         2   # BR(A0 -> Up up)
     2.29395084E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.38080805E-03   2        -6         6   # BR(A0 -> Top top)
     7.08292095E-07   2        -1         1   # BR(A0 -> Down down)
     2.56274868E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.75751549E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.85108276E-05   2        23        25   # BR(A0 -> Z h0)
     6.06308835E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     8.47510881E+00   # Gamma(Hp)
     1.31018202E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.60143691E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.58437139E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.53582096E-07   2        -1         2   # BR(Hp -> Down up)
     1.24360817E-05   2        -3         2   # BR(Hp -> Strange up)
     1.07298101E-05   2        -5         2   # BR(Hp -> Bottom up)
     3.58530394E-08   2        -1         4   # BR(Hp -> Down charm)
     2.71727751E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.50254105E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.29238252E-07   2        -1         6   # BR(Hp -> Down top)
     3.13524509E-06   2        -3         6   # BR(Hp -> Strange top)
     8.39165293E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.59147579E-05   2        24        25   # BR(Hp -> W h0)
     4.16584850E-09   2        24        35   # BR(Hp -> W HH)
     3.71857935E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.14781794E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.19110090E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.19124872E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99875913E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    9.63541805E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    8.39455255E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.14781794E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.19110090E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.19124872E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99995725E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.27529024E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99995725E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.27529024E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.17850378E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.37284452E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.59128213E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.27529024E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99995725E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.54383177E-04   # BR(b -> s gamma)
    2    1.58451897E-06   # BR(b -> s mu+ mu-)
    3    3.51933880E-05   # BR(b -> s nu nu)
    4    3.75221816E-15   # BR(Bd -> e+ e-)
    5    1.60275573E-10   # BR(Bd -> mu+ mu-)
    6    3.26821304E-08   # BR(Bd -> tau+ tau-)
    7    1.25644422E-13   # BR(Bs -> e+ e-)
    8    5.36704878E-09   # BR(Bs -> mu+ mu-)
    9    1.11023701E-06   # BR(Bs -> tau+ tau-)
   10    8.08998693E-05   # BR(B_u -> tau nu)
   11    8.35664677E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42247869E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93123068E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15851333E-03   # epsilon_K
   17    2.28169757E-15   # Delta(M_K)
   18    2.47772442E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28193862E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.40813836E-14   # Delta(g-2)_electron/2
   21   -6.02014135E-10   # Delta(g-2)_muon/2
   22   -2.23343200E-07   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.18471249E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.17442903E-01   # C7
     0305 4322   00   2    -6.23941250E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.52608713E-01   # C8
     0305 6321   00   2    -1.15591881E-03   # C8'
 03051111 4133   00   0     1.58337366E+00   # C9 e+e-
 03051111 4133   00   2     1.58379586E+00   # C9 e+e-
 03051111 4233   00   2    -1.98992587E-04   # C9' e+e-
 03051111 4137   00   0    -4.40606576E+00   # C10 e+e-
 03051111 4137   00   2    -4.39845050E+00   # C10 e+e-
 03051111 4237   00   2     1.57397043E-03   # C10' e+e-
 03051313 4133   00   0     1.58337366E+00   # C9 mu+mu-
 03051313 4133   00   2     1.58379190E+00   # C9 mu+mu-
 03051313 4233   00   2    -1.99055354E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.40606576E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.39845446E+00   # C10 mu+mu-
 03051313 4237   00   2     1.57403138E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50371026E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -3.43697211E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50371026E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -3.43683593E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50371041E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -3.39846011E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85895080E-07   # C7
     0305 4422   00   2    -1.93300639E-05   # C7
     0305 4322   00   2     1.48586470E-05   # C7'
     0305 6421   00   0     3.30544657E-07   # C8
     0305 6421   00   2    -1.01728673E-05   # C8
     0305 6321   00   2     7.45316944E-06   # C8'
 03051111 4133   00   2    -6.54337391E-06   # C9 e+e-
 03051111 4233   00   2     2.45059811E-06   # C9' e+e-
 03051111 4137   00   2     5.09055193E-05   # C10 e+e-
 03051111 4237   00   2    -1.93369004E-05   # C10' e+e-
 03051313 4133   00   2    -6.54325093E-06   # C9 mu+mu-
 03051313 4233   00   2     2.45059066E-06   # C9' mu+mu-
 03051313 4137   00   2     5.09056553E-05   # C10 mu+mu-
 03051313 4237   00   2    -1.93369263E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.06693742E-05   # C11 nu_1 nu_1
 03051212 4237   00   2     4.22247974E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.06693741E-05   # C11 nu_2 nu_2
 03051414 4237   00   2     4.22247974E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.06669680E-05   # C11 nu_3 nu_3
 03051616 4237   00   2     4.22247979E-06   # C11' nu_3 nu_3
