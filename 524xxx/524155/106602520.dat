# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 10.04.2021,  07:55
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.10275335E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    8.11299070E+02  # scale for input parameters
    1   -6.27054374E+02  # M_1
    2    1.15156324E+03  # M_2
    3    2.33731557E+03  # M_3
   11    2.04178530E+03  # A_t
   12   -5.00079135E+02  # A_b
   13    8.30497561E+01  # A_tau
   23    6.16257840E+02  # mu
   25    2.03198872E+01  # tan(beta)
   26    9.65098504E+02  # m_A, pole mass
   31    6.14671690E+02  # M_L11
   32    6.14671690E+02  # M_L22
   33    1.03624306E+03  # M_L33
   34    1.84174424E+03  # M_E11
   35    1.84174424E+03  # M_E22
   36    1.17064282E+03  # M_E33
   41    4.07490943E+03  # M_Q11
   42    4.07490943E+03  # M_Q22
   43    1.05916827E+03  # M_Q33
   44    1.21496264E+03  # M_U11
   45    1.21496264E+03  # M_U22
   46    8.42245995E+02  # M_U33
   47    2.15636400E+03  # M_D11
   48    2.15636400E+03  # M_D22
   49    7.35123888E+02  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  8.11299070E+02  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  8.11299070E+02  # (SUSY scale)
  1  1     8.39451908E-06   # Y_u(Q)^DRbar
  2  2     4.26441569E-03   # Y_c(Q)^DRbar
  3  3     1.01412533E+00   # Y_t(Q)^DRbar
Block Yd Q=  8.11299070E+02  # (SUSY scale)
  1  1     3.42765703E-04   # Y_d(Q)^DRbar
  2  2     6.51254837E-03   # Y_s(Q)^DRbar
  3  3     3.39916171E-01   # Y_b(Q)^DRbar
Block Ye Q=  8.11299070E+02  # (SUSY scale)
  1  1     5.98151217E-05   # Y_e(Q)^DRbar
  2  2     1.23678701E-02   # Y_mu(Q)^DRbar
  3  3     2.08007229E-01   # Y_tau(Q)^DRbar
Block Au Q=  8.11299070E+02  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.04178540E+03   # A_t(Q)^DRbar
Block Ad Q=  8.11299070E+02  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -5.00079082E+02   # A_b(Q)^DRbar
Block Ae Q=  8.11299070E+02  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     8.30497562E+01   # A_tau(Q)^DRbar
Block MSOFT Q=  8.11299070E+02  # soft SUSY breaking masses at Q
   1   -6.27054374E+02  # M_1
   2    1.15156324E+03  # M_2
   3    2.33731557E+03  # M_3
  21    5.75109151E+05  # M^2_(H,d)
  22   -3.83232147E+05  # M^2_(H,u)
  31    6.14671690E+02  # M_(L,11)
  32    6.14671690E+02  # M_(L,22)
  33    1.03624306E+03  # M_(L,33)
  34    1.84174424E+03  # M_(E,11)
  35    1.84174424E+03  # M_(E,22)
  36    1.17064282E+03  # M_(E,33)
  41    4.07490943E+03  # M_(Q,11)
  42    4.07490943E+03  # M_(Q,22)
  43    1.05916827E+03  # M_(Q,33)
  44    1.21496264E+03  # M_(U,11)
  45    1.21496264E+03  # M_(U,22)
  46    8.42245995E+02  # M_(U,33)
  47    2.15636400E+03  # M_(D,11)
  48    2.15636400E+03  # M_(D,22)
  49    7.35123888E+02  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23109541E+02  # h0
        35     9.64787813E+02  # H0
        36     9.65098504E+02  # A0
        37     9.67446369E+02  # H+
   1000001     4.08440934E+03  # ~d_L
   2000001     2.17298808E+03  # ~d_R
   1000002     4.08383005E+03  # ~u_L
   2000002     1.16687228E+03  # ~u_R
   1000003     4.08440993E+03  # ~s_L
   2000003     2.17299086E+03  # ~s_R
   1000004     4.08383068E+03  # ~c_L
   2000004     1.16687488E+03  # ~c_R
   1000005     6.61665959E+02  # ~b_1
   2000005     1.00213031E+03  # ~b_2
   1000006     6.07573188E+02  # ~t_1
   2000006     1.08333645E+03  # ~t_2
   1000011     5.99592205E+02  # ~e_L-
   2000011     1.86095350E+03  # ~e_R-
   1000012     5.94190829E+02  # ~nu_eL
   1000013     5.99592571E+02  # ~mu_L-
   2000013     1.86095120E+03  # ~mu_R-
   1000014     5.94190185E+02  # ~nu_muL
   1000015     1.02816113E+03  # ~tau_1-
   2000015     1.19935857E+03  # ~tau_2-
   1000016     1.02552334E+03  # ~nu_tauL
   1000021     2.38064851E+03  # ~g
   1000022     5.93307441E+02  # ~chi_10
   1000023     6.16569386E+02  # ~chi_20
   1000025     6.54594794E+02  # ~chi_30
   1000035     1.18470977E+03  # ~chi_40
   1000024     6.16779074E+02  # ~chi_1+
   1000037     1.18508852E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.84843080E-02   # alpha
Block Hmix Q=  8.11299070E+02  # Higgs mixing parameters
   1    6.16257840E+02  # mu
   2    2.03198872E+01  # tan[beta](Q)
   3    2.44638854E+02  # v(Q)
   4    9.31415122E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -4.41289704E-01   # Re[R_st(1,1)]
   1  2     8.97364696E-01   # Re[R_st(1,2)]
   2  1    -8.97364696E-01   # Re[R_st(2,1)]
   2  2    -4.41289704E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     4.45758190E-02   # Re[R_sb(1,1)]
   1  2     9.99006004E-01   # Re[R_sb(1,2)]
   2  1    -9.99006004E-01   # Re[R_sb(2,1)]
   2  2     4.45758190E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.98241968E-01   # Re[R_sta(1,1)]
   1  2     5.92703433E-02   # Re[R_sta(1,2)]
   2  1    -5.92703433E-02   # Re[R_sta(2,1)]
   2  2     9.98241968E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     6.74872922E-01   # Re[N(1,1)]
   1  2     2.16361187E-02   # Re[N(1,2)]
   1  3     5.34106622E-01   # Re[N(1,3)]
   1  4     5.08732281E-01   # Re[N(1,4)]
   2  1    -2.65026201E-02   # Re[N(2,1)]
   2  2    -1.01636210E-01   # Re[N(2,2)]
   2  3     7.05341949E-01   # Re[N(2,3)]
   2  4    -7.01042386E-01   # Re[N(2,4)]
   3  1     7.37453864E-01   # Re[N(3,1)]
   3  2    -2.01639890E-02   # Re[N(3,2)]
   3  3    -4.63264385E-01   # Re[N(3,3)]
   3  4    -4.91061424E-01   # Re[N(3,4)]
   4  1    -2.43894717E-03   # Re[N(4,1)]
   4  2     9.94381905E-01   # Re[N(4,2)]
   4  3     5.10779910E-02   # Re[N(4,3)]
   4  4    -9.26807293E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -7.20816037E-02   # Re[U(1,1)]
   1  2     9.97398738E-01   # Re[U(1,2)]
   2  1     9.97398738E-01   # Re[U(2,1)]
   2  2     7.20816037E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.30855503E-01   # Re[V(1,1)]
   1  2     9.91401451E-01   # Re[V(1,2)]
   2  1     9.91401451E-01   # Re[V(2,1)]
   2  2     1.30855503E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     1.67628320E-04   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99954820E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     7.34690801E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     4.67815742E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.08466918E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     5.31473098E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000013     1.68056052E-04   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.99954891E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     7.35612138E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     4.67405471E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.01114438E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     5.30934019E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     6.06837977E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000015     9.47188440E-01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.53256091E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.22610029E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     2.92105886E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.20279931E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     4.36078276E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.89634950E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     6.22290218E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.00916111E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.23506821E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.27816427E-02    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     5.30845074E-03    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     5.60921903E-03    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     2.65170031E-06   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     2.64784122E-06   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     9.35124559E-01   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.45375719E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.50604709E-02    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.90686221E-01    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     4.48877589E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
DECAY   2000001     1.01941472E+00   # ~d_R
#    BR                NDA      ID1      ID2
     4.64215756E-01    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.08807118E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     5.35067732E-01    2     1000025         1   # BR(~d_R -> chi^0_3 d)
DECAY   1000001     1.86373247E+02   # ~d_L
#    BR                NDA      ID1      ID2
     8.97562039E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     8.65240252E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.10130355E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     8.00754816E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.57175508E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.60964625E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.54138611E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     1.02250160E+00   # ~s_R
#    BR                NDA      ID1      ID2
     4.63250231E-01    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.45893089E-03    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     5.33772427E-01    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     1.50801530E-03    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
DECAY   1000003     1.86378061E+02   # ~s_L
#    BR                NDA      ID1      ID2
     9.02577500E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     8.73975741E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.10500966E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     8.00734731E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.64573928E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.60960627E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.54119459E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.64922096E-02   # ~b_1
#    BR                NDA      ID1      ID2
     6.30800960E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.61579260E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     7.61978012E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
DECAY   2000005     3.91689390E+01   # ~b_2
#    BR                NDA      ID1      ID2
     6.96598028E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.15058172E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.49985982E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.63719874E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.03390393E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.76962813E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     6.14844789E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     1.31898384E+00   # ~u_R
#    BR                NDA      ID1      ID2
     4.94692797E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.20961107E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     5.04586241E-01    2     1000025         2   # BR(~u_R -> chi^0_3 u)
DECAY   1000002     1.86358571E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.85876682E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.04187174E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.13132478E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     7.99264970E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.15427953E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.59016379E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.53870881E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     1.31941470E+00   # ~c_R
#    BR                NDA      ID1      ID2
     4.94592542E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     8.03282392E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     5.04440874E-01    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     1.63302158E-04    2     1000024         3   # BR(~c_R -> chi^+_1 s)
DECAY   1000004     1.86363286E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.86067335E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.04555264E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.13312112E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     7.99245560E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.17170797E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.59012478E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.53851757E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.90532228E-08   # ~t_1
#    BR                NDA      ID1      ID2
     9.72203354E-01    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
#    BR                NDA      ID1      ID2       ID3
     1.35558312E-02    3     1000012       -11         5   # BR(~t_1 -> ~nu_e e^+ b)
     1.39733250E-02    3     1000014       -13         5   # BR(~t_1 -> ~nu_mu mu^+ b)
     1.33767651E-04    3    -1000011        12         5   # BR(~t_1 -> ~e^+_L nu_e b)
     1.33722308E-04    3    -1000013        14         5   # BR(~t_1 -> ~mu^+_L nu_mu b)
DECAY   2000006     4.11223169E+01   # ~t_2
#    BR                NDA      ID1      ID2
     3.69059798E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     8.58594806E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     5.12807724E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     8.68222353E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     7.82195914E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.32995236E-04    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     5.00045707E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.31130870E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     9.96642025E-03   # chi^+_1
#    BR                NDA      ID1      ID2
     4.27415041E-03    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     4.27397094E-03    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     2.41152046E-02    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     2.46331969E-02    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     9.41035047E-01    2     1000006        -5   # BR(chi^+_1 -> ~t_1 b_bar)
#    BR                NDA      ID1      ID2       ID3
     5.60430874E-04    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     5.52672488E-04    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.86807819E-04    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.86795522E-04    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.81723918E-04    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     2.56516128E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.11980690E-01    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     1.11980596E-01    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     1.22952526E-02    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     1.12007750E-01    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     1.12008045E-01    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     1.26067158E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.26021886E-01    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     1.05606018E-02    2     2000006        -5   # BR(chi^+_2 -> ~t_2 b_bar)
     1.12215184E-02    2    -2000005         6   # BR(chi^+_2 -> ~b^*_2 t)
     5.75611129E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     9.31903221E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.89311688E-02    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     9.13419475E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     9.45175238E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.71187542E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.83046723E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     8.58856217E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.35846340E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     4.43068964E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.42125718E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     1.21386687E-01    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     1.21386687E-01    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     1.24738285E-01    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     1.24738285E-01    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     1.19305174E-01    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     1.19305174E-01    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     1.19311914E-01    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     1.19311914E-01    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     2.92520068E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     3.44065388E-03    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     3.30900211E-03    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     4.41121760E-03    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     4.41038676E-03    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     3.18689621E-03    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     9.95378588E-04    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     9.95083006E-04    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     9.16686448E-04    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     5.89755502E-03    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     5.19919181E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     1.00342990E-01    2     1000011       -11   # BR(chi^0_3 -> ~e^-_L e^+)
     1.00342990E-01    2    -1000011        11   # BR(chi^0_3 -> ~e^+_L e^-)
     1.00461211E-01    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     1.00461211E-01    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     1.47262986E-01    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     1.47262986E-01    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     1.47265974E-01    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     1.47265974E-01    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     1.64783942E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     3.11137990E-04    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     3.06847225E-04    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     3.98977913E-04    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     3.98951467E-04    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     3.82512178E-04    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     5.29148046E-04    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     1.10120512E-03    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     1.10120512E-03    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     1.09577842E-03    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     1.09577842E-03    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     3.61672961E-04    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     3.61672961E-04    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     3.61668246E-04    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     3.61668246E-04    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     3.64030444E-04    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     3.64030444E-04    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     2.76903963E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     5.13805367E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     5.13805367E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     5.13805538E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     5.13805538E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     5.62138708E-03    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     5.62138708E-03    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     5.22905128E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     5.22905128E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     5.22905890E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     5.22905890E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     5.86597383E-03    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     5.86597383E-03    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     5.20332129E-02    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     5.20332129E-02    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     2.26375053E-02    2     2000005        -5   # BR(chi^0_4 -> ~b_2 b_bar)
     2.26375053E-02    2    -2000005         5   # BR(chi^0_4 -> ~b^*_2 b)
     8.52993172E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     8.52993172E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.90645171E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.86007176E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     3.40705776E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.05071094E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     8.04069579E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     2.77281249E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.03623253E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.30660038E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.60807189E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.60807189E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.01097188E+02   # ~g
#    BR                NDA      ID1      ID2
     6.75454865E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     6.75454865E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     6.75452802E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     6.75452802E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     1.14710726E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.14710726E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     6.27241942E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     6.27241942E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     3.25756742E-03    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     3.25756742E-03    2    -2000001         1   # BR(~g -> ~d^*_R d)
     3.25748387E-03    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     3.25748387E-03    2    -2000003         3   # BR(~g -> ~s^*_R s)
     9.96030997E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     9.96030997E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     7.92511807E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     7.92511807E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     8.78340684E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     9.28022375E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.52155009E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     8.58816239E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     8.58816239E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.12997859E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.12997859E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.63871283E-03   # Gamma(h0)
     2.38564961E-03   2        22        22   # BR(h0 -> photon photon)
     1.34445388E-03   2        22        23   # BR(h0 -> photon Z)
     2.31831542E-02   2        23        23   # BR(h0 -> Z Z)
     1.98922931E-01   2       -24        24   # BR(h0 -> W W)
     6.94455009E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.46267393E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.42988379E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.00118907E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.50003213E-07   2        -2         2   # BR(h0 -> Up up)
     2.91084143E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.31862627E-07   2        -1         1   # BR(h0 -> Down down)
     2.28529696E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.05125700E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.42701390E+00   # Gamma(HH)
     4.44051125E-08   2        22        22   # BR(HH -> photon photon)
     1.05572061E-07   2        22        23   # BR(HH -> photon Z)
     3.88446225E-05   2        23        23   # BR(HH -> Z Z)
     5.42256781E-05   2       -24        24   # BR(HH -> W W)
     1.33218835E-05   2        21        21   # BR(HH -> gluon gluon)
     1.66924607E-08   2       -11        11   # BR(HH -> Electron electron)
     7.42968139E-04   2       -13        13   # BR(HH -> Muon muon)
     2.07553722E-01   2       -15        15   # BR(HH -> Tau tau)
     1.01888913E-12   2        -2         2   # BR(HH -> Up up)
     1.97613332E-07   2        -4         4   # BR(HH -> Charm charm)
     9.28276513E-03   2        -6         6   # BR(HH -> Top top)
     1.20558323E-06   2        -1         1   # BR(HH -> Down down)
     4.35962461E-04   2        -3         3   # BR(HH -> Strange strange)
     7.81284111E-01   2        -5         5   # BR(HH -> Bottom bottom)
     5.92509545E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.36799288E+00   # Gamma(A0)
     1.79119756E-07   2        22        22   # BR(A0 -> photon photon)
     7.15547084E-08   2        22        23   # BR(A0 -> photon Z)
     1.39516529E-04   2        21        21   # BR(A0 -> gluon gluon)
     1.66665686E-08   2       -11        11   # BR(A0 -> Electron electron)
     7.41815394E-04   2       -13        13   # BR(A0 -> Muon muon)
     2.07229036E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.01318865E-12   2        -2         2   # BR(A0 -> Up up)
     1.96484125E-07   2        -4         4   # BR(A0 -> Charm charm)
     1.11535043E-02   2        -6         6   # BR(A0 -> Top top)
     1.20359846E-06   2        -1         1   # BR(A0 -> Down down)
     4.35244938E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.80241206E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.80088768E-05   2        23        25   # BR(A0 -> Z h0)
     5.85129016E-36   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.06510069E+00   # Gamma(Hp)
     1.74017500E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     7.43979145E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     2.10438340E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.21861444E-06   2        -1         2   # BR(Hp -> Down up)
     2.02722357E-05   2        -3         2   # BR(Hp -> Strange up)
     8.46106860E-06   2        -5         2   # BR(Hp -> Bottom up)
     7.05217148E-08   2        -1         4   # BR(Hp -> Down charm)
     4.39359480E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.18484913E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.48796982E-06   2        -1         6   # BR(Hp -> Down top)
     3.30468035E-05   2        -3         6   # BR(Hp -> Strange top)
     7.87065690E-01   2        -5         6   # BR(Hp -> Bottom top)
     6.32078465E-05   2        24        25   # BR(Hp -> W h0)
     4.43425433E-11   2        24        35   # BR(Hp -> W HH)
     2.38290809E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.72199085E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    4.12925617E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    4.12897816E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00006733E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.35457551E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.42190673E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.72199085E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    4.12925617E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    4.12897816E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999525E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.74571543E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999525E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.74571543E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.20254042E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    9.38506723E-04        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.08061356E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.74571543E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999525E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.92286125E-04   # BR(b -> s gamma)
    2    1.58482535E-06   # BR(b -> s mu+ mu-)
    3    3.51718688E-05   # BR(b -> s nu nu)
    4    2.70882100E-15   # BR(Bd -> e+ e-)
    5    1.15717547E-10   # BR(Bd -> mu+ mu-)
    6    2.42091402E-08   # BR(Bd -> tau+ tau-)
    7    9.07402907E-14   # BR(Bs -> e+ e-)
    8    3.87641404E-09   # BR(Bs -> mu+ mu-)
    9    8.21631966E-07   # BR(Bs -> tau+ tau-)
   10    9.35286797E-05   # BR(B_u -> tau nu)
   11    9.66115454E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.45473561E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94779811E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.17609005E-03   # epsilon_K
   17    2.28185371E-15   # Delta(M_K)
   18    2.47604056E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28035212E-11   # BR(K^+ -> pi^+ nu nu)
   20    6.06279940E-15   # Delta(g-2)_electron/2
   21    2.59202196E-10   # Delta(g-2)_muon/2
   22    6.25276598E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    9.12242851E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.64837098E-01   # C7
     0305 4322   00   2    -8.45562525E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.55950822E-01   # C8
     0305 6321   00   2    -8.60313016E-04   # C8'
 03051111 4133   00   0     1.56988856E+00   # C9 e+e-
 03051111 4133   00   2     1.57240335E+00   # C9 e+e-
 03051111 4233   00   2    -6.46628153E-06   # C9' e+e-
 03051111 4137   00   0    -4.39258066E+00   # C10 e+e-
 03051111 4137   00   2    -4.38302491E+00   # C10 e+e-
 03051111 4237   00   2     5.49467041E-05   # C10' e+e-
 03051313 4133   00   0     1.56988856E+00   # C9 mu+mu-
 03051313 4133   00   2     1.57240089E+00   # C9 mu+mu-
 03051313 4233   00   2    -6.46744630E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.39258066E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.38302736E+00   # C10 mu+mu-
 03051313 4237   00   2     5.49475369E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50326368E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -1.20362900E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50326368E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -1.20360484E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50326394E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -1.19680911E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85754097E-07   # C7
     0305 4422   00   2     1.60522615E-05   # C7
     0305 4322   00   2    -3.24290134E-06   # C7'
     0305 6421   00   0     3.30423896E-07   # C8
     0305 6421   00   2    -1.95117367E-06   # C8
     0305 6321   00   2    -1.44211422E-06   # C8'
 03051111 4133   00   2    -1.57679985E-06   # C9 e+e-
 03051111 4233   00   2     8.50796692E-07   # C9' e+e-
 03051111 4137   00   2     1.33466057E-05   # C10 e+e-
 03051111 4237   00   2    -6.81821580E-06   # C10' e+e-
 03051313 4133   00   2    -1.57677649E-06   # C9 mu+mu-
 03051313 4233   00   2     8.50795733E-07   # C9' mu+mu-
 03051313 4137   00   2     1.33466302E-05   # C10 mu+mu-
 03051313 4237   00   2    -6.81822077E-06   # C10' mu+mu-
 03051212 4137   00   2    -2.81058888E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.49340049E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.81058907E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.49340049E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.80607249E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.49339826E-06   # C11' nu_3 nu_3
