##
## 18.4GeV tetrab (4b bound state) scalar --> Upsilon+2mu --> 4mu without jet
##
m4b=18.4
cVV4=0.001
cGG6=0.001
tcVV6=0.0
tcGG6=0.0
evgenConfig.nEventsPerJob = 2000
include("./MGCtrl_Py8EG_A14NNPDF23LO_tetrabNJ0_4mu_3pt2_v2.py")
