###############################################################
# Job options file
# Pythia8 Z' --> A+ph --> 3ph
# April 2019
# Contact: James Beacham -- j.beacham@cern.ch
#===============================================================

evgenConfig.description = "Zprime(200)->A0(50)+ph production"
evgenConfig.keywords = [ "exotic", "Zprime", "3photon" ]
evgenConfig.contact = ["j.beacham@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",
                            "Zprime:gmZmode = 3",
                            "Zprime:universality = off",
                            "Zprime:vt = 0.000000000001", # t and b channels need to stay on,
                            "Zprime:at = 0.000000000001", # so we set their couplings very small
                            "Zprime:vb = 0.000000000001",
                            "Zprime:ab = 0.000000000001",
                            "32:addChannel 1 0.5 101 36 22", # add Z'->A+ph channels
                            "32:addChannel 1 0.5 101 22 36", # add Z'->A+ph channels
                            "32:m0 = 200.0", # set Zprime mass
                            "32:mWidth = 2.0", # Narrow width approximation

                            "32:onIfMatch = 36 22",
                            "32:onIfMatch = 22 36",
                            "32:offIfAny = 1 2 3 4 11 12 13 14 15 16",
                            "36:onMode = off",
                            "36:addChannel 1 1 101 22 22",
                            "36:m0 = 50.0", # set A mass
                            "36:mMin = 0.001",
                            "36:mWidth = 0.001", # Narrow width approximation
                            "36:onIfMatch = 22 22" # decay A --> 2photon
                            ]

#==============================================================
#
# End of job options file
#
###############################################################
