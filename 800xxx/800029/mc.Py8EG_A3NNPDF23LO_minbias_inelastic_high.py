evgenConfig.description = "High-pT inelastic minimum bias events for pile-up, with the A3 NNPDF23LO tune and EvtGen"
evgenConfig.keywords = ["QCD", "minBias", "SM"]
evgenConfig.contact = ['Zach Marshall <ZLMarshall@lbl.gov>','Tommaso Lari <Tommaso.Lari@mi.infn.it>']

evgenConfig.saveJets = True

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
include("Pythia8_i/Pythia8_Base_Fragment.py")

genSeq.Pythia8.Commands += [
    "Tune:ee = 7",
    "Tune:pp = 14",
    "MultipartonInteractions:bProfile = 2",
    "MultipartonInteractions:pT0Ref = 2.45",
    "MultipartonInteractions:ecmPow = 0.21",
    "MultipartonInteractions:coreRadius = 0.55",
    "MultipartonInteractions:coreFraction = 0.9",
    "SigmaDiffractive:PomFlux = 4",
    "SigmaDiffractive:PomFluxEpsilon = 0.07",
    "SigmaDiffractive:PomFluxAlphaPrime = 0.25",
	]

genSeq.Pythia8.Commands += ["PDF:pSet=LHAPDF6:NNPDF23_lo_as_0130_qed",
"ColourReconnection:range = 1.8"]

evgenConfig.tune = "A3 NNPDF23LO"

include("Pythia8_i/Pythia8_EvtGen.py")
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

genSeq.Pythia8.Commands += \
    [
    "SoftQCD:inelastic = on",
    ]

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq,0.6)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)  
from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MinPt = 35.*GeV
filtSeq.QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
filtSeq.QCDTruthJetFilter.DoShape = False

evgenConfig.nEventsPerJob = 1000