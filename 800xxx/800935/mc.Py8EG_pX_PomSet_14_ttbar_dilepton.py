evgenConfig.description = "Pythia8 Pomeron ttbar"
evgenConfig.keywords    = ["SM", "top", "ttbar"]
evgenConfig.contact     = ["jhowrth@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [
	"PDF:PomSet               = 14",
	"PartonLevel:ISR          = off",
	"PartonLevel:FSR          = off",
	"PartonLevel:MPI          = off",
	"PartonLevel:all          = on",
	"PartonLevel:all          = on",
	"SigmaDiffractive:PomFlux = 7",
	"Diffraction:hardDiffSide = 2",
	"Diffraction:doHard       = on",
	"Diffraction:sampleType   = 3",
        "BeamRemnants:unresolvedHadron = 2",
	"Top:gg2ttbar             = on",
	"Top:qqbar2ttbar          = on",
	"Top:ffbar2ttbar(s:gmZ)   = on",
]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------

include("Pythia8_i/Pythia8_ShowerWeights.py")
include('GeneratorFilters/TTbarWToLeptonFilter.py')

filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
