evgenConfig.description = "pp->Upsilon(ee) + Z->ee via second hard process"
evgenConfig.keywords = ["bottomonium","Z","2electron","SM"]
evgenConfig.contact = ["Ketevi Adikle Assamagan <ketevi.adikle.assamagan@cern.ch>"]
evgenConfig.process = "pp -> Upsilon Z -> e+-e-+ e+-e-+"
evgenConfig.minevents = 5000

include('Pythia8B_i/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8B_i/Pythia8B_Bottomonium_Common.py")

genSeq.Pythia8B.Commands += ['553:onMode = off']
genSeq.Pythia8B.Commands += ['553:2:onMode = on']
genSeq.Pythia8B.Commands += ['23:onMode = off']
genSeq.Pythia8B.Commands += ['23:5:onMode = on']
genSeq.Pythia8B.Commands += ['SecondHard:generate = on']
genSeq.Pythia8B.Commands += ['SecondHard:SingleGmZ = on']
genSeq.Pythia8B.SignalPDGCodes = [553,-11,11]

genSeq.Pythia8B.TriggerPDGCode = 11
genSeq.Pythia8B.TriggerStatePtCut = [20.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [1]

