process="VBF"
#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')


#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
if process=="ggH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']
elif process=="VBF":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WpH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WmH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ggZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']


genSeq.Pythia8.Commands += [ 'WeakZ0:gmZmode = 2']
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:addChannel = 1 0.002 100 313 22',
                             '313:mMax = 5' 
                             ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "VBF H->Kstar0Gamma"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H(125)->Kstar0Gamma"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","VBF","photon", "mH125" ]
evgenConfig.contact     = [ 'g.s.virdee@cern.ch' ]
evgenConfig.generators += [ "Powheg", "Pythia8", "EvtGen"]
evgenConfig.inputFilesPerJob = 2 #Specify the number of LHEs files needed 
evgenConfig.nEventsPerJob = 10000
