selected_operators = ['ctGRe','ctWRe','ctBRe','cHQ1','cHQ3','cHt','cQj11','cQj18','cQj31','cQj38','cQQ1','cQQ8','ctu1','ctu8','ctd1','ctd8','cQu1','cQu8','ctj1','ctj8','cQt1','cQt8','cQd1','cQd8','ctGIm','ctWIm','ctBIm']

process_definition = 'set group_subprocesses False\ndefine uc = u c\ndefine uc~ = u~ c~\ndefine ds = d s\ndefine ds~ = d~ s~\n' # define W hadronic decay
process_definition+= 'generate p p > t t~ a QCD=2 QED=3 NP=1, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > uc~ ds NP=0) @0 NPprop=0 SMHLOOP=0\n' # semi-leptonic +
process_definition+= 'add process p p > t t~ a QCD=2 QED=3 NP=1, (t > w+ b NP=0, w+ > uc ds~ NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @1 NPprop=0 SMHLOOP=0\n' # semi-leptonic -
process_definition+= 'add process p p > t t~ a QCD=2 QED=3 NP=1, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @2 NPprop=0 SMHLOOP=0' # di-leptonic

fixed_scale = 431.25 # ~ 2.5*m(top)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tt+photon, production mode, top model, inclusive, reweighted, EFT vertices, no propagator correction'
evgenConfig.nEventsPerJob=5000

include("Common_SMEFTsim_topmW_ttgamma_prod_reweighted.py")
