#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators   += ["MadGraph", "Pythia8"]
evgenConfig.keywords      = ["SM","Higgs"]
evgenConfig.description   = 'ggf H->jj events with mH=3TeV and a 50% width'
evgenConfig.contact	  = ['Dominik Duda <dominik.duda@cern.ch>', 'Marcos Miralles Lopez <marcos.miralles.lopez@cern.ch>']
evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
nevents = evgenConfig.nEventsPerJob*1.3

# Addition of processes to aim for desired event BR
process = """
import model HC_UFO
define p = g u c b d s u~ c~ d~ s~ b~
define j = g u c b d s u~ c~ d~ s~ b~
generate p p > x2 > g g
add process p p > x2 > b b~
add process p p > x2 > b b~
add process p p > x2 > b b~
add process p p > x2 > b b~
add process p p > x2 > c c~
add process p p > x2 > c c~
add process p p > x2 > c c~
add process p p > x2 > c c~
add process p p > x2 > s s~
add process p p > x2 > s s~
add process p p > x2 > d d~
add process p p > x2 > d d~
add process p p > x2 > u u~
add process p p > x2 > u u~
add process p p > x2 > ta+ ta-
add process p p > x2 > ta+ ta-
add process p p > x2 > ta+ ta-
output -f
"""

process_dir = new_process(process)

#Fetch default run_card.dat and set parameters
settings = {'nevents':int(nevents)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

masses    = {'5000002': '3.0000e+03'}
higgsDecay= {'5000002':'DECAY 5000002 1.50e+03 \n'+ \
           ' 0.20 2 21 21 \n'+ \
           ' 0.20 2 5 -5 \n' + \
           ' 0.20 2 4 -4 \n' + \
           ' 0.10 2 3 -3 \n' + \
           ' 0.10 2 2 -2 \n' + \
           ' 0.10 2 1 -1 \n' + \
           ' 0.10 2 15 -15 \n'}

params={}
params['mass']    = masses
params['DECAY']   = higgsDecay

modify_param_card(process_dir=process_dir,params=params)

generate(process_dir=process_dir,runArgs=runArgs,gridpack_compile=True,required_accuracy=0.001)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

