#Contact person : Dominic Hirschbuehl ["dhirsch@mail.cern.ch"]
import shutil

foundInitTag = False
newlhe=open("temp.lhe",'w')
for line in open(runArgs.inputGeneratorFile):
    newline = line                

    if foundInitTag:
        print line
        vals=line.split(" ")[:]
        if len(vals) == 10:
            if vals[8] == "4": vals[8] = "3"
            if vals[8] == "-4": vals[8] = "-3"
            newline = " ".join(vals)
        print newline   
        foundInitTag = False

    if "<init>" in line:                         
        foundInitTag = True


    newlhe.write(newline)
newlhe.close()

shutil.move("temp.lhe",runArgs.inputGeneratorFile)

