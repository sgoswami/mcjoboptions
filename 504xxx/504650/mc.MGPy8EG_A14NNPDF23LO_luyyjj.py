
#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "SM p p > lv j j gam gam, qcd=2 at 13TeV"
evgenConfig.keywords = ["SM","diphoton"]
evgenConfig.contact = ['Shuiting Xin<Shuiting.Xin@cern.ch>']

# one LHE file contains 40000 event
#Filter efficiency is about 70%
evgenConfig.nEventsPerJob = 20000
evgenConfig.inputFilesPerJob = 1
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("LepOneFilter")
filtSeq.LepOneFilter.NLeptons = 1
filtSeq.LepOneFilter.Ptcut = 7000
filtSeq.LepOneFilter.Etacut = 3

filtSeq += MultiLeptonFilter("LepTwoFilter")
filtSeq.LepTwoFilter.NLeptons = 2
filtSeq.LepTwoFilter.Ptcut = 7000
filtSeq.LepTwoFilter.Etacut = 3
filtSeq.Expression = "LepOneFilter and not LepTwoFilter"

