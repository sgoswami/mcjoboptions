evgenConfig.nEventsPerJob=10000

procVVjj="WZvvqq"

safefactor = 4.3
include("MadGraphControl_Pythia8EvtGen_VVjj_EW6_Semi.py")

## B filter/veto
include("GeneratorFilters/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter

filtSeq.Expression = "(not HeavyFlavorBHadronFilter)"
