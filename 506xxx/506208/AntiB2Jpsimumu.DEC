# Close all EvtGen anti-B-baryon decays
# to allow user open only decay(s) for one selected type of B hadron
# other B-hadrons will remain closed and will effect in removing the event
# from beign written into output

# Define qoverp_incohMix_B_s0 0.0
Define dm_incohMix_B_s0 0.0
# Define qoverp_incohMix_B0 0.0
Define dm_incohMix_B0 0.0

#JetSet parameter modifications
#(Very important that there are no blank spaces in the parameter string!)
#Turn of B0-B0B mixing in JetSet:
#JetSetPar MSTJ(26)=0

#
##########################################################################"
#
Decay anti-B0
#
#       B -> cc= s
#
#   Charmonium states - updated from Lange's recommendations (august 23,2000)
# Based on new BABAR results I'm making the following changes (Lange, March 13, 2001
#       J/psi K0   was 10, now 9 x 10^-4
#       J/psi pi0   was 2.5, now 2.0 x 10^-4
#       J/psi Kstar was 15, now 13
#       Fix chic1 KS CP eigenstate
#       adding J/psi rho and omega - for lack of better thing will use the Kstar helicity amplitudes.
#       Psi2sKs  30 ->31
#       chic1 Kstar: 12 ->6
0.000435500 J/psi   K_S0                                    SVS; #[Reconstructed PDG2011]
0.000435500 J/psi   K_L0                                    SVS; #[Reconstructed PDG2011]
#
#
0.001330000 J/psi   anti-K*0                                SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus; #[Reconstructed PDG2011]
0.000017600 J/psi   pi0                                     SVS; #[Reconstructed PDG2011]
0.000027000 J/psi   rho0                                    SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus; #[Reconstructed PDG2011]
0.000030     J/psi  omega             SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus;
0.000000000 J/psi   K-      pi+                             PHSP; #[Reconstructed PDG2011]
0.0001     J/psi  anti-K0   pi0           PHSP;
#rl0.0007     J/psi  anti-K0   pi+  pi-      PHSP;
#rl0.00035     J/psi  anti-K0   pi0  pi0      PHSP;
#rl0.00035     J/psi  K-  pi+  pi0      PHSP;
0.001300000 J/psi   anti-K_10                               SVV_HELAMP 0.5 0.0 1.0 0.0 0.5 0.0; #[Reconstructed PDG2011]
0.0001     J/psi  anti-K'_10             SVV_HELAMP 0.5 0.0 1.0 0.0 0.5 0.0;
0.0005     J/psi  anti-K_2*0              PHSP;
0.000094000 J/psi   phi     anti-K0                         PHSP; #[Reconstructed PDG2011]
#
0.000310000 psi(2S) K_S0                                    SVS; #[Reconstructed PDG2011]
0.000310000 psi(2S) K_L0                                    SVS; #[Reconstructed PDG2011]
#
#
0.000610000 psi(2S) anti-K*0                                SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus; #[Reconstructed PDG2011]

0.0004     psi(2S)  K-  pi+          PHSP;
0.0002     psi(2S)  anti-K0   pi0          PHSP;
0.0002     psi(2S)  anti-K0   pi+  pi-     PHSP;
0.0001     psi(2S)  anti-K0   pi0  pi0     PHSP;
0.0001     psi(2S)  K-  pi+  pi0     PHSP;
0.0004     psi(2S)  anti-K_10              PHSP;
#
0.000445000 eta_c   K_S0                                    PHSP; #[Reconstructed PDG2011]
0.000445000 eta_c   K_L0                                    PHSP; #[Reconstructed PDG2011]
#
0.000610000 anti-K*0 eta_c                                  SVS; #[Reconstructed PDG2011]
0.0002    eta_c  K-  pi+          PHSP;
0.0001    eta_c  anti-K0   pi0          PHSP;
0.0002    eta_c  anti-K0   pi+  pi-     PHSP;
0.0001    eta_c  anti-K0   pi0  pi0     PHSP;
0.0001    eta_c  K-  pi+  pi0     PHSP;
#
0.00024    eta_c(2S) K_S0               PHSP;
0.00024    eta_c(2S) K_L0               PHSP;
#
#
0.00066    anti-K*0 eta_c(2S)          SVS;
0.00008    eta_c(2S)   K-  pi+          PHSP;
0.00005    eta_c(2S)   anti-K0   pi0          PHSP;
0.00008    eta_c(2S)   anti-K0   pi+  pi-     PHSP;
0.00005    eta_c(2S)   anti-K0   pi0  pi0     PHSP;
0.00005    eta_c(2S)   K-  pi+  pi0     PHSP;
#
0.000070000 chi_c0  K_S0                                    PHSP; #[Reconstructed PDG2011]
0.000070000 chi_c0  K_L0                                    PHSP; #[Reconstructed PDG2011]
#
#
0.00030     anti-K*0 chi_c0              SVS;
0.0002     chi_c0  K-  pi+          PHSP;
0.0001     chi_c0  anti-K0   pi0          PHSP;
0.0002     chi_c0  anti-K0   pi+  pi-     PHSP;
0.0001     chi_c0  anti-K0   pi0  pi0     PHSP;
0.0001     chi_c0  K-  pi+  pi0     PHSP;
#
0.000195000 chi_c1  K_S0                                    SVS; #[Reconstructed PDG2011]
0.000195000 chi_c1  K_L0                                    SVS; #[Reconstructed PDG2011]
#
#
0.000222000 chi_c1  anti-K*0                                SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus; #[Reconstructed PDG2011]
0.0004     chi_c1  K-  pi+          PHSP;
0.0002     chi_c1  anti-K0   pi0          PHSP;
0.0004     chi_c1  anti-K0   pi+  pi-     PHSP;
0.0002     chi_c1  anti-K0   pi0  pi0     PHSP;
0.0002     chi_c1  K-  pi+  pi0     PHSP;
#
0.00005     chi_c2 K_S0               STS;
0.00005     chi_c2 K_L0               STS;
#
#
0.00003     chi_c2  anti-K*0         PHSP;
0.0002     chi_c2  K-  pi+          PHSP;
0.0001     chi_c2  anti-K0   pi0          PHSP;
0.0002     chi_c2  anti-K0   pi+  pi-     PHSP;
0.0001    chi_c2  anti-K0   pi0  pi0     PHSP;
0.0001     chi_c2  K-  pi+  pi0     PHSP;
#
0.00024     psi(3770)  K_S0              SVS;
0.00024     psi(3770)  K_L0              SVS;
#
#
0.00048     psi(3770)  anti-K*0            SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus;
0.00014     psi(3770)  K-  pi+                PHSP;
0.00014     psi(3770)  anti-K0   pi0          PHSP;
0.00014     psi(3770)  anti-K0   pi+  pi-     PHSP;
0.00007     psi(3770)  anti-K0   pi0  pi0     PHSP;
0.00007     psi(3770)  K-  pi+  pi0           PHSP;
0.00029     psi(3770)  anti-K_10              PHSP;
#
0.000890000 eta_c   anti-K0                                 PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000871000 J/psi   anti-K0                                 PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000310000 J/psi   omega   anti-K0                         PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000009500 J/psi   eta                                     PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000019000 J/psi   pi-     pi+                             PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000460000 J/psi   anti-K0 pi-     pi+                     PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000540000 J/psi   anti-K0 rho0                            PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000800000 J/psi   K*-     pi+                             PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000660000 J/psi   anti-K*0 pi-     pi+                    PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000620000 psi(2S) anti-K0                                 PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000140000 chi_c0  anti-K0                                 PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000011200 chi_c1  pi0                                     PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000390000 chi_c1  anti-K0                                 PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000158000 chi_c1  K+      pi-                             PHSP;  #[New mode added] #[Reconstructed PDG2011]
#
Enddecay
#
##########################################################################################################
#
Decay B-
#
#   B -> cc= s          sum = 1.92%
#
0.001014000 J/psi   K-                                      SVS; #[Reconstructed PDG2011]
0.001430000 J/psi   K*-                                     SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus; #[Reconstructed PDG2011]
0.000049000 J/psi   pi-                                     SVS; #[Reconstructed PDG2011]
0.000050000 J/psi   rho-                                    SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus; #[Reconstructed PDG2011]
0.0002   J/psi anti-K0   pi-                    PHSP;
0.0001   J/psi K-  pi0                    PHSP;
#rl0.0007   J/psi K-  pi+  pi-               PHSP;
#rl0.00035   J/psi K-  pi0  pi0               PHSP;
#rl0.00035   J/psi anti-K0   pi-  pi0               PHSP;
0.0001   J/psi K'_1-                       SVV_HELAMP 0.5 0.0 1.0 0.0 0.5 0.0;
0.0005   J/psi K_2*-                       PHSP;
0.001800000 J/psi   K_1-                                    SVV_HELAMP 0.5 0.0 1.0 0.0 0.5 0.0; #[Reconstructed PDG2011]
0.000052000 J/psi   phi     K-                              PHSP; #[Reconstructed PDG2011]
#
0.000646000 psi(2S) K-                                      SVS; #[Reconstructed PDG2011]
0.000620000 psi(2S) K*-                                     SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus; #[Reconstructed PDG2011]
0.0004   psi(2S) anti-K0   pi-                   PHSP;
0.0002   psi(2S) K-  pi0                   PHSP;
0.001900000 psi(2S) K-      pi+     pi-                     PHSP; #[Reconstructed PDG2011]
0.0001   psi(2S) K-  pi0  pi0              PHSP;
0.0001   psi(2S) anti-K0   pi-  pi0              PHSP;
0.0004     psi(2S)  K_1-              PHSP;
#
0.000910000 eta_c   K-                                      PHSP; #[Reconstructed PDG2011]
0.001200000 K*-     eta_c                                   SVS; #[Reconstructed PDG2011]
0.0002    eta_c  anti-K0   pi-                 PHSP;
0.0001    eta_c  K-  pi0                 PHSP;
0.0002    eta_c  K-  pi+  pi-            PHSP;
0.0001    eta_c  K-  pi0  pi0            PHSP;
0.0001    eta_c  anti-K0   pi-  pi0            PHSP;
#
0.000340000 eta_c(2S) K-                                    PHSP; #[Reconstructed PDG2011]
0.00048    K*- eta_c(2S)                     SVS;
0.00008    eta_c(2S)   anti-K0   pi-                 PHSP;
0.00005    eta_c(2S)   K-  pi0                 PHSP;
0.00008    eta_c(2S)   K-  pi+  pi-            PHSP;
0.00005    eta_c(2S)   K-  pi0  pi0            PHSP;
0.00005    eta_c(2S)   anti-K0   pi-  pi0            PHSP;
#
0.000133000 chi_c0  K-                                      PHSP; #[Reconstructed PDG2011]
0.0004    K*-   chi_c0                    SVS;
0.0002    chi_c0  anti-K0   pi-                 PHSP;
0.0001    chi_c0  K-  pi0                 PHSP;
0.0002    chi_c0  K-  pi+  pi-            PHSP;
0.0001    chi_c0  K-  pi0  pi0            PHSP;
0.0001    chi_c0  anti-K0   pi-  pi0            PHSP;
#
0.000460000 chi_c1  K-                                      SVS; #[Reconstructed PDG2011]
0.000300000 chi_c1  K*-                                     SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus; #[Reconstructed PDG2011]
0.0004    chi_c1  anti-K0   pi-                 PHSP;
0.0002    chi_c1  K-  pi0                 PHSP;
0.0004    chi_c1  K-  pi+  pi-            PHSP;
0.0002    chi_c1  K-  pi0  pi0            PHSP;
0.0002    chi_c1  anti-K0   pi-  pi0            PHSP;
#
0.00002    chi_c2  K-                      STS;
0.00002    chi_c2  K*-                     PHSP;
0.0002    chi_c2  anti-K0   pi-                 PHSP;
0.0001    chi_c2  K-  pi0                 PHSP;
0.0002    chi_c2  K-  pi+  pi-            PHSP;
0.0001    chi_c2  K-  pi0  pi0            PHSP;
0.0001    chi_c2  anti-K0   pi-  pi0            PHSP;
#
0.000490000 psi(3770) K-                                    SVS; #[Reconstructed PDG2011]
0.0005    psi(3770) K*-                   PHSP;
0.0003    psi(3770) anti-K0   pi-         PHSP;
0.0002    psi(3770) K-  pi0               PHSP;
0.0002    psi(3770) K-  pi+  pi-          PHSP;
0.0001    psi(3770) K-  pi0  pi0          PHSP;
0.0001    psi(3770) anti-K0   pi-  pi0    PHSP;
0.0003    psi(3770) K_1-                  PHSP;
#
0.001070000 J/psi   K-      pi-     pi+                     PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000108000 J/psi   eta     K-                              PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000350000 J/psi   omega   K-                              PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000011800 J/psi   anti-p- Lambda0                         PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000025800 psi(2S) pi-                                     PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.000020000 chi_c1  pi-                                     PHSP;  #[New mode added] #[Reconstructed PDG2011]
#
Enddecay
#
#########################################################################################
#
Decay anti-B_s0
#
#                                        B --> (c c=)  (s s=)
#					2.65%
#					should be: psi = 0.80%  CLNS 94/1315 but isn't quite right.
0.00064   J/psi   eta'               SVS;
0.00032   J/psi   eta                SVS;
# 0.001300000 J/psi   phi                                     SVV_HELAMP  1.0 0.0 1.0 0.0 1.0 0.0; #[Reconstructed PDG2011]
# Update BR to J/psi phi
0.00135   J/psi   phi                SVV_HELAMP  1.0 0.0 1.0 0.0 1.0 0.0;
0.00008   J/psi   K0                 SVS;
0.00070   J/psi   K-       K+        PHSP;
0.00070   J/psi   anti-K0  K0        PHSP;
0.00070   J/psi   anti-K0  K+   pi-  PHSP;
0.00070   J/psi   anti-K0  K0   pi0  PHSP;
0.00070   J/psi   K-       K+   pi0  PHSP;
# LHCb PR 04/02/04 Add (cc) phi n pi(+/0)
0.00039   J/psi   phi      pi+  pi-  PHSP;
0.00039   J/psi   phi      pi0  pi0  PHSP;
# LHCb PR add (cc) phi eta(') + npi see CDF QQ
0.0002    J/psi   eta      pi+  pi-  PHSP;
0.0002    J/psi   eta      pi0  pi0  PHSP;
0.0004    J/psi   eta'     pi+  pi-  PHSP;
0.0004    J/psi   eta'     pi0  pi0  PHSP;
0.0002    J/psi   pi+      pi-       PHSP;
0.0002    J/psi   pi0      pi0       PHSP;
#					psi' = 0.34%  CLNS 94/1315
#
0.000465  psi(2S) eta'               SVS;
0.000235  psi(2S) eta                SVS;
0.000680000 psi(2S) phi                                     SVV_HELAMP  1.0 0.0 1.0 0.0 1.0 0.0; #[Reconstructed PDG2011]
0.0003    psi(2S) K-       K+        PHSP;
0.0003    psi(2S) anti-K0  K0        PHSP;
0.0003    psi(2S) anti-K0  K+    pi- PHSP;
0.0003    psi(2S) anti-K0  K0    pi0 PHSP;
0.0003    psi(2S) K-       K+    pi0 PHSP;
0.00034   psi(2S) phi      pi+  pi-  PHSP;
0.00034   psi(2S) phi      pi0  pi0  PHSP;
0.0002    psi(2S) eta      pi+  pi-  PHSP;
0.0002    psi(2S) eta      pi0  pi0  PHSP;
0.0004    psi(2S) eta'     pi+  pi-  PHSP;
0.0004    psi(2S) eta'     pi0  pi0  PHSP;
0.0002    psi(2S) pi+      pi-       PHSP;
0.0002    psi(2S) pi0      pi0       PHSP;
#
#					chic0 = 0.05% (20% of chic2)
#					Bodwin et.al. Phys Rev D46 1992
0.00010   chi_c0  eta'               PHSP;
0.00005   chi_c0  eta                PHSP;
0.00020   phi     chi_c0             SVS;
0.00003   chi_c0  K-       K+        PHSP;
0.00003   chi_c0  anti-K0  K0        PHSP;
0.00003   chi_c0  anti-K0  K+   pi-  PHSP;
0.00003   chi_c0  anti-K0  K0   pi0  PHSP;
0.00003   chi_c0  K-       K+   pi0  PHSP;
#
#
#					chic1 = 0.37%  CLNS 94/1315
0.0007    chi_c1  eta'		     SVS;
0.0003    chi_c1  eta                SVS;
0.0014    chi_c1  phi                SVV_HELAMP  1.0 0.0 1.0 0.0 1.0 0.0;
0.00026   chi_c1  K-       K+        PHSP;
0.00026   chi_c1  anti-K0  K0        PHSP;
0.00026   chi_c1  anti-K0  K+   pi-  PHSP;
0.00026   chi_c1  anti-K0  K0   pi0  PHSP;
0.00026   chi_c1  K-       K+   pi0  PHSP;
0.00040   chi_c1  phi      pi+  pi-  PHSP;
0.00040   chi_c1  phi      pi0  pi0  PHSP;
0.0001    chi_c1  eta      pi+  pi-  PHSP;
0.0001    chi_c1  eta      pi0  pi0  PHSP;
0.0002    chi_c1  eta'     pi+  pi-  PHSP;
0.0002    chi_c1  eta'     pi0  pi0  PHSP;
#
#
#					chic2 = 0.25%  CLNS 94/1315
0.000465    chi_c2      eta'                  STS;
0.000235    chi_c2      eta                   STS;
#0.0010      chi_c2      phi                   STV; whb: model doesn't exist!
0.00016     chi_c2      K-         K+	      PHSP;
0.00016     chi_c2      anti-K0    K0	      PHSP;
0.00016     chi_c2      anti-K0    K+   pi-   PHSP;
0.00016     chi_c2      anti-K0    K0   pi0   PHSP;
0.00016     chi_c2      K-         K+   pi0   PHSP;
#
#
#                                       etac(1s) = 0.41%  Guess: CBX 97-65
#
0.0008      eta_c       eta'                  PHSP;
0.0004      eta_c       eta                   PHSP;
0.0015      phi         eta_c                 SVS;
0.00028     eta_c       K-         K+         PHSP;
0.00028     eta_c       anti-K0    K0         PHSP;
0.00028     eta_c       anti-K0    K+   pi-   PHSP;
0.00028     eta_c       anti-K0    K0   pi0   PHSP;
0.00028     eta_c       K-         K+   pi0   PHSP;
0.00040   eta_c   phi      pi+  pi-  PHSP;
0.00040   eta_c   phi      pi0  pi0  PHSP;
0.0001    eta_c   eta      pi+  pi-  PHSP;
0.0001    eta_c   eta      pi0  pi0  PHSP;
0.0002    eta_c   eta'     pi+  pi-  PHSP;
0.0002    eta_c   eta'     pi0  pi0  PHSP;
#
#
#                                        etac(2s) = 0.18%  Guess: CBX 97-65
0.0004      eta_c(2S)   eta'                  PHSP;
0.0002      eta_c(2S)   eta                   PHSP;
0.0006      phi         eta_c(2S)             SVS;
0.00012     eta_c(2S)   K-         K+         PHSP;
0.00012     eta_c(2S)   anti-K0    K0         PHSP;
0.00012     eta_c(2S)   anti-K0    K+   pi-   PHSP;
0.00012     eta_c(2S)   anti-K0    K0   pi0   PHSP;
0.00012     eta_c(2S)   K-         K+   pi0   PHSP;
#
#
#                                       hc = 0.25%  (100% of chic2)
#					 Bodwin et.al. Phys Rev D46 1992
#
0.000465    h_c         eta'	              SVS;
0.000235    h_c         eta                   SVS;
0.0010      h_c         phi                   SVV_HELAMP  1.0 0.0 1.0 0.0 1.0 0.0;
0.00016     h_c         K-         K+	      PHSP;
0.00016     h_c         anti-K0    K0         PHSP;
0.00016     h_c         anti-K0    K+   pi-   PHSP;
0.00016     h_c         anti-K0    K0   pi0   PHSP;
0.00016     h_c         K-         K+   pi0   PHSP;
#
#
#
Enddecay

#
#
#   Bottom Baryons
#
Decay Lambda_b0
  0.00047    Lambda0    J/psi                             PHSP;
Enddecay
#CDecay anti-Lambda_b0

Decay Xi_b-
  0.00047    Xi-        J/psi                             PHSP;
Enddecay
#CDecay anti-Xi_b+

Decay Xi_b0
  0.00047    Xi0        J/psi                             PHSP;
Enddecay
#CDecay anti-Xi_b0

#
###################################################################################
###################################################################################
###################################################################################
#
Decay J/psi
1.0000    mu+  mu-               PHOTOS  VLL;
Enddecay
#
End

