evgenConfig.description = 'aMcAtNlo Zee+0,1,2,3j NLO FxFx HT2-biased CVetoBVeto'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch", "jan.kretzschmar@cern.ch"]
evgenConfig.keywords += ['SM', 'Z', 'electron', 'jets', 'NLO']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 29.6%
# CVetoBVeto - eff ~72.5%
# one LHE file contains 50000 events
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO                                                 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)
PYTHIA8_nJetMax=3
PYTHIA8_qCut=20.

include("Pythia8_i/Pythia8_FxFx_A14mod.py")

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
include("GeneratorFilters/BHadronFilter.py")
HeavyFlavorBHadronFilter.BottomEtaMax = 2.9
HeavyFlavorBHadronFilter.BottomPtMin = 5*GeV
HeavyFlavorBHadronFilter.RequireTruthJet = True
HeavyFlavorBHadronFilter.JetPtMin = 10*GeV
HeavyFlavorBHadronFilter.JetEtaMax = 2.9
HeavyFlavorBHadronFilter.TruthContainerName = "AntiKt4TruthJets"
HeavyFlavorBHadronFilter.DeltaRFromTruth = 0.4
filtSeq += HeavyFlavorBHadronFilter 

include("GeneratorFilters/CHadronPt4Eta3_Filter.py")
filtSeq += HeavyFlavorCHadronPt4Eta3_Filter

filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (not HeavyFlavorCHadronPt4Eta3_Filter)"
