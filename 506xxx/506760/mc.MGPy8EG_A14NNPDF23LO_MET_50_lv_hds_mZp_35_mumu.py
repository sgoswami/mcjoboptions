model="LightVector" 
fs = "mumu" 
mDM1 = 17. 
mDM2 = 70. 
mZp = 35. 
mHD = 125. 
filteff = 1.941748e-01 

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MadGraphControl_MGPy8EG_mono_zp_lep.py")