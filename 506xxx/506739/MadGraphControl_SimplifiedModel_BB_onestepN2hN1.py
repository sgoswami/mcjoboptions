include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
splitConfig = get_physics_short().split('_')

masses['1000005'] = float(splitConfig[4])
masses['1000023'] = float(splitConfig[5])
masses['1000022'] = float(splitConfig[6])

if masses['1000022']<0.5: masses['1000022']=0.5
gentype = str(splitConfig[2])
decaytype = str(splitConfig[3])

print masses
print gentype
print decaytype

process = '''
generate p p > b1 b1~ $ go susylq susylq~ b2 t1 t2 b2~ t1~ t2~ @1
add process p p > b1 b1~ j $ go susylq susylq~ b2 t1 t2 b2~ t1~ t2~ @2
add process p p > b1 b1~ j j $ go susylq susylq~ b2 t1 t2 b2~ t1~ t2~ @3
'''
njets = 2
evt_multiplier =450

# set up decays
sbottom_decay = {'1000005':'''DECAY   1000005     1.37684102E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
'''}

neutralino_decay ={'1000023':'''DECAY   1000023     9.37327589E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
'''}
decays.update(sbottom_decay)
decays.update(neutralino_decay)

evgenLog.info('Generation of sbottom pair production, sbottom to b+N2, N2 to h+LSP; mass point ' + str(masses['1000005']) + ' N2 mass '  + str(masses['1000023']) + ' N1 mass '  + str(masses['1000022']))

# Debug the SM higgs mass/branching ratio in the default param_card (which uses the values of 110.8GeV higgs)
masses['25'] = 125.00

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
filtSeq += DirectPhotonFilter()

filtSeq.DirectPhotonFilter.NPhotons = 2

filtSeq.Expression = "DirectPhotonFilter"


evgenConfig.contact  = [ "elodie.deborah.resseguie@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel', 'SUSY', 'sbottom', 'neutralino', '2photon']
evgenConfig.description = 'sbottom direct pair production, sb->b+N2, N2->h+N1 in simplified model, m_sbottom = %s GeV, m_N2 = %s GeV, m_N1 = %s GeV'%(masses['1000005'],masses['1000023'],masses['1000022'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )


if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{b1,1000005}{b1~,-1000005}"]
