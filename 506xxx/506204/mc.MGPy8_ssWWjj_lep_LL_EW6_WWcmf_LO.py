from MadGraphControl.MadGraphUtils import *
import os

# set low_mem_multicore_nlo_generation True

MADGRAPH_CATCH_ERRORS=False

evgenConfig.generators += ["MadGraph", "EvtGen"]
evgenConfig.keywords = ['SM', 'diboson', 'VBS', 'WW', 'SameSign', 'electroweak', '2lepton', '2jet']
evgenConfig.contact = ['Karolos Potamianos <karolos.potamianos@cern.ch>', 'Oleg Kuprash <oleg.kuprash@cern.ch', 'Prasham Jain <prasham.jain@cern.ch>' ]

physShort = get_physics_short()

evgenConfig.nEventsPerJob = 2000
safetyFactor = 1.1
nevents = int(runArgs.maxEvents*safetyFactor if runArgs.maxEvents>0 else safetyFactor*evgenConfig.nEventsPerJob)

required_accuracy = 0.01

isLL = physShort.find("_LL") != -1
isTL = physShort.find("_TL") != -1
# Don't need btoh TL and LT in unordered syntax, but used for debugging studies
isLT = physShort.find("_LT") != -1
isTT = physShort.find("_TT") != -1

is_ssWWjj = physShort.find("_ssWWjj_") != -1
isWWcmFrame = physShort.find("_WWcmf_") != -1

isHad = physShort.find("_had_") != -1
isSemilep = physShort.find("_semilep_") != -1

isEW4 = physShort.find("_EW4") != -1
isEW6 = physShort.find("_EW6") != -1
isINT = physShort.find("_INT") != -1

isLO = physShort.find("_LO") != -1
isNLO = physShort.find("_NLO") != -1

isPythia = physShort.find("Py8") != -1
isHerwig = physShort.find("H7") != -1

# Use dipole shower for LO and Pythia
useDipoleRecoil = isLO and isPythia
# Use dipole shower for LO and Herwig
useDipoleShower = isLO and isHerwig

thisDSID = int(os.path.basename(runArgs.jobConfig[0]))

if isNLO and (useDipoleRecoil or useDipoleShower):
  errMsg = "Cannot use dipole recoil or dipole shower with NLO"
  print(errMsg)
  raise RuntimeError(errMsg)

if isPythia: evgenConfig.generators += ["Pythia8"]
elif isHerwig: evgenConfig.generators += ["Herwig7"]

gridpack_mode=True

# Processes


evgenConfig.description = 'MadGraph_' + physShort

if isEW6:
  if isLO:
    if is_ssWWjj:
      if isSemilep:
        if isLL:
          gen = """
        generate p p > w+{0} w+{0} j j QCD=0, w+ > l+ vl, w+ > j j @0
        add process p p > w-{0} w-{0} j j QCD=0, w- > l- vl~, w- > j j@0
        """
        elif isTT:
          gen = """
        generate p p > w+{T} w+{T} j j QCD=0, w+ > l+ vl , w+ > j j @0
        add process p p > w-{T} w-{T} j j QCD=0, w- > l- vl~ , w- > j j @0
        """
        elif isTL:
          gen = """
        generate p p > w+{T} w+{0} j j QCD=0, w+ > l+ vl , w+ > j j @0
        add process p p > w-{T} w-{0} j j QCD=0, w- > l- vl~ , w- > j j @0
        """
        elif isLT:
          gen = """
        generate p p > w+{0} w+{T} j j QCD=0, w+ > l+ vl , w+ > j j @0
        add process p p > w-{0} w-{T} j j QCD=0, w- > l- vl~ , w- > j j @0
        """
        else:
          gen = """
        generate p p > w+ w+ j j QCD=0, w+ > l+ vl , w+ > j j @0
        add process p p > w- w- j j QCD=0, w- > l- vl~ , w- > j j @0
        """

      if isHad:
        if isLL:
          gen = """
        generate p p > w+{0} w+{0} j j QCD=0, w+ > j j @0
        add process p p > w-{0} w-{0} j j QCD=0, w- > j j @0
        """
        elif isTT:
          gen = """
        generate p p > w+{T} w+{T} j j QCD=0, w+ > j j @0
        add process p p > w-{T} w-{T} j j QCD=0, w- > j j @0
        """
        elif isTL:
          gen = """
        generate p p > w+{T} w+{0} j j QCD=0, w+ > j j @0
        add process p p > w-{T} w-{0} j j QCD=0, w- > j j @0
        """
        elif isLT:
          gen = """
        generate p p > w+{0} w+{T} j j QCD=0, w+ > j j @0
        add process p p > w-{0} w-{T} j j QCD=0, w- > j j @0
        """
        else:
          gen = """
        generate p p > w+ w+ j j QCD=0, w+ > j j @0
        add process p p > w- w- j j QCD=0, w- > j j @0
        """

      else:
        if isLL:
          gen = """
        generate p p > w+{0} w+{0} j j QCD=0, w+ > l+ vl @0
        add process p p > w-{0} w-{0} j j QCD=0, w- > l- vl~ @0
        """
        elif isTT:
          gen = """
        generate p p > w+{T} w+{T} j j QCD=0, w+ > l+ vl @0
        add process p p > w-{T} w-{T} j j QCD=0, w- > l- vl~ @0
        """
        elif isTL:
          gen = """
        generate p p > w+{T} w+{0} j j QED=4 QCD=0, w+ > l+ vl @1
        add process p p > w+{T} w+{0} j j QED=4 QCD=0, w+ > l+ vl, w+ > ta+ vt @1
        add process p p > w+{T} w+{0} j j QED=4 QCD=0, w+ > ta+ vt, w+ > l+ vl @2
        add process p p > w+{T} w+{0} j j QED=4 QCD=0, w+ > ta+ vt @1
        add process p p > w-{T} w-{0} j j QED=4 QCD=0, w- > l- vl~ @1
        add process p p > w-{T} w-{0} j j QED=4 QCD=0, w- > l- vl~, w- > ta- vt~ @1
        add process p p > w-{T} w-{0} j j QED=4 QCD=0, w- > ta- vt~, w- > l- vl~ @2
        add process p p > w-{T} w-{0} j j QED=4 QCD=0, w- > ta- vt~ @1
        """
        elif isLT:
          gen = """
        generate p p > w+{0} w+{T} j j QED=4 QCD=0, w+ > l+ vl @1
        add process p p > w+{0} w+{T} j j QED=4 QCD=0, w+ > l+ vl, w+ > ta+ vt @1
        add process p p > w+{0} w+{T} j j QED=4 QCD=0, w+ > ta+ vt, w+ > l+ vl @2
        add process p p > w+{0} w+{T} j j QED=4 QCD=0, w+ > ta+ vt @1
        add process p p > w-{0} w-{T} j j QED=4 QCD=0, w- > l- vl~ @1
        add process p p > w-{0} w-{T} j j QED=4 QCD=0, w- > l- vl~, w- > ta- vt~ @1
        add process p p > w-{0} w-{T} j j QED=4 QCD=0, w- > ta- vt~, w- > l- vl~ @2
        add process p p > w-{0} w-{T} j j QED=4 QCD=0, w- > ta- vt~ @1
        """
        else:
          gen = """
        generate p p > w+ w+ j j QCD=0, w+ > l+ vl @0
        add process p p > w- w- j j QCD=0, w- > l- vl~ @0
        """

      if not isHad and not isSemilep and (isLT or isTL):
        process = """
      import model sm-no_b_mass
      define l+ = e+ mu+
      define vl = ve vm
      define l- = e- mu-
      define vl~ = ve~ vm~
      define p = g u c d s u~ c~ d~ s~ b b~
      define j = g u c d s u~ c~ d~ s~ b b~
      {}
      output -f""".format(gen)
      else:
        process = """
      import model sm-no_b_mass
      define l+ = e+ mu+ ta+
      define vl = ve vm vt
      define l- = e- mu- ta-
      define vl~ = ve~ vm~ vt~
      define p = g u c d s u~ c~ d~ s~ b b~
      define j = g u c d s u~ c~ d~ s~ b b~
      {}
      output -f""".format(gen)
    else:
      process = """
    import model sm-no_b_mass
    define l+ = e+ mu+ ta+
    define vl = ve vm vt
    define l- = e- mu- ta-
    define vl~ = ve~ vm~ vt~
    define p = g u c d s u~ c~ d~ s~ b b~
    define j = g u c d s u~ c~ d~ s~ b b~
    generate p p > l+ vl l+ vl j j QED==6 QCD=0 @1
    add process p p > l- vl~ l- vl~ j j QED==6 QCD=0 @1
    output -f"""
  elif isNLO:
    process = """
  set complex_mass_scheme
  import model loop_qcd_qed_sm_Gmu-atlas
  define p = g u c d s u~ c~ d~ s~ b b~
  define j = g u c d s u~ c~ d~ s~ b b~
  generate p p > w+ w+  j j QED=4 QCD=0 [QCD] @1
  add process p p > w- w-  j j QED=4 QCD=0 [QCD] @1
  output -f"""

elif isINT:
  if isLO:
    process = """
  import model sm-no_b_mass
  define l+ = e+ mu+ ta+
  define vl = ve vm vt
  define l- = e- mu- ta-
  define vl~ = ve~ vm~ vt~
  define p = g u c d s u~ c~ d~ s~ b b~
  define j = g u c d s u~ c~ d~ s~ b b~
  generate p p > l+ vl l+ vl j j QCD^2==2 @1
  add process p p > l- vl~ l- vl~ j j QCD^2==2 @1
  output -f"""
  elif isNLO:
    process = """
  set complex_mass_scheme
  import model loop_qcd_qed_sm_Gmu-atlas
  define p = g u c d s u~ c~ d~ s~ b b~
  define j = g u c d s u~ c~ d~ s~ b b~
  generate p p > w+ w+ j j QCD^2==2 [QCD] @1
  add process p p > w- w- j j QCD^2==2 [QCD] @1
  output -f"""

elif isEW4:
  if isLO:
    process = """
  import model sm-no_b_mass
  define l+ = e+ mu+ ta+
  define vl = ve vm vt
  define l- = e- mu- ta-
  define vl~ = ve~ vm~ vt~
  define p = g u c d s u~ c~ d~ s~ b b~
  define j = g u c d s u~ c~ d~ s~ b b~
  generate p p > l+ vl l+ vl j j QED=4 QCD=2 @1
  add process p p > l- vl~ l- vl~ j j QED=4 QCD=2 @1
  output -f"""
  elif isNLO:
    process = """
  set complex_mass_scheme
  import model loop_qcd_qed_sm_Gmu-atlas
  define p = g u c d s u~ c~ d~ s~ b b~
  define j = g u c d s u~ c~ d~ s~ b b~
  generate p p > w+ w+ j j QED=2 QCD=2 [QCD] @1
  add process p p > w- w- j j QED=2 QCD=2 [QCD] @1
  output -f"""

#Fetch default NLO run_card.dat and set parameters
extras = { 'lhe_version'  :'3.0',
           'pdlabel'      :"'lhapdf'",
           'lhaid'        : 260000,
           'bwcutoff'     :'15',
           'nevents'      :nevents,
           'dynamical_scale_choice': 0,
           'maxjetflavor': 5, 
           'ptl': 4.0, 
           'ptj': 15.0, 
           'etal': 3.0, 
           'etaj': 5.5, 
           'drll': 0.2, 
           'systematics_program': 'systematics',  
           'systematics_arguments': "['--mur=0.5,1,2', '--muf=0.5,1,2', '--dyn=-1,1,2,3,4', '--weight_info=MUR%(mur).1f_MUF%(muf).1f_PDF%(pdf)i_DYNSCALE%(dyn)i', '--pdf=errorset,NNPDF30_nlo_as_0119@0,NNPDF30_nlo_as_0117@0,CT14nlo@0,MMHT2014nlo68clas118@0,PDF4LHC15_nlo_30_pdfas']"}

process_dir = new_process(process)

if not is_gen_from_gridpack(): # When generating the gridpack
  # Todo: replace in line to be safer
  os.system("cp collect_events.f "+process_dir+"/SubProcesses")


if isLO:
  if not is_gen_from_gridpack(): # When generating the gridpack
    os.system("cp setscales_lo.f  "+process_dir+"/SubProcesses/setscales.f")
  lo_extras = { 'asrwgtflavor': 5, 
                'auto_ptj_mjj': False, 
                'cut_decays': True, 
                'ptb': 15.0, 
                'etab': 5.5, 
                'dral': 0.1, 
                'drbb': 0.2, 
                'drbj': 0.2, 
                'drbl': 0.2, 
                'drjj': 0.2,
                'drjl': 0.2, 
                'mmll': 0.0, 
                'gridpack': '.true.',
                'use_syst': True, 
              }
  extras.update(lo_extras)

if isWWcmFrame:
  lo_extras = { 'me_frame' : '3, 4, 5, 6' }
  extras.update(lo_extras)

elif isNLO:
  if not is_gen_from_gridpack(): # When generating the gridpack
    # See https://answers.launchpad.net/mg5amcnlo/+faq/2720
    # Todo: replace in line to be safer
    os.system("cp FKS_params.dat "+process_dir+"/Cards/")
    os.system("cp setscales_nlo.f  "+process_dir+"/SubProcesses/setscales.f")

  nlo_extras = { 'lhe_version'   :'3.0',
           'pdlabel'      : "'lhapdf'",
           'lhaid'         : 260000,
           'parton_shower' : 'PYTHIA8' if isPythia else 'HERWIGPP',
           'jetalgo'       : -1,
           'jetradius'     : 0.4,
           #'ptj'           : 20.0,
           #'etaj'          : -1,
           'ickkw'         : 0,
           'store_rwgt_info' : '.true.',
           'event_norm' : 'sum',
           'reweight_scale': '.true.',
           'reweight_PDF'  : '.true.',
           'dynamical_scale_choice': 10  
         }
  extras.update(nlo_extras)

  required_accuracy = 0.001

  if not is_gen_from_gridpack(): # When generating the gridpack
    madspin_card = process_dir+"/Cards/madspin_card.dat"
    # If one wants to use runName one needs to change MADGRAPH_RUN_NAME anyhow (tbc)
    ms_import = process_dir+"/Events/"+MADGRAPH_RUN_NAME+"/events.lhe"
    ms_dir = "MadSpin"
  else:
    gridpack_dir = "madevent/"
    madspin_card = gridpack_dir+"Cards/madspin_card.dat"
    # If one wants to use runName one needs to change MADGRAPH_RUN_NAME anyhow (tbc)
    ms_import = gridpack_dir+"Events/"+MADGRAPH_RUN_NAME+"/events.lhe"
    ms_dir = gridpack_dir+"MadSpin"

  if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
  mscard = open(madspin_card,'w')  
  mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
import %s
set ms_dir %s
set seed %i
# specify the decay for the final state particles
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define v = ve vm vt
define v~ = ve~ vm~ vt~
decay w+ > l+ v                                                                   
decay w- > l- v~          
# running the actual code
launch"""%(ms_import, ms_dir, 10000000+int(runArgs.randomSeed)))
  mscard.close()

modify_config_card(process_dir=process_dir,settings={'cluster_type':'condor'})
modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=extras)
#modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs,required_accuracy=required_accuracy)
arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=True)

#### Shower                    

if isPythia:
  include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
  if isNLO: include("Pythia8_i/Pythia8_aMcAtNlo.py")
  # fix for VBS processes (requires version>=8.230)
  genSeq.Pythia8.Commands += [ "SpaceShower:pTmaxMatch=1",
                               "SpaceShower:pTmaxFudge=1",
                               "SpaceShower:MEcorrections=off",
                               "TimeShower:pTmaxMatch=1",
                               "TimeShower:pTmaxFudge=1",
                               "TimeShower:MEcorrections=off",
                               "TimeShower:globalRecoil=on",
                               "TimeShower:limitPTmaxGlobal=on",
                               "TimeShower:nMaxGlobalRecoil=1",
                               "TimeShower:globalRecoilMode=2",
                               "TimeShower:nMaxGlobalBranch=1",
                               "TimeShower:weightGluonToQuark=1",
                               "Check:epTolErr=1e-2"]

  if useDipoleRecoil:
    genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]


  include("Pythia8_i/Pythia8_MadGraph.py")
  include("Pythia8_i/Pythia8_ShowerWeights.py")

elif isHerwig:
  from Herwig7_i.Herwig7_iConf import Herwig7
  from Herwig7_i.Herwig72ConfigLHEF import Hw72ConfigLHEF

  genSeq += Herwig7()
  Herwig7Config = Hw72ConfigLHEF(genSeq, runArgs)

# configure Herwig7
  Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
  Herwig7Config.tune_commands()
  lhe_filename = runArgs.outputTXTFile.split('.tar.gz')[0]+'.events'
  Herwig7Config.lhef_mg5amc_commands(lhe_filename=lhe_filename, me_pdf_order="NLO")
  
  if useDipoleShower:
     dipoleShowerCommands = """
     cd /Herwig/EventHandlers
     set EventHandler:CascadeHandler /Herwig/DipoleShower/DipoleShowerHandler
     cd /Herwig/DipoleShower
     do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=2.0 2.0 2.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=1.0 2.0 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=0.5 2.0 0.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=2.0" 1.0 2.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.5" 1.0 0.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=2.0" 0.5 2.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=1.0 0.5 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=0.5 0.5 0.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.75_fsr:muRfac=1.0 1.75 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.5_fsr:muRfac=1.0 1.5 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.25_fsr:muRfac=1.0 1.25 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.625_fsr:muRfac=1.0" 0.625 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.75_fsr:muRfac=1.0 0.75 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.875_fsr:muRfac=1.0 0.875 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.75 1.0 1.75 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.5 1.0 1.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.25 1.0 1.25 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.625 1.0 0.625 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.75 1.0 0.75 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.875 1.0 0.85 All
     """
     print(dipoleShowerCommands)
     Herwig7Config.add_commands(dipoleShowerCommands)
  
# add EvtGen
  include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
  Herwig7Config.run()
