evgenConfig.description = "Single upsilon(3s) with zero pt and gaussian rapidity distribution"
evgenConfig.keywords = ["singleParticle", "upsilon"]
evgenConfig.contact = ["Mateusz.Dyndal@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ['ParticleGun']
genSeq.ParticleGun.sampler.pid = 200553
genSeq.ParticleGun.sampler.mom = PG.PtRapMPhiSampler(pt=0., rap=PG.GaussianSampler(0., 1.2), mass=10355.)


include("EvtGen_i/EvtGen_Fragment.py")
evgenConfig.auxfiles+=['inclusive.pdt']
genSeq.EvtInclusiveDecay.allowAllKnownDecays=True


if not hasattr(filtSeq, "ChargedTracksFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
  pt3Filter = ChargedTracksFilter("pt3Filter")
  filtSeq += pt3Filter

filtSeq.pt3Filter.Ptcut = 3000. #MeV
filtSeq.pt3Filter.Etacut = 2.6
filtSeq.pt3Filter.NTracks = 1
