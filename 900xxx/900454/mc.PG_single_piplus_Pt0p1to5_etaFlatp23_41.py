evgenConfig.description = "Single pi+ with pt in [0.1-5] GeV, flat eta (between 2.3 and 4.1), and flat phi"
evgenConfig.keywords = ["singleParticle", "pi+"]
evgenConfig.contact = ["lderamo@cern.ch"]
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ["ParticleGun"]

genSeq.ParticleGun.sampler.pid = 211
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[100.0, 5000.0], eta=[2.3, 4.1])
