evgenConfig.description = "Hard coherent diffraction"
evgenConfig.keywords = ["QCD","coherent"]
evgenConfig.tune = "none"
evgenConfig.contact  = ["angerami@cern.ch"]
evgenConfig.nEventsPerJob = 1000

from TruthIO.TruthIOConf import HepMCReadFromFile
genSeq += HepMCReadFromFile()
genSeq.HepMCReadFromFile.InputFile="events.hepmc"
evgenConfig.generators += ["HepMCAscii"]

include("EvtGen_i/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
genSeq.EvtInclusiveDecay.whiteList+=[-5334, 5334]
