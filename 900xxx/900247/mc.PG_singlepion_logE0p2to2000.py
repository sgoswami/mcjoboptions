evgenConfig.description = "Single charged pion with log energy distribution"
evgenConfig.keywords = ["singleParticle", "pi+","pi-"]
evgenConfig.contact = ["angerami@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ['ParticleGun']
genSeq.ParticleGun.sampler.pid = (-211,211)
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(200, 2000000.), eta=[-3.0, 3.0])

