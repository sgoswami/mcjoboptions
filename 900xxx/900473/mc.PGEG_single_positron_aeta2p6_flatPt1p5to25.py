evgenConfig.description = "Single photon with flat 1.5 < pt < 25 GeV and eta distributions, |eta| < 2.6"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact = ["ewelina.maria.lobodzinska@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ['ParticleGun']
genSeq.ParticleGun.sampler.pid = (-11)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=PG.UniformSampler(1500., 25000.), eta=[-2.6, 2.6])

include("EvtGen_i/EvtGen_Fragment.py")
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles+=['inclusive.pdt']
genSeq.EvtInclusiveDecay.allowAllKnownDecays=True

