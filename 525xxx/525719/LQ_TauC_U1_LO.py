import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
import re
import os
import math
import shutil
import subprocess

from MadGraphControl.MadGraphUtils import *


safefactor=1.1
nevents=runArgs.maxEvents*safefactor

#nevents = 5000

#job_option_name = runArgs.jobConfig[0]
THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
job_option_name = [ f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]
print(job_option_name)

matches = re.search("M([0-9]+).*\.py", job_option_name)
if matches is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    lqmass = float(matches.group(1))

my_process = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model sm
import model vector_LQ_UFO
define p = g u c d s b u~ c~ d~ s~ b~
generate p p >  vt vt~ c
add process p p > vt~ vt c~
output -f
"""

beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(my_process)

run_settings = {
          'lhe_version':'3.0',
          'ickkw' : 0,
		  'nevents' : int(nevents), 
          'ktdurham': lqmass * 0.25,
    }

modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=run_settings)

vLQ_Mass={
    '9000007' : '{:e}' .format(lqmass),
    }

params = {}

params['MASS'] = vLQ_Mass

modify_param_card(process_dir=process_dir, params=params)

print_cards()

pdgfile = open("pdgid_extras.txt" , "w+")
pdgfile.write("""9000007
    -9000007
""")
pdgfile.close()

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs)

evgenConfig.description = 'Vector LO single production of U1, mLQ={0:d}'.format(int(lqmass))
evgenConfig.keywords += ['BSM', 'exotic', 'leptoquark']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> vt vt~ c~'
evgenConfig.contact = ["Bingxuan Liu <bingxuan.liu@cern.ch>"] 

###Pythia8 commands
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

