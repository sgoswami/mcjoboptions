include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

jobConfigParts = JOName.split("_")

mn1=float(jobConfigParts[4])
mx1=float(jobConfigParts[5])

masses['1000022'] = mn1 # N1 mass
masses['1000024'] = mx1 # C1 mass
# N1: 1000022 ;  N2: 1000023 ; C1: 1000024 ; C2: 1000037

chargino_decay = {'1000024':'''DECAY   1000024    1.70414503E-02   # chargino1+ decays
#          BR        NDA      ID1       ID2      ID3  
        0.0000000000E+00    2    23    -11    # BR(C1 -> Z e+)
        0.0000000000E+00    2    23    -13    # BR(C1 -> Z mu+)
        0.0000000000E+00    2    23    -15    # BR(C1 -> Z tau+)
        3.3333333333E-01    2    25    -11    # BR(C1 -> H e+)
        3.3333333333E-01    2    25    -13    # BR(C1 -> H mu+)
        3.3333333334E-01    2    25    -15    # BR(C1 -> H tau+)
        0.0000000000E+00    2    24    -12    # BR(C1 -> W+ nu_ebar)
        0.0000000000E+00    2    24    -14    # BR(C1 -> W+ nu_mubar)
        0.0000000000E+00    2    24    -16    # BR(C1 -> W+ nu_taubar)
#
  '''}
decays.update(chargino_decay)

neutralino_decay = {'1000022':'''DECAY   1000022    1.70414503E-02   # neutralino1 decays
#          BR        NDA      ID1       ID2      ID3  
     0.0000000E+00    2       15        24    # BR(N1 -> tau- W+)
     0.0000000E+00    2      -15       -24    # BR(N1 -> tau+ W-)
     0.0000000E+00    2       11        24    # BR(N1 -> e-   W+)
     0.0000000E+00    2      -11       -24    # BR(N1 -> e+   W-)
     0.0000000E+00    2       13        24    # BR(N1 -> mu-  W+)
     0.0000000E+00    2      -13       -24    # BR(N1 -> mu+  W-)
     0.0000000E+00    2       23        12    # BR(N1 -> Z nu_e)
     0.0000000E+00    2       23       -12    # BR(N1 -> Z nu_ebar)
     0.0000000E+00    2       23        14    # BR(N1 -> Z nu_mu)
     0.0000000E+00    2       23       -14    # BR(N1 -> Z nu_mubar)
     0.0000000E+00    2       23        16    # BR(N1 -> Z nu_tau)
     0.0000000E+00    2       23       -16    # BR(N1 -> Z nu_taubar)
     1.6666666E-01    2       25        12    # BR(N1 -> H nu_e)
     1.6666667E-01    2       25       -12    # BR(N1 -> H nu_ebar)
     1.6666666E-01    2       25        14    # BR(N1 -> H nu_mu)
     1.6666667E-01    2       25       -14    # BR(N1 -> H nu_mubar)
     1.6666667E-01    2       25        16    # BR(N1 -> H nu_tau)
     1.6666667E-01    2       25       -16    # BR(N1 -> H nu_taubar)
  '''}
decays.update(neutralino_decay)

# Debug the SM higgs mass/branching ratio in the default param_card
# (which uses the values of 110.8GeV higgs)
masses['25'] = 125.00
higgs_decay = {'25':'''DECAY   25     4.06911399E-03   # higgs decays
#          BR         NDA          ID1       ID2       ID3       ID4
        0.00000000E+00    2          15       -15   # BR(H1 -> tau- tau+)
        1.00000000E+00    2           5        -5   # BR(H1 -> b bbar)
        0.00000000E+00    2          24       -24   # BR(H1 -> W+ W-)
        0.00000000E+00    2          23        23   # BR(H1 -> Z Z)
        0.00000000E+00    2          23        22   # BR(H1 -> Z gamma)  
        0.00000000E+00    2          22        22   # BR(H1 -> gamma gamma)   
        0.00000000E+00    2          13       -13   # BR(H1 -> mu mu)         
        0.00000000E+00    2           4        -4   # BR(H1 -> c cbar)       
        0.00000000E+00    2           3        -3   # BR(H1 -> s sbar)     
        0.00000000E+00    2          21        21   # BR(H1 -> g g)
#
  '''}
decays.update(higgs_decay)

# Overwrite neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
param_blocks['NMIX']={}
param_blocks['NMIX']['1  1']=' 0.00E+00'   # N_11
param_blocks['NMIX']['1  2']=' 1.00E+00'   # N_12 wino
param_blocks['NMIX']['1  3']=' 0.00E+00'   # N_13
param_blocks['NMIX']['1  4']=' 0.00E+00'   # N_14
param_blocks['NMIX']['2  1']='-1.00E+00'   # N_21 bino
param_blocks['NMIX']['2  2']=' 0.00E+00'   # N_22
param_blocks['NMIX']['2  3']=' 0.00E+00'   # N_23
param_blocks['NMIX']['2  4']=' 0.00E+00'   # N_24
param_blocks['NMIX']['3  1']=' 0.00E+00'   # N_31
param_blocks['NMIX']['3  2']=' 0.00E+00'   # N_32
param_blocks['NMIX']['3  3']=' 7.07E-01'   # N_33 higgsino
param_blocks['NMIX']['3  4']='-7.07E-01'   # N_34 higgsino
param_blocks['NMIX']['4  1']=' 0.00E+00'   # N_41
param_blocks['NMIX']['4  2']=' 0.00E+00'   # N_42
param_blocks['NMIX']['4  3']='-7.07E-01'   # N_43 higgsino
param_blocks['NMIX']['4  4']='-7.07E-01'   # N_44 higgsino

# Enforce 5FS
masses['5'] = 0.00 # https://answers.launchpad.net/mg5amcnlo/+question/294747

gentype = str(jobConfigParts[2])
decaytype = str(jobConfigParts[3])
flavourScheme = 5

print("gentype", gentype)
print("decaytype", decaytype)
print("decays", decays)
print("masses", masses)
print("flavourScheme", flavourScheme)

process = '''
   import model RPVMSSM_UFO
   define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~\
   dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
   define p = g u c d s b u~ c~ d~ s~ b~
   define j = g u c d s b u~ c~ d~ s~ b~
   generate p p > x1+ n1     QED=2 RPV=0 / susystrong @1
add process p p > x1- n1     QED=2 RPV=0 / susystrong @1
add process p p > x1+ n1 j   QED=2 RPV=0 / susystrong @2
add process p p > x1- n1 j   QED=2 RPV=0 / susystrong @2
add process p p > x1+ n1 j j QED=2 RPV=0 / susystrong @3
add process p p > x1- n1 j j QED=2 RPV=0 / susystrong @3
'''

njets = 2
evgenLog.info(
  'Registered generation ~chi1+/- ~chi10 production, decay via Hl and Hv; grid point '
  + str(runArgs.jobConfig[0].split('/')[-1])
)

evgenConfig.contact  = [ "robert.mcgovern@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','SUSY','chargino','neutralino','RPV']
evgenConfig.description = (
  '~chi1+/- ~chi10 production, decay via Hl and Hv (all RPV) in simplified model, '
  'm_N1 = %s GeV, m_C1 = %s GeV' % (masses['1000022'], masses['1000024'])
)

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{x1+,1000024}{n1,1000022}",
                              "1000024:spinType = 1","1000022:spinType = 1"]
