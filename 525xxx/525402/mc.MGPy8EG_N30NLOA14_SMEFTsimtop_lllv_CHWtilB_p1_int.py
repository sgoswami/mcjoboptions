import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
}
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl.MadGraphUtils import check_reset_proc_number
#include('MadGraphControl/setupEFT.py')


# create dictionary of processes this JO can create
process="""
import model SMEFTsim_top_MwScheme_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = p
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm
define vl~ = ve~ vm~
define W = W+ W-
generate p p > l+ l- l+ vl NP^2==1
add process p p > l- l+ l- vl~ NP^2==1
output -f"""

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
    
# define cuts
settings={
    'ptl':'10', #min charged lepton pt
    'etal':'2.7', #max charged lepton abs eta
    'drll':'0.2', #min dr charged leptons
    'nevents' :int(nevents),
}

params=dict()
params['SMEFTcpv']=dict()
params['SMEFTcpv']['6']=1.0

process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=params)

print_cards()

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

# add meta data
evgenConfig.description = 'lllv production with SMEFTSIM'
evgenConfig.keywords+=['diboson']
evgenConfig.contact = ['Juan Rivera <juan.cristobal.rivera.vergara@cern.ch>']

check_reset_proc_number(opts)

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# turn off mpi for faster generation and smaller files in 'noMPI' is part of name
if 'noMPI' in get_physics_short():
    genSeq.Pythia8.Commands += [' PartonLevel:MPI = off']

evgenConfig.nEventsPerJob = 100000

