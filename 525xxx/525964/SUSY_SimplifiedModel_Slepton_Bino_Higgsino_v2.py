
#--------------------------------------------------------------
# Standard pre-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

#--------------------------------------------------------------
# Some options for local testing.  Shouldn't hurt anything in production.
#
# Nominal configuration for production: MadSpin decays, with 0,1,2+ partom emissions in the matrix element
#
#--------------------------------------------------------------
# Interpret the name of the JO file to figure out the mass spectrum
#
def MassToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)

#--------------------------------------------------------------
# split up the JO file input name to interpret it
# e.g. jofile: mc.MGPy8EG_WLH_C2C2_llll_850_180_170_MadSpin.py
splitConfig = jofile.rstrip('.py').split('_')

#slepton/sneutrino degenerate
masses['1000011'] = MassToFloat(splitConfig[3])
masses['1000012'] = MassToFloat(splitConfig[3])
masses['1000013'] = MassToFloat(splitConfig[3])
masses['1000014'] = MassToFloat(splitConfig[3])

#N3
masses['1000025'] = MassToFloat(splitConfig[4])

#N1/C1/N2
masses['1000023'] = - MassToFloat(splitConfig[5]) - 0.5
masses['1000024'] = MassToFloat(splitConfig[5]) + 0.5
masses['1000022'] = MassToFloat(splitConfig[5])

deltam_SLN3 = MassToFloat(splitConfig[3]) - MassToFloat(splitConfig[4])
deltam_N3N1 = MassToFloat(splitConfig[4]) - MassToFloat(splitConfig[5])
deltam_SLN1 = deltam_SLN3 + deltam_N3N1

# interpret the generation type.
gentype = splitConfig[2]

if deltam_N3N1 >= 125:
  evgenLog.info('Force Br(N3->WN1)=50% Br(N3->ZC1)=Br(N3->hN1)=25%')
  decays = {'1000011':'''DECAY   1000011     1.498816e-01   # selectron decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        11   # BR(selectron- -> ~chi_30   e- )
#
       ''',
          '1000012':'''DECAY   1000012     1.498816e-01   # electron sneutrino decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        12   # BR(electron neutrino -> ~chi_30   eneu )
#
       ''',
          '1000013':'''DECAY   1000013     1.498816e-01   # smuon decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        13   # BR(smuon- -> ~chi_30   mu- )
#
       ''',
          '1000014':'''DECAY   1000014     1.498816e-01   # muon sneutrino decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        14   # BR(muon sneutrino -> ~chi_30   muneu )
#
       ''',
          '1000025':'''DECAY   1000025      2.136822e-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     0.12500000E+00    2     1000022        23   # BR(~chi30 -> ~chi_10   Z )
     0.12500000E+00    2     1000022        25   # BR(~chi30 -> ~chi_10   h )
     0.12500000E+00    2     1000023        23   # BR(~chi30 -> ~chi_20   Z )
     0.12500000E+00    2     1000023        25   # BR(~chi30 -> ~chi_20   h )
     0.25000000E+00    2     1000024        -24   # BR(~chi30 -> ~chi_1+   W- )
     0.25000000E+00    2     -1000024        24   # BR(~chi30 -> ~chi_1-   W+ )
       ''',
          '1000023':'''DECAY   1000023     1.704145e-02   # neutralino2 decays
#          BR         NDA      ID1       ID2       ID3
     1.00000000E+00    2     1000022        23   # BR(~chi20 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        -11        11  # BR(~chi20 -> ~chi_10   e+ e- ), dummy on-shell decay to force Pythia to decay off-shell
#
       ''',
          '1000024':'''DECAY   1000024     1.704145e-02   # chargino1 decays
#          BR         NDA      ID1       ID2       ID3
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10 w+)
     0.00000000E+00    3     1000022       -11        12  # BR(~chi_1+ -> ~chi_10 e+ nu_e), dummy on-shell decay to force Pythia to decay off-shell
#
       '''}
elif deltam_N3N1 >= 90:
  evgenLog.info('Force Br(N3->WN1)=50% Br(N3->ZC1)=50%')
  decays = {'1000011':'''DECAY   1000011     1.498816e-01   # selectron decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        11   # BR(selectron- -> ~chi_30   e- )
#
       ''',
          '1000012':'''DECAY   1000012     1.498816e-01   # electron sneutrino decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        12   # BR(electron neutrino -> ~chi_30   eneu )
#
       ''',
          '1000013':'''DECAY   1000013     1.498816e-01   # smuon decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        13   # BR(smuon- -> ~chi_30   mu- )
#
       ''',
          '1000014':'''DECAY   1000014     1.498816e-01   # muon sneutrino decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        14   # BR(muon sneutrino -> ~chi_30   muneu )
#
       ''',
          '1000025':'''DECAY   1000025      2.136822e-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     0.25000000E+00    2     1000022        23   # BR(~chi30 -> ~chi_10   Z )
     0.25000000E+00    2     1000023        23   # BR(~chi30 -> ~chi_20   Z )
     0.25000000E+00    2     1000024        -24   # BR(~chi30 -> ~chi_1+   W- )
     0.25000000E+00    2     -1000024        24   # BR(~chi30 -> ~chi_1-   W+ )
       ''',
          '1000023':'''DECAY   1000023     1.704145e-02   # neutralino2 decays
#          BR         NDA      ID1       ID2       ID3
     1.00000000E+00    2     1000022        23   # BR(~chi20 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        -11        11  # BR(~chi20 -> ~chi_10   e+ e- ), dummy on-shell decay to force Pythia to decay off-shell
#
       ''',
          '1000024':'''DECAY   1000024     1.704145e-02   # chargino1 decays
#          BR         NDA      ID1       ID2       ID3
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10 w+)
     0.00000000E+00    3     1000022       -11        12  # BR(~chi_1+ -> ~chi_10 e+ nu_e), dummy on-shell decay to force Pythia to decay off-shell
#
       '''}
else:
  evgenLog.info('Force offshell Br(N3->WN1)=50% Br(N3->ZC1)=50%')
  decays = {'1000011':'''DECAY   1000011     1.498816e-01   # selectron decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        11   # BR(selectron- -> ~chi_30   e- )
#
       ''',
          '1000012':'''DECAY   1000012     1.498816e-01   # electron sneutrino decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        12   # BR(electron neutrino -> ~chi_30   eneu )
#
       ''',
          '1000013':'''DECAY   1000013     1.498816e-01   # smuon decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        13   # BR(smuon- -> ~chi_30   mu- )
#
       ''',
          '1000014':'''DECAY   1000014     1.498816e-01   # muon sneutrino decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000025        14   # BR(muon sneutrino -> ~chi_30   muneu )
#
       ''',
          '1000025':'''DECAY   1000025      2.136822e-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     0.25000000E+00    2     1000022        23   # BR(~chi30 -> ~chi_10   Z )
     0.25000000E+00    2     1000023        23   # BR(~chi30 -> ~chi_20   Z )
     0.25000000E+00    2     1000024        -24   # BR(~chi30 -> ~chi_1+   W- )
     0.25000000E+00    2     -1000024        24   # BR(~chi30 -> ~chi_1-   W+ )
     0.00000000E+00    3     1000022       -11        11  # BR(~chi_30 -> ~chi_10 e+ e-), dummy on-shell decay to force Pythia to decay off-shell
       ''',
          '1000023':'''DECAY   1000023     1.704145e-02   # neutralino2 decays
#          BR         NDA      ID1       ID2       ID3
     1.00000000E+00    2     1000022        23   # BR(~chi20 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        -11        11  # BR(~chi20 -> ~chi_10   e+ e- ), dummy on-shell decay to force Pythia to decay off-shell
#
       ''',
          '1000024':'''DECAY   1000024     1.704145e-02   # chargino1 decays
#          BR         NDA      ID1       ID2       ID3
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10 w+)
     0.00000000E+00    3     1000022       -11        12  # BR(~chi_1+ -> ~chi_10 e+ nu_e), dummy on-shell decay to force Pythia to decay off-shell
#
       '''}


#
# Mixing matrix for the Higgsino LSP / Bino NLSP / decoupled Wino
# Refer to SUSY parameter scan, slepton = 400 GeV, bino = 350 GeV, higgsino = 200 GeV
#
# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='-0.051542' # V_11
param_blocks['VMIX']['1 2']='0.9986708' # V_12 higgsino (C1)
param_blocks['VMIX']['2 1']='0.9986708' # V_21 wino (C2)
param_blocks['VMIX']['2 2']='0.0515422' # V_22
# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='-0.004054' # U_11
param_blocks['UMIX']['1 2']='0.9999917' # U_12 higgsino (C1)
param_blocks['UMIX']['2 1']='0.9999917' # U_21 wino (C2)
param_blocks['UMIX']['2 2']='0.0040549' # U_22
# Neutralino mixing matrix chi_i0 = N_ij i(B,W,H_d,H_u)
param_blocks['NMIX']={}
param_blocks['NMIX']['1 1']='0.1422423'  # N_11
param_blocks['NMIX']['1 2']='-0.026885'  # N_12
param_blocks['NMIX']['1 3']='0.7143735'  # N_13 higgsino (N1)
param_blocks['NMIX']['1 4']='-0.684627'  # N_14 higgsino (N1)
param_blocks['NMIX']['2 1']='0.0624107'  # N_21
param_blocks['NMIX']['2 2']='-0.023966'  # N_22
param_blocks['NMIX']['2 3']='-0.697283'  # N_23 higgsino (N2)
param_blocks['NMIX']['2 4']='-0.713671'  # N_24 higgsino (N2)
param_blocks['NMIX']['3 1']='0.9878619'  # N_31 bino (N3)
param_blocks['NMIX']['3 2']='0.0062915'  # N_32
param_blocks['NMIX']['3 3']='-0.058807'  # N_33
param_blocks['NMIX']['3 4']='0.1436346'  # N_34
param_blocks['NMIX']['4 1']='0.0008957'  # N_41
param_blocks['NMIX']['4 2']='-0.999331'  # N_42 wino (N4)
param_blocks['NMIX']['4 3']='-0.002866'  # N_43
param_blocks['NMIX']['4 4']='0.0364390'  # N_44

higgs_decay = {'25':'''DECAY   25     4.06911399E-03   # higgs decays
#          BR         NDA      ID1       ID2
     5.82000000E-01    2           5        -5   # BR(H1 -> b bbar)
     2.13700000E-01    2          24       -24   # BR(H1 -> W+ W-)
     8.18700000E-02    2          21        21   # BR(H1 -> g g)
     6.27200000E-02    2          15       -15   # BR(H1 -> tau- tau+)
     2.89100000E-02    2           4        -4   # BR(H1 -> c cbar)
     2.61900000E-02    2          23        23   # BR(H1 -> Z Z)
     2.27000000E-03    2          22        22   # BR(H1 -> gamma gamma)
     2.17600000E-04    2          13       -13   # BR(H1 -> mu mu)
     1.53300000E-03    2          23        22   # BR(H1 -> Z gamma)
     2.46000000E-04    2           3        -3   # BR(H1 -> s sbar)
#
         '''}
decays.update(higgs_decay)

madspindecays=False

print("gentype", gentype)
print("decays", decays)
print("masses", masses)
print("madspindecays", madspindecays)

# max number of jets will be two, unless otherwise specified.
njets = 2

#--------------------------------------------------------------
# MadGraph options
#
process = '''
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lv = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~

generate p p > el- el+ $ susystrong @1
add process p p > el- el+ j $ susystrong @2
add process p p > el- el+ j j $ susystrong @3

add process p p > el+ sve $ susystrong @1
add process p p > el+ sve j $ susystrong @2
add process p p > el+ sve j j $ susystrong @3

add process p p > el- sve~ $ susystrong @1
add process p p > el- sve~ j $ susystrong @2
add process p p > el- sve~ j j $ susystrong @3

add process p p > sve sve~ $ susystrong @1
add process p p > sve sve~ j $ susystrong @2
add process p p > sve sve~ j j $ susystrong @3

add process p p > mul- mul+ $ susystrong @1
add process p p > mul- mul+ j $ susystrong @2
add process p p > mul- mul+ j j $ susystrong @3

add process p p > mul+ svm $ susystrong @1
add process p p > mul+ svm j $ susystrong @2
add process p p > mul+ svm j j $ susystrong @3

add process p p > mul- svm~ $ susystrong @1
add process p p > mul- svm~ j $ susystrong @2
add process p p > mul- svm~ j j $ susystrong @3

add process p p > svm svm~ $ susystrong @1
add process p p > svm svm~ j $ susystrong @2
add process p p > svm svm~ j j $ susystrong @3
'''

mergeproc = "guess"


# print the process, just to confirm we got everything right
print("Final process card:")
print(process)

# mergeproc+="LEPTONS,NEUTRINOS"

#--------------------------------------------------------------
# Pythia options
#
pythia = genSeq.Pythia8
pythia.Commands += ["23:mMin = 0.2"]
pythia.Commands += ["24:mMin = 0.2"]
pythia.Commands += ["-24:mMin = 0.2"]
# pythia.Commands += ["24:onMode = off"]                       # switch off all W decays
# pythia.Commands += ["24:onIfAny = 11 12 13 14 15 16"]        # switch on W->lnu
# pythia.Commands += ["-24:onMode = off"]                      # switch off all W decays
# pythia.Commands += ["-24:onIfAny = -11 -12 -13 -14 -15 -16"] # switch on W->lnu

# information about this generation
evgenLog.info('Registered generation of slepton production, decaying into ~chi10 via bino; grid point decoded into mass point ' + str(masses['1000011']) + ' ' + str(masses['1000025']) + ' ' + str(masses['1000022']))
evgenConfig.contact  = [ "takumi.aoki@cern.ch" ]
evgenConfig.keywords += ['gaugino', 'neutralino', 'chargino']
evgenConfig.description = 'slepton production, decaying into ~chi10 via bino in simplified model. m_el = %s GeV, m_N3 = %s GeV, m_N2C1N1 = %s GeV'%(masses['1000011'], masses['1000025'], masses['1000022'])

#--------------------------------------------------------------
# add some filter here
#
evt_multiplier=2

if deltam_SLN3 == 10 and deltam_SLN1 < 50:
    evt_multiplier*=8
elif deltam_SLN3 == 10 and deltam_SLN1 < 80:
    evt_multiplier*=5
elif deltam_SLN3 == 10 and deltam_SLN1 <= 150:
    evt_multiplier*=4
elif deltam_SLN3 == 10 and deltam_SLN1 > 150:
    evt_multiplier*=3
elif deltam_SLN3 > 10 and deltam_SLN1 <= 100:
    evt_multiplier*=3
elif deltam_SLN3 > 10 and deltam_SLN1 > 100:
    evt_multiplier*=2


from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("SingleLepTriggerFilter")
SingleLepTriggerFilter = filtSeq.SingleLepTriggerFilter
SingleLepTriggerFilter.Ptcut = 25000.
SingleLepTriggerFilter.Etacut = 2.8
SingleLepTriggerFilter.NLeptons = 1

filtSeq += MultiLeptonFilter("DiLepTriggerFilter")
DiLepTriggerFilter = filtSeq.DiLepTriggerFilter
DiLepTriggerFilter.Ptcut = 12000.
DiLepTriggerFilter.Etacut = 2.8
DiLepTriggerFilter.NLeptons = 2

filtSeq += MultiLeptonFilter("TwoLepFilter")
TwoLepFilter = filtSeq.TwoLepFilter
TwoLepFilter.Ptcut = 2000.
TwoLepFilter.Etacut = 2.8
TwoLepFilter.NLeptons = 2

filtSeq.Expression="(SingleLepTriggerFilter or DiLepTriggerFilter) and TwoLepFilter"

#--------------------------------------------------------------
# Standard post-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

#--------------------------------------------------------------
# Merging options
#
if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = guess",
                                 "1000025:spinType = 1",
                                 "1000023:spinType = 1",
                                 "1000024:spinType = 1",
                                 "1000022:spinType = 1" ]
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO'] 
