from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

# Define number of generated LHE events
nevents=4*(runArgs.maxEvents)
mode=0

##Getting the Physics short, as while running in release 21, need to pass the directory itself##
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short() 
           
ss=str(phys_short.split('_')[4][0:])
print(ss)

process="""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm 
    define vl~ = ve~ vm~ 
    import model 2HDMCPF_NLO                     
    generate p p > h3 > ta+ ta- j [noborn=QCD]              
    output -f
    """

#---------------------------------------------------------------------------------------------------
# Set masses
#---------------------------------------------------------------------------------------------------
mhc = 0
mhc = float(phys_short.split('_')[4][1:])
print("mhc", mhc)

import math
mh1=1.250e+02                 
mh2=1.250e+10    
mh4=1.250e+10   

extras = { 'lhe_version':'3.0', 
           'cut_decays':'F',
           'ptj1min' : '120'
        }


extras['nevents'] = nevents 

# set up process
process_dir = new_process(process)
print("process_dir", process_dir)

#------------------------------------------------
# Run Card
#--------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

params = {}

masses = {'25':str(mh1)+'  #  mh1',
          '35':str(mh2)+'  #  mh2',
          '36':str(mhc)+'  #  mhc',
          '37':str(mh4)+'  #  mh4'}

params['mass'] = masses
print(masses)

decays = {'WH3':'1.801e-02'}

params['decay'] = decays
print(decays)

#------------------------------------------------
# Param Card
#--------------------------------------------------
modify_param_card(process_dir=process_dir,params=params)

print_cards()

runName='run_01'     
    
generate(process_dir=process_dir, runArgs=runArgs)
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Showering via Pythia8
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

evgenConfig.description = 'MG5 ggF X to ta+ ta-, mX = ' + str(mhc) + ' GeV'
evgenConfig.keywords+=['Higgs','BSMHiggs']
evgenConfig.contact = ['Nadav Tamir <ntamir@cern.ch>']
        
runArgs.inputGeneratorFile=outputDS

from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended   # lep filter

filtSeq += XtoVVDecayFilterExtended("XtoVVDecayFilterExtended")

filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 36
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [111,130,211,221,223,310,311,321,323]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [111,130,211,221,223,310,311,321,323]

filtSeq.Expression = "(XtoVVDecayFilterExtended)"
