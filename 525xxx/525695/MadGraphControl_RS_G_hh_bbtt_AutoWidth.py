import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import re

#Getting the Physics short, as while running in release 21, need to pass the directory itself##
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

mh  = 125

#Getting Graviton Mass from JO name
parameter_re = re.compile('RS_G_hh_bbtt_(lh|hh)_AW_c([0-9]+)_M([0-9]+)')
channel, c_str, mhh = parameter_re.search(jofile).groups()
c = float(c_str) * 0.1

params={
    'frblock' : {'c':str(c)},
    'mass'    : {'Mh':str(mh), 'Mhh':str(mhh)},
    'decay'   : {'Whh':'auto'},
}

nevents = runArgs.maxEvents * 5.1 if runArgs.maxEvents > 0 else evgenConfig.nEventsPerJob * 5.1

#Change defaults for run_card.dat
extras = { 'lhe_version':'2.0',
           'cut_decays':'F',
           'nevents':int(nevents)}

process = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set gauge unitary
set complex_mass_scheme False
import model SMRS_Decay
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~

generate p p > hh, (hh > h h)
output -f
"""

# second process to generate hh -> cc cc events using parameters from first process
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs, settings=extras)
modify_param_card(process_dir=process_dir,params=params)
generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py") 

genSeq.Pythia8.Commands += [
    "25:oneChannel = on 0.5 100 5 -5",          #bb decay
    "25:addChannel = on 0.5 100 15 -15",        # tautau decay
] 

if channel == 'lh':
    from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended
    filtSeq += XtoVVDecayFilterExtended("TauTauLepHadFilter")
    filtSeq.TauTauLepHadFilter.PDGGrandParent = 25
    filtSeq.TauTauLepHadFilter.PDGParent = 15
    filtSeq.TauTauLepHadFilter.StatusParent = 2
    filtSeq.TauTauLepHadFilter.PDGChild1 = [11,13]
    filtSeq.TauTauLepHadFilter.PDGChild2 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
    filtSeq.Expression = "TauTauLepHadFilter"
elif channel == 'hh':
    from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended
    filtSeq += XtoVVDecayFilterExtended("TauTauHadHadFilter")
    filtSeq.TauTauHadHadFilter.PDGGrandParent = 25
    filtSeq.TauTauHadHadFilter.PDGParent = 15
    filtSeq.TauTauHadHadFilter.StatusParent = 2
    filtSeq.TauTauHadHadFilter.PDGChild1 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
    filtSeq.TauTauHadHadFilter.PDGChild2 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
    filtSeq.Expression = "TauTauHadHadFilter"

# MC21 metadata
evgenConfig.contact = ["valentina.vecchio@cern.ch", "qichen.dong@cern.ch"]
evgenConfig.description = "Bulk Randall-Sundrum model KK graviton -> hh -> bbtautau with NNPDF2.3 LO A14 tune"
evgenConfig.keywords = ["exotic", "RandallSundrum", "warpedED", "graviton", "Higgs"]
evgenConfig.process = "RS_G_hh_bbtt"
