from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

"""
This JO is long-lived stop RHadrons decaying to b+mu 
Migrated from r19 JO: https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/MadGraphControl_SimplifiedModel_TT_RPVdirectBL_LongLived_RHadron.py
JIRA: https://its.cern.ch/jira/browse/ATLMCPROD-5979
"""

jobConfig = get_physics_short()
infoStrings = jobConfig.split("_")

evgenLog.info( "  jobConfig: %s  ", jobConfig[0] )
evgenLog.info( "  stop mass: %s  ", infoStrings[4] )
evgenLog.info( "  stop ctau: %s  ", infoStrings[6].replace('p','.') )

gentype   = str(infoStrings[2])
decaytype = str(infoStrings[3])

## Setting masses from filename parsing
masses['1000006'] = float(infoStrings[4])
masses['1000005'] = 3.00000000E+05
masses['1000022'] = 100000.

## Converting lifetime from filename to width
lifetimeString = str(infoStrings[6])
stopLifetime = lifetimeString.replace("ns","").replace(".py","").replace("p",".")
hbar = 6.582119514e-16
stopWidth = hbar/float(stopLifetime)

evgenLog.info( "  stop lifetime, width: %f, %f  ", float(stopLifetime), stopWidth )

## Set flavour scheme to 4  (default setup in mc15 JO) 
flavourScheme = 4

## Optional custom gluinoball probability in the filename
 
if len(infoStrings)>7:
	gluinoBallProbabilityString = str(infoStrings[7])
else:
	gluinoBallProbabilityString = "gball10"

gluinoBallProbability = float(gluinoBallProbabilityString.replace("gball",""))/100.
evgenLog.info( "  gluino-ball probability: %f  ", gluinoBallProbability )

## Defining the narrow width stop and its decay modes to b+mu

decays['1000006'] = """DECAY   1000006     %s   # stop1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
     1.00000000000    2      -13        5         # stop1 to b mu
"""%(stopWidth)


# Specify the process here, use MSSM_SLHA2 since its used in MC15 JO

process = '''
import model MSSM_SLHA2-full
define susylq = ul ur dl dr cl cr sl sr
define susylq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~
generate p p > t1 t1~ $ go susylq susylq~ b2 t1 t2 b2~ t1~ t2~ @1
add process p p > t1 t1~ j $ go susylq susylq~ b2 t1 t2 b2~ t1~ t2~ @2
add process p p > t1 t1~ j j $ go susylq susylq~ b2 t1 t2 b2~ t1~ t2~ @3
output -f
'''
njets = 2


# Register generation
 
evgenLog.info('Registered generation of stop pair production to b+mu; grid point '+str(runArgs.generatorJobNumber))
evgenConfig.contact  = [ "laura.elaine.bruce@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel']
evgenConfig.description = 'stop direct pair production, st->b+mu in simplified model, m_stop = %s GeV, Long-lived with R-Hadron treatment.'%(masses['1000006'])


# Helper for the simulation step
 
if hasattr(runArgs,'inputEVNTFile'):
    SLHAonly=True


keepOutput = False ## turn on for debugging param cards
add_lifetimes_lhe = True
run_settings.update({'time_of_flight':'1E-2',  'event_norm':'sum'}) ## In MC15 JO 'use_syst': F but code complains here about trusting base fragment (?) 
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

# Pythia8 R-Hadron Module Config

genSeq.Pythia8.Commands += ["Init:showChangedSettings = on"]
genSeq.Pythia8.Commands += ["Rhadrons:allow = on"]
genSeq.Pythia8.Commands += ["RHadrons:allowDecay = on"] 
genSeq.Pythia8.Commands += ["RHadrons:probGluinoball = %f"%gluinoBallProbability]
genSeq.Pythia8.Commands += ["Next:showScaleAndVertex = on"]
genSeq.Pythia8.Commands += ["Check:nErrList = 2"]

# Merging configuration in case of extra jets in ME

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,-1000006}"]

# Make sure that the RHadrons are allowed by TestHepMC.

if 'testSeq' in dir():
    extra_pdgids_f = open('extra_pdgids.txt','w')
    listOfPDGIDs = [1000512,1000522,1000612,1000622,1000632,1000642,1000652,1000991,1000993,1005211,1006113,1006211,1006213,1006223,1006311,1006313,1006321,1006323,1006333,1009111,1009113,1009211,1009213,1009223,1009311,1009313,1009321,1009323,1009333,1091114,1092112,1092114,1092212,1092214,1092224,1093114,1093122,1093214,1093224,1093314,1093324,1093334,-1000512,-1000522,-1000612,-1000622,-1000632,-1000642,-1000652,-1000991,-1000993,-1005211,-1006113,-1006211,-1006213,-1006223,-1006311,-1006313,-1006321,-1006323,-1006333,-1009111,-1009113,-1009211,-1009213,-1009223,-1009311,-1009313,-1009321,-1009323,-1009333,-1091114,-1092112,-1092114,-1092212,-1092214,-1092224,-1093114,-1093122,-1093214,-1093224,-1093314,-1093324,-1093334]
    extra_pdgids_f.write( "\n ".join([str(x) for x in listOfPDGIDs]) )
    extra_pdgids_f.close()
    testSeq.TestHepMC.G4ExtraWhiteFile='extra_pdgids.txt'
    testSeq.TestHepMC.MaxVtxDisp=1000000.# in mm
    # Recommended from (https://twiki.cern.ch/twiki/bin/view/AtlasProtected/SUSYMcRequestProcedure#8_Known_issues)
    testSeq.TestHepMC.MaxTransVtxDisp = 1e8

## Adding special config to metadata for use in sim step
## LB: needs update from https://gitlab.cern.ch/atlas/athena/-/blob/main/Simulation/SimulationJobOptions/share/specialConfig/preInclude.RHadronsPythia8.py ? (r22 update, not in r21)
## Those options are not needed since the RHadron decays in EVGEN step. Also those options are not supported yet: https://its.cern.ch/jira/browse/ATLASSIM-6687
# evgenConfig.specialConfig = "CASE=stop;MASS={mass};LIFETIME={lifetime};MASSX={massx};SimulationJobOptions/preInclude.RhadronsPythia8.py;".format(mass = masses['1000006'], lifetime = stopLifetime, massx = masses['1000022'])
