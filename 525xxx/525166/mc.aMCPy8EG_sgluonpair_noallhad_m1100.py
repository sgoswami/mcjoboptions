import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import os

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
nevents =  2*nevents 
bonus_file = open('pdg_extras.dat','w')
bonus_file.write('9000001 Sgluon 400 (MeV/c) fermion Sgluon 1\n')
bonus_file.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'pdg_extras.dat'
os.system("get_files %s" % testSeq.TestHepMC.UnknownPDGIDFile)
BasePath = os.getcwd()
process=""" 
import model sgluonsFull_UFO
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
generate p p > sig8 sig8
output -f"""

process_dir = new_process(process)
name='sgluon'
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

extras = { 'lhe_version':'3.0',
        # 'pdlabel'       : "'lhapdf'",
        # 'lhaid'       : lhaid,
        'nevents':int(nevents),
        'event_norm':'sum',
        # 'parton_shower':'PYTHIA8'
}

modify_run_card(runArgs=runArgs,
                process_dir=process_dir,
                settings=extras)

run_card=process_dir+'/Cards/run_card.dat'


params = {}
# Set masses
# Get the card set up
masses = {}
decays = {}
coupling_NPYLD = {}
coupling_NPYLU = {}
coupling_NPYRD = {}
coupling_NPYRU = {}
masses = {'9000001' : 1.10000000e+03}





# sminputs = {'aEWM1' : 1.323489e+02}
params['MASS']=masses

decays['9000001'] = """DECAY 9000001 AUTO # w8"""

params['DECAY']=decays
### coupling setting ###
coupling_NPYLD['1 1'] = '0.'
coupling_NPYLD['2 2'] = '0.'
coupling_NPYLD['3 3'] = '0.'

coupling_NPYLU['1 1'] = '0.'
coupling_NPYLU['2 2'] = '0.'
coupling_NPYLU['3 3'] = '0.5'

coupling_NPYRD['1 1'] = '0.'
coupling_NPYRD['2 2'] = '0.'
coupling_NPYRD['3 3'] = '0.'

coupling_NPYRU['1 1'] = '0.'
coupling_NPYRU['2 2'] = '0.'
coupling_NPYRU['3 3'] = '0.5'
params['NPYLD']= coupling_NPYLD
params['NPYLU']= coupling_NPYLU
params['NPYRD']= coupling_NPYRD
params['NPYRU']= coupling_NPYRU
params['NPG8G']={
    '1' : 0
}
# modify_param_card(process_dir=process_dir,params={'MASS':masses,'DECAY':decays})
print ("Final process card:")
print (process)

runName='run_01' 

modify_param_card(process_dir=process_dir,params=params)


mcprod_maddec = "define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ g u c d s b u~ c~ d~ s~ b~ \ndecay sig8 > t t~, (t > b w+, w+ > wdec wdec), (t~ > b~ w-, w- > wdec wdec) \ndecay t > w+ b, w+ > wdec wdec \ndecay t~ > w- b~, w- > wdec wdec"

# Decay with MadSpin
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w') 
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
#set BW_cut 15                # cut on how far the particle can be off-shell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
set seed %i
%s
launch
"""%(runArgs.randomSeed, mcprod_maddec))
mscard.close()



generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
evgenConfig.description = 'MadGraph_tttt_ttsgluons'
evgenConfig.generators += ["aMcAtNlo","Pythia8","EvtGen"]

evgenConfig.process= "p p ->sig8+sig8-> t+t~+t+t~"
evgenConfig.keywords += ["top","SUSY","jets"]

evgenConfig.contact = ["qjiang@cern.ch"]
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
# genSeq.Pythia8.Commands += [   "TimeShower:globalRecoilMode = 2",
#                                "TimeShower:nMaxGlobalBranch = 1",
#                                "TimeShower:nPartonsInBorn = 2",
#                                "TimeShower:limitPTmaxGlobal = on"]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #no-allhad
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

testSeq.TestHepMC.EffFailThreshold = 0.96

